/*
  Author: 
	Simon Makwarth simak@mst.dk
	Jakob Lanstorp jakla@mst.dk
  Date: 
  	10-09-2019
  Last edited:
  	22-06-2023
    20-09-2024 Municipality added (JAKLA)
  Desc: 
  	This sql queries generate 5 themes from boreholes in jupiter DB
		1) view and mat view of all pesticides in jupiter boreholes
		2) mat view of all GW chem samples that displays 
			an exeedance for each pesticide
		3) view and mat view of all samples containing Chlorthalonilamid pesticide 
		4) view and mat view of all samples containing Chloridazon pesticide
		5) view and mat view of the sum of all pesticides 
			with a standat number from the latest GwChem sample
*/

BEGIN;

	-- All pesticides in jupiter boreholes
	DROP TABLE IF EXISTS mstjupiter.pesticide_gvk_all CASCADE;

	CREATE TABLE mstjupiter.pesticide_gvk_all AS (
		SELECT 	
			ROW_NUMBER() over (ORDER BY gcs.boreholeno) AS ID,
			gcs.boreholeno,
			gcs.boreholeid, 
			gcs.intakeno,
			gca.sampleid,
			gcs.sampledate,
			gca.compoundno,
			cpl.long_text AS compound,
			cpl.casno,
			gca."attribute",
			CASE 
				WHEN gca.unit = 1
					THEN gca.amount * 1000
				WHEN gca.unit = 20
					THEN gca.amount
				WHEN gca.unit = 28
					THEN gca.amount * 1000000000
				WHEN gca.unit = 59
					THEN gca.amount * 1000000
				WHEN gca.unit = 155
					THEN gca.amount * 0.001
				WHEN gca.unit = 163
					THEN gca.amount * 0.000001
				ELSE NULL 
				END AS amount_µgprl,
			b.abandondat,
			pl.plantid,
			pl.plantname,
			pl.planttype,
		    pl.kommune,
			pl.companytype,
			pl.longtext as companytype_desc,
			gcs.project,
			b.xutm32euref89,
			b.yutm32euref89, 
			gcs.guid_intake,
			gca.guid AS guid_grwchemanalysis,
			b.geom
		FROM jupiter.grwchemanalysis gca
		LEFT JOIN 
			(
				SELECT 
					c.code::NUMERIC AS unit,
					c.longtext AS unit_descr
				FROM jupiter.code c 
				WHERE codetype = 752
			) AS unit USING (unit)
		INNER JOIN jupiter.grwchemsample gcs USING (sampleid) 
		LEFT JOIN jupiter.compoundlist cpl USING (compoundno)
		LEFT JOIN jupiter.borehole b USING (boreholeid)
		LEFT JOIN 
			(
				SELECT DISTINCT 
					dpi.boreholeid, 
					dp.plantid, 
					dp.plantname,
				    kom.kommune,
					dp.planttype,
					dpct.companytype, 
					c.longtext
				FROM jupiter.drwplant dp
				INNER JOIN jupiter.drwplantcompanytype dpct USING (plantid)
				INNER JOIN jupiter.drwplantintake dpi USING (plantid)
				INNER JOIN jupiter.code c on dpct.companytype = c.code
                left join
				    (
				      select code, longtext kommune
				      from jupiter.code
				      where codetype = 808 --Municipalities
				    ) kom on kom.code = dp.MUNICIPALITYNO2007::text
				ORDER BY dp.plantid
			) pl ON gcs.boreholeid = pl.boreholeid
		WHERE b.geom IS NOT NULL
			AND gca.compoundno IN 
				(
					SELECT compoundno
					FROM jupiter.compoundgroup
					WHERE compoundgroupno = 50 --Pesticider, nedbrydningsprodukter og beslægtede stoffer
				)
		ORDER BY pl.plantname, gcs.boreholeno, gcs.sampledate desc, cpl.long_text
	);
-- [2024-09-20 12:59:12] 5,703,412 rows affected in 1 m 1 s 299 ms

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mstjupiter.pesticide_gvk_all IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: all pesticide analysis from GRW samples'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter/pesticide_all_borehole.sql'
	     || '''';
		END
	$do$
	;

    -- 20240926 JAKLA
    -- All GW chem samples that displays the presence of pesticide,
    -- not only exceedances, but also where the amount is larger than the detection limit
	DROP TABLE IF EXISTS mstjupiter.pesticide_gvk_presence CASCADE;

	CREATE TABLE mstjupiter.pesticide_gvk_presence AS (
        SELECT *
        FROM mstjupiter.pesticide_gvk_all
        WHERE COALESCE("attribute", '>') IN ('>', 'B', 'C'));
    --[2024-09-26 10:11:18] 216,268 rows affected in 1 s 676 ms

	-- All GW chem samples that displays an exeedance for each pesticide
	DROP TABLE IF EXISTS mstjupiter.pesticide_gvk_exceedance CASCADE;

	CREATE TABLE mstjupiter.pesticide_gvk_exceedance AS (
		WITH 
			aldrin AS 
				(
					SELECT *
					FROM mstjupiter.pesticide_gvk_all
					WHERE COALESCE("attribute", '>') IN ('>', 'B', 'C')
						AND compoundno IN (3503) 
						AND amount_µgprl >= 0.03
				),
			dieldrin AS 
				(
				  SELECT *
				  FROM mstjupiter.pesticide_gvk_all
				  WHERE COALESCE("attribute", '>') IN ('>', 'B', 'C')
						AND compoundno IN (3134) 
						AND amount_µgprl >= 0.03
				),
			heptachlor AS 
				(
				  SELECT *
				  FROM mstjupiter.pesticide_gvk_all
				  WHERE COALESCE("attribute", '>') IN ('>', 'B', 'C')
						AND compoundno IN (3136) 
						AND amount_µgprl >= 0.03
				),
			heptachlor_epoxid AS 
				(
					SELECT *
					FROM mstjupiter.pesticide_gvk_all
					WHERE COALESCE("attribute", '>') IN ('>', 'B', 'C')
						AND compoundno IN (3137) 
						AND amount_µgprl >= 0.03
				),
			restpesticide AS 
				(
					SELECT *
					FROM mstjupiter.pesticide_gvk_all
					WHERE COALESCE("attribute", '>') IN ('>', 'B', 'C')
						AND compoundno NOT IN (3503, 3134, 3136, 3137) 
						AND amount_µgprl >= 0.1
				),
			pesticide AS 
				(
					SELECT *
					FROM aldrin
					UNION ALL
					SELECT *
					FROM dieldrin
					UNION ALL
					SELECT *
					FROM heptachlor
					UNION ALL
					SELECT *
					FROM heptachlor_epoxid
					UNION ALL
					SELECT *
					FROM restpesticide
				)
		SELECT
			p.*
		FROM pesticide p
		)
;
-- [2024-09-20 13:23:05] 63,224 rows affected in 6 s 770 ms

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mstjupiter.pesticide_gvk_exceedance IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: exceedances from all pesticide analysis from GRW samples'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

-- Sum of all pesticides with a standat number from the latest GwChem sample
	DROP TABLE IF EXISTS mstjupiter.pesticide_gvk_sum CASCADE;

	CREATE TABLE mstjupiter.pesticide_gvk_sum AS 
		(
			WITH 
				pest_recent AS 
					(
						SELECT DISTINCT ON (guid_intake)
							guid_intake
						FROM mstjupiter.pesticide_gvk_all
						ORDER BY guid_intake, sampledate DESC NULLS LAST 
					),
				pest_recent_sum AS 	
					(
						SELECT 
							sampleid,
							SUM
								(
									CASE 
										WHEN COALESCE("attribute", '>') IN ('>', 'B', 'C')
											AND amount_µgprl IS NOT NULL
											THEN amount_µgprl 
										ELSE 0
										END
								) AS sum_pesticide,
							count(*) AS number_of_pesticide
						FROM mstjupiter.pesticide_gvk_all t1
						INNER JOIN pest_recent USING (guid_intake)
						GROUP BY sampleid 
					)
			SELECT DISTINCT ON (guid_intake)
				ROW_NUMBER() over () AS id,
				sampleid, 
				sampledate, 
				boreholeno,
				boreholeid,
				intakeno, 
				sum_pesticide, 
				number_of_pesticide,
				xutm32euref89,
				yutm32euref89,
				guid_intake,
				geom
			FROM pest_recent_sum
			LEFT JOIN mstjupiter.pesticide_gvk_all USING (sampleid)
		                    left join
				    (
				      select code, longtext kommune
				      from jupiter.code
				      where codetype = 808 --Municipalities
				    ) kom on kom.code = dp.MUNICIPALITYNO2007::text
		)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mstjupiter.pesticide_gvk_sum IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: Sum of all pesticide analysis from most recent grw sample contaning pesticides'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- All samples containing Chloridazon pesticide
--	DROP MATERIALIZED VIEW IF EXISTS mstjupiter.mstmvw_pesticide_chloridazon_GVK_proeve CASCADE;
--
--	CREATE MATERIALIZED VIEW mstjupiter.mstmvw_pesticide_chloridazon_GVK_proeve AS (
--		SELECT 	
--			ROW_NUMBER() over (ORDER BY gcs.boreholeno) AS ID,
--			gcs.boreholeno,
--			gcs.intakeno,
--			gca.sampleid,
--			gcs.sampledate,
--			cpl.long_text AS stof,
--			gca."attribute",
--			gca.amount,
--			gca.compoundno,
--			cpl.casno,
--			b.abandondat,
--			b.geom,
--			pl.plantid,
--			pl.plantname,
--			pl.planttype,
--			pl.companytype,
--			pl.longtext as companytype_desc,
--			gcs.project
--		FROM jupiter.grwchemanalysis gca
--		INNER JOIN jupiter.grwchemsample gcs USING (sampleid) 
--		INNER JOIN jupiter.compoundlist cpl USING (compoundno)
--		INNER JOIN jupiter.borehole b USING (boreholeid)
--		INNER JOIN 
--			(
--				SELECT DISTINCT 
--					dpi.boreholeid, 
--					dp.plantid, 
--					dp.plantname, 
--					dp.planttype, 
--					dpct.companytype, 
--					c.longtext
--				FROM jupiter.drwplant dp
--				INNER JOIN jupiter.drwplantcompanytype dpct USING (plantid)
--				INNER JOIN jupiter.drwplantintake dpi USING (plantid)
--				INNER JOIN jupiter.code c on dpct.companytype = c.code
--				ORDER BY dp.plantid
--			) pl ON gcs.boreholeid = pl.boreholeid
--		WHERE gca.compoundno = ANY (ARRAY[
--			3528::NUMERIC, 4696::NUMERIC, 4712::NUMERIC, 4761::NUMERIC
--		])
--		ORDER BY pl.plantname, gcs.boreholeno, gcs.sampledate desc, cpl.long_text
--	);
	
--	DROP MATERIALIZED VIEW IF EXISTS mstjupiter.mstmvw_pesticide_chlorthalonilamid_GVK_proeve CASCADE;
--
--	CREATE MATERIALIZED VIEW mstjupiter.mstmvw_pesticide_chlorthalonilamid_GVK_proeve  AS (
--		SELECT 	
--			ROW_NUMBER() over (ORDER BY gcs.boreholeno) AS ID,
--			gcs.boreholeno,
--			gcs.intakeno,
--			gca.sampleid,
--			gcs.sampledate,
--			cpl.long_text AS stof,
--			gca."attribute",
--			gca.amount,
--			gca.compoundno,
--			cpl.casno,
--			b.abandondat,
--			b.geom,
--			pl.plantid,
--			pl.plantname,
--			pl.planttype,
--			pl.companytype,
--			gcs.project
--		FROM jupiter.grwchemanalysis gca
--		INNER JOIN jupiter.grwchemsample gcs USING (sampleid) 
--		INNER JOIN jupiter.compoundlist cpl USING (compoundno)
--		INNER JOIN jupiter.borehole b USING (boreholeid)
--		INNER JOIN (
--			SELECT DISTINCT dpi.boreholeid, dp.plantid, dp.plantname, dp.planttype, dpct.companytype
--			FROM jupiter.drwplant dp
--			INNER JOIN jupiter.drwplantcompanytype dpct USING (plantid)
--			INNER JOIN jupiter.drwplantintake dpi USING (plantid)
--			ORDER BY dp.plantid
--			) pl ON gcs.boreholeid = pl.boreholeid
--		WHERE gca.compoundno = ANY (ARRAY[4945::NUMERIC, 4989::NUMERIC, 4990::NUMERIC, 4961::NUMERIC])
--			-- AND dpct.companytype = ANY (ARRAY[st'V01'::TEXT, 'V02'::TEXT, 'V03'::TEXT])
--		ORDER BY pl.plantname, gcs.boreholeno, gcs.sampledate DESC
--	);

	GRANT USAGE ON SCHEMA mstjupiter TO jupiterrole;

	GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO jupiterrole;

COMMIT;