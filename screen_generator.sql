/*
    JAKLA 20221129 Extract intake and screens for geometry generator rendering
*/
drop table if exists mstjupiter.screen;
create table mstjupiter.screen as
    select distinct
           s.boreholeno,
           s.boreholeid,
           s.intakeid,
           i.intakeno,
           s.screenid,
           s.screenno,
           top,
           bottom,
           b.geom
    from jupiter.screen s
    left join jupiter.intake i on i.boreholeid = s.boreholeid
    left join jupiter.borehole b on b.boreholeid = s.boreholeid;
-- [2023-01-12 11:13:24] 251,669 rows affected in 2 s 120 ms

/*
    Hent filter og indtag for alle almene vandforsyningsboringer
*/
drop table if exists mstjupiter.screen_watersupply_grw_jylland;
create table mstjupiter.screen_watersupply_grw_jylland as
select distinct
    --row_number() over (order by b.boreholeno, pi.intakeno, s.screenno desc) as id,
    b.boreholeid,
    b.boreholeno,
    pi.intakeno,
    s.screenid,
    s.screenno,
    t.companytype,
    c1.longtext companytype_longtext,
    c2.longtext as use_longtext,
    s.top,
    s.bottom,
    round(b.xutm) xutm,
    round(b.yutm) yutm,
    b.geom
from jupiter.screen s
inner join jupiter.drwplantintake pi using (boreholeid)
inner join jupiter.drwplant p on p.plantid = pi.plantid
inner join jupiter.drwplantcompanytype t on t.plantid = pi.plantid
inner join jupiter.code c1 on c1.code = t.companytype and c1.codetype = 852
inner join jupiter.borehole b on b.boreholeid = pi.boreholeid
inner join jupiter.code c2 on c2.code = b.use and c2.codetype = 855
where t.companytype in ('V01', 'V02') and c2.longtext = 'Vandværksboring'
and b.xutm is not null and b.yutm is not null and b.borhtownno2007 > 500;
--DK:       [2023-01-12 11:16:55] 10,024 rows affected in 265 ms
-- Jylland: [2023-01-12 15:25:22] 4,168 rows affected in 243 ms
comment on table mstjupiter.screen_watersupply_grw is '20230112 JAKLA filter, indtag for almene vandforsyningsboringer i Jylland';





