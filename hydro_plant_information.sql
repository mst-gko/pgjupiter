BEGIN; 
	DROP TABLE IF EXISTS mstjupiter.hydrology_plant_information;
	
	CREATE TABLE mstjupiter.hydrology_plant_information AS 
		(
			SELECT DISTINCT ON (dp.plantid)
				row_number() OVER () AS row_num,
				dp.plantid AS anlaegsid,
				dp.plantname AS anlaegnavn,
				dp.plantaddress AS anlaegadr,
				dp.plantpostalcode AS anl_postnr,
				dp.municipalityno2007 AS kommunenr,
				dataowner.longtext AS dataejer,
				active.longtext AS aktiv,
				permit.longtext AS permstatus,
				companytype.longtext AS virktype,
				planttype.longtext AS anl_type,
				indvformaal.longtext AS indvformal,
				watertype.longtext AS vandtype,
				so.latest_yr_pump AS yr_pump,
				COALESCE(ao.count_pump_10yr, 0) AS cnt_pump_10yr,
				yr_grw_chem AS lc.yr_grwchem,
				pc.yr_pltchem,
				COALESCE(cb.count_borehole, 0) AS cnt_bh,
				COALESCE(cb.count_intake, 0) AS cnt_itk,
				COALESCE(cs.count_old_borehole, 0) AS cnt_old_bh,
				COALESCE(cs.count_old_intake, 0) AS cnt_old_itk,
				CASE 
					WHEN dp.supplant IS NOT NULL 
						THEN TRUE 
					ELSE FALSE 
					END AS har_anl_over,
				CASE 
					WHEN sp.plantid_sub IS NOT NULL 
						THEN TRUE 
					ELSE FALSE 
					END AS har_anl_under,
				concat('https://data.geus.dk/JupiterWWW/anlaeg.jsp?anlaegid=', dp.plantid) url_plant,
			    regexp_replace(dp.locatremark, '[\n\r]+', ' ', 'g') AS kommentar,
			    now() AS dato_qry,
			    CASE 
			    	WHEN dp.geom IS NOT NULL
			    		THEN 'Plant geometry'
			    	WHEN dp.geom IS NULL 
			    		AND bg.geom IS NOT NULL 
			    		THEN 'Centroid of plants Boreholes'
			    	WHEN dp.geom IS NULL 
			    		AND bg.geom IS NULL 
			    		THEN 'No geometry found' 
			    	END AS geom_descr,
			    COALESCE(dp.geom, bg.geom) AS geom
			FROM jupiter.drwplant dp
			LEFT JOIN jupiter.drwplantcompanytype t using (plantid)
			LEFT JOIN 
				(
					SELECT 
						dp2.supplant AS plantid, 
						dp2.plantid AS plantid_sub
					FROM jupiter.drwplant dp2
					WHERE supplant IS NOT NULL
				) AS sp USING (plantid)
			LEFT JOIN 
				(
					SELECT 
						code, longtext
					FROM jupiter.code 
					WHERE codetype = 734
				) AS planttype ON planttype.code = dp.planttype::text
			LEFT JOIN 
				(
					SELECT 
						code, longtext
					FROM jupiter.code 
					WHERE codetype = 741
				) AS indvformaal ON indvformaal.code = dp.vrrpurpose::text
			LEFT JOIN 
				(
					SELECT 
						code, longtext
					FROM jupiter.code 
					WHERE codetype = 737
				) AS watertype ON watertype.code = dp.watertype::text
			LEFT JOIN 
				(
				 	SELECT 
				 		code, longtext
				 	FROM jupiter.code 
				 	WHERE codetype = 733
				 ) AS active ON active.code = dp.active::text
			LEFT JOIN 
				(
					SELECT 
						code, longtext
					FROM jupiter.code WHERE codetype = 867
				) AS dataowner ON dataowner.code = dp.dataowner::text
			LEFT JOIN 
				(
					SELECT 
						code, longtext
					FROM jupiter.code 
					WHERE codetype = 852
				) AS companytype ON companytype.code = t.companytype
			LEFT JOIN 
				(
					SELECT
						code, longtext
					FROM jupiter.code 
					WHERE codetype = 735
				) AS permit ON permit.code = dp.permit::TEXT
			LEFT JOIN 
				(
					SELECT DISTINCT ON (wc.plantid)
						wc.plantid,
						EXTRACT( YEAR FROM wc.startdate) AS latest_yr_pump
					FROM jupiter.wrrcatchment wc 
					WHERE amount > 0
					ORDER BY plantid, startdate DESC 
				) AS so ON dp.plantid = so.plantid
			LEFT JOIN 
				(
					SELECT DISTINCT ON (wc.plantid)
						wc.plantid,
						count(*) AS count_pump_10yr
					FROM jupiter.wrrcatchment wc 
					WHERE amount > 0
						AND EXTRACT(YEAR FROM current_date) - EXTRACT(YEAR FROM wc.startdate) <= 10
					GROUP BY wc.plantid
				) AS ao ON dp.plantid = ao.plantid
			LEFT JOIN 
				(
					SELECT DISTINCT ON (dpi.plantid)
						dpi.plantid,
						EXTRACT( YEAR FROM gcs.sampledate ) AS yr_grw_chem 
					FROM jupiter.grwchemsample gcs 
					LEFT JOIN jupiter.drwplantintake dpi USING (boreholeid) 
					WHERE plantid IS NOT NULL 
					ORDER BY dpi.plantid, gcs.sampledate DESC
				) AS lc ON dp.plantid = lc.plantid
			LEFT JOIN 
				(
					SELECT DISTINCT ON (pcs.plantid)
						pcs.plantid, EXTRACT(YEAR FROM pcs.sampledate) yr_plt_chem 
					FROM jupiter.pltchemsample pcs
					WHERE pcs.plantid IS NOT NULL 
					ORDER BY pcs.plantid, pcs.sampledate DESC
				) AS pc ON dp.plantid = pc.plantid
			LEFT JOIN 
				(
					SELECT 
						dpi.plantid,
						count(DISTINCT b.boreholeid) AS count_borehole,
						count(DISTINCT dpi.guid_intake) AS count_intake
					FROM jupiter.borehole b
					LEFT JOIN jupiter.drwplantintake dpi USING (boreholeid)
					WHERE b.use != 'S'
						AND COALESCE(dpi.enddate, current_date) >= current_date
					GROUP BY dpi.plantid 
				) AS cb ON dp.plantid = cb.plantid
			LEFT JOIN 
				(
					SELECT 
						dpi.plantid,
						count(DISTINCT b.boreholeid) AS count_old_borehole,
						count(DISTINCT dpi.guid_intake) AS count_old_intake
					FROM jupiter.borehole b
					LEFT JOIN jupiter.drwplantintake dpi USING (boreholeid)
					WHERE b.use = 'S'
						OR dpi.enddate < current_date
					GROUP BY dpi.plantid 
				) AS cs ON dp.plantid = cs.plantid
			LEFT JOIN 
				(
					WITH tmp AS 
						(
							SELECT DISTINCT ON (plantid, boreholeid)
								dpi.plantid,
								boreholeid,
								b.geom
							FROM jupiter.drwplantintake dpi 
							INNER JOIN jupiter.borehole b USING (boreholeid)
							WHERE b.use != 'S'
								AND COALESCE(dpi.enddate, current_date) >= current_date
						)
					SELECT 
						plantid,
						st_centroid(st_union(geom)) as geom
					FROM tmp
					GROUP BY plantid
				) AS bg ON dp.plantid = bg.plantid
		)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mstjupiter.hydrology_plant_information IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' simak Table containing drwplant information from the Jupiter database'
			     || '''';
		END
	$do$
	;

	GRANT SELECT ON ALL TABLES IN SCHEMA jupiter TO grukosreader;

   	GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO grukosreader;

COMMIT; 

