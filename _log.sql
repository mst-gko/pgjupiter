CREATE TABLE jupiter.mst_postgres_log
(
  log_time timestamp(3) with time zone,
  user_name text,
  database_name text,
  process_id integer,
  connection_from text,
  session_id text,
  session_line_num bigint,
  command_tag text,
  session_start_time timestamp with time zone,
  virtual_transaction_id text,
  transaction_id bigint,
  error_severity text,
  sql_state_code text,
  message text,
  detail text,
  hint text,
  internal_query text,
  internal_query_pos integer,
  context text,
  query text,
 query_pos integer,
  location text,
  application_name text,
  PRIMARY KEY (session_id, session_line_num)
);

ALTER TABLE jupiter.mst_postgres_log
    OWNER to postgres;

GRANT ALL ON TABLE jupiter.mst_postgres_log TO postgres;

TRUNCATE jupiter.mst_postgres_log;
COPY jupiter.mst_postgres_log FROM '/var/lib/pgsql/9.4/data/pg_log/postgresql.csv' WITH CSV encoding 'LATIN1';

DROP VIEW IF EXISTS jupiter.mstvw_log;
CREATE OR REPLACE VIEW jupiter.mstvw_log AS
(
  SELECT * FROM jupiter.mst_postgres_log
  WHERE database_name = 'pcjupiterxl'
);

DROP VIEW IF EXISTS jupiter.mstvw_log_select;
CREATE OR REPLACE VIEW jupiter.mstvw_log_select AS
(
  SELECT
    log_time,
    user_name,
    database_name,
    connection_from,
    command_tag,
    session_start_time,
    error_severity,
    sql_state_code,
    message,
    query,
    application_name
  FROM jupiter.mst_postgres_log
  --WHERE command_tag = 'PARSE' AND query ilike '%SELECT%'
);

--SET client_min_messages TO INFO;
SET log_min_messages TO DEBUG2;   --mindste level som giver command_tag=parse og sql i query

SELECT * FROM jupiter.mst_postgres_log
WHERE command_tag = 'testZZZ';

SELECT count(*) FROM jupiter.mst_postgres_log;

/*
    New 20210121 setup using a file datawrapper
*/
show all;
show log_destination;
show include_if_exists;
show shared_buffers; --hvorfor er der fejl når den sættes op i postgresqæ_gko.conf?
show effective_cache_size;

SELECT pg_reload_conf();

select version();

select pg_available_extension_versions();

create extension file_fdw;
create server logserver FOREIGN DATA WRAPPER file_fdw;

drop foreign table if exists log.postgres_log_mon;
create foreign table log.postgres_log_mon
(
  log_time timestamp(3) with time zone,
  user_name text,
  database_name text,
  process_id integer,
  connection_from text,
  session_id text,
  session_line_num bigint,
  command_tag text,
  session_start_time timestamp with time zone,
  virtual_transaction_id text,
  transaction_id bigint,
  error_severity text,
  sql_state_code text,
  message text,
  detail text,
  hint text,
  internal_query text,
  internal_query_pos integer,
  context text,
  query text,
  query_pos integer,
  location text,
  application_name text)
 SERVER logserver OPTIONS (filename '/u02/pgdata10/log/postgresql-Mon.csv', format 'csv');

drop foreign table if exists log.postgres_log_tue;
create foreign table log.postgres_log_tue
(
  log_time timestamp(3) with time zone,
  user_name text,
  database_name text,
  process_id integer,
  connection_from text,
  session_id text,
  session_line_num bigint,
  command_tag text,
  session_start_time timestamp with time zone,
  virtual_transaction_id text,
  transaction_id bigint,
  error_severity text,
  sql_state_code text,
  message text,
  detail text,
  hint text,
  internal_query text,
  internal_query_pos integer,
  context text,
  query text,
  query_pos integer,
  location text,
  application_name text)
 SERVER logserver OPTIONS (filename '/u02/pgdata10/log/postgresql-Tue.csv', format 'csv');

drop foreign table if exists log.postgres_log_wed;
create foreign table log.postgres_log_wed
(
  log_time timestamp(3) with time zone,
  user_name text,
  database_name text,
  process_id integer,
  connection_from text,
  session_id text,
  session_line_num bigint,
  command_tag text,
  session_start_time timestamp with time zone,
  virtual_transaction_id text,
  transaction_id bigint,
  error_severity text,
  sql_state_code text,
  message text,
  detail text,
  hint text,
  internal_query text,
  internal_query_pos integer,
  context text,
  query text,
  query_pos integer,
  location text,
  application_name text)
 SERVER logserver OPTIONS (filename '/u02/pgdata10/log/postgresql-Wed.csv', format 'csv');

drop foreign table if exists log.postgres_log_thu;
create foreign table log.postgres_log_thu
(
  log_time timestamp(3) with time zone,
  user_name text,
  database_name text,
  process_id integer,
  connection_from text,
  session_id text,
  session_line_num bigint,
  command_tag text,
  session_start_time timestamp with time zone,
  virtual_transaction_id text,
  transaction_id bigint,
  error_severity text,
  sql_state_code text,
  message text,
  detail text,
  hint text,
  internal_query text,
  internal_query_pos integer,
  context text,
  query text,
  query_pos integer,
  location text,
  application_name text)
 SERVER logserver OPTIONS (filename '/u02/pgdata10/log/postgresql-Thu.csv', format 'csv');

drop foreign table if exists log.postgres_log_fri;
create foreign table log.postgres_log_fri
(
  log_time timestamp(3) with time zone,
  user_name text,
  database_name text,
  process_id integer,
  connection_from text,
  session_id text,
  session_line_num bigint,
  command_tag text,
  session_start_time timestamp with time zone,
  virtual_transaction_id text,
  transaction_id bigint,
  error_severity text,
  sql_state_code text,
  message text,
  detail text,
  hint text,
  internal_query text,
  internal_query_pos integer,
  context text,
  query text,
  query_pos integer,
  location text,
  application_name text)
 SERVER logserver OPTIONS (filename '/u02/pgdata10/log/postgresql-Fri.csv', format 'csv');

drop foreign table if exists log.postgres_log_sat;
create foreign table log.postgres_log_sat
(
  log_time timestamp(3) with time zone,
  user_name text,
  database_name text,
  process_id integer,
  connection_from text,
  session_id text,
  session_line_num bigint,
  command_tag text,
  session_start_time timestamp with time zone,
  virtual_transaction_id text,
  transaction_id bigint,
  error_severity text,
  sql_state_code text,
  message text,
  detail text,
  hint text,
  internal_query text,
  internal_query_pos integer,
  context text,
  query text,
  query_pos integer,
  location text,
  application_name text)
 SERVER logserver OPTIONS (filename '/u02/pgdata10/log/postgresql-Sat.csv', format 'csv');

drop foreign table if exists log.postgres_log_sun;
create foreign table log.postgres_log_sun
(
  log_time timestamp(3) with time zone,
  user_name text,
  database_name text,
  process_id integer,
  connection_from text,
  session_id text,
  session_line_num bigint,
  command_tag text,
  session_start_time timestamp with time zone,
  virtual_transaction_id text,
  transaction_id bigint,
  error_severity text,
  sql_state_code text,
  message text,
  detail text,
  hint text,
  internal_query text,
  internal_query_pos integer,
  context text,
  query text,
  query_pos integer,
  location text,
  application_name text)
 SERVER logserver OPTIONS (filename '/u02/pgdata10/log/postgresql-Sun.csv', format 'csv');

/*
  Fix carrage return for COPY command on Linux:
  sed 's|\r|\\r|g' jupiter-download.log > jupiter-download-sedded.log
  sed 's|\r|\\r|g' jupiter-restore.log > jupiter-restore-sedded.log
*/

drop foreign table if exists log.log_jupiter_restore;
create foreign table log.log_jupiter_restore
(
  message text
)
 SERVER logserver OPTIONS (filename '/home/sharedfolder/logs/jupiter-restore-sedded.log', format 'text');

drop foreign table if exists log.log_jupiter_download;
create foreign table log.log_jupiter_download
(
  message text
)
 SERVER logserver OPTIONS (filename '/home/sharedfolder/logs/jupiter-download-sedded.log', format 'text');
