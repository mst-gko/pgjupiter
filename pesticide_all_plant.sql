/*
  Author: 
	Simon Makwarth simak@mst.dk
	Jakob Lanstorp jakla@mst.dk
  Date: 10-09-2019
  Desc: The sql quaries generate 4 themes from GW plants in jupiter DB:
	1) view and mat view of all pesticides in jupiter boreholes
	2) mat view of all GW chem samples that displays 
		an exeedance for each pesticide
	3) view and mat view of all samples containing Chlorthalonilamid pesticide 
	4) view and mat view of all samples containing Chloridazon pesticide
*/

BEGIN;

	-- All pesticides in jupiter boreholes
	DROP TABLE IF EXISTS mstjupiter.pesticide_drv_all CASCADE;

	CREATE TABLE mstjupiter.pesticide_drv_all AS (
		SELECT 	
			row_number() over (order by pcs.plantid) as ID,
			pcs.plantid, 
			drp.plantname, 
			pcs.sampleid,
			pcs.sampledate,
			pca."attribute",
			CASE 
				WHEN pca.unit = 1
					THEN pca.amount * 1000
				WHEN pca.unit = 20
					THEN pca.amount
				WHEN pca.unit = 28
					THEN pca.amount * 1000000000
				WHEN pca.unit = 59
					THEN pca.amount * 1000000
				WHEN pca.unit = 155
					THEN pca.amount * 0.001
				WHEN pca.unit = 163
					THEN pca.amount * 0.000001
				ELSE NULL 
				END AS amount_µgprl, 
			cpl.long_text as compound, 
			cpl.casno,
			cpl.compoundno,
			dpct.companytype,
			drp.xutm32euref89, 
			drp.yutm32euref89,
			drp.planttype,
			pcs.project,
			drp.geom
		FROM jupiter.pltchemanalysis pca
		INNER JOIN jupiter.pltchemsample pcs USING (sampleid) 
		INNER JOIN jupiter.compoundlist cpl USING (compoundno)
		INNER JOIN jupiter.drwplant drp ON pcs.plantid = drp.plantid
		INNER JOIN jupiter.drwplantcompanytype dpct ON pcs.plantid = dpct.plantid
		WHERE cpl.compoundno IN 
			(
				SELECT compoundno
				FROM jupiter.compoundgroup
				WHERE compoundgroupno = 50
			)	
		ORDER BY drp.plantname, pcs.sampledate DESC, cpl.long_text
	);

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mstjupiter.pesticide_drv_all IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: all pesticide analysis from DRW samples'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- All drinkingwater chem samples that displays an exeedance for each pesticide
	DROP TABLE IF EXISTS mstjupiter.pesticide_drv_exceedance CASCADE;

	CREATE TABLE mstjupiter.pesticide_drv_exceedance AS (
		WITH 
			aldrin AS 
				(
					SELECT *
					FROM mstjupiter.pesticide_drv_all
					WHERE COALESCE("attribute", '>') IN ('>', 'B', 'C')
						AND compoundno IN (3503) 
						AND amount_µgprl >= 0.03
				),
			dieldrin AS 
				(
					SELECT *
					FROM mstjupiter.pesticide_drv_all
					WHERE COALESCE("attribute", '>') IN ('>', 'B', 'C')
						AND compoundno IN (3134) 
						AND amount_µgprl >= 0.03
				),
			heptachlor AS 
				(
					SELECT *
					FROM mstjupiter.pesticide_drv_all
					WHERE COALESCE("attribute", '>') IN ('>', 'B', 'C')
						AND compoundno IN (3136) 
						AND amount_µgprl >= 0.03
				),
			heptachlorepoxid AS 
				(
					SELECT *
					FROM mstjupiter.pesticide_drv_all
					WHERE COALESCE("attribute", '>') IN ('>', 'B', 'C')
						AND compoundno IN (3137) 
						AND amount_µgprl >= 0.03
				),	
			restpesticide AS 
				(
					SELECT *
					FROM mstjupiter.pesticide_drv_all
					WHERE COALESCE("attribute", '>') IN ('>', 'B', 'C')
						AND compoundno NOT IN (3503, 3134, 3136, 3137) 
						AND amount_µgprl >= 0.1
				),
			pesticide AS 
				(
					SELECT *
					FROM aldrin
					UNION ALL
					SELECT *
					FROM dieldrin
					UNION ALL
					SELECT *
					FROM heptachlor
					UNION ALL
					SELECT *
					FROM heptachlorepoxid
					UNION ALL
					SELECT *
					FROM restpesticide
				)
		SELECT
			p.*
		FROM pesticide p
	);

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mstjupiter.pesticide_gvk_exceedance IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: exceedances from all pesticide analysis from DRW samples'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- All samples containing Chloridazon pesticide
--	DROP TABLE mstjupiter.mstmvw_pesticide_chloridazon_DRV_proeve CASCADE;
--
--	CREATE TABLE mstjupiter.mstmvw_pesticide_chloridazon_DRV_proeve AS (
--		SELECT 	
--			row_number() over (order by pcs.plantid) as ID,
--			pcs.plantid, 
--			drp.plantname, 
--			pcs.sampleid,
--			pcs.sampledate,
--			pca."attribute",
--			pca.amount, 
--			cpl.long_text as Stof, 
--			cpl.casno,
--			cpl.compoundno,
--			dpct.companytype,
--			c.longtext as companytype_desc,
--			drp.xutm32euref89, 
--			drp.yutm32euref89,
--			drp.planttype,
--			pcs.project,
--			drp.geom	
--		FROM jupiter.pltchemanalysis pca
--		INNER JOIN jupiter.pltchemsample pcs USING (sampleid) 
--		INNER JOIN jupiter.compoundlist cpl USING (compoundno)
--		INNER JOIN jupiter.drwplant drp ON pcs.plantid = drp.plantid
--		INNER JOIN jupiter.drwplantcompanytype dpct ON pcs.plantid = dpct.plantid
--		INNER JOIN jupiter.code c on dpct.companytype = c.code
--		WHERE cpl.compoundno = ANY (ARRAY[3528::NUMERIC, 4696::NUMERIC, 4712::NUMERIC, 4761::NUMERIC])
--		ORDER BY drp.plantname, pcs.sampledate DESC
--	);
	
--	DROP MATERIALIZED VIEW IF EXISTS mstjupiter.mstmvw_pesticide_chlorthalonilamid_drv_proeve CASCADE;
--
--	CREATE MATERIALIZED VIEW mstjupiter.mstmvw_pesticide_chlorthalonilamid_drv_proeve AS 
--		(
--			SELECT 	
--				row_number() over (order by pcs.plantid) as ID,
--				pcs.plantid, 
--				drp.plantname, 
--				pcs.sampleid,
--				pcs.sampledate,
--				pca."attribute",
--				pca.amount, 
--				cpl.long_text as Stof, 
--				cpl.casno,
--				cpl.compoundno,
--				dpct.companytype,
--				drp.xutm32euref89, 
--				drp.yutm32euref89,
--				drp.planttype,
--				pcs.project,
--				drp.geom	
--			FROM jupiter.pltchemanalysis pca
--			INNER JOIN jupiter.pltchemsample pcs USING (sampleid) 
--			INNER JOIN jupiter.compoundlist cpl USING (compoundno)
--			INNER JOIN jupiter.drwplant drp ON pcs.plantid = drp.plantid
--			INNER JOIN jupiter.drwplantcompanytype dpct ON pcs.plantid = dpct.plantid
--	
--			WHERE cpl.compoundno = ANY (ARRAY[4945::numeric, 4989::numeric, 4990::numeric, 4961::numeric])
--				-- AND dpct.companytype = ANY (ARRAY['V01'::text, 'V02'::text, 'V03'::text])
--			ORDER BY drp.plantname, pcs.sampledate DESC
--		)
--	;

	GRANT USAGE ON SCHEMA mstjupiter TO jupiterrole;

	GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO jupiterrole;

COMMIT;
