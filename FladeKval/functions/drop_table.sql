BEGIN;

	DROP TABLE IF EXISTS fohm_layer CASCADE;
	
	CREATE SCHEMA fohm_layer;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON SCHEMA fohm_layer IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' simak'
	     || ' Schema containing all fohm layers tiled in 200x200'
	     || '''';
		END
	$do$
	;

COMMIT;
