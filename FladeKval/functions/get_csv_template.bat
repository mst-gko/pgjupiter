@echo off
setlocal enabledelayedexpansion

REM author: Simon Makwarth <simak@mst.dk>
REM encoding: UTF-8 using Windows (CR LF)
REM descr: to run this script the database credentials must be stored as an environmental variable
REM note: modified by Frederikke Hansen for KS Flader programme

REM set parameters
SET PGCLIENTENCODING=UTF8
SET PGPASSWORD=gebr470ne
SET pg_hostname=10.33.131.50
SET pg_username=grukosreader
SET pg_port=5432
SET dbname=fohm
SET schema_name=flader_ks_in
SET file_ext=grd


cd C:\Program Files\PostgreSQL\14\bin
psql -h !pg_hostname! -p !pg_port! -U !pg_username! -d !dbname! -c "\copy (SELECT *, ST_AsText(geom) as geom_wkt FROM __SCHEMATABLE__) to '__RESNAME__'  WITH DELIMITER ';' CSV HEADER" 

