Tool for quality assurance of geologic models by comparison to borehole lithology descriptions.
Geologic model is loaded into the program, processed, and .csv files are produced in a folder chosen by the user.
Author: Frederikke Hansen (frsha@mst.dk)
Based on work by Simon Makwarth (simak@mst.dk)
----------------------------------------------------------------------------------------------------------------------------------

How to run:
1. Call main.py from terminal (python main.py) or run in editor.
2. Choose folder containing .grd files of model and (empty) folder where results should be placed.
3. Run program.

Results in .csv files (one for each layer), showing the percentage of each lithology from the borehole description within the layer interval at the borehole location.
This may be loaded into QGIS along with the accompanying layer settings (.qml) file (in results folder) showing a pie chart for each borehole. There is a .qml file showing general lithology, and another showing more detailed grainsize breakdown.

---------------------------------------------------------------------------------------------------------------------------------- 
File structure (in a folder called "KS_App"):
1. main.py - runs the program
2. tempfiles - folder for temporary files so the input files can be edited without messing with the originals
3. themes - folder containing 3 files which should be copied into the results folder together with the output. These files are not changed during the program, only copied at the end.
4. functions - folder containing 3 template .bat files, one .py file, and one .sql file.

The code:
1. Everything is called from main.py.
2. User chooses directory containing input files and directory where output files should be placed.
3. .bat template files are edited by main.py to use the directories chosen by the user.
4. Input files are loaded into a postgresql database via a .bat file.
5. SQL code is run.
6. Final tables are copied into .csv files in the user's chosen results directory via a .bat file.
7. Any temporary files and database tables/views are deleted.

