# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 12:50:08 2023

@author: B165456
"""
import re
import configparser
import psycopg2 as pg
import pandas as pd
import sys
import os
import shutil
from PyQt5.QtWidgets import (QApplication, QWidget, QPushButton, QLineEdit, 
                             QFileDialog, QGridLayout, QLabel)
from PyQt5.QtCore import Qt
import subprocess

basedir = os.path.dirname(__file__)

class Database:
    """Class handles all database related function"""
    def __init__(self, database_name, usr):
        self.database_name = database_name.upper()
        self.connection = self.connect_to_pg_db()

    def connect_to_pg_db(self):
        try:
            pg_con = pg.connect(
                host='10.33.131.50',
                port='5432',
                database='fohm',
                user='grukosreader',
                password='gebr470ne'
            )
        except Exception as e:
            print(e)
            raise ConnectionError('Could not connect to database')
        else:
            print('Connected to database: ' + self.database_name)

        return pg_con
    

class FohmLayer:
    def __init__(self, source, schema, table, tabfin, grdloc, resloc):
        self.source = source
        self.schema = schema
        self.table = table
        self.tabfin = tabfin
        self.grdloc = grdloc
        self.resloc = resloc
        d = Database(database_name='FOHM', usr='reader')
        self.con_pg = d.connection
        self.cur = self.con_pg.cursor()

    def test_production_table(self, prod_table, temp_table):
        sql_test_size = f'''
                SELECT 
                    CASE 
                        WHEN pg_total_relation_size('{prod_table}') <= pg_total_relation_size('{temp_table}') 
                            THEN 1
                        ELSE 0
                        END AS prod_test
        '''
        with self.con_pg as con:
            df_test = pd.read_sql_query(sql_test_size, con=con)
            test_prod_theme = df_test['prod_test'].iloc[0]
            test_prod_theme = int(test_prod_theme)

        return test_prod_theme

    def fetch_fohm_layer_name(self):
        print('Fetching table name from fohm layers...')
        tablenames=[]
        for fi in os.listdir(self.grdloc):
            if fi.endswith(".grd"):
                tablenames_ini = fi[:-4]
                tablenames += [tablenames_ini.lower()]
        
        print('Table name from fohm layers fetched')
        return tablenames

    def create_temp_borehole(self):
        schemaname_source=self.source
        print('Creating temporary table containing jupiter boreholes...')
        sql_temp_borehole = f'''
            DROP TABLE IF EXISTS {schemaname_source}.temp_borehole;

            CREATE TABLE {schemaname_source}.temp_borehole AS 
                (
                    SELECT 
                        b.boreholeid, b.geom
                    FROM jupiter_fdw.borehole_view b
                    WHERE b.geom IS NOT NULL 
                )
            ;

            CREATE INDEX temp_borehole_geom_idx
                ON {schemaname_source}.temp_borehole
                USING GIST (geom);
        '''

        self.cur.execute(sql_temp_borehole)
        print('Temporary table containing jupiter boreholes created')

    def fohm_borehole(self):
        schemaname_end=self.schema
        tablename=self.table
        schemaname_source=self.source
        
        # create temporary table containing jupiter boreholes
        self.create_temp_borehole()

        # fetch fohm layer names used for tablenames
        tablenames = self.fetch_fohm_layer_name()

        # find the elevation of each fohm layer for all jupiter borehole and combine non-zero thickness layers
        sql_union = f'''
            DROP TABLE IF EXISTS {schemaname_end}.{tablename} CASCADE;

            CREATE TABLE {schemaname_end}.{tablename} AS 
                (
                    WITH 
                        fohm_lbh AS
                            (
        '''

        for i, row in enumerate(tablenames):

            layername = row
            layer_id = int(re.search(r"\d+", layername).group())

            print(f'Processing layer: {layername}...')

            # execute sql for a temp layer elevation-values each fohm layer with respect to jupiter boreholes
            sql_fohm = f'''
                DROP TABLE IF EXISTS {schemaname_source}.boreholes_{layername};

                CREATE TABLE {schemaname_source}.boreholes_{layername} AS 
                    (
                        SELECT
                            b.boreholeid,
                            '{layername}' AS layer,
                            {layer_id} AS layer_num,
                            (ST_ValueCount(ST_Clip(rast,b.geom))).value AS layer_ele
                        FROM {schemaname_source}.temp_borehole b
                        INNER JOIN {schemaname_source}."{layername}" r ON st_intersects(r.rast, b.geom)
                    )
                ;
                DROP TABLE IF EXISTS {schemaname_source}."{layername}";
            '''
            self.cur.execute(sql_fohm)

            # generate the union all sql
            if i == 0:
                sql_union += f'''
                            SELECT 
                                *
                            FROM {schemaname_source}.boreholes_{layername}
                '''
            else:
                sql_union += f'''
                    UNION ALL
                    SELECT 
                        *
                    FROM {schemaname_source}.boreholes_{layername}
                '''

            print(f'Layer: {layername} processed')

        sql_union += f'''
                        ),
                    tmp AS 
                        (
                            SELECT 
                                boreholeid, 
                                layer, 
                                layer_num, 
                                lag(layer_ele) OVER (PARTITION BY boreholeid ORDER BY layer_num) AS layer_top, 
                                layer_ele AS layer_bot 
                            FROM fohm_lbh flb  
                            ORDER BY boreholeid, layer_num
                        ),
                    tmp2 AS 
                        (
                            SELECT 
                                *,
                                CASE
                                    WHEN layer_top < layer_bot
                                        THEN 0
                                    ELSE layer_top - layer_bot
                                    END AS layer_thick,
                                CASE
                                    WHEN lower(layer) SIMILAR to '%(sand|billund|bastrup|odderup)%'
                                        THEN 'Sand'
                                    WHEN lower(layer) SIMILAR to '%(ler|vejle_fjord|klintinghoved|arnum|maadegruppen)%'
                                        THEN 'Clay'
                                    WHEN layer ILIKE '%toerv%'
                                        THEN 'Peat'
                                    WHEN layer ILIKE '%stensalt%' 	
                                        THEN 'Halite'
                                    WHEN lower(layer) SIMILAR to '%(kalk|kridt)%'
                                        THEN 'Limestone'	
                                    ELSE NULL
                                    END AS litho,
                                CASE
                                    WHEN layer_num = 100
                                        THEN 'Post-Glacial'
                                    WHEN layer_num > 100
                                        AND layer_num <= 2400
                                        THEN 'Quaternary'
                                    WHEN layer_num >= 5100
                                        AND layer_num <= 9500
                                        THEN 'Pre-Quaternary'
                                    ELSE NULL
                                    END AS time_period
                            FROM tmp 
                            WHERE layer_top IS NOT NULL 
                        )
                SELECT 
                    tmp2.*,
                    b.geom
                FROM tmp2
                INNER JOIN jupiter_fdw.borehole_view b USING (boreholeid)
                WHERE layer_thick != 0
            )
        ;
        DROP TABLE IF EXISTS {schemaname_source}.boreholes_{layername};
        '''
        print('\nUnion all borehole-fohm tables to one\n')
        self.cur.execute(sql_union)
        self.con_pg.commit()
        self.cur.close()
        self.con_pg.close()


    def lithologyComparison(self):
        schemaname_end=self.schema
        tablename=self.table
        schemaname_source=self.source
        
        # fetch fohm layer names used for tablenames
        tablenames = self.fetch_fohm_layer_name()

        for i, row in enumerate(tablenames):
            
            layername = row
            layer_id = int(re.search(r"\d+", layername).group())
            target_tab = self.tabfin
            resloc=self.resloc
            
            print(f'\nComparing layer to borehole lithology: {layername}...')
            
            sql_comp = f'''
            /*Boringsbeskrivelser i intervaller af FOHM lag. Bruger rocksymbol og texture fra jupitertabellen lithsamp, for en detaljeret opdeling hvor kornstørrelser er registreret.
 * Regnes både som tykkelse i boringen, og som procentdel af fohmlagets tykkelse.*/
drop view if exists {schemaname_end}.lit_model_comp_temp;
create view {schemaname_end}.lit_model_comp_temp as
with 	fohm_depth as ( --get the depth of fohm and top point of fohm
			select
				flba.boreholeid,
				flba.terraintop,
				layer_top,
				layer_bot,
				case -- hvis ingen elevation er målt for boringen, rykkes toppen af boringen til at matche toppen af modellen
					when b.elevation is null
						then flba.terraintop
					else b.elevation
				end as borehole_elev
			from (
			select
			flb.boreholeid,
			max(flb.layer_top) as terraintop
			from {schemaname_end}.{tablename} flb
			group by flb.boreholeid
			) flba
			left join jupiter_fdw.borehole_view b using (boreholeid)
			right join {schemaname_end}.{tablename} flb using (boreholeid) where layer_num = {layer_id}
		),
		ler_interval as (
			select 			
				fd.boreholeid,
				case 
					when ls.texture in ('bfe','bmf','brf','fe','mfe','rfe')
						then 'fed ler'
					when ls.texture is null
						then 'ler'
					else 'dårlig beskrevet, usikker'
				end as soil_type,
				abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
			from fohm_depth fd
			inner join jupiter_fdw.lithsamp_view ls using (boreholeid)
			inner join jupiter_fdw.borehole_view b using (boreholeid)
			where ls.rocksymbol in (
			'dl', 'fl', 'hl', 'il', 'ql',						
			'l', 'll', 'j', 'ml', 'ol',
			'rl', 'sl', 'u', 'vl', 'zl',
			'yl', 'tl', 'xl', 'gl', 'pl',
			'pr', 'ed', 'e', 'ee'
			) 
			and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
			and ls.bottom is not null
			and ls.top is not null
			),
		silt_interval as (
			select 			
				fd.boreholeid,
				case 
					when ls.texture in ('uso','fmg','uof','uog','mgu','gou','fgu','fmu','fou','mou')
						then 'usorteret silt'
					when ls.texture in ('bfi','fof','fin','fom','fmf','feg','ftm','fgr','fog','fgf','fgg')
						then 'fint silt'
					when ls.texture in ('mof','mgf','mgr','mog','mel','mgg','mtg','bme')
						then 'mellem silt'
					when ls.texture in ('mgo','gof','gog','gro','bgr')
						then 'groft silt'
					when ls.texture is null
						then 'silt'
					else 'dårlig beskrevet, usikker'
				end as soil_type,
				abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
			from fohm_depth fd
			inner join jupiter_fdw.lithsamp_view ls using (boreholeid)
			inner join jupiter_fdw.borehole_view b using (boreholeid)
			where ls.rocksymbol in (
			'di', 'fi', 'hi', 'i', 'ii',						
			'mi', 'qi', 'oi', 'yi', 'zi',
			'ti', 'gi', 'pi'
			) 
			and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
			and ls.bottom is not null
			and ls.top is not null
			),
		sand_interval as (
			select 			
				fd.boreholeid,
				case 
					when ls.texture in ('uso','fmg','uof','uog','mgu','gou','fgu','fmu','fou','mou')
						then 'usorteret sand'
					when ls.texture in ('bfi','fof','fin','fom','fmf','feg','ftm','fgr','fog','fgf','fgg')
						then 'fint sand'
					when ls.texture in ('mof','mgf','mgr','mog','mel','mgg','mtg','bme')
						then 'mellem sand'
					when ls.texture in ('mgo','gof','gog','gro','bgr')
						then 'groft sand'
					when ls.texture is null
						then 'sand'
					else 'dårlig beskrevet, usikker'
				end as soil_type,
				abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
			from fohm_depth fd
			inner join jupiter_fdw.lithsamp_view ls using (boreholeid)
			inner join jupiter_fdw.borehole_view b using (boreholeid)
			where ls.rocksymbol in (
			'ds', 'es', 'hs', 'fs', 'is',						
			'ks', 'ms', 'os', 's', 'zs',
			'ys', 'ts', 'qs', 'gs', 'pq',
			'ps'
			) 
			and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
			and ls.bottom is not null
			and ls.top is not null
			),
		grus_interval as (
			select 			
				fd.boreholeid,
				case 
					when ls.texture in ('uso','fmg','uof','uog','mgu','gou','fgu','fmu','fou','mou')
						then 'usorteret grus'
					when ls.texture in ('bfi','fof','fin','fom','fmf','feg','ftm','fgr','fog','fgf','fgg')
						then 'fint grus'
					when ls.texture in ('mof','mgf','mgr','mog','mel','mgg','mtg','bme')
						then 'mellem grus'
					when ls.texture in ('mgo','gof','gog','gro','bgr')
						then 'groft grus'
					when ls.texture is null
						then 'grus'
					else 'dårlig beskrevet, usikker'
				end as soil_type,
				abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
			from fohm_depth fd
			inner join jupiter_fdw.lithsamp_view ls using (boreholeid)
			inner join jupiter_fdw.borehole_view b using (boreholeid)
			where ls.rocksymbol in (
			'dg', 'fg', 'hg', 'ig', 'kg',						
			'g', 'mg', 'qg', 'yg', 'tg'
			) 
			and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
			and ls.bottom is not null
			and ls.top is not null
			),
		sten_interval as (
			select 			
				fd.boreholeid,
				case 
					when ls.texture in ('uso','fmg','uof','uog','mgu','gou','fgu','fmu','fou','mou')
						then 'usorteret sten'
					when ls.texture in ('bfi','fof','fin','fom','fmf','feg','ftm','fgr','fog','fgf','fgg')
						then 'fint sten'
					when ls.texture in ('mof','mgf','mgr','mog','mel','mgg','mtg','bme')
						then 'mellem sten'
					when ls.texture in ('mgo','gof','gog','gro','bgr')
						then 'groft sten'
					when ls.texture is null
						then 'sten'
					else 'dårlig beskrevet, usikker'
				end as soil_type,
				abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
			from fohm_depth fd
			inner join jupiter_fdw.lithsamp_view ls using (boreholeid)
			inner join jupiter_fdw.borehole_view b using (boreholeid)
			where ls.rocksymbol in (
			'dz', 'mz', 'z'
			) 
			and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
			and ls.bottom is not null
			and ls.top is not null
			),
		kalk_interval as (
			select 			
				fd.boreholeid,
				case 
					when ls.texture in ('sla','sts','svs','tæt')
						then 'slammet kalk'
					when ls.texture is null
						then 'kalk'
					else 'dårlig beskrevet, usikker'
				end as soil_type,
				abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
			from fohm_depth fd
			inner join jupiter_fdw.lithsamp_view ls using (boreholeid)
			inner join jupiter_fdw.borehole_view b using (boreholeid)
			where ls.rocksymbol in (
			'dk', 'kk', 'bk', 'ak', 'as',						
			'k', 'lk', 'sk', 'zk', 'tk',
			'gk', 'pk', 'fk'
			) 
			and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
			and ls.bottom is not null
			and ls.top is not null
			),
		organisk_interval as (
			select 			
				fd.boreholeid,
				case 
					when ls.texture in ('sla','sts','svs','tæt')
						then 'fed organisk'
					when ls.texture is null
						then 'organisk'
					else 'dårlig beskrevet, usikker'
				end as soil_type,
				abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
			from fohm_depth fd
			inner join jupiter_fdw.lithsamp_view ls using (boreholeid)
			inner join jupiter_fdw.borehole_view b using (boreholeid)
			where ls.rocksymbol in (
			'hp', 'ht', 'id', 'fp', 'c',						
			'ip', 'it', 'p', 'tp', 't',
			'yp', 'yt', 'tt', 'gc', 'qp',
			'qt', 'ft', 'gp'
			) 
			and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
			and ls.bottom is not null
			and ls.top is not null
			),
		vekslende_interval as (
			select 			
				fd.boreholeid,
				'vekslende' as soil_type,
				abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
			from fohm_depth fd
			inner join jupiter_fdw.lithsamp_view ls using (boreholeid)
			inner join jupiter_fdw.borehole_view b using (boreholeid)
			where ls.rocksymbol in (
			'dv', 'hv', 'iv', 'mv', 'yv',						
			'v', 'tv', 'qv', 'gv', 'fv',
			'ev', 'pv', 'av', 'o', 'ij',
			'fj', 'm'
			) 
			and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
			and ls.bottom is not null
			and ls.top is not null
			),
		ukendt_interval as (
			select 			
				fd.boreholeid,
				'ukendt' as soil_type,
				abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
			from fohm_depth fd
			inner join jupiter_fdw.lithsamp_view ls using (boreholeid)
			inner join jupiter_fdw.borehole_view b using (boreholeid)
			where ls.rocksymbol in (
			'b', 'x', 'h'
			)
			and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
			and ls.bottom is not null
			and ls.top is not null
			),
		andet_interval as (
			select 			
				fd.boreholeid,
				'andet' as soil_type,
				abs(greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)-least((fd.borehole_elev-ls.top),fd.layer_top)) as tykkelse
			from fohm_depth fd
			inner join jupiter_fdw.lithsamp_view ls using (boreholeid)
			inner join jupiter_fdw.borehole_view b using (boreholeid)
			where ls.rocksymbol in (
			'dr', 'cj', 'ck', 'cl', 'cq', 'cr', 'cv', 'cw', 'eq', 'ar',
			'kj', 'kl', 'kq', 'kr', 'jc', 'ji', 'jj', 'jl', 'jq', 'jr',
			'js', 'jv', 'ka', 'nq', 'nr', 'nw', 'r', 'mk', 'nd', 'nj',
			'nk', 'nl', 'oq', 'or', 'q', 'rq', 'rr', 'rs', 'rv', 'sj',						
			'sr', 'tr', 'wr', 'xk', 'xq', 'xr', 'wl', 'qk', 'rc', 'rg',
			'rj', 'rk', 'vc', 'vi', 'vj', 'vk', 'vq', 'vr', 'vs', 'vv',
			'w', 'wk', 'ok', 'oe', 'gr', 'ls', 'gf', 'ek', 'bl', 'bs',
			'bv', 'd', 'pj', 'uc', 'ui', 'uj', 'ul', 'uq', 'ur', 'us',
			'uv', 'el', 'f', 'a', 'pa', 'pd', 'af', 'al'
			) 
			and greatest((fd.borehole_elev-ls.bottom),fd.layer_bot)<least((fd.borehole_elev-ls.top),fd.layer_top)
			and ls.bottom is not null
			and ls.top is not null
			),
		fohm_soil_types as (
			select			
				boreholeid,
				soil_type,
				sum(tykkelse)  as tykkelse_fohmlag -- alle ens lithologier i fohmlagets interval i en boring lægges sammen
			from ler_interval
			full outer join silt_interval using (boreholeid,soil_type,tykkelse)
			full outer join sand_interval using (boreholeid,soil_type,tykkelse)
			full outer join grus_interval using (boreholeid,soil_type,tykkelse)
			full outer join sten_interval using (boreholeid,soil_type,tykkelse)
			full outer join kalk_interval using (boreholeid,soil_type,tykkelse)
			full outer join organisk_interval using (boreholeid,soil_type,tykkelse)
			full outer join vekslende_interval using (boreholeid,soil_type,tykkelse)
			full outer join ukendt_interval using (boreholeid,soil_type,tykkelse)
			full outer join andet_interval using (boreholeid,soil_type,tykkelse)
			group by boreholeid,soil_type
			)
select 
	b.boreholeno,
	fst.*,
	tykkelse_fohmlag/(fd.layer_top-fd.layer_bot)*100 as proc_fohmlag --omregn tykkelse af lithologi i boringen til procentdel af fohmlagets totale tykkelse
from fohm_soil_types fst
left join fohm_depth fd using (boreholeid)
left join jupiter_fdw.borehole_view b using (boreholeid);

/*Boringsbeskrivelser i intervaller af fohmlag sorteres pr boringsID via crosstab. Coalesce bruges til at erstatte NULL values, så QGIS læser det rigtigt.
 * Indhold af boringer i intervallet af fohmlag kan vises både som procent af total tykkelse af fohmlaget, eller som tykkelse i boringen*/
drop table if exists {schemaname_end}.{target_tab}_{layer_id};
create table {schemaname_end}.{target_tab}_{layer_id} as
select
	b.boreholeno,
	coalesce("fed ler",0) as "fed ler",coalesce("ler",0) as "ler",
	coalesce("silt",0) as "silt",coalesce("fint silt",0) as "fint silt",coalesce("mellem silt",0) as "mellem silt",coalesce("grov silt",0) as "grov silt",coalesce("usorteret silt",0) as "usorteret silt",
	coalesce("sand",0) as "sand",coalesce("fint sand",0) as "fint sand",coalesce("mellem sand",0) as "mellem sand",coalesce("grov sand",0) as "grov sand",coalesce("usorteret sand",0) as "usorteret sand",
	coalesce("grus",0) as "grus",coalesce("fint grus",0) as "fint grus",coalesce("mellem grus",0) as "mellem grus",coalesce("grov grus",0) as "grov grus",coalesce("usorteret grus",0) as "usorteret grus",
	coalesce("sten",0) as "sten",coalesce("fint sten",0) as "fint sten",coalesce("mellem sten",0) as "mellem sten",coalesce("grov sten",0) as "grov sten",coalesce("usorteret sten",0) as "usorteret sten",
	coalesce("kalk",0) as "kalk",coalesce("slammet kalk",0) as "slammet kalk",
	coalesce("organisk",0) as "organisk",coalesce("fed organisk",0) as "fed organisk",
	coalesce("vekslende",0) as "vekslende",coalesce("ukendt",0) as "ukendt",coalesce("andet",0) as "andet",coalesce("dårligt beskrevet, usikker",0) as "dårligt beskrevet, usikker",
	b.geom
from crosstab(
	'SELECT 
		boreholeid, 
		soil_type, 
		proc_fohmlag 						/*eller tykkelse_fohmlag, hvis tykkelse i boringen ønskes*/
    FROM {schemaname_end}.lit_model_comp_temp
    ORDER BY 1,2',
    $$VALUES ('ler'::text), ('fed ler'), ('silt'), ('fint silt'), ('mellem silt'), ('groft silt'), ('usorteret silt'),
   ('sand'), ('fint sand'), ('mellem sand'), ('groft sand'), ('usorteret sand'), ('grus'), ('fint grus'),
   ('mellem grus'), ('groft grus'), ('usorteret grus'), ('sten'), ('fint sten'), ('mellem sten'), ('grov sten'), ('usorteret sten'),
   ('slammet kalk'), ('kalk'), ('fed organisk'), ('organisk'), ('dårlig beskrevet, usikker'), ('vekslende'),
   ('ukendt'), ('andet')$$
	)
   AS ct("boreholeid" int, "ler" float8, "fed ler" float8, "silt" float8, "fint silt" float8, "mellem silt" float8, "grov silt" float8, "usorteret silt" float8, 
   "sand" float8, "fint sand" float8, "mellem sand" float8, "grov sand" float8, "usorteret sand" float8, "grus" float8, "fint grus" float8, "mellem grus" float8,
   "grov grus" float8, "usorteret grus" float8, "sten" float8, "fint sten" float8, "mellem sten" float8, "grov sten" float8, "usorteret sten" float8,
   "slammet kalk" float8, "kalk" float8, "fed organisk" float8, "organisk" float8, "dårligt beskrevet, usikker" float8, "vekslende" float8, "ukendt" float8, "andet" float8)
 inner join jupiter_fdw.borehole_view b using (boreholeid);
            '''
            try:
                self.cur.execute(sql_comp)
            except:
                try:
                    self.cur.close()
                    self.cur = self.con_pg.cursor()
                except:
                    self.con_pg.close()
                    d = Database(database_name='FOHM', usr='reader')
                    self.con_pg = d.connection
                self.cur = self.con_pg.cursor()
                
            self.con_pg.commit()
            print(f'Table created: {target_tab}_{layer_id}')
            
            #gem som csv
            f = open(r'.\functions\get_csv_template.bat','r')
            filedata = f.read()
            f.close()
            
            
            newdata = filedata.replace('__SCHEMATABLE__',f'{schemaname_end}.{target_tab}_{layer_id}').replace('__RESNAME__',f'{resloc}\{target_tab}_{layer_id}.csv')
            
            f = open(r'.\functions\get_csv.bat','w')
            f.write(newdata)
            f.close()
            
            direct=os.getcwd()
            subprocess.call([direct+r'\functions\get_csv.bat'])
            os.remove(direct+r'\functions\get_csv.bat')
            

            #drop table
            sql_drop = f'''drop table if exists {schemaname_end}.{target_tab}_{layer_id};
            DROP TABLE IF EXISTS {schemaname_source}.boreholes_{layername};'''
            try:
                self.cur.execute(sql_drop)
            except:
                try:
                    self.cur.close()
                    self.cur = self.con_pg.cursor()
                except:
                    self.con_pg.close()
                    d = Database(database_name='FOHM', usr='reader')
                    self.con_pg = d.connection
                self.cur = self.con_pg.cursor()
                
            self.con_pg.commit()
            
        #drop intermediate tables
        sql_drop = f'''drop view if exists {schemaname_end}.lit_model_comp_temp;
        DROP TABLE IF EXISTS {schemaname_source}.temp_borehole;
        DROP TABLE IF EXISTS {schemaname_end}.{tablename} CASCADE;
        '''
        try:
            self.cur.execute(sql_drop)
        except:
            try:
                self.cur.close()
                self.cur = self.con_pg.cursor()
            except:
                self.con_pg.close()
                d = Database(database_name='FOHM', usr='reader')
                self.con_pg = d.connection
            self.cur = self.con_pg.cursor()
            
        self.con_pg.commit()
            
class MyApp(QWidget):
    def __init__(self):
        super().__init__()
        self.window_width, self.window_height = 500, 100
        self.setMinimumSize(self.window_width, self.window_height)
        
        layout = QGridLayout()
        layout.setColumnStretch(0, 3)
        layout.setColumnStretch(1, 10)
        layout.setColumnStretch(2, 5)
        self.setLayout(layout)
        
        layout.addWidget(QLabel('Mappe med gridflader:'), 0, 0)
        
        self.linebox =QLineEdit()
        layout.addWidget(self.linebox)
        
        btn = QPushButton('Vælg mappe')
        btn.clicked.connect(self.launchDialog)
        layout.addWidget(btn)
        
        layout.addWidget(QLabel('Mappe til resultat:'), 1, 0)
        
        self.linebox1 =QLineEdit()
        layout.addWidget(self.linebox1)
            
        btn1 = QPushButton('Vælg mappe')
        btn1.clicked.connect(self.launchDialog1)
        layout.addWidget(btn1)

        btn2 =QPushButton('Sammenlign med boringer!')
        btn2.clicked.connect(self.ksFlader)
        layout.addWidget(btn2,2,0,1,3,alignment=Qt.AlignmentFlag.AlignCenter)
    
    def launchDialog(self):
        response = QFileDialog.getExistingDirectory(self,caption='vælg mappe')
        self.linebox.setText(str(response))
    
    def launchDialog1(self):
        response = QFileDialog.getExistingDirectory(self,caption='vælg mappe')
        self.linebox1.setText(str(response))
        
    def ksFlader(self):
        direct=basedir
        grd_folder=self.linebox.text().replace('/','\\')
        csv_folder=self.linebox1.text().replace('/','\\')
        tmp_folder=direct+r'\tempfiles'
        #indlæs flader til database
        f = open(direct+r'\functions\save_grd_to_DB_template.bat','r')
        filedata = f.read()
        f.close()
        
        newdata = filedata.replace('__path__',grd_folder)
        
        f = open(direct+r'\functions\save_grd_to_DB.bat','w')
        f.write(newdata)
        f.close()
        
        subprocess.call([direct+r'\functions\save_grd_to_DB.bat'])
        os.remove(direct+r'\functions\save_grd_to_DB.bat')
        
        # set vars
        schemaname_source = 'flader_ks_in' #schema containing surfaces for each layer in model
        schemaname_end = 'flader_ks_out' # target schema where results are placed
        tablename = 'flader_ks_int' # intermediate table
        tablename_final = 'flader_ks' # final comparison tables (one for each input layer) with percentage borehole lithology

        fb = FohmLayer(schemaname_source, schemaname_end, tablename, tablename_final, tmp_folder, csv_folder)
        fb.fohm_borehole()
        fb = FohmLayer(schemaname_source, schemaname_end, tablename, tablename_final, tmp_folder, csv_folder)
        fb.lithologyComparison()
        
        subprocess.call([direct+r'\functions\clear_temp.bat'])
        shutil.copytree(direct+r'\QGIS_tematisering', csv_folder+r'\QGIS_tematisering')
        
        
        
if __name__== '__main__':
    app=QApplication(sys.argv)
    app.setStyleSheet('''
        QWidget {
            font-size: 15px;
        }
        ''')
        
    myApp = MyApp()
    myApp.show()
    try:
        sys.exit(app.exec_())
    except SystemExit:
        print('finished!')
    