<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis labelsEnabled="0" simplifyDrawingHints="0" simplifyDrawingTol="1" minScale="100000000" styleCategories="AllStyleCategories" hasScaleBasedVisibilityFlag="0" symbologyReferenceScale="-1" maxScale="0" version="3.22.0-Białowieża" simplifyAlgorithm="0" simplifyLocal="1" simplifyMaxScale="1" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal startField="" mode="0" endExpression="" accumulate="0" endField="" durationUnit="min" fixedDuration="0" enabled="0" durationField="" limitMode="0" startExpression="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 type="singleSymbol" symbollevels="0" enableorderby="0" forceraster="0" referencescale="-1">
    <symbols>
      <symbol force_rhr="0" type="marker" name="0" alpha="1" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;fed ler&quot;)*1.8,(&quot;fed ler&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;fed ler&quot;)*1.8,(&quot;fed ler&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@0" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="138,67,43,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="138,67,43,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;ler&quot;)*1.8 + &quot;fed ler&quot;*3.6,(&quot;ler&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;ler&quot;)*1.8 + &quot;fed ler&quot;*3.6,(&quot;ler&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@1" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="203,98,63,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="203,98,63,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;silt&quot;)*1.8+(&quot;ler&quot; + &quot;fed ler&quot;)*3.6,(&quot;silt&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;silt&quot;)*1.8+(&quot;ler&quot; + &quot;fed ler&quot;)*3.6,(&quot;silt&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@2" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="255,242,1,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="255,242,1,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;fint silt&quot;)*1.8+(&quot;silt&quot; + &quot;ler&quot; +  &quot;fed ler&quot;)*3.6,(&quot;fint silt&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;fint silt&quot;)*1.8+(&quot;silt&quot; + &quot;ler&quot; +  &quot;fed ler&quot;)*3.6,(&quot;fint silt&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@3" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="228,228,13,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="228,228,13,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;mellem silt&quot;)*1.8+(&quot;silt&quot; +  &quot;fint silt&quot; +  &quot;ler&quot; +  &quot;fed ler&quot;)*3.6,(&quot;mellem silt&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;mellem silt&quot;)*1.8+(&quot;silt&quot; +  &quot;fint silt&quot; +  &quot;ler&quot; +  &quot;fed ler&quot;)*3.6,(&quot;mellem silt&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@4" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="248,209,34,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="248,209,34,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;grov silt&quot;)*1.8+(&quot;silt&quot; +  &quot;fint silt&quot; +  &quot;mellem silt&quot; +  &quot;ler&quot; +  &quot;fed ler&quot;)*3.6,(&quot;grov silt&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;grov silt&quot;)*1.8+(&quot;silt&quot; +  &quot;fint silt&quot; +  &quot;mellem silt&quot; +  &quot;ler&quot; +  &quot;fed ler&quot;)*3.6,(&quot;grov silt&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@5" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="237,255,35,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="237,255,35,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;usorteret silt&quot;)*1.8+(&quot;silt&quot; +  &quot;fint silt&quot; +  &quot;mellem silt&quot; +  &quot;grov silt&quot; +  &quot;ler&quot; +  &quot;fed ler&quot;)*3.6,(&quot;usorteret silt&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;usorteret silt&quot;)*1.8+(&quot;silt&quot; +  &quot;fint silt&quot; +  &quot;mellem silt&quot; +  &quot;grov silt&quot; +  &quot;ler&quot; +  &quot;fed ler&quot;)*3.6,(&quot;usorteret silt&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@6" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="200,176,20,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="200,176,20,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;sand&quot;)*1.8+(&quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;sand&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;sand&quot;)*1.8+(&quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;sand&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@7" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="255,0,0,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="255,0,0,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;fint sand&quot;)*1.8+(&quot;sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;fint sand&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;fint sand&quot;)*1.8+(&quot;sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;fint sand&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@8" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="235,27,89,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="235,27,89,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;mellem sand&quot;)*1.8+(&quot;sand&quot; + &quot;fint sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;mellem sand&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;mellem sand&quot;)*1.8+(&quot;sand&quot; + &quot;fint sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;mellem sand&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@9" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="255,105,79,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="255,105,79,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;grov sand&quot;)*1.8+(&quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;grov sand&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;grov sand&quot;)*1.8+(&quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;grov sand&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@10" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="224,80,76,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="224,80,76,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;usorteret sand&quot;)*1.8+(&quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;usorteret sand&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;usorteret sand&quot;)*1.8+(&quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;usorteret sand&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@11" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="255,78,55,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="255,78,55,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;grus&quot;)*1.8+(&quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;grus&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;grus&quot;)*1.8+(&quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;grus&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@12" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="255,129,198,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="255,129,198,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;fint grus&quot;)*1.8+(&quot;grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;fint grus&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;fint grus&quot;)*1.8+(&quot;grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;fint grus&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@13" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="255,207,245,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="255,207,245,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;mellem grus&quot;)*1.8+(&quot;grus&quot; + &quot;fint grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;mellem grus&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;mellem grus&quot;)*1.8+(&quot;grus&quot; + &quot;fint grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;mellem grus&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@14" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="226,153,191,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="226,153,191,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;grov grus&quot;)*1.8+(&quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;grov grus&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;grov grus&quot;)*1.8+(&quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;grov grus&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@15" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="255,47,168,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="255,47,168,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;usorteret grus&quot;)*1.8+(&quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;usorteret grus&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;usorteret grus&quot;)*1.8+(&quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;usorteret grus&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@16" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="255,77,237,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="255,77,237,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;sten&quot;)*1.8+(&quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;sten&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;sten&quot;)*1.8+(&quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;sten&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@17" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="168,0,174,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="168,0,174,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;fint sten&quot;)*1.8+(&quot;sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;fint sten&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;fint sten&quot;)*1.8+(&quot;sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;fint sten&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@18" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="197,30,206,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="197,30,206,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;mellem sten&quot;)*1.8+(&quot;sten&quot; + &quot;fint sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;mellem sten&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;mellem sten&quot;)*1.8+(&quot;sten&quot; + &quot;fint sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;mellem sten&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@19" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="172,88,176,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="172,88,176,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;grov sten&quot;)*1.8+(&quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;grov sten&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;grov sten&quot;)*1.8+(&quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;grov sten&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@20" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="138,0,110,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="138,0,110,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;usorteret sten&quot;)*1.8+(&quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;usorteret sten&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;usorteret sten&quot;)*1.8+(&quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;usorteret sten&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@21" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="159,80,211,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="159,80,211,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;kalk&quot;)*1.8+(&quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;usorteret sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,( &quot;kalk&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;kalk&quot;)*1.8+(&quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;usorteret sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,( &quot;kalk&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@22" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="0,188,19,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="0,188,19,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;slammet kalk&quot;)*1.8+(&quot;kalk&quot; + &quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;usorteret sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;slammet kalk&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;slammet kalk&quot;)*1.8+(&quot;kalk&quot; + &quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;usorteret sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;slammet kalk&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@23" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="0,120,12,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="0,120,12,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),( &quot;fed organisk&quot; )*1.8+( &quot;organisk&quot; + &quot;kalk&quot; + &quot;slammet kalk&quot; + &quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;usorteret sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;fed organisk&quot; )*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),( &quot;fed organisk&quot; )*1.8+( &quot;organisk&quot; + &quot;kalk&quot; + &quot;slammet kalk&quot; + &quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;usorteret sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;fed organisk&quot; )*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@24" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="97,176,255,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="97,176,255,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),( &quot;organisk&quot;)*1.8+( &quot;kalk&quot; + &quot;slammet kalk&quot; + &quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;usorteret sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,( &quot;organisk&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),( &quot;organisk&quot;)*1.8+( &quot;kalk&quot; + &quot;slammet kalk&quot; + &quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;usorteret sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,( &quot;organisk&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@25" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="43,112,181,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="43,112,181,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),(&quot;vekslende&quot;)*1.8+(&quot;organisk&quot; + &quot;fed organisk&quot; + &quot;kalk&quot; + &quot;slammet kalk&quot; + &quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;usorteret sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;vekslende&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),(&quot;vekslende&quot;)*1.8+(&quot;organisk&quot; + &quot;fed organisk&quot; + &quot;kalk&quot; + &quot;slammet kalk&quot; + &quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;usorteret sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;vekslende&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@26" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="60,60,60,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="60,60,60,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),( &quot;ukendt&quot;)*1.8+(&quot;vekslende&quot; + &quot;organisk&quot; + &quot;fed organisk&quot; + &quot;kalk&quot; + &quot;slammet kalk&quot; + &quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;usorteret sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;ukendt&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),( &quot;ukendt&quot;)*1.8+(&quot;vekslende&quot; + &quot;organisk&quot; + &quot;fed organisk&quot; + &quot;kalk&quot; + &quot;slammet kalk&quot; + &quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;usorteret sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; +&quot;usorteret silt&quot;)*3.6,(&quot;ukendt&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@27" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="158,158,158,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="158,158,158,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),( &quot;andet&quot;)*1.8+(&quot;vekslende&quot; + &quot;ukendt&quot; +  &quot;organisk&quot; + &quot;fed organisk&quot; + &quot;kalk&quot; + &quot;slammet kalk&quot; + &quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;usorteret sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; + &quot;usorteret silt&quot;)*3.6,(&quot;andet&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),( &quot;andet&quot;)*1.8+(&quot;vekslende&quot; + &quot;ukendt&quot; +  &quot;organisk&quot; + &quot;fed organisk&quot; + &quot;kalk&quot; + &quot;slammet kalk&quot; + &quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;usorteret sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; + &quot;usorteret silt&quot;)*3.6,(&quot;andet&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@28" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="200,200,200,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="200,200,200,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option type="QString" value="Fill" name="SymbolType"/>
            <Option type="QString" value="wedge_buffer(centroid($geometry),( &quot;dårligt beskrevet, usikker&quot; )*1.8+(&quot;vekslende&quot; + &quot;ukendt&quot; + &quot;andet&quot; + &quot;organisk&quot; + &quot;fed organisk&quot; + &quot;kalk&quot; + &quot;slammet kalk&quot; + &quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;usorteret sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; + &quot;usorteret silt&quot;)*3.6,(&quot;dårligt beskrevet, usikker&quot;)*3.6,200,50)" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <prop k="SymbolType" v="Fill"/>
          <prop k="geometryModifier" v="wedge_buffer(centroid($geometry),( &quot;dårligt beskrevet, usikker&quot; )*1.8+(&quot;vekslende&quot; + &quot;ukendt&quot; + &quot;andet&quot; + &quot;organisk&quot; + &quot;fed organisk&quot; + &quot;kalk&quot; + &quot;slammet kalk&quot; + &quot;sten&quot; + &quot;fint sten&quot; + &quot;mellem sten&quot; + &quot;grov sten&quot; + &quot;usorteret sten&quot; + &quot;grus&quot; + &quot;fint grus&quot; + &quot;mellem grus&quot; + &quot;grov grus&quot; + &quot;usorteret grus&quot; + &quot;sand&quot; + &quot;fint sand&quot; + &quot;mellem sand&quot; + &quot;grov sand&quot; + &quot;usorteret sand&quot; + &quot;ler&quot; + &quot;fed ler&quot; + &quot;silt&quot; + &quot;fint silt&quot; + &quot;mellem silt&quot; + &quot;grov silt&quot; + &quot;usorteret silt&quot;)*3.6,(&quot;dårligt beskrevet, usikker&quot;)*3.6,200,50)"/>
          <prop k="units" v="MapUnit"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol force_rhr="0" type="fill" name="@0@29" alpha="1" clip_to_extent="1">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" pass="0" enabled="1" class="SimpleFill">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="230,230,230,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="230,230,230,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
        <layer locked="0" pass="0" enabled="1" class="SimpleMarker">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="square" name="cap_style"/>
            <Option type="QString" value="0,0,0,255" name="color"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="circle" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="35,35,35,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="1" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <prop k="angle" v="0"/>
          <prop k="cap_style" v="square"/>
          <prop k="color" v="0,0,0,255"/>
          <prop k="horizontal_anchor_point" v="1"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="name" v="circle"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="scale_method" v="diameter"/>
          <prop k="size" v="1"/>
          <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="size_unit" v="MM"/>
          <prop k="vertical_anchor_point" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option type="List" name="dualview/previewExpressions">
        <Option type="QString" value="&quot;boreholeno&quot;"/>
      </Option>
      <Option type="int" value="0" name="embeddedWidgets/count"/>
      <Option type="invalid" name="variableNames"/>
      <Option type="invalid" name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory direction="0" penAlpha="255" lineSizeType="MM" labelPlacementMethod="XHeight" scaleBasedVisibility="0" sizeScale="3x:0,0,0,0,0,0" minimumSize="0" penWidth="0" spacing="5" enabled="0" backgroundColor="#ffffff" scaleDependency="Area" showAxis="1" minScaleDenominator="0" spacingUnitScale="3x:0,0,0,0,0,0" diagramOrientation="Up" barWidth="5" opacity="1" maxScaleDenominator="1e+08" spacingUnit="MM" rotationOffset="270" sizeType="MM" penColor="#000000" backgroundAlpha="255" lineSizeScale="3x:0,0,0,0,0,0" height="15" width="15">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute field="" color="#000000" label=""/>
      <axisSymbol>
        <symbol force_rhr="0" type="line" name="" alpha="1" clip_to_extent="1">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <layer locked="0" pass="0" enabled="1" class="SimpleLine">
            <Option type="Map">
              <Option type="QString" value="0" name="align_dash_pattern"/>
              <Option type="QString" value="square" name="capstyle"/>
              <Option type="QString" value="5;2" name="customdash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
              <Option type="QString" value="MM" name="customdash_unit"/>
              <Option type="QString" value="0" name="dash_pattern_offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
              <Option type="QString" value="0" name="draw_inside_polygon"/>
              <Option type="QString" value="bevel" name="joinstyle"/>
              <Option type="QString" value="35,35,35,255" name="line_color"/>
              <Option type="QString" value="solid" name="line_style"/>
              <Option type="QString" value="0.26" name="line_width"/>
              <Option type="QString" value="MM" name="line_width_unit"/>
              <Option type="QString" value="0" name="offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="offset_unit"/>
              <Option type="QString" value="0" name="ring_filter"/>
              <Option type="QString" value="0" name="trim_distance_end"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_end_unit"/>
              <Option type="QString" value="0" name="trim_distance_start"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_start_unit"/>
              <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
              <Option type="QString" value="0" name="use_custom_dash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
            </Option>
            <prop k="align_dash_pattern" v="0"/>
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="dash_pattern_offset" v="0"/>
            <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="dash_pattern_offset_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="trim_distance_end" v="0"/>
            <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_end_unit" v="MM"/>
            <prop k="trim_distance_start" v="0"/>
            <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_start_unit" v="MM"/>
            <prop k="tweak_dash_pattern_on_corners" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings linePlacementFlags="18" obstacle="0" showAll="1" dist="0" zIndex="0" placement="0" priority="0">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend showLabelLegend="0" type="default-vector"/>
  <referencedLayers/>
  <fieldConfiguration/>
  <aliases/>
  <defaults/>
  <constraints/>
  <constraintExpressions/>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column type="field" hidden="0" name="boreholeno" width="-1"/>
      <column type="field" hidden="0" name="ler" width="-1"/>
      <column type="field" hidden="0" name="fed ler" width="-1"/>
      <column type="field" hidden="0" name="silt" width="-1"/>
      <column type="field" hidden="0" name="fint silt" width="-1"/>
      <column type="field" hidden="0" name="mellem silt" width="-1"/>
      <column type="field" hidden="0" name="grov silt" width="-1"/>
      <column type="field" hidden="0" name="usorteret silt" width="-1"/>
      <column type="field" hidden="0" name="sand" width="-1"/>
      <column type="field" hidden="0" name="fint sand" width="-1"/>
      <column type="field" hidden="0" name="mellem sand" width="-1"/>
      <column type="field" hidden="0" name="grov sand" width="-1"/>
      <column type="field" hidden="0" name="usorteret sand" width="-1"/>
      <column type="field" hidden="0" name="grus" width="-1"/>
      <column type="field" hidden="0" name="fint grus" width="-1"/>
      <column type="field" hidden="0" name="mellem grus" width="-1"/>
      <column type="field" hidden="0" name="grov grus" width="-1"/>
      <column type="field" hidden="0" name="usorteret grus" width="-1"/>
      <column type="field" hidden="0" name="sten" width="-1"/>
      <column type="field" hidden="0" name="fint sten" width="-1"/>
      <column type="field" hidden="0" name="mellem sten" width="-1"/>
      <column type="field" hidden="0" name="grov sten" width="-1"/>
      <column type="field" hidden="0" name="usorteret sten" width="-1"/>
      <column type="field" hidden="0" name="slammet kalk" width="-1"/>
      <column type="field" hidden="0" name="kalk" width="-1"/>
      <column type="field" hidden="0" name="fed organisk" width="-1"/>
      <column type="field" hidden="0" name="organisk" width="-1"/>
      <column type="field" hidden="0" name="dårligt beskrevet, usikker" width="-1"/>
      <column type="field" hidden="0" name="vekslende" width="-1"/>
      <column type="field" hidden="0" name="ukendt" width="-1"/>
      <column type="field" hidden="0" name="andet" width="-1"/>
      <column type="actions" hidden="1" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="andet" editable="1"/>
    <field name="boreholeid" editable="1"/>
    <field name="boreholeno" editable="1"/>
    <field name="dårligt beskrevet, usikker" editable="1"/>
    <field name="fed ler" editable="1"/>
    <field name="fed organisk" editable="1"/>
    <field name="fint grus" editable="1"/>
    <field name="fint sand" editable="1"/>
    <field name="fint silt" editable="1"/>
    <field name="fint sten" editable="1"/>
    <field name="grov grus" editable="1"/>
    <field name="grov sand" editable="1"/>
    <field name="grov silt" editable="1"/>
    <field name="grov sten" editable="1"/>
    <field name="grus" editable="1"/>
    <field name="kalk" editable="1"/>
    <field name="ler" editable="1"/>
    <field name="mellem grus" editable="1"/>
    <field name="mellem sand" editable="1"/>
    <field name="mellem silt" editable="1"/>
    <field name="mellem sten" editable="1"/>
    <field name="organisk" editable="1"/>
    <field name="sand" editable="1"/>
    <field name="silt" editable="1"/>
    <field name="slammet kalk" editable="1"/>
    <field name="sten" editable="1"/>
    <field name="ukendt" editable="1"/>
    <field name="usorteret grus" editable="1"/>
    <field name="usorteret sand" editable="1"/>
    <field name="usorteret silt" editable="1"/>
    <field name="usorteret sten" editable="1"/>
    <field name="vekslende" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="andet"/>
    <field labelOnTop="0" name="boreholeid"/>
    <field labelOnTop="0" name="boreholeno"/>
    <field labelOnTop="0" name="dårligt beskrevet, usikker"/>
    <field labelOnTop="0" name="fed ler"/>
    <field labelOnTop="0" name="fed organisk"/>
    <field labelOnTop="0" name="fint grus"/>
    <field labelOnTop="0" name="fint sand"/>
    <field labelOnTop="0" name="fint silt"/>
    <field labelOnTop="0" name="fint sten"/>
    <field labelOnTop="0" name="grov grus"/>
    <field labelOnTop="0" name="grov sand"/>
    <field labelOnTop="0" name="grov silt"/>
    <field labelOnTop="0" name="grov sten"/>
    <field labelOnTop="0" name="grus"/>
    <field labelOnTop="0" name="kalk"/>
    <field labelOnTop="0" name="ler"/>
    <field labelOnTop="0" name="mellem grus"/>
    <field labelOnTop="0" name="mellem sand"/>
    <field labelOnTop="0" name="mellem silt"/>
    <field labelOnTop="0" name="mellem sten"/>
    <field labelOnTop="0" name="organisk"/>
    <field labelOnTop="0" name="sand"/>
    <field labelOnTop="0" name="silt"/>
    <field labelOnTop="0" name="slammet kalk"/>
    <field labelOnTop="0" name="sten"/>
    <field labelOnTop="0" name="ukendt"/>
    <field labelOnTop="0" name="usorteret grus"/>
    <field labelOnTop="0" name="usorteret sand"/>
    <field labelOnTop="0" name="usorteret silt"/>
    <field labelOnTop="0" name="usorteret sten"/>
    <field labelOnTop="0" name="vekslende"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="andet"/>
    <field reuseLastValue="0" name="boreholeid"/>
    <field reuseLastValue="0" name="boreholeno"/>
    <field reuseLastValue="0" name="dårligt beskrevet, usikker"/>
    <field reuseLastValue="0" name="fed ler"/>
    <field reuseLastValue="0" name="fed organisk"/>
    <field reuseLastValue="0" name="fint grus"/>
    <field reuseLastValue="0" name="fint sand"/>
    <field reuseLastValue="0" name="fint silt"/>
    <field reuseLastValue="0" name="fint sten"/>
    <field reuseLastValue="0" name="grov grus"/>
    <field reuseLastValue="0" name="grov sand"/>
    <field reuseLastValue="0" name="grov silt"/>
    <field reuseLastValue="0" name="grov sten"/>
    <field reuseLastValue="0" name="grus"/>
    <field reuseLastValue="0" name="kalk"/>
    <field reuseLastValue="0" name="ler"/>
    <field reuseLastValue="0" name="mellem grus"/>
    <field reuseLastValue="0" name="mellem sand"/>
    <field reuseLastValue="0" name="mellem silt"/>
    <field reuseLastValue="0" name="mellem sten"/>
    <field reuseLastValue="0" name="organisk"/>
    <field reuseLastValue="0" name="sand"/>
    <field reuseLastValue="0" name="silt"/>
    <field reuseLastValue="0" name="slammet kalk"/>
    <field reuseLastValue="0" name="sten"/>
    <field reuseLastValue="0" name="ukendt"/>
    <field reuseLastValue="0" name="usorteret grus"/>
    <field reuseLastValue="0" name="usorteret sand"/>
    <field reuseLastValue="0" name="usorteret silt"/>
    <field reuseLastValue="0" name="usorteret sten"/>
    <field reuseLastValue="0" name="vekslende"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"boreholeno"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
