BEGIN;

	DROP TABLE IF EXISTS mstjupiter.hydrology_permit_not_vv;

	CREATE TABLE mstjupiter.hydrology_permit_not_vv AS 
		(
			SELECT
				ROW_NUMBER() OVER (ORDER BY plantid) AS row_num,
				dp.plantid AS anlaegsid,
				dp.municipalityno2007,
				dp.plantname AS anlaegsnavn,
				dp.plantaddress AS anlaegsadr,
				dp.plantpostalcode AS anlaegpostnr,
				dp.active,
				pc.companytype AS virksomhedstype,
				c.longtext AS virksomhedstypetext,
				cp.permissionid AS tilladelsesid,
				cp.amountperyear AS tilladelsepraar,
				date(cp.startdate) AS tilladelsestart,
				date(cp.enddate) AS tilladelseslut,
				CASE
					WHEN cp.enddate < now() OR cp.revoked = '1' 
						THEN 'Nej'
					ELSE 'Ja'
				END aktiv,
				CASE
					WHEN cp.revoked = '0' 
						THEN 'Nej'
					WHEN cp.revoked = '1' 
						THEN 'Ja'
					END AS tilbagekaldt,
				'https://data.geus.dk/JupiterWWW/anlaeg.jsp?anlaegid=' || dp.plantid::TEXT AS url_plant,
				CASE 
			    	WHEN dp.geom IS NOT NULL
			    		THEN 'Plant geometry'
			    	WHEN dp.geom IS NULL 
			    		AND bg.geom IS NOT NULL 
			    		THEN 'Borehole centroid'
			    	WHEN dp.geom IS NULL 
			    		AND bg.geom IS NULL 
			    		THEN 'No geometry found' 
			    	END AS geom_descr,
			    COALESCE(dp.geom, bg.geom) AS geom
			FROM jupiter.drwplant dp
			INNER JOIN jupiter.drwplantcompanytype pc USING (plantid)
			INNER JOIN jupiter.code c ON c.code = pc.companytype
			INNER JOIN jupiter.catchperm cp USING (plantid)
			LEFT JOIN 
				(
					WITH tmp AS 
						(
							SELECT DISTINCT ON (plantid, boreholeid)
								dpi.plantid,
								boreholeid,
								b.geom
							FROM jupiter.drwplantintake dpi 
							INNER JOIN jupiter.borehole b USING (boreholeid)
							WHERE b.use != 'S'
								AND COALESCE(dpi.enddate, current_date) >= current_date
						)
					SELECT 
						plantid,
						st_centroid(st_union(geom)) as geom
					FROM tmp
					GROUP BY plantid
				) AS bg USING (plantid)
			WHERE pc.companytype NOT IN ('V01', 'V02')
			ORDER BY dp.plantid, dp.municipalityno2007
	    )
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mstjupiter.hydro_permit_not_vv IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' Simak Table containing permit for pumping groundwater (not alm vv) the Jupiter database'
			     || '''';
		END
	$do$
	;
	
	GRANT SELECT ON ALL TABLES IN SCHEMA jupiter TO grukosreader;
	
	GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO grukosreader;

COMMIT;