/*
    Mainly meta-tables - helper tables - lookup tables for mstjupiter
*/
drop table if exists mstjupiter.strat_grp;
create table mstjupiter.strat_grp (
  id serial primary key not null,
  chronocode text,
  chronolongtext text
);

insert into mstjupiter.strat_grp(chronocode, chronolongtext) values ('k', 'Kvartær');
insert into mstjupiter.strat_grp(chronocode, chronolongtext) values ('n', 'Neogen');
insert into mstjupiter.strat_grp(chronocode, chronolongtext) values ('p', 'Palæogen');
insert into mstjupiter.strat_grp(chronocode, chronolongtext) values ('pp', 'Prepalæogen');
insert into mstjupiter.strat_grp(chronocode, chronolongtext) values ('u', 'Ukendt');

