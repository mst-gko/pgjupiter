/*
 *  Test if any plant is set inactive, but still has new chemical analysis and reported water use.
*/

SELECT
 kl.longtext AS kommune,
 p.plantid||' - '||p.plantname AS anlaeg,
 vtno.companytype AS virksomhedstype,
 CASE WHEN p.participantvatno IS NOT NULL THEN 'https://datacvr.virk.dk/enhed/virksomhed/'||TRIM(TO_CHAR(p.participantvatno,'999999990')) END virk_dk,
 TO_CHAR(p.vrrpurpose)||' - '||ifl.longtext AS indvindingsformaal,
 TO_CHAR(COALESCE(MAX(cp.enddate),MAX(cp.startdate)),'yyyy') AS  tilladelse,
 TO_CHAR(COALESCE(MAX(pcs.sampledate),MAX(gcs.sampledate)),'yyyy') AS proever,
 TO_CHAR(COALESCE(MAX(wc.enddate),MAX(ic.enddate)),'yyyy') AS indberetning_m3,
-- TO_CHAR(MAX(lr.lastreported),'yyyy') AS indberetning_boringer,
 'https://data.geus.dk/JupiterWWW/anlaeg.jsp?anlaegid='||p.plantid AS www,
 CASE WHEN p.xutm32euref89+p.yutm32euref89>0 THEN 'POINT ('||TRIM(TO_CHAR(ROUND(p.xutm32euref89),'999999999990'))||' '||TRIM(TO_CHAR(ROUND(p.yutm32euref89),'999999999990'))||')' END AS wkt
FROM drwplant p
LEFT JOIN code_808 kl  /* Kommune https://data.geus.dk/tabellerkoder/koder.html?codetype=808 */
  ON p.municipalityno2007 = kl.code
LEFT JOIN code_741 ifl ON p.vrrpurpose = ifl.code /* Indvindingsformaal https://data.geus.dk/tabellerkoder/koder.html?codetype=741 */
LEFT JOIN drwplant sp
  ON p.plantid = sp.supplant
  AND (sp.active != 2 OR sp.active IS NULL)
JOIN (
 SELECT /* Virksomhedstype https://data.geus.dk/tabellerkoder/?tablename=DRWPLANTCOMPANYTYPE */
   vt.plantid, listagg(vt.companytype||' - '||vl.longtext,', ') WITHIN GROUP (ORDER BY vt.companytype) AS companytype
 FROM drwplantcompanytype vt
 JOIN code_852 vl ON vt.companytype = vl.code /* Virksomhedstype https://data.geus.dk/tabellerkoder/koder.html?codetype=852 */
 WHERE vt.companytype IN ('V01','V02','M42') /* Stort set ingen V03-V07 og V95 har tilladelser, proever eller indberetninger */
 GROUP BY vt.plantid) vtno ON p.plantid = vtno.plantid
LEFT JOIN catchperm cp /* Indvindingstilladelse https://data.geus.dk/tabellerkoder/?tablename=CATCHPERM */
  ON p.plantid = cp.plantid
 AND (cp.enddate IS NULL OR cp.enddate > SYSDATE-365*10)
 AND (cp.revoked IS NULL OR cp.revoked != 1)
 /* AND cp.companytype IS NOT NULL */ /* TODO: Kriterie boer undersoeges naermere */
 AND cp.amountperyear IS NOT NULL
LEFT JOIN wrrcatchment wc
  ON p.plantid = wc.plantid
  /* Indberetning skal ske foerste kvartal */
 AND (wc.enddate IS NULL OR wc.enddate > SYSDATE-365*10)
--LEFT JOIN drwplant_lastreported lr /* TODO: Ikke oprettet officielt i PCJupiterXL endnu */
--  ON p.plantid = lr.plantid
LEFT JOIN pltchemsample pcs
  ON p.plantid = pcs.plantid
  AND (pcs.sampledate IS NULL OR pcs.sampledate > SYSDATE-365)
LEFT JOIN drwplantintake ia /* Indtag-anlaeg-kobling https://data.geus.dk/tabellerkoder/?tablename=DRWPLANTINTAKE */
  ON ia.plantid = p.plantid
  AND (ia.startdate <= sysdate OR ia.startdate IS NULL)
  AND (ia.enddate >= sysdate OR ia.enddate IS NULL)
LEFT JOIN grwchemsample gcs
  ON ia.boreholeid = gcs.boreholeid
  AND (gcs.sampledate IS NULL OR gcs.sampledate > SYSDATE-365)
LEFT JOIN intakecatchment ic
  ON p.plantid = ic.intakeplantid
  /* Indberetning skal ske foerste kvartal */
 AND (ic.enddate IS NULL OR ic.enddate > SYSDATE-365*10)
/* andre tabeller med anlaegsoplysninger */
/* wrrexport - ikke brugbar her */
/* wrrsupply */
/* frequencymodification - aendringer af analysefrekvens (med ref til lovgivning CODE_359:ANALYSEFREKVENSLOV BEK 802 af 2016-06-01 + BEK 1070 af 2019-10-28) */
/* plant_injunction med ref til lovgivning CODE_357:ANLAEGSLOV $62, stk. 1 */
/* pltairsample - ingen raekker endnu */
/* wrrfeebill 2006-2011 */
/* drwplantlandreg anlaeggets matrikel 2006-2011 */
WHERE p.vrrpurpose IN (1,2,3,5,7) /* Der skal være tale om indvinding i drikkevandskvalitet */
  AND sp.supplant IS NULL /* Udelad overanlaeg - boringer og tilladelser sidder normalt på underanlæggene */
  AND p.active = 2 /* Kun aktive anlaeg */
  AND p.region IS NOT NULL /* Udelad groenlandske anlaeg */
  /* AND p.plantid = 72878 /* DEBUG */
GROUP BY p.municipalityno2007, kl.longtext, p.plantid, p.plantname, vtno.companytype, p.participantvatno, p.vrrpurpose, ifl.longtext, p.xutm32euref89, p.yutm32euref89
HAVING
      /* Indikationer på aktivt anlæg */
    (COALESCE(MAX(wc.enddate),MAX(ic.enddate)) > SYSDATE-365-90 AND COALESCE(MAX(wc.plantid),MAX(ic.intakeplantid)) IS NOT NULL)  /* Indberetning for sidste år (periode er altid et år bagud + 90 dages indberetningsfrist */
    OR (COALESCE(MAX(pcs.sampledate),MAX(gcs.sampledate)) > SYSDATE-365) /* Prøver det sidste år */
ORDER BY kl.longtext, p.plantid DESC
