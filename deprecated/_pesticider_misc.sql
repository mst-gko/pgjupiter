/*
  Tæl antal af hver type i attributfeltet i grwchemanalysis
*/
WITH countcode AS (
  SELECT count(*) antal, attribute FROM jupiter.mstvw_pesticide_raw GROUP BY attribute
  --SELECT count(*) antal, attribute FROM jupiter.grwchemanalysis GROUP BY attribute
)
SELECT c.code, c.longtext, cc.antal
FROM jupiter.code c
INNER JOIN countcode cc on c.code = cc.attribute
WHERE c.codetype = 754;


/*
  GRUMO boringer - vanddatering
*/
DROP VIEW IF EXISTS jupiter.grw_alder;
CREATE VIEW jupiter.grw_alder AS (
    SELECT
      row_number() OVER (ORDER BY b.boreholeno) AS id,
      b.boreholeno,
      b.geom,
      g.cfc_aar alder
    FROM jupiter.borehole b
    RIGHT JOIN kort.grumo_grw_alder g ON g.dgu = b.boreholeno
    WHERE g.cfc_aar IS NOT NULL
);

UPDATE kort.grumo_grw_alder set cfc_aar = null where cfc_aar = '';
