BEGIN;
/*
  
*/

DROP VIEW IF EXISTS jupiter.mstvw_Chlorthalonilamid_GVK_proeve CASCADE;

CREATE OR REPLACE VIEW jupiter.mstvw_Chlorthalonilamid_GVK_proeve AS (
	SELECT 	
		ROW_NUMBER() over (ORDER BY gcs.boreholeno) AS ID,
		gcs.boreholeno,
		gcs.intakeno,
   		gca.sampleid,
    	gcs.sampledate,
    	cpl.long_text AS stof,
    	gca."attribute",
		gca.amount,
    	gca.compoundno,
    	cpl.casno,
    	b.abandondat,
    	b.geom,
    	pl.plantid,
    	pl.plantname,
		pl.planttype,
		pl.companytype,
		gcs.project
	FROM jupiter.grwchemanalysis gca
	INNER JOIN jupiter.grwchemsample gcs USING (sampleid) 
	INNER JOIN jupiter.compoundlist cpl USING (compoundno)
	INNER JOIN jupiter.borehole b USING (boreholeid)
	INNER JOIN (
		SELECT DISTINCT dpi.boreholeid, dp.plantid, dp.plantname, dp.planttype, dpct.companytype
		FROM jupiter.drwplant dp
		INNER JOIN jupiter.drwplantcompanytype dpct USING (plantid)
		INNER JOIN jupiter.drwplantintake dpi USING (plantid)
		ORDER BY dp.plantid
		) pl ON gcs.boreholeid = pl.boreholeid
	WHERE gca.compoundno = ANY (ARRAY[4945::NUMERIC, 4989::NUMERIC, 4990::NUMERIC, 4961::NUMERIC])
		-- AND dpct.companytype = ANY (ARRAY['V01'::TEXT, 'V02'::TEXT, 'V03'::TEXT])
	ORDER BY pl.plantname, gcs.boreholeno, gcs.sampledate DESC
);

DROP MATERIALIZED VIEW IF EXISTS jupiter.mstmvw_Chlorthalonilamid_GVK_proeve;

CREATE MATERIALIZED VIEW jupiter.mstmvw_Chlorthalonilamid_GVK_proeve AS (
    SELECT * FROM jupiter.mstvw_Chlorthalonilamid_GVK_proeve
);

GRANT SELECT ON ALL TABLES IN SCHEMA jupiter TO jupiterrole;

COMMIT;



/*
    Dimethachlor metabolit CGA 373464 - in Jupiter
    Jakob Lanstorp, GKO 9/25/2020
*/
SELECT
    ROW_NUMBER() over (ORDER BY gcs.boreholeno) AS ID,
    gcs.boreholeno,
    gcs.intakeno,
    gca.sampleid,
    gcs.sampledate,
    cpl.long_text AS stof,
    gca."attribute",
    gca.amount,
    gca.compoundno,
    cpl.casno,
    b.abandondat,
    b.geom,
    gcs.project
FROM jupiter.grwchemanalysis gca
INNER JOIN jupiter.grwchemsample gcs USING (sampleid)
INNER JOIN jupiter.compoundlist cpl USING (compoundno)
INNER JOIN jupiter.borehole b USING (boreholeid)
WHERE gca.compoundno = 4942 --and long_text = 'Dimethachlor Metabolite CGA 373464';
-- 1239

SELECT
    ROW_NUMBER() over () AS id,
    gca.sampleid,
    gca."attribute",
    gca.amount,
    gca.compoundno
FROM jupiter.grwchemanalysis gca
WHERE gca.compoundno in (4904, 4905, 3562, 4756, 4755, 4978);
--1239, joins leaves nothing out