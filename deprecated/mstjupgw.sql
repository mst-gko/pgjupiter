/*
    Make a FDW to GEUS PCJupiterXL SQL Gateway
*/
set search_path to public, jupiter_geusgateway_fdw;

--select * from pg_extension;
--select * from pg_available_extensions;

/*
-- PostgreSQL-PostgreSQL FDW
DROP SERVER IF EXISTS pcjupiterxl_gateway;
CREATE SERVER pcjupiterxl_gateway
    FOREIGN DATA WRAPPER postgres_fdw
    OPTIONS (host '195.41.77.199', dbname 'postgres', port '5678');

CREATE USER MAPPING FOR grukosadmin
    SERVER pcjupiterxl_gateway
    OPTIONS (user 'jakla', password 'blyanthesttegning');

IMPORT FOREIGN SCHEMA pcjupiterXL
    FROM SERVER pcjupiterxl_gateway INTO jupiter_geusgateway_fdw;
*/

create table mstjupiter_geus_gateway.exporttime as
    select * from jupiter_geusgateway_fdw.exporttime;

create table mstjupiter_geus_gateway.grwchemanalysis as
    select * from jupiter_geusgateway_fdw.grwchemanalysis;
