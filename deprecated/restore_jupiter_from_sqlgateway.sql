/*
 * Owner: 	Danish Environmental Protection Agency (EPA) 
 * Descr: 	This sql script updates MST-GKOs pcjupiterxl postgresql database
 * 			via GEUS' SQL-gateway where existing tables are updated using 
 * 			select statements (upserts) executed on a foreign data wrapper. 
 * 			The script is overall optimized toward lowering the execution time
 * 			of the queries from the geus foreign wrapper.
 * 			This means for the large tables, the uuids are translated prior to updating 
 * 			such that the fdw queries does not have to translate bytea into uuid. This is done
 * 			to lower the queries time of execution on the fdw server due to 
 * 			timeout errors.
 * 			The script consist of 3 steps: 
 * 				1) Staging the tables for the update
 * 				2) Updating tables from geus fdw
 * 				3) Preparing the tables for daily use
 * ERT: 	~60 min 
 * Author: 	Simon Makwarth <simak@mst.dk>
 * Created: 2022-02-22
 * Updated: 2022-06-22
 * License:	GNU GPL v3
 */

/* 
 * Staging the tables for the update:
 * 	1) create schemas if not exists
 * 	2) create empty tables if not exists with correct columnnames and datatype 
 * 	3) drop and create all pks to make sure they exist
 * 	4) drop all fk and index to improve upsert performance
 */

BEGIN;

	-- schema for tables
	CREATE SCHEMA IF NOT EXISTS jupiter;

	-- codetype 
	CREATE TABLE IF NOT EXISTS jupiter.codetype AS
		(
			SELECT 
				codetype,
				shorttext,
				longtext,
				lookuptype,
				lookuptabl,
				insertdate,
				updatedate
			FROM geus_fdw.codetype
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.codetype 
		DROP CONSTRAINT IF EXISTS codetype_pkey CASCADE
	;

	ALTER TABLE jupiter.codetype 
		ADD PRIMARY KEY (codetype)
	;

	-- code
	CREATE TABLE IF NOT EXISTS jupiter.code AS
		(
			SELECT 
				code,
				codetype,
				shorttext,
				longtext,
				sortno,
				insertdate,
				updatedate
			FROM geus_fdw.code
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.code 
		DROP CONSTRAINT IF EXISTS code_pkey CASCADE
	;

	ALTER TABLE jupiter.code 
		ADD PRIMARY KEY (code, codetype)
	;

	ALTER TABLE jupiter.code 
		DROP CONSTRAINT IF EXISTS fk_codetype_code CASCADE
	;

	DROP INDEX IF EXISTS code_code_idx;

	DROP INDEX IF EXISTS code_codetype_idx;

	-- municipality2007
	CREATE TABLE IF NOT EXISTS jupiter.municipality2007 AS
		(
			SELECT 
				municipalityno2007,
				envcen,
				region,
				"name",
				xutmmin,
				yutmmin,
				xutmmax,
				yutmmax,
				www,
				insertdate,
				updatedate
			FROM geus_fdw.municipality2007
			LIMIT 0
		)
	;
	
	ALTER TABLE jupiter.municipality2007 
		DROP CONSTRAINT IF EXISTS municipality2007_pkey CASCADE
	;

	ALTER TABLE jupiter.municipality2007 
		ADD PRIMARY KEY (municipalityno2007)
	;

	-- borehole
	CREATE TABLE IF NOT EXISTS jupiter.borehole AS 
		(
			SELECT 
				boreholeid,
				boreholeno,
				namingsys,
				purpose,
				use,
				status,
				drilldepth,
				elevation,
				ctrpeleva,
				verticaref,
				ctrpdescr,
				ctrpprecis,
				ctrpzprecis,
				ctrpheight,
				elevametho,
				elevaquali,
				elevasourc,
				"location",
				"comments",
				various,
				xutm,
				yutm,
				utmzone,
				datum,
				mapsheet,
				mapdistx,
				mapdisty,
				sys34x,
				sys34y,
				sys34zone,
				latitude,
				longitude,
				locatmetho,
				locatquali,
				locatsourc,
				borhpostc,
				borhtownno,
				countyno,
				municipal,
				houseownas,
				landregno,
				driller,
				drilllogno,
				drillborno,
				reportedby,
				consultant,
				consulogno,
				consuborno,
				drilledfor,
				drforadres,
				drforpostc,
				drilstdate,
				drilendate,
				abandondat,
				prevborhno,
				numsuplbor,
				samrecedat,
				samdescdat,
				numofsampl,
				numsamsto,
				litholnote,
				togeusdate,
				grumocountyno,
				grumoborno,
				grumobortype,
				grumoareano,
				borhtownno2007,
				locquali,
				loopareano,
				loopstation,
				looptype,
				usechangedate,
				envcen,
				abandcause,
				abandcontr,
				startdayunknown,
				startmnthunknwn,
				wwboreholeno,
				xutm32euref89,
				yutm32euref89,
				zdvr90,
				installation,
				workingconditions,
				approach,
				accessremark,
				locatepersonemail,
				preservationzone,
				protectionzone,
				region,
				usechangecause,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				dataowner,
				ST_SetSRID(st_makepoint(xutm32euref89, yutm32euref89), 25832) AS geom
			FROM geus_fdw.borehole b
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.borehole 
		DROP CONSTRAINT IF EXISTS borehole_pkey CASCADE
	;

	ALTER TABLE jupiter.borehole 
		DROP CONSTRAINT IF EXISTS boreholeid_unique CASCADE
	;

	ALTER TABLE jupiter.borehole 
		ADD PRIMARY KEY (guid)
	;

	DROP INDEX IF EXISTS borehole_geom_idx;

	DROP INDEX IF EXISTS borehole_boreholeid_idx;
	
	DROP INDEX IF EXISTS borehole_boreholeno_idx;

	-- drilmeth
	CREATE TABLE IF NOT EXISTS jupiter.drilmeth AS
		(
			SELECT 
				boreholeid,
				methodid,
				boreholeno,
				methodno,
				top,
				bottom,
				"method",
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.drilmeth dm
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.drilmeth 
		DROP CONSTRAINT IF EXISTS drilmeth_pkey CASCADE
	;

	ALTER TABLE jupiter.drilmeth 
		ADD PRIMARY KEY (guid)
	;

	ALTER TABLE jupiter.drilmeth 
		DROP CONSTRAINT IF EXISTS fk_borehole_drilmeth CASCADE
	;

	-- intake
	CREATE TABLE IF NOT EXISTS jupiter.intake AS
		(
			SELECT 
				boreholeid,
				intakeid,
				boreholeno,
				intakeno,
				stringno,
				waterage,
				depositno,
				deposittype,
				mainclass,
				monitoringtype,
				reservoirrock,
				reservoirtype,
				specialusable,
				watertabletype,
				soundability,
				soundabilityremark,
				soundtubeinsidediam,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.intake i
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.intake 
		DROP CONSTRAINT IF EXISTS intake_pkey CASCADE
	;

	ALTER TABLE jupiter.intake 
		ADD PRIMARY KEY (guid)
	;

	ALTER TABLE jupiter.intake 
		DROP CONSTRAINT IF EXISTS fk_borehole_intake CASCADE
	;

	DROP INDEX IF EXISTS intake_intakeno_idx;
	
	DROP INDEX IF EXISTS intake_guid_borehole_idx;

	DROP INDEX IF EXISTS intake_intakeid_idx;

	-- screen
	CREATE TABLE IF NOT EXISTS jupiter.screen AS
		(
			SELECT 
				boreholeid,
				intakeid,
				screenid,
				boreholeno,
				screenno,
				intakeno,
				top,
				bottom,
				"diameter",
				unit,
				diametermm,
				material,
				strength,
				slotopenin,
				startdate,
				enddate,
				wallthickn,
				fitting,
				topbotquali,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_intake, 'hex') AS UUID) AS guid_intake,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.screen s 
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.screen 
		DROP CONSTRAINT IF EXISTS screen_pkey CASCADE
	;

	ALTER TABLE jupiter.screen 
		ADD PRIMARY KEY (guid)
	;

	ALTER TABLE jupiter.screen 
		DROP CONSTRAINT IF EXISTS fk_intake_screen CASCADE
	;

	ALTER TABLE jupiter.screen 
		DROP CONSTRAINT IF EXISTS fk_borehole_screen CASCADE
	;

	DROP INDEX IF EXISTS screen_screenno_idx;
	
	DROP INDEX IF EXISTS screen_screenid_idx;

	-- drwplant
	CREATE TABLE IF NOT EXISTS jupiter.drwplant AS
		(
			SELECT 
				plantid,
				municipalityno2007,
				envcen,
				region,
				administratorid,
				active,
				planttype,
				permit,
				municipalityno,
				planttypeno,
				serialno,
				subno,
				countyjournalno,
				municipalityno2,
				utmzone,
				datum,
				xutm,
				yutm,
				plantname,
				plantaddress,
				plantpostalcode,
				permitdate,
				permitamount,
				permitexpiredate,
				watertype,
				"owner",
				vrrpurpose,
				reportingcounty,
				companyserialno,
				dischargeto,
				supplant,
				locatremark,
				startdate,
				enddate,
				verticaref,
				gridtype,
				locatmetho,
				elevametho,
				areaha,
				xutm32euref89,
				yutm32euref89,
				propertyno,
				feeduty,
				feedutyamount,
				feeaddressid,
				dataowner,
				ctrlmunicipalno,
				"method",
				conversionfactor,
				www,
				participantvatno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwfirm, 'hex') AS UUID) AS guid_drwfirm,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				ST_SetSRID(st_makepoint(xutm32euref89, yutm32euref89),25832) AS geom
			FROM geus_fdw.drwplant dp
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.drwplant 
		DROP CONSTRAINT IF EXISTS drwplant_pkey CASCADE
	;

	ALTER TABLE jupiter.drwplant 
		DROP CONSTRAINT IF EXISTS fk_municipality CASCADE
	;

	ALTER TABLE jupiter.drwplant 
		ADD PRIMARY KEY (guid)
	;

	DROP INDEX IF EXISTS drwplant_geom_idx;

	DROP INDEX IF EXISTS drwplant_plantid_idx;

	DROP INDEX IF EXISTS drwplant_plantname_idx;

	-- drwplantintake
	CREATE TABLE IF NOT EXISTS jupiter.drwplantintake AS
		(
			SELECT 
				intakeplantid,
				boreholeid,
				intakeid,
				plantid,
				boreholeno,
				intakeno,
				startdate,
				enddate,
				intakeusage,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				CAST(ENCODE(guid_intake, 'hex') AS UUID) AS guid_intake,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.drwplantintake dpi
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.drwplantintake 
		DROP CONSTRAINT IF EXISTS drwplantintake_pkey CASCADE
	;

	ALTER TABLE jupiter.drwplantintake 
		ADD PRIMARY KEY (guid)
	;

	ALTER TABLE jupiter.drwplantintake 
		DROP CONSTRAINT IF EXISTS fk_intake_dpi CASCADE
	;

	ALTER TABLE jupiter.drwplantintake 
		DROP CONSTRAINT IF EXISTS fk_plant_dpi CASCADE
	;

	ALTER TABLE jupiter.drwplantintake 
		DROP CONSTRAINT IF EXISTS fk_borehole_dpi CASCADE
	;

	DROP INDEX IF EXISTS drwplantintake_boreholeid_idx;
	
	DROP INDEX IF EXISTS drwplantintake_boreholeno_idx;

	-- compoundgrouplist
	CREATE TABLE IF NOT EXISTS jupiter.compoundgrouplist AS
		(
			SELECT 
				compoundgroupno,
				longtext,
				shorttext,
				insertdate,
				updatedate
			FROM geus_fdw.compoundgrouplist
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.compoundgrouplist 
		DROP CONSTRAINT IF EXISTS compoundgrouplist_pkey CASCADE
	;

	ALTER TABLE jupiter.compoundgrouplist 
		ADD PRIMARY KEY (compoundgroupno)
	;

	-- compoundlist
	CREATE TABLE IF NOT EXISTS jupiter.compoundlist AS
		(
			SELECT 
				compoundno,
				long_text,
				short_text,
				sortno,
				casno,
				euno,
				limitationdate,
				molarweight,
				charge,
				remark,
				drwunit,
				grwunit,
				insertdate,
				updatedate
			FROM geus_fdw.compoundlist
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.compoundlist 
		DROP CONSTRAINT IF EXISTS compoundlist_pkey CASCADE
	;

	ALTER TABLE jupiter.compoundlist 
		ADD PRIMARY KEY (compoundno)
	;

	-- compoundgroup
	CREATE TABLE IF NOT EXISTS jupiter.compoundgroup AS
		(
			SELECT 
				compoundno,
				compoundgroupno,
				subcompoundgroupno,
				insertdate,
				updatedate,
				convtocompoundno,
				convfactor,
				stancode,
				stancodetype
			FROM geus_fdw.compoundgroup
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.compoundgroup 
		DROP CONSTRAINT IF EXISTS compoundgroup_pkey CASCADE
	;

	ALTER TABLE jupiter.compoundgroup 
		ADD PRIMARY KEY (compoundno)
	;

	ALTER TABLE jupiter.compoundgroup 
		DROP CONSTRAINT IF EXISTS fk_cpl_cpg CASCADE
	;

	ALTER TABLE jupiter.compoundgroup 
		DROP CONSTRAINT IF EXISTS fk_cpgl_cpg CASCADE
	;

	-- grwchemsample
	CREATE TABLE IF NOT EXISTS jupiter.grwchemsample AS 
		(
			SELECT 
				sampleid,									
				boreholeid,
				intakeid,
				outtakeid,
				boreholeno,
				intakeno,
				outtakeno,
				top,
				bottom,
				sampledate,
				project,
				laboratory,
				laboratoriyrefno,
				purpose,
				extent,
				cause,
				sourcetype,
				watertype,
				remark,
				samplelocality,
				sumcationscalculated,
				sumanoinscalculated,
				preprocessing,
				sampledby,
				client,
				clientname,
				samplereportdate,
				validatedby,
				cleaningstart,
				cleaningend,
				cleaningyield,
				partofsampleid,
				amount,
				resamplestatus,
				unit,
				previoussampleid,
				qualitycontrol,
				labreferencename,
				projectphase,
				laboratoryreceiveddate,
				samplingfirm,
				samplingequipment,
				samplestatus,
				samplequalitymark,
				samplestatususer,
				samplestatusdate,
				dataowner,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_intake, 'hex') AS UUID) AS guid_intake,
				CAST(ENCODE(guid_outtake, 'hex') AS UUID) AS guid_outtake,
				CAST(ENCODE(guid_outtake, 'hex') AS UUID) AS guid_outtake,
				CAST(ENCODE(guid_projectphase, 'hex') AS UUID) AS guid_projectphase,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				samplenewdate,
				laboratory_desc
			FROM geus_fdw.grwchemsample t2
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.grwchemsample 
		DROP CONSTRAINT IF EXISTS grwchemsample_pkey CASCADE
	;

	ALTER TABLE jupiter.grwchemsample 
		ADD PRIMARY KEY (guid)
	;

	ALTER TABLE jupiter.grwchemsample 
		DROP CONSTRAINT IF EXISTS fk_intake_gcs CASCADE
	;

	ALTER TABLE jupiter.grwchemsample 
		DROP CONSTRAINT IF EXISTS fk_borehole_gcs CASCADE
	;

	DROP INDEX IF EXISTS grwchemsample_sampleid_idx;

	DROP INDEX IF EXISTS grwchemsample_boreholeid_idx;

	-- pltchemsample
	CREATE TABLE IF NOT EXISTS jupiter.pltchemsample AS
		(
			SELECT 
				sampleid,					
				plantid,
				sampledate,
				samplesite,
				reportdate,
				journalno,
				laboratory,
				datasource,
				remark,
				extent,
				extentold,
				evaluationlaboratory,
				evaluationsite,
				evaluationjournalno,
				purpose,
				odeur,
				taste,
				look,
				colour,
				sampledby,
				measuringsiteno,
				client,
				clientname,
				pipesite,
				pipeaddress,
				pipepostalcode,
				qualitycontrol,
				estimatetemp,
				labreferencename,
				project,
				volume,
				volumeunit,
				measuringstationid,
				resamplestatus,
				previoussample,
				projectphase,
				laboratoryreceiveddate,
				samplingfirm,
				samplestatus,
				samplequalitymark,
				samplestatususer,
				samplestatusdate,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				CAST(ENCODE(guid_measuringstation, 'hex') AS UUID) AS guid_measuringstation,
				CAST(ENCODE(guid_projectphase, 'hex') AS UUID) AS guid_projectphase,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				samplenewdate,
				laboratory_desc
			FROM geus_fdw.pltchemsample pcs
			LIMIT 0
		)
	;
	
	ALTER TABLE jupiter.pltchemsample 
		DROP CONSTRAINT IF EXISTS pltchemsample_pkey CASCADE
	;

	ALTER TABLE jupiter.pltchemsample 
		DROP CONSTRAINT IF EXISTS fk_drwplant_pcs CASCADE
	;

	ALTER TABLE jupiter.pltchemsample 
		ADD PRIMARY KEY (guid)
	;

	DROP INDEX IF EXISTS pltchemsample_sampleid_idx;

	DROP INDEX IF EXISTS pltchemsample_boreholeid_idx;

	-- grwchemanalysis
	CREATE TABLE IF NOT EXISTS jupiter.grwchemanalysis AS
		(
			SELECT 
				sampleid,
				compoundno,
				analysisno,
				analysissite,
				fieldfiltration,
				"attribute",
				amount,
				unit,
				reportedcompoundno,
				reportedunit,
				reportedamount,
				laboratory,
				laboratoryrefno,
				analysismethod,
				qualitycontrol,
				remark,
				preprocessing,
				preservation,
				packing,
				detectionlimit,
				analysisid,
				reporteddetectionlimit,
				analysisdate,
				laboratoryreceiveddate,
				absoluteexpmeasuncertainty,
				relativeexpmeasuncertainty,
				analysisresponsible,
				accreditedindicator,
				fractionationmethod,
				analysisresultidentifier,
				sortno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_grwchemsample, 'hex') AS UUID) AS guid_grwchemsample,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				compoundno_desc,
				unit_desc,
				analysissite_desc
			FROM geus_fdw.grwchemanalysis gcs
			LIMIT 0
		)
	;

	

	ALTER TABLE jupiter.grwchemanalysis 
		DROP CONSTRAINT IF EXISTS fk_gcs_gca CASCADE
	;
	
	ALTER TABLE jupiter.grwchemanalysis 
		DROP CONSTRAINT IF EXISTS fk_cpl_gca CASCADE
	;

	DROP INDEX IF EXISTS grwchemanalysis_sampleid_idx;

	DROP INDEX IF EXISTS grwchemanalysis_amount_idx;

	DROP INDEX IF EXISTS grwchemanalysis_compoundno_idx;

	-- pltchemanalysis
	CREATE TABLE IF NOT EXISTS jupiter.pltchemanalysis AS
		(
			SELECT 
				sampleid,
				compoundno,
				analysisno,
				qualitycontrol,
				amountreported,
				unitreported,
				compoundreported,
				amount,
				unit,
				"attribute",
				detectionlimit,
				filtering,
				analysissite,
				"method",
				laboratory,
				remark,
				journalno,
				preparation,
				packaging,
				preservation,
				analysisid,
				reporteddetectionlimit,
				analysisdate,
				laboratoryreceiveddate,
				absoluteexpmeasuncertainty,
				relativeexpmeasuncertainty,
				analysisresponsible,
				accreditedindicator,
				fractionationmethod,
				analysisresultidentifier,
				sortno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_pltchemsample, 'hex') AS UUID) AS guid_pltchemsample,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				compoundno_desc,
				unit_desc,
				analysissite_desc	
			FROM geus_fdw.pltchemanalysis
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.pltchemanalysis 
		DROP CONSTRAINT IF EXISTS pltchemanalysis_pkey CASCADE
	;

	ALTER TABLE jupiter.pltchemanalysis 
		ADD PRIMARY KEY (guid)
	;

	ALTER TABLE jupiter.pltchemanalysis 
		DROP CONSTRAINT IF EXISTS fk_pcs_pca CASCADE
	;

	ALTER TABLE jupiter.pltchemanalysis 
		DROP CONSTRAINT IF EXISTS fk_cpl_pca CASCADE
	;

	DROP INDEX IF EXISTS pltchemanalysis_sampleid_idx;
	
	DROP INDEX IF EXISTS pltchemanalysis_amount_idx;

	DROP INDEX IF EXISTS pltchemanalysis_compoundno_idx;

	-- lithsamp
	CREATE TABLE IF NOT EXISTS jupiter.lithsamp AS
		(
			SELECT 
				boreholeid,
				sampleid,
				boreholeno,
				sampleno,
				bagno,
				top,
				bottom,
				sampledep,
				sampletop,
				samplebottom,
				samplekept,
				depestim,
				drillcolor,
				drillrockt,
				drillcolsb,
				drilldescr,
				drillbagno,
				drillsampleref,
				drillremrk,
				rocktype,
				texture,
				color,
				"structure",
				hardness,
				cementatio,
				diagenesis,
				calcareous,
				classifica,
				trivialnam,
				rocksymbol,
				sorting,
				rounding,
				remarks,
				grainshape,
				minorcomps,
				analyses,
				minerals,
				fossils,
				oldcolor,
				munsellcolor,
				totaldescr,
				otherdescr,
				sampletype,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.lithsamp ls
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.lithsamp 
		DROP CONSTRAINT IF EXISTS lithsamp_pkey CASCADE
	;

	ALTER TABLE jupiter.lithsamp 
		ADD PRIMARY KEY (guid)
	;

	DROP INDEX IF EXISTS lithsamp_boreholeid_idx;

	DROP INDEX IF EXISTS lithsamp_rocksymbol_idx;

	DROP INDEX IF EXISTS lithsamp_top_idx;

	DROP INDEX IF EXISTS lithsamp_bottom_idx;

	ALTER TABLE jupiter.lithsamp 
		DROP CONSTRAINT IF EXISTS fk_borehole_lithsamp CASCADE
	;
	
	-- watlevel
	CREATE TABLE IF NOT EXISTS jupiter.watlevel AS
		(
			SELECT 
				t2.watlevelid,
				t2.boreholeid,
				t2.intakeid,
				t2.boreholeno,
				t2.watlevelno,
				t2.intakeno,
				t2.timeofmeas,
				t2.project,
				t2.waterlevel,
				t2.watlevgrsu,
				t2.watlevmsl,
				t2.watlevmp,
				t2.hoursnopum,
				t2.category,
				t2."method",
				t2.quality,
				t2.refpoint,
				t2.remark,
				t2.verticaref,
				t2.atmospresshpa,
				t2.extremes,
				t2.situation,
				t2.watlevelroundno,
				t2.qualitycontrol,
				CAST(ENCODE(t2.guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(t2.guid_intake, 'hex') AS UUID) AS guid_intake,
				CAST(ENCODE(t2.guid_watlevround, 'hex') AS UUID) AS guid_watlevround,
				t2.insertdate,
				t2.updatedate,
				t2.insertuser,
				t2.updateuser,
				t2.dataowner
			FROM geus_fdw.watlevel t2
			LIMIT 0
		)
	;

	

	ALTER TABLE jupiter.watlevel 
		DROP CONSTRAINT IF EXISTS fk_borehole_watlevel CASCADE
	;
	
	ALTER TABLE jupiter.watlevel 
		DROP CONSTRAINT IF EXISTS fk_intake_watlevel CASCADE
	;

	DROP INDEX IF EXISTS watlevel_boreholeid_idx CASCADE;

	-- catchperm
	CREATE TABLE IF NOT EXISTS jupiter.catchperm AS
		(
			SELECT 
				permissionid,
				permissiontype,
				catchpurpose,
				startdate,
				enddate,
				journalno,
				specialterms,
				remark,
				boreholemaxdepth,
				medianminwatflow,
				"source",
				companytype,
				watertype,
				municipalityno2007,
				amountperhour,
				amountperyear,
				plantid,
				revoked,
				revokeddate,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.catchperm cp
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.catchperm 
		DROP CONSTRAINT IF EXISTS catchperm_pkey CASCADE
	;

	ALTER TABLE jupiter.catchperm 
		ADD PRIMARY KEY (guid)
	;

	ALTER TABLE jupiter.catchperm 
		DROP CONSTRAINT IF EXISTS fk_drwplant_catchperm
	;

	DROP INDEX IF EXISTS catchperm_plantid_idx;

	-- wrrcatchment
	CREATE TABLE IF NOT EXISTS jupiter.wrrcatchment AS
		(
			SELECT 
				plantcatchmentid,
				plantid,
				catchmentno,
				startdate,
				enddate,
				"attribute",
				amount,
				"method",
				flowmeterstart,
				flowmeterend,
				remark,
				conversionfactor,
				surfacewatervolume,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.wrrcatchment wc
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.wrrcatchment 
		DROP CONSTRAINT IF EXISTS wrrcatchment_pkey CASCADE
	;

	ALTER TABLE jupiter.wrrcatchment 
		ADD PRIMARY KEY (guid)
	;
	
	ALTER TABLE jupiter.wrrcatchment 
		DROP CONSTRAINT IF EXISTS fk_drwplant_wrrcatchment
	;

	DROP INDEX IF EXISTS wrrcatchment_plantid_idx;

	-- drwplantcompanytype
	CREATE TABLE IF NOT EXISTS jupiter.drwplantcompanytype AS
		(
			SELECT 
				plantid,
				companytype,
				companytypeno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				insertdate,
				insertuser,
				updatedate,
				updateuser
			FROM geus_fdw.drwplantcompanytype dpct
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.drwplantcompanytype 
		DROP CONSTRAINT IF EXISTS drwplantcompanytype_pkey CASCADE
	;

	ALTER TABLE jupiter.drwplantcompanytype 
		ADD PRIMARY KEY (guid)
	;

	ALTER TABLE jupiter.drwplantcompanytype 
		DROP CONSTRAINT IF EXISTS fk_drwplant_drwplantcompanytype
	;
	
	DROP INDEX IF EXISTS drwplantcompanytype_plantid_idx;

	-- storedoc
	CREATE TABLE IF NOT EXISTS jupiter.storedoc AS 
		(
			SELECT 
				fileid,
				filetype,
				url,
				"comments",
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.storedoc sd
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.storedoc 
		DROP CONSTRAINT IF EXISTS storedoc_pkey CASCADE
	;
	
	ALTER TABLE jupiter.storedoc 
		ADD PRIMARY KEY (fileid)
	;

	-- boredoc
	CREATE TABLE IF NOT EXISTS jupiter.boredoc AS 
		(
			SELECT 
				fileid,
				boreholeid,
				boreholeno,
				doctype,
				versionno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.boredoc bd
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.boredoc 
		DROP CONSTRAINT IF EXISTS boredoc_pkey CASCADE
	;
	
	ALTER TABLE jupiter.boredoc 
		ADD PRIMARY KEY (guid)
	;

	ALTER TABLE jupiter.boredoc 
		DROP CONSTRAINT IF EXISTS fk_borehole_boredoc
	;

	ALTER TABLE jupiter.boredoc 
		DROP CONSTRAINT IF EXISTS fk_storedoc_boredoc
	;

	DROP INDEX IF EXISTS boredoc_fileid_idx;

	DROP INDEX IF EXISTS boredoc_boreholeid_idx;

	/*
	 * Updating tables from geus fdw:
	 * 	1) execute upserts from geus fdw
	 * 	2) create comments on schema and tables maked with timestamp
	 */

	-- schema
	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON SCHEMA jupiter IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl schema'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;
	
	-- codetype 
	INSERT INTO jupiter.codetype AS t1
		(
			codetype,
			shorttext,
			longtext,
			lookuptype,
			lookuptabl,
			insertdate,
			updatedate
		)
		(
			SELECT 
				codetype,
				shorttext,
				longtext,
				lookuptype,
				lookuptabl,
				insertdate,
				updatedate
			FROM geus_fdw.codetype t2
		) 
		ON CONFLICT (codetype) --ON CONSTRAINT codetype_pkey 
			DO UPDATE SET 
				codetype = excluded.codetype,
				shorttext = excluded.shorttext,
				longtext = excluded.longtext,
				lookuptype = excluded.lookuptype,
				lookuptabl = excluded.lookuptabl,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;
	
	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.codetype IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl codetype'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;
	
	-- code 
	INSERT INTO jupiter.code AS t1
		(
			code,
			codetype,
			shorttext,
			longtext,
			sortno,
			insertdate,
			updatedate
		)
		(
			SELECT 
				code,
				codetype,
				shorttext,
				longtext,
				sortno,
				insertdate,
				updatedate
			FROM geus_fdw.code t2
		) 
		ON CONFLICT (code, codetype) --ON CONSTRAINT code_pkey 
			DO UPDATE SET 
				code = excluded.code,
				codetype = excluded.codetype,
				shorttext = excluded.shorttext,
				longtext = excluded.longtext,
				sortno = excluded.sortno,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.code IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl code'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;
	
	-- municipality2007
	INSERT INTO jupiter.municipality2007 AS t1
		(
			municipalityno2007,
			envcen,
			region,
			"name",
			xutmmin,
			yutmmin,
			xutmmax,
			yutmmax,
			www,
			insertdate,
			updatedate
		)
		(
			SELECT 
				municipalityno2007,
				envcen,
				region,
				"name",
				xutmmin,
				yutmmin,
				xutmmax,
				yutmmax,
				www,
				insertdate,
				updatedate
			FROM geus_fdw.municipality2007 t2
		) 
		ON CONFLICT (municipalityno2007) --ON CONSTRAINT municipality2007_pkey 
			DO UPDATE SET 
				municipalityno2007 = excluded.municipalityno2007,
				envcen = excluded.envcen,  
				region = excluded.region,
				"name" = excluded."name",
				xutmmin = excluded.xutmmin,
				yutmmin = excluded.yutmmin,
				xutmmax = excluded.xutmmax,
				yutmmax = excluded.yutmmax,
				www = excluded.www,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.municipality2007 IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl municipality2007'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- borehole
	INSERT INTO jupiter.borehole AS t1
		(
			SELECT 
				boreholeid,
				boreholeno,
				namingsys,
				purpose,
				use,
				status,
				drilldepth,
				elevation,
				ctrpeleva,
				verticaref,
				ctrpdescr,
				ctrpprecis,
				ctrpzprecis,
				ctrpheight,
				elevametho,
				elevaquali,
				elevasourc,
				"location",
				"comments",
				various,
				xutm,
				yutm,
				utmzone,
				datum,
				mapsheet,
				mapdistx,
				mapdisty,
				sys34x,
				sys34y,
				sys34zone,
				latitude,
				longitude,
				locatmetho,
				locatquali,
				locatsourc,
				borhpostc,
				borhtownno,
				countyno,
				municipal,
				houseownas,
				landregno,
				driller,
				drilllogno,
				drillborno,
				reportedby,
				consultant,
				consulogno,
				consuborno,
				drilledfor,
				drforadres,
				drforpostc,
				drilstdate,
				drilendate,
				abandondat,
				prevborhno,
				numsuplbor,
				samrecedat,
				samdescdat,
				numofsampl,
				numsamsto,
				litholnote,
				togeusdate,
				grumocountyno,
				grumoborno,
				grumobortype,
				grumoareano,
				borhtownno2007,
				locquali,
				loopareano,
				loopstation,
				looptype,
				usechangedate,
				envcen,
				abandcause,
				abandcontr,
				startdayunknown,
				startmnthunknwn,
				wwboreholeno,
				xutm32euref89,
				yutm32euref89,
				zdvr90,
				installation,
				workingconditions,
				approach,
				accessremark,
				locatepersonemail,
				preservationzone,
				protectionzone,
				region,
				usechangecause,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				dataowner,
				ST_SetSRID(st_makepoint(xutm32euref89, yutm32euref89), 25832) AS geom
			FROM geus_fdw.borehole t2
		) 
		ON CONFLICT (guid) --ON CONSTRAINT borehole_pkey 
			DO UPDATE SET 
				status = excluded.status,
				drilldepth = excluded.drilldepth,
				elevation = excluded.elevation,
				ctrpeleva = excluded.ctrpeleva,
				verticaref = excluded.verticaref,
				ctrpdescr = excluded.ctrpdescr,
				ctrpprecis = excluded.ctrpprecis,
				ctrpzprecis = excluded.ctrpzprecis,
				ctrpheight = excluded.ctrpheight,
				elevametho = excluded.elevametho,
				elevaquali = excluded.elevaquali,
				elevasourc = excluded.elevasourc,
				"location" = excluded."location",
				"comments" = excluded."comments",
				various = excluded.various,
				xutm = excluded.xutm,
				yutm = excluded.yutm,
				utmzone = excluded.utmzone,
				datum = excluded.datum,
				mapsheet = excluded.mapsheet,
				mapdistx = excluded.mapdistx,
				mapdisty = excluded.mapdisty,
				sys34x = excluded.sys34x,
				sys34y = excluded.sys34y,
				sys34zone = excluded.sys34zone,
				latitude = excluded.latitude,
				longitude = excluded.longitude,
				locatmetho = excluded.locatmetho,
				locatquali = excluded.locatquali,
				locatsourc = excluded.locatsourc,
				borhpostc = excluded.borhpostc,
				borhtownno = excluded.borhtownno,
				countyno = excluded.countyno,
				municipal = excluded.municipal,
				houseownas = excluded.houseownas,
				landregno = excluded.landregno,
				driller = excluded.driller,
				drilllogno = excluded.drilllogno,
				drillborno = excluded.drillborno,
				reportedby = excluded.reportedby,
				consultant = excluded.consultant,
				consulogno = excluded.consulogno,
				consuborno = excluded.consuborno,
				drilledfor = excluded.drilledfor,
				drforadres = excluded.drforadres,
				drforpostc = excluded.drforpostc,
				drilstdate = excluded.drilstdate,
				drilendate = excluded.drilendate,
				abandondat = excluded.abandondat,
				prevborhno = excluded.prevborhno,
				numsuplbor = excluded.numsuplbor,
				samrecedat = excluded.samrecedat,
				samdescdat = excluded.samdescdat,
				numofsampl = excluded.numofsampl,
				numsamsto = excluded.numsamsto,
				litholnote = excluded.litholnote,
				togeusdate = excluded.togeusdate,
				grumocountyno = excluded.grumocountyno,
				grumoborno = excluded.grumoborno,
				grumobortype = excluded.grumobortype,
				grumoareano = excluded.grumoareano,
				borhtownno2007 = excluded.borhtownno2007,
				locquali = excluded.locquali,
				loopareano = excluded.loopareano,
				loopstation = excluded.loopstation,
				looptype = excluded.looptype,
				usechangedate = excluded.usechangedate,
				envcen = excluded.envcen,
				abandcause = excluded.abandcause,
				abandcontr = excluded.abandcontr,
				startdayunknown = excluded.startdayunknown,
				startmnthunknwn = excluded.startmnthunknwn,
				wwboreholeno = excluded.wwboreholeno,
				xutm32euref89 = excluded.xutm32euref89,
				yutm32euref89 = excluded.yutm32euref89,
				zdvr90 = excluded.zdvr90,
				installation = excluded.installation,
				workingconditions = excluded.workingconditions,
				approach = excluded.approach,
				accessremark = excluded.accessremark,
				locatepersonemail = excluded.locatepersonemail,
				preservationzone = excluded.preservationzone,
				protectionzone = excluded.protectionzone,
				region = excluded.region,
				usechangecause = excluded.usechangecause,	
				guid = excluded.guid,			
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate,
				insertuser = excluded.insertuser,
				updateuser = excluded.updateuser,
				dataowner  = excluded.dataowner,
				geom = excluded.geom 
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;	

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.borehole IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl borehole'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	ALTER TABLE jupiter.borehole
		ADD CONSTRAINT boreholeid_unique UNIQUE (boreholeid)
	;

	-- drilmeth
	INSERT INTO jupiter.drilmeth AS t1
		(
			SELECT 
				boreholeid,
				methodid,
				boreholeno,
				methodno,
				top,
				bottom,
				"method",
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.drilmeth t2
		) 
		ON CONFLICT (guid) --ON CONSTRAINT drilmeth_pkey 
			DO UPDATE SET  
				boreholeid = excluded.boreholeid,
				methodid = excluded.methodid,
				boreholeno = excluded.boreholeno,
				methodno = excluded.methodno,
				top = excluded.top,
				bottom = excluded.bottom,
				"method" = excluded."method",
				guid = excluded.guid,
				guid_borehole = excluded.guid_borehole,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate,
				insertuser = excluded.insertuser,
				updateuser = excluded.updateuser
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.drilmeth IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl drilmeth'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- intake
	INSERT INTO jupiter.intake AS t1
		(
			SELECT 
				boreholeid,
				intakeid,
				boreholeno,
				intakeno,
				stringno,
				waterage,
				depositno,
				deposittype,
				mainclass,
				monitoringtype,
				reservoirrock,
				reservoirtype,
				specialusable,
				watertabletype,
				soundability,
				soundabilityremark,
				soundtubeinsidediam,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.intake t2
		) 
		ON CONFLICT (guid) --ON CONSTRAINT intake_pkey 
			DO UPDATE SET  
				boreholeid = excluded.boreholeid,
				intakeid = excluded.intakeid,
				boreholeno = excluded.boreholeno,
				intakeno = excluded.intakeno,
				stringno = excluded.stringno,
				waterage = excluded.waterage,
				depositno = excluded.depositno,
				deposittype = excluded.deposittype,
				mainclass = excluded.mainclass,
				monitoringtype = excluded.monitoringtype,
				reservoirrock = excluded.reservoirrock,
				reservoirtype = excluded.reservoirtype,
				specialusable = excluded.specialusable,
				watertabletype = excluded.watertabletype,
				soundability = excluded.soundability,
				soundabilityremark = excluded.soundabilityremark,
				soundtubeinsidediam = excluded.soundtubeinsidediam,
				guid = excluded.guid,
				guid_borehole = excluded.guid_borehole,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate,
				insertuser = excluded.insertuser,
				updateuser = excluded.updateuser
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.intake IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl intake'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;
	
	-- screen
	INSERT INTO jupiter.screen AS t1
		(
			SELECT 
				boreholeid,
				intakeid,
				screenid,
				boreholeno,
				screenno,
				intakeno,
				top,
				bottom,
				"diameter",
				unit,
				diametermm,
				material,
				strength,
				slotopenin,
				startdate,
				enddate,
				wallthickn,
				fitting,
				topbotquali,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_intake, 'hex') AS UUID) AS guid_intake,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.screen t2
		) 
		ON CONFLICT (guid) --ON CONSTRAINT screen_pkey 
			DO UPDATE SET 
				boreholeid = excluded.boreholeid,
				intakeid = excluded.intakeid,
				screenid = excluded.screenid,
				boreholeno = excluded.boreholeno,
				screenno = excluded.screenno,
				intakeno = excluded.intakeno,
				top = excluded.top,
				bottom = excluded.bottom,
				"diameter" = excluded."diameter",
				unit = excluded.unit,
				diametermm = excluded.diametermm,
				material = excluded.material,
				strength = excluded.strength,
				slotopenin = excluded.slotopenin,
				startdate = excluded.startdate,
				enddate = excluded.enddate,
				wallthickn = excluded.wallthickn,
				fitting = excluded.fitting,
				topbotquali = excluded.topbotquali,
				guid = excluded.guid,
				guid_intake = excluded.guid_intake,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate,
				insertuser = excluded.insertuser,
				updateuser = excluded.updateuser
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.screen IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl screen'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;
	
	-- drwplant
	INSERT INTO jupiter.drwplant AS t1
		(
			SELECT 
				plantid,
				municipalityno2007,
				envcen,
				region,
				administratorid,
				active,
				planttype,
				permit,
				municipalityno,
				planttypeno,
				serialno,
				subno,
				countyjournalno,
				municipalityno2,
				utmzone,
				datum,
				xutm,
				yutm,
				plantname,
				plantaddress,
				plantpostalcode,
				permitdate,
				permitamount,
				permitexpiredate,
				watertype,
				"owner",
				vrrpurpose,
				reportingcounty,
				companyserialno,
				dischargeto,
				supplant,
				locatremark,
				startdate,
				enddate,
				verticaref,
				gridtype,
				locatmetho,
				elevametho,
				areaha,
				xutm32euref89,
				yutm32euref89,
				propertyno,
				feeduty,
				feedutyamount,
				feeaddressid,
				dataowner,
				ctrlmunicipalno,
				"method",
				conversionfactor,
				www,
				participantvatno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwfirm, 'hex') AS UUID) AS guid_drwfirm,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				ST_SetSRID(st_makepoint(xutm32euref89, yutm32euref89),25832) AS geom
			FROM geus_fdw.drwplant t2
		) 
		ON CONFLICT (guid) --ON CONSTRAINT drwplant_pkey 
			DO UPDATE SET 
				plantid = excluded.plantid,
				municipalityno2007 = excluded.municipalityno2007,
				envcen = excluded.envcen,
				region = excluded.region,
				administratorid = excluded.administratorid,
				active = excluded.active,					
				planttype = excluded.planttype,
				permit = excluded.permit,
				municipalityno = excluded.municipalityno,
				planttypeno = excluded.planttypeno,
				serialno = excluded.serialno,
				subno = excluded.subno,
				countyjournalno = excluded.countyjournalno,
				municipalityno2 = excluded.municipalityno2,
				utmzone = excluded.utmzone,
				datum = excluded.datum,
				xutm = excluded.xutm,
				yutm = excluded.yutm,
				plantname = excluded.plantname,
				plantaddress = excluded.plantaddress,
				plantpostalcode = excluded.plantpostalcode,
				permitdate = excluded.permitdate,
				permitamount = excluded.permitamount,
				permitexpiredate = excluded.permitexpiredate,
				watertype = excluded.watertype,
				"owner" = excluded."owner",
				vrrpurpose = excluded.vrrpurpose,
				reportingcounty = excluded.reportingcounty,
				companyserialno = excluded.companyserialno,
				dischargeto = excluded.dischargeto,
				supplant = excluded.supplant,
				locatremark = excluded.locatremark,
				startdate = excluded.startdate,
				enddate = excluded.enddate,
				verticaref = excluded.verticaref,
				gridtype = excluded.gridtype,
				locatmetho = excluded.locatmetho,
				elevametho = excluded.elevametho,
				areaha = excluded.areaha,
				xutm32euref89 = excluded.xutm32euref89,
				yutm32euref89 = excluded.yutm32euref89,
				propertyno = excluded.propertyno,
				feeduty = excluded.feeduty,
				feedutyamount = excluded.feedutyamount,
				feeaddressid = excluded.feeaddressid,
				dataowner = excluded.dataowner,
				ctrlmunicipalno = excluded.ctrlmunicipalno,
				"method" = excluded."method",
				conversionfactor = excluded.conversionfactor,
				www = excluded.www,
				participantvatno = excluded.participantvatno,
				guid = excluded.guid,
				guid_drwfirm = excluded.guid_drwfirm,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate,
				insertuser = excluded.insertuser,
				updateuser = excluded.updateuser,
				geom = excluded.geom
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.drwplant IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl drwplant'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;
	
	-- drwplantintake
	INSERT INTO jupiter.drwplantintake AS t1
		(
			SELECT 
				intakeplantid,
				boreholeid,
				intakeid,
				plantid,
				boreholeno,
				intakeno,
				startdate,
				enddate,
				intakeusage,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				CAST(ENCODE(guid_intake, 'hex') AS UUID) AS guid_intake,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.drwplantintake t2
		) 
		ON CONFLICT (guid) --ON CONSTRAINT drwplantintake_pkey 
			DO UPDATE SET 
				intakeplantid = excluded.intakeplantid,
				boreholeid = excluded.boreholeid,
				intakeid = excluded.intakeid,
				plantid = excluded.plantid,
				boreholeno = excluded.boreholeno,
				intakeno = excluded.intakeno,
				startdate = excluded.startdate,
				enddate = excluded.enddate,
				intakeusage = excluded.intakeusage,
				guid = excluded.guid,
				guid_drwplant = excluded.guid_drwplant,
				guid_intake = excluded.guid_intake,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate,
				insertuser = excluded.insertuser,
				updateuser = excluded.updateuser
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.drwplantintake IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl drwplantintake'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;
	
	-- compoundgrouplist
	INSERT INTO jupiter.compoundgrouplist AS t1
		(
			SELECT 
				compoundgroupno,
				longtext,
				shorttext,
				insertdate,
				updatedate
			FROM geus_fdw.compoundgrouplist t2
		) 
		ON CONFLICT (compoundgroupno) --ON CONSTRAINT compoundgrouplist_pkey 
			DO UPDATE SET
				compoundgroupno = excluded.compoundgroupno,
				longtext = excluded.compoundgroupno,
				shorttext = excluded.shorttext,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.compoundgrouplist IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl compoundgrouplist'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- compoundlist
	INSERT INTO jupiter.compoundlist AS t1
		(
			SELECT 
				compoundno,
				long_text,
				short_text,
				sortno,
				casno,
				euno,
				limitationdate,
				molarweight,
				charge,
				remark,
				drwunit,
				grwunit,
				insertdate,
				updatedate
			FROM geus_fdw.compoundlist t2
		) 
		ON CONFLICT (compoundno) --ON CONSTRAINT compoundlist_pkey 
			DO UPDATE SET
				compoundno = excluded.compoundno,
				long_text = excluded.long_text,
				short_text = excluded.short_text,
				sortno = excluded.sortno,
				casno = excluded.casno,
				euno = excluded.euno,
				limitationdate = excluded.limitationdate,
				molarweight = excluded.molarweight,
				charge = excluded.charge,
				remark = excluded.remark,
				drwunit = excluded.drwunit,
				grwunit = excluded.grwunit,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.compoundlist IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl compoundlist'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- compoundgroup
	INSERT INTO jupiter.compoundgroup AS t1
		(
			SELECT 
				compoundno,
				compoundgroupno,
				subcompoundgroupno,
				insertdate,
				updatedate,
				convtocompoundno,
				convfactor,
				stancode,
				stancodetype
			FROM geus_fdw.compoundgroup t2
		) 
		ON CONFLICT (compoundno) --ON CONSTRAINT compoundgroup_pkey 
			DO UPDATE SET
				compoundno = excluded.compoundno,
				compoundgroupno = excluded.compoundgroupno,
				subcompoundgroupno = excluded.subcompoundgroupno,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate,
				convtocompoundno = excluded.convtocompoundno,
				convfactor = excluded.convfactor,
				stancode = excluded.stancode,
				stancodetype = excluded.stancodetype
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.compoundgroup IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl compoundgroup'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- grwchemsample
	INSERT INTO jupiter.grwchemsample AS t1
		(
			SELECT 
				sampleid,									
				boreholeid,
				intakeid,
				outtakeid,
				boreholeno,
				intakeno,
				outtakeno,
				top,
				bottom,
				sampledate,
				project,
				laboratory,
				laboratoriyrefno,
				purpose,
				extent,
				cause,
				sourcetype,
				watertype,
				remark,
				samplelocality,
				sumcationscalculated,
				sumanoinscalculated,
				preprocessing,
				sampledby,
				client,
				clientname,
				samplereportdate,
				validatedby,
				cleaningstart,
				cleaningend,
				cleaningyield,
				partofsampleid,
				amount,
				resamplestatus,
				unit,
				previoussampleid,
				qualitycontrol,
				labreferencename,
				projectphase,
				laboratoryreceiveddate,
				samplingfirm,
				samplingequipment,
				samplestatus,
				samplequalitymark,
				samplestatususer,
				samplestatusdate,
				dataowner,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				CAST(ENCODE(guid_intake, 'hex') AS UUID) AS guid_intake,
				CAST(ENCODE(guid_outtake, 'hex') AS UUID) AS guid_outtake,
				CAST(ENCODE(guid_projectphase, 'hex') AS UUID) AS guid_projectphase,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				samplenewdate,
				laboratory_desc
			FROM geus_fdw.grwchemsample t2
		) 
		ON CONFLICT (guid) --ON CONSTRAINT grwchemsample_pkey 
			DO UPDATE SET
				sampleid = excluded.sampleid,
				boreholeid = excluded.boreholeid,
				intakeid = excluded.intakeid,
				outtakeid = excluded.outtakeid,
				boreholeno = excluded.boreholeno,
				intakeno = excluded.intakeno,
				outtakeno = excluded.outtakeno,
				top = excluded.top,
				bottom = excluded.bottom,
				sampledate = excluded.sampledate,
				project = excluded.project,
				laboratory = excluded.laboratory,
				laboratoriyrefno = excluded.laboratoriyrefno,
				purpose = excluded.purpose,
				extent = excluded.extent,
				cause = excluded.cause,
				sourcetype = excluded.sourcetype,
				watertype = excluded.watertype,
				remark = excluded.remark,
				samplelocality = excluded.samplelocality,
				sumcationscalculated = excluded.sumcationscalculated,
				sumanoinscalculated = excluded.sumanoinscalculated,
				preprocessing = excluded.preprocessing,
				sampledby = excluded.sampledby,
				client = excluded.client,
				clientname = excluded.clientname,
				samplereportdate = excluded.samplereportdate,
				validatedby = excluded.validatedby,
				cleaningstart = excluded.cleaningstart,
				cleaningend = excluded.cleaningend,
				cleaningyield = excluded.cleaningyield,
				partofsampleid = excluded.partofsampleid,
				amount = excluded.amount,
				resamplestatus = excluded.resamplestatus,
				unit = excluded.unit,
				previoussampleid = excluded.previoussampleid,
				qualitycontrol = excluded.qualitycontrol,
				labreferencename = excluded.labreferencename,
				projectphase = excluded.projectphase,
				laboratoryreceiveddate = excluded.laboratoryreceiveddate,
				samplingfirm = excluded.samplingfirm,
				samplingequipment = excluded.samplingequipment,
				samplestatus = excluded.samplestatus,
				samplequalitymark = excluded.samplequalitymark,
				samplestatususer = excluded.samplestatususer,
				samplestatusdate = excluded.samplestatusdate,
				dataowner = excluded.dataowner,
				guid = excluded.guid,
				guid_borehole = excluded.guid_borehole,
				guid_intake = excluded.guid_intake,
				guid_outtake = excluded.guid_outtake,
				guid_projectphase = excluded.guid_projectphase,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate,
				insertuser = excluded.insertuser,
				updateuser = excluded.updateuser,
				samplenewdate = excluded.samplenewdate,
				laboratory_desc = excluded.laboratory_desc
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.grwchemsample IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl grwchemsample'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;
	
	-- grwchemanalysis
	ALTER TABLE jupiter.grwchemanalysis 
		DROP CONSTRAINT IF EXISTS grwchemanalysis_pkey CASCADE
	;

	ALTER TABLE jupiter.grwchemanalysis
		ALTER COLUMN guid TYPE bytea USING decode(replace(guid::text, '-', ''), 'hex'),
		ALTER COLUMN guid_grwchemsample TYPE bytea USING decode(replace(guid_grwchemsample::text, '-', ''), 'hex')
	; 

	ALTER TABLE jupiter.grwchemanalysis 
		ADD PRIMARY KEY (guid)
	;

	INSERT INTO jupiter.grwchemanalysis AS t1
		(
			SELECT
				sampleid,
				compoundno,
				analysisno,
				analysissite,
				fieldfiltration,
				"attribute",
				amount,
				unit,
				reportedcompoundno,
				reportedunit,
				reportedamount,
				laboratory,
				laboratoryrefno,
				analysismethod,
				qualitycontrol,
				remark,
				preprocessing,
				preservation,
				packing,
				detectionlimit,
				analysisid,
				reporteddetectionlimit,
				analysisdate,
				laboratoryreceiveddate,
				absoluteexpmeasuncertainty,
				relativeexpmeasuncertainty,
				analysisresponsible,
				accreditedindicator,
				fractionationmethod,
				analysisresultidentifier,
				sortno,
				guid,
				guid_grwchemsample,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				compoundno_desc,
				unit_desc,
				analysissite_desc
			FROM geus_fdw.grwchemanalysis t2
		) 
		ON CONFLICT (guid) --ON CONSTRAINT grwchemanalysis_pkey 
			DO UPDATE SET 
				sampleid = excluded.sampleid,
				compoundno = excluded.compoundno, 
				analysisno = excluded.analysisno,
				analysissite = excluded.analysissite,
				fieldfiltration = excluded.fieldfiltration,
				"attribute" = excluded."attribute",
				amount = excluded.amount,
				unit = excluded.unit,
				reportedcompoundno = excluded.reportedcompoundno,
				reportedunit = excluded.reportedunit ,
				reportedamount = excluded.reportedamount,
				laboratory = excluded.laboratory,
				laboratoryrefno = excluded.laboratoryrefno,
				analysismethod = excluded.analysismethod,
				qualitycontrol = excluded.qualitycontrol,
				remark = excluded.remark,
				preprocessing = excluded.preprocessing,
				preservation = excluded.preservation,
				packing = excluded.packing,
				detectionlimit = excluded.detectionlimit,
				analysisid = excluded.analysisid,
				reporteddetectionlimit = excluded.reporteddetectionlimit,
				analysisdate = excluded.analysisdate,
				laboratoryreceiveddate = excluded.laboratoryreceiveddate,
				absoluteexpmeasuncertainty = excluded.absoluteexpmeasuncertainty,
				relativeexpmeasuncertainty = excluded.relativeexpmeasuncertainty,
				analysisresponsible = excluded.analysisresponsible,
				accreditedindicator = excluded.accreditedindicator,
				fractionationmethod = excluded.fractionationmethod,
				analysisresultidentifier = excluded.analysisresultidentifier,
				sortno = excluded.sortno,
				guid = excluded.guid, 
				guid_grwchemsample = excluded.guid_grwchemsample,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate,
				insertuser = excluded.insertuser,
				updateuser = excluded.updateuser,
				compoundno_desc = excluded.compoundno_desc,
				unit_desc = excluded.unit_desc,
				analysissite_desc = excluded.analysissite_desc
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.grwchemanalysis 
		DROP CONSTRAINT IF EXISTS grwchemanalysis_pkey CASCADE
	;

	ALTER TABLE jupiter.grwchemanalysis
		ALTER COLUMN guid TYPE uuid USING CAST(ENCODE(guid, 'hex') AS UUID),
		ALTER COLUMN guid_grwchemsample TYPE uuid USING CAST(ENCODE(guid_grwchemsample, 'hex') AS UUID)
	; 

	ALTER TABLE jupiter.grwchemanalysis 
		ADD PRIMARY KEY (guid)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.grwchemanalysis IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl grwchemanalysis'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;
	
	-- pltchemsample
	INSERT INTO jupiter.pltchemsample AS t1
		(
			SELECT 
				sampleid,					
				plantid,
				sampledate,
				samplesite,
				reportdate,
				journalno,
				laboratory,
				datasource,
				remark,
				extent,
				extentold,
				evaluationlaboratory,
				evaluationsite,
				evaluationjournalno,
				purpose,
				odeur,
				taste,
				look,
				colour,
				sampledby,
				measuringsiteno,
				client,
				clientname,
				pipesite,
				pipeaddress,
				pipepostalcode,
				qualitycontrol,
				estimatetemp,
				labreferencename,
				project,
				volume,
				volumeunit,
				measuringstationid,
				resamplestatus,
				previoussample,
				projectphase,
				laboratoryreceiveddate,
				samplingfirm,
				samplestatus,
				samplequalitymark,
				samplestatususer,
				samplestatusdate,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				CAST(ENCODE(guid_measuringstation, 'hex') AS UUID) AS guid_measuringstation,
				CAST(ENCODE(guid_projectphase, 'hex') AS UUID) AS guid_projectphase,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				samplenewdate,
				laboratory_desc
			FROM geus_fdw.pltchemsample t2
		) 
		ON CONFLICT (guid) --ON CONSTRAINT pltchemsample_pkey 
			DO UPDATE SET
				sampleid = excluded.sampleid,
				plantid = excluded.plantid,
				sampledate = excluded.sampledate,
				samplesite = excluded.samplesite,
				reportdate = excluded.reportdate,
				journalno = excluded.journalno,
				laboratory = excluded.laboratory,
				datasource = excluded.datasource,
				remark = excluded.remark,
				extent = excluded.extent,
				extentold = excluded.extentold,
				evaluationlaboratory = excluded.evaluationlaboratory,
				evaluationsite = excluded.evaluationsite,
				evaluationjournalno = excluded.evaluationjournalno,
				purpose = excluded.purpose,
				odeur = excluded.odeur,
				taste = excluded.taste,
				look = excluded.look,
				colour = excluded.colour,
				sampledby = excluded.sampledby,
				measuringsiteno = excluded.measuringsiteno,
				client = excluded.client,
				clientname = excluded.clientname,
				pipesite = excluded.pipesite,
				pipeaddress = excluded.pipeaddress,
				pipepostalcode = excluded.pipepostalcode,
				qualitycontrol = excluded.qualitycontrol,
				estimatetemp = excluded.estimatetemp,
				labreferencename = excluded.labreferencename,
				project = excluded.project,
				volume = excluded.volume,
				volumeunit = excluded.volumeunit,
				measuringstationid = excluded.measuringstationid,
				resamplestatus = excluded.resamplestatus,
				previoussample = excluded.previoussample,
				projectphase = excluded.projectphase,
				laboratoryreceiveddate = excluded.laboratoryreceiveddate,
				samplingfirm = excluded.samplingfirm,
				samplestatus = excluded.samplestatus,
				samplequalitymark = excluded.samplequalitymark,
				samplestatususer = excluded.samplestatususer,
				samplestatusdate = excluded.samplestatusdate,
				guid = excluded.guid,
				guid_drwplant = excluded.guid_drwplant,
				guid_measuringstation = excluded.guid_measuringstation,
				guid_projectphase = excluded.guid_projectphase,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate,
				insertuser = excluded.insertuser,
				updateuser = excluded.updateuser,
				samplenewdate = excluded.samplenewdate,
				laboratory_desc = excluded.laboratory_desc
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.pltchemsample IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl pltchemsample'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;
	
	-- pltchemanalysis
	INSERT INTO jupiter.pltchemanalysis AS t1
		(
			SELECT
				sampleid,
				compoundno,
				analysisno,
				qualitycontrol,
				amountreported,
				unitreported,
				compoundreported,
				amount,
				unit,
				"attribute",
				detectionlimit,
				filtering,
				analysissite,
				"method",
				laboratory,
				remark,
				journalno,
				preparation,
				packaging,
				preservation,
				analysisid,
				reporteddetectionlimit,
				analysisdate,
				laboratoryreceiveddate,
				absoluteexpmeasuncertainty,
				relativeexpmeasuncertainty,
				analysisresponsible,
				accreditedindicator,
				fractionationmethod,
				analysisresultidentifier,
				sortno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_pltchemsample, 'hex') AS UUID) AS guid_pltchemsample,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				compoundno_desc,
				unit_desc,
				analysissite_desc
			FROM geus_fdw.pltchemanalysis t2
		) 
		ON CONFLICT (guid) --ON CONSTRAINT pltchemanalysis_pkey 
			DO UPDATE SET 
				sampleid = excluded.sampleid,
				compoundno = excluded.compoundno,
				analysisno = excluded.analysisno,
				qualitycontrol = excluded.qualitycontrol,
				amountreported = excluded.amountreported,
				unitreported = excluded.unitreported,
				compoundreported = excluded.compoundreported,
				amount = excluded.amount,
				unit = excluded.unit,
				"attribute" = excluded."attribute",
				detectionlimit = excluded.detectionlimit,
				filtering = excluded.filtering,
				analysissite = excluded.analysissite,
				"method" = excluded."method",
				laboratory = excluded.laboratory,
				remark = excluded.remark,
				journalno = excluded.journalno,
				preparation = excluded.preparation,
				packaging = excluded.packaging,
				preservation = excluded.preservation,
				analysisid = excluded.analysisid,
				reporteddetectionlimit = excluded.reporteddetectionlimit,
				analysisdate = excluded.analysisdate,
				laboratoryreceiveddate = excluded.laboratoryreceiveddate,
				absoluteexpmeasuncertainty = excluded.absoluteexpmeasuncertainty,
				relativeexpmeasuncertainty = excluded.relativeexpmeasuncertainty,
				analysisresponsible = excluded.analysisresponsible,
				accreditedindicator = excluded.accreditedindicator,
				fractionationmethod = excluded.fractionationmethod,
				analysisresultidentifier = excluded.analysisresultidentifier,
				sortno = excluded.sortno,
				guid = excluded.guid,
				guid_pltchemsample = excluded.guid_pltchemsample,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate,
				insertuser = excluded.insertuser,
				updateuser = excluded.updateuser,
				compoundno_desc = excluded.compoundno_desc,
				unit_desc = excluded.unit_desc,
				analysissite_desc = excluded.analysissite_desc
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.pltchemanalysis IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl pltchemanalysis'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;
		
	-- lithsamp
	INSERT INTO jupiter.lithsamp AS t1
		(
			SELECT 
				boreholeid,
				sampleid,
				boreholeno,
				sampleno,
				bagno,
				top,
				bottom,
				sampledep,
				sampletop,
				samplebottom,
				samplekept,
				depestim,
				drillcolor,
				drillrockt,
				drillcolsb,
				drilldescr,
				drillbagno,
				drillsampleref,
				drillremrk,
				rocktype,
				texture,
				color,
				"structure",
				hardness,
				cementatio,
				diagenesis,
				calcareous,
				classifica,
				trivialnam,
				rocksymbol,
				sorting,
				rounding,
				remarks,
				grainshape,
				minorcomps,
				analyses,
				minerals,
				fossils,
				oldcolor,
				munsellcolor,
				totaldescr,
				otherdescr,
				sampletype,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.lithsamp t2
		) 
		ON CONFLICT (guid) --ON CONSTRAINT lithsamp_pkey 
			DO UPDATE SET 
				boreholeid = excluded.boreholeid,
				sampleid = excluded.sampleid,
				boreholeno = excluded.boreholeno,
				sampleno = excluded.sampleno,
				bagno = excluded.bagno,
				top = excluded.top,
				bottom = excluded.bottom,
				sampledep = excluded.sampledep,
				sampletop = excluded.sampletop,
				samplebottom = excluded.samplebottom,
				samplekept = excluded.samplekept,
				depestim = excluded.depestim,
				drillcolor = excluded.drillcolor,
				drillrockt = excluded.drillrockt,
				drillcolsb = excluded.drillcolsb,
				drilldescr = excluded.drilldescr,
				drillbagno = excluded.drillbagno,
				drillsampleref = excluded.drillsampleref,
				drillremrk = excluded.drillremrk,
				rocktype = excluded.rocktype,
				texture = excluded.texture,
				color = excluded.color,
				"structure" = excluded."structure",
				hardness = excluded.hardness,
				cementatio = excluded.cementatio,
				diagenesis = excluded.diagenesis,
				calcareous = excluded.calcareous,
				classifica = excluded.classifica,
				trivialnam = excluded.trivialnam,
				rocksymbol = excluded.rocksymbol,
				sorting = excluded.sorting,
				rounding = excluded.rounding,
				remarks = excluded.remarks,
				grainshape = excluded.grainshape,
				minorcomps = excluded.minorcomps,
				analyses = excluded.analyses,
				minerals = excluded.minerals,
				fossils = excluded.fossils,
				oldcolor = excluded.oldcolor,
				munsellcolor = excluded.munsellcolor,
				totaldescr = excluded.totaldescr,
				otherdescr = excluded.otherdescr,
				sampletype = excluded.sampletype,
				guid = excluded.guid,
				guid_borehole = excluded.guid_borehole,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate,
				insertuser = excluded.insertuser,
				updateuser = excluded.updateuser
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.lithsamp IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl lithsamp'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;
	
	-- watlevel 
	ALTER TABLE jupiter.watlevel 
		DROP CONSTRAINT IF EXISTS watlevel_pkey CASCADE
	;

	ALTER TABLE jupiter.watlevel
		ALTER COLUMN guid TYPE bytea USING decode(replace(guid::text, '-', ''), 'hex'),
		ALTER COLUMN guid_intake TYPE bytea USING decode(replace(guid_intake::text, '-', ''), 'hex'),
		ALTER COLUMN guid_watlevround TYPE bytea USING decode(replace(guid_watlevround::text, '-', ''), 'hex')
	; 	

	ALTER TABLE jupiter.watlevel 
		ADD PRIMARY KEY (guid)
	;

	INSERT INTO jupiter.watlevel AS t1
		(
			SELECT 
				watlevelid,
				boreholeid,
				intakeid,
				boreholeno,
				watlevelno,
				intakeno,
				timeofmeas,
				project,
				waterlevel,
				watlevgrsu,
				watlevmsl,
				watlevmp,
				hoursnopum,
				category,
				"method",
				quality,
				refpoint,
				remark,
				verticaref,
				atmospresshpa,
				extremes,
				situation,
				watlevelroundno,
				qualitycontrol,
				guid,
				guid_intake,
				guid_watlevround,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				dataowner
			FROM geus_fdw.watlevel t2
		) 
		ON CONFLICT (guid) --ON CONSTRAINT watlevel_pkey 
			DO UPDATE SET 
				watlevelid = excluded.watlevelid,
				boreholeid = excluded.boreholeid,
				intakeid = excluded.intakeid,
				boreholeno = excluded.boreholeno,
				watlevelno = excluded.watlevelno,
				intakeno = excluded.intakeno,
				timeofmeas = excluded.timeofmeas,
				project = excluded.project,
				waterlevel = excluded.waterlevel,
				watlevgrsu = excluded.watlevgrsu,
				watlevmsl = excluded.watlevmsl,
				watlevmp = excluded.watlevmp,
				hoursnopum = excluded.hoursnopum,
				category = excluded.category,
				"method" = excluded."method",
				quality = excluded.quality,
				refpoint = excluded.refpoint,
				remark = excluded.remark,
				verticaref = excluded.verticaref,
				atmospresshpa = excluded.atmospresshpa,
				extremes = excluded.extremes,
				situation = excluded.situation,
				watlevelroundno = excluded.watlevelroundno,
				qualitycontrol = excluded.qualitycontrol,
				guid = excluded.guid,
				guid_intake = excluded.guid_intake,
				guid_watlevround = excluded.guid_watlevround,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate,
				insertuser = excluded.insertuser,
				updateuser = excluded.updateuser,
				dataowner = excluded.dataowner
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.watlevel 
		DROP CONSTRAINT IF EXISTS watlevel_pkey CASCADE
	;

	ALTER TABLE jupiter.watlevel
		ALTER COLUMN guid TYPE uuid USING CAST(ENCODE(guid, 'hex') AS UUID),
		ALTER COLUMN guid_intake TYPE uuid USING CAST(ENCODE(guid_intake, 'hex') AS UUID),
		ALTER COLUMN guid_watlevround TYPE uuid USING CAST(ENCODE(guid_watlevround, 'hex') AS UUID)
	; 

	ALTER TABLE jupiter.watlevel 
		ADD PRIMARY KEY (guid)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.watlevel IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl watlevel'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- catchperm
	INSERT INTO jupiter.catchperm AS t1
		(
			SELECT 
				permissionid,
				permissiontype,
				catchpurpose,
				startdate,
				enddate,
				journalno,
				specialterms,
				remark,
				boreholemaxdepth,
				medianminwatflow,
				"source",
				companytype,
				watertype,
				municipalityno2007,
				amountperhour,
				amountperyear,
				plantid,
				revoked,
				revokeddate,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.catchperm t2
		) 
		ON CONFLICT (guid) --ON CONSTRAINT catchperm_pkey 
			DO UPDATE SET 
				permissionid = excluded.permissionid,
				permissiontype = excluded.permissiontype,
				catchpurpose = excluded.catchpurpose,
				startdate = excluded.startdate,
				enddate = excluded.enddate,
				journalno = excluded.journalno,
				specialterms = excluded.specialterms,
				remark = excluded.remark,
				boreholemaxdepth = excluded.boreholemaxdepth,
				medianminwatflow = excluded.medianminwatflow,
				"source" = excluded."source",
				companytype = excluded.companytype,
				watertype = excluded.watertype,
				municipalityno2007 = excluded.municipalityno2007,
				amountperhour = excluded.amountperhour,
				amountperyear = excluded.amountperyear,
				plantid = excluded.plantid,
				revoked = excluded.revoked,
				revokeddate = excluded.revokeddate,
				guid = excluded.guid,
				guid_drwplant = excluded.guid_drwplant,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate,
				insertuser = excluded.insertuser,
				updateuser = excluded.updateuser
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.catchperm IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl catchperm'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;
	
	-- wrrcatchment
	INSERT INTO jupiter.wrrcatchment AS t1
		(
			SELECT 
				plantcatchmentid,
				plantid,
				catchmentno,
				startdate,
				enddate,
				"attribute",
				amount,
				"method",
				flowmeterstart,
				flowmeterend,
				remark,
				conversionfactor,
				surfacewatervolume,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.wrrcatchment t2
		) 
		ON CONFLICT (guid) --ON CONSTRAINT wrrcatchment_pkey 
			DO UPDATE SET 
				plantcatchmentid = excluded.plantcatchmentid,
				plantid = excluded.plantid,
				catchmentno = excluded.catchmentno,
				startdate = excluded.startdate,
				enddate = excluded.enddate,
				"attribute" = excluded."attribute",
				amount = excluded.amount,
				"method" = excluded."method",
				flowmeterstart = excluded.flowmeterstart,
				flowmeterend = excluded.flowmeterend,
				remark = excluded.remark,
				conversionfactor = excluded.conversionfactor,
				surfacewatervolume = excluded.surfacewatervolume,
				guid = excluded.guid,
				guid_drwplant = excluded.guid_drwplant,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate,
				insertuser = excluded.insertuser,
				updateuser = excluded.updateuser
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.wrrcatchment IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl wrrcatchment'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- drwplantcompanytype
	INSERT INTO jupiter.drwplantcompanytype AS t1
		(
			SELECT 
				plantid,
				companytype,
				companytypeno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				insertdate,
				insertuser,
				updatedate,
				updateuser
			FROM geus_fdw.drwplantcompanytype t2
		) 
		ON CONFLICT (guid) --ON CONSTRAINT drwplantcompanytype_pkey 
			DO UPDATE SET 
				plantid = excluded.plantid,
				companytype = excluded.companytype,
				companytypeno = excluded.companytypeno,
				guid = excluded.guid,
				guid_drwplant = excluded.guid_drwplant,
				insertdate = excluded.insertdate,
				insertuser = excluded.insertuser,
				updatedate = excluded.updatedate,
				updateuser = excluded.updateuser
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.drwplantcompanytype IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl drwplantcompanytype'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- storedoc
	INSERT INTO jupiter.storedoc AS t1
		(
			SELECT 
				fileid,
				filetype,
				url,
				"comments",
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.storedoc t2
		) 
		ON CONFLICT (fileid) --ON CONSTRAINT storedoc_pkey 
			DO UPDATE SET 
				fileid = excluded.fileid,
				filetype = excluded.filetype,
				url = excluded.url,
				"comments" = excluded."comments",
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate,
				insertuser = excluded.insertuser,
				updateuser = excluded.updateuser
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.storedoc IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl storedoc'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- boredoc
	INSERT INTO jupiter.boredoc AS t1
		(
			SELECT 
				fileid,
				boreholeid,
				boreholeno,
				doctype,
				versionno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.boredoc t2
		) 
		ON CONFLICT (guid) --ON CONSTRAINT boredoc_pkey 
			DO UPDATE SET 
				fileid = excluded.fileid,
				boreholeid = excluded.boreholeid,
				boreholeno = excluded.boreholeno,
				doctype = excluded.doctype,
				versionno = excluded.versionno,
				guid = excluded.guid,
				guid_borehole = excluded.guid_borehole,
				insertdate = excluded.insertdate,
				updatedate = excluded.updatedate,
				insertuser = excluded.insertuser,
				updateuser = excluded.updateuser
			WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.boredoc IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl boredoc'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- exporttime
	DROP TABLE IF EXISTS jupiter.exporttime CASCADE;	

	CREATE TABLE IF NOT EXISTS jupiter.exporttime AS 
		(
			SELECT 
				*
			FROM geus_fdw.exporttime
		)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.exporttime IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl exporttime'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- give access to read user
	GRANT USAGE ON SCHEMA jupiter TO grukosreader;
	GRANT SELECT ON ALL TABLES IN SCHEMA jupiter TO grukosreader;

COMMIT;

/*
 * create indexes
 */
CREATE INDEX IF NOT EXISTS code_code_idx
	ON jupiter.code (code)
;
	
CREATE INDEX IF NOT EXISTS code_codetype_idx
	ON jupiter.code(codetype)
;

CREATE INDEX IF NOT EXISTS borehole_geom_idx
	ON jupiter.borehole
	USING GIST (geom)
;

CREATE INDEX IF NOT EXISTS borehole_boreholeid_idx
	ON jupiter.borehole (boreholeid)
;

CREATE INDEX IF NOT EXISTS borehole_boreholeno_idx
	ON jupiter.borehole (boreholeno)
;

CREATE INDEX IF NOT EXISTS intake_intakeno_idx
	ON jupiter.intake(intakeno)
;

CREATE INDEX IF NOT EXISTS intake_guid_borehole_idx
	ON jupiter.intake(guid_borehole)
;

CREATE INDEX IF NOT EXISTS intake_intakeid_idx
	ON jupiter.intake(intakeid)
;

CREATE INDEX IF NOT EXISTS screen_screenno_idx
	ON jupiter.screen(screenno)
;

CREATE INDEX IF NOT EXISTS screen_screenid_idx
	ON jupiter.screen(screenid)
;

CREATE INDEX IF NOT EXISTS drwplant_geom_idx
	ON jupiter.drwplant
	USING GIST (geom)
;

CREATE INDEX IF NOT EXISTS drwplant_plantid_idx
	ON jupiter.drwplant(plantid)
;

CREATE INDEX IF NOT EXISTS drwplant_plantname_idx
	ON jupiter.drwplant(plantname)
;

CREATE INDEX IF NOT EXISTS drwplantintake_boreholeid_idx
	ON jupiter.drwplantintake(boreholeid)
;
	
CREATE INDEX IF NOT EXISTS drwplantintake_boreholeno_idx
	ON jupiter.drwplantintake(boreholeno)
;

CREATE INDEX IF NOT EXISTS grwchemsample_sampleid_idx
	ON jupiter.grwchemsample(sampleid)
;

CREATE INDEX IF NOT EXISTS grwchemsample_boreholeid_idx
	ON jupiter.grwchemsample(boreholeid)
;

CREATE INDEX IF NOT EXISTS grwchemanalysis_sampleid_idx
	ON jupiter.grwchemanalysis(sampleid)
;

CREATE INDEX IF NOT EXISTS grwchemanalysis_amount_idx
	ON jupiter.grwchemanalysis(amount)
;

CREATE INDEX IF NOT EXISTS grwchemanalysis_compoundno_idx
	ON jupiter.grwchemanalysis(compoundno)
;

CREATE INDEX IF NOT EXISTS pltchemsample_sampleid_idx
	ON jupiter.pltchemsample (sampleid)
;

CREATE INDEX IF NOT EXISTS pltchemsample_boreholeid_idx
	ON jupiter.pltchemsample (plantid)
;

CREATE INDEX IF NOT EXISTS pltchemanalysis_sampleid_idx
	ON jupiter.pltchemanalysis(sampleid)
;

CREATE INDEX IF NOT EXISTS pltchemanalysis_amount_idx
	ON jupiter.pltchemanalysis(amount)
;

CREATE INDEX IF NOT EXISTS pltchemanalysis_compoundno_idx
	ON jupiter.pltchemanalysis(compoundno)
;

CREATE INDEX IF NOT EXISTS lithsamp_boreholeid_idx
	ON jupiter.lithsamp(boreholeid)
;

CREATE INDEX IF NOT EXISTS lithsamp_rocksymbol_idx
	ON jupiter.lithsamp(rocksymbol)
;

CREATE INDEX IF NOT EXISTS lithsamp_top_idx
	ON jupiter.lithsamp(top)
;

CREATE INDEX IF NOT EXISTS lithsamp_bottom_idx
	ON jupiter.lithsamp(bottom)
;

CREATE INDEX IF NOT EXISTS watlevel_boreholeid_idx
	ON jupiter.watlevel (boreholeid)
;
	
CREATE INDEX IF NOT EXISTS wrrcatchment_plantid_idx
	ON jupiter.wrrcatchment (plantid)
;

CREATE INDEX IF NOT EXISTS catchperm_plantid_idx
	ON jupiter.catchperm (plantid)
;

CREATE INDEX IF NOT EXISTS drwplantcompanytype_plantid_idx
	ON jupiter.drwplantcompanytype (plantid)
;

CREATE INDEX IF NOT EXISTS boredoc_fileid_idx
	ON jupiter.boredoc (fileid)
;

CREATE INDEX IF NOT EXISTS boredoc_boreholeid_idx
	ON jupiter.boredoc (boreholeid)
;



/*
 * 	create Foreign keys 
 */

ALTER TABLE jupiter.code 
	ADD CONSTRAINT fk_codetype_code FOREIGN KEY (codetype) 
	REFERENCES jupiter.codetype (codetype)
;

-- drilmeth
ALTER TABLE jupiter.drilmeth 
	ADD CONSTRAINT fk_borehole_drilmeth FOREIGN KEY (guid_borehole) 
	REFERENCES jupiter.borehole (guid)
;

 --intake
ALTER TABLE jupiter.intake 
	ADD CONSTRAINT fk_borehole_intake FOREIGN KEY (guid_borehole) 
	REFERENCES jupiter.borehole (guid)
;

-- screen
ALTER TABLE jupiter.screen 
	ADD CONSTRAINT fk_intake_screen FOREIGN KEY (guid_intake) 
	REFERENCES jupiter.intake (guid)
;

ALTER TABLE jupiter.screen 
	ADD CONSTRAINT fk_borehole_screen FOREIGN KEY (boreholeid) 
	REFERENCES jupiter.borehole (boreholeid)
;

ALTER TABLE jupiter.drwplant 
	ADD CONSTRAINT fk_municipality FOREIGN KEY (municipalityno2007) 
	REFERENCES jupiter.municipality2007 (municipalityno2007)
;

-- drwplantintake
ALTER TABLE jupiter.drwplantintake 
	ADD CONSTRAINT fk_intake_dpi FOREIGN KEY (guid_intake) 
	REFERENCES jupiter.intake (guid)
;

ALTER TABLE jupiter.drwplantintake 
	ADD CONSTRAINT fk_plant_dpi FOREIGN KEY (guid_drwplant) 
	REFERENCES jupiter.drwplant (guid)
;

ALTER TABLE jupiter.drwplantintake 
	ADD CONSTRAINT fk_borehole_dpi FOREIGN KEY (boreholeid) 
	REFERENCES jupiter.borehole (boreholeid)
;

-- compoundgroup
ALTER TABLE jupiter.compoundgroup 
	ADD CONSTRAINT fk_cpl_cpg FOREIGN KEY (compoundno) 
	REFERENCES jupiter.compoundlist (compoundno)
;

ALTER TABLE jupiter.compoundgroup 
	ADD CONSTRAINT fk_cpgl_cpg FOREIGN KEY (compoundgroupno) 
	REFERENCES jupiter.compoundgrouplist (compoundgroupno)
;

-- grwchemsample
ALTER TABLE jupiter.grwchemsample 
ADD CONSTRAINT fk_intake_gcs FOREIGN KEY (guid_intake) 
REFERENCES jupiter.intake (guid)
;

ALTER TABLE jupiter.grwchemsample 
	ADD CONSTRAINT fk_borehole_gcs FOREIGN KEY (guid_borehole) 
	REFERENCES jupiter.borehole (guid)
;

-- grwchemanalysis
ALTER TABLE jupiter.grwchemanalysis 
	ADD CONSTRAINT fk_cpl_gca FOREIGN KEY (compoundno) 
	REFERENCES jupiter.compoundlist (compoundno)
;

ALTER TABLE jupiter.grwchemanalysis 
	ADD CONSTRAINT fk_gcs_gca FOREIGN KEY (guid_grwchemsample) 
	REFERENCES jupiter.grwchemsample (guid)
;	

-- pltchemsample
ALTER TABLE jupiter.pltchemsample 
	ADD CONSTRAINT fk_drwplant_pcs FOREIGN KEY (guid_drwplant) 
	REFERENCES jupiter.drwplant (guid)
;

-- pltchemanalysis
ALTER TABLE jupiter.pltchemanalysis 
	ADD CONSTRAINT fk_pcs_pca FOREIGN KEY (guid_pltchemsample) 
	REFERENCES jupiter.pltchemsample (guid)
;

ALTER TABLE jupiter.pltchemanalysis 
	ADD CONSTRAINT fk_cpl_pca FOREIGN KEY (compoundno) 
	REFERENCES jupiter.compoundlist (compoundno)
;

-- lithsamp
ALTER TABLE jupiter.lithsamp 
	ADD CONSTRAINT fk_borehole_lithsamp FOREIGN KEY (guid_borehole) 
	REFERENCES jupiter.borehole (guid)
;

ALTER TABLE jupiter.watlevel 
	ADD CONSTRAINT fk_intake_watlevel FOREIGN KEY (guid_intake) 
	REFERENCES jupiter.intake (guid)
;

ALTER TABLE jupiter.watlevel 
	ADD CONSTRAINT fk_borehole_watlevel FOREIGN KEY (boreholeid) 
	REFERENCES jupiter.borehole (boreholeid)
;

-- catchperm
ALTER TABLE jupiter.catchperm 
	ADD CONSTRAINT fk_drwplant_catchperm FOREIGN KEY (guid_drwplant) 
	REFERENCES jupiter.drwplant (guid)
;

ALTER TABLE jupiter.wrrcatchment 
	ADD CONSTRAINT fk_drwplant_wrrcatchment FOREIGN KEY (guid_drwplant) 
	REFERENCES jupiter.drwplant (guid)
;

-- drwplantcompanytype
ALTER TABLE jupiter.drwplantcompanytype 
	ADD CONSTRAINT fk_drwplant_drwplantcompanytype FOREIGN KEY (guid_drwplant) 
	REFERENCES jupiter.drwplant (guid)
;

ALTER TABLE jupiter.boredoc 
	ADD CONSTRAINT fk_borehole_boredoc FOREIGN KEY (guid_borehole) 
	REFERENCES jupiter.borehole (guid)
;

-- boredoc
ALTER TABLE jupiter.boredoc 
	ADD CONSTRAINT fk_storedoc_boredoc FOREIGN KEY (fileid) 
	REFERENCES jupiter.storedoc (fileid)
;
