BEGIN;

	DROP VIEW IF EXISTS mstjupiter.inorganic_crosstab_json CASCADE;

	CREATE OR REPLACE VIEW mstjupiter.inorganic_crosstab_json AS (
		-- https://postgresql.verite.pro/blog/2018/06/19/crosstab-pivot.html
		SELECT 	
			sampleid,
		   	json_object_agg(compoundno, amount ORDER BY compoundno) AS samples_json
		FROM (
		     	SELECT 
		     		gca.sampleid, 
		     		cpl.compoundno, 
		     		CASE
						WHEN gca.attribute LIKE '<%' 
							OR gca.attribute LIKE 'A%' 
							OR gca.attribute LIKE 'o%' 
							OR gca.attribute LIKE '0%' 
							THEN 0::NUMERIC
						WHEN gca.attribute LIKE 'B%'
							OR gca.attribute LIKE 'C%'
							OR gca.attribute LIKE 'D%' 
							OR gca.attribute LIKE 'S%' 
							OR gca.attribute LIKE '!%' 
							OR gca.attribute LIKE '/%' 
							OR gca.attribute LIKE '*%' 
							OR gca.attribute LIKE '>%' 
							THEN -99::NUMERIC
						ELSE gca.amount
						END AS amount
		        FROM jupiter.grwchemanalysis gca
		        INNER JOIN jupiter.compoundlist cpl ON gca.compoundno = cpl.compoundno
		   ) as s 
		GROUP BY sampleid
	);

GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO jupiterrole;

COMMIT;
