/*
    Shows the thickness of the top most Quarternary layer
*/
drop view if exists mstjupiter.faxestevns_quaternary_toplayer;
create or replace view mstjupiter.faxestevns_quaternary_toplayer as
    select distinct on(b.boreholeid)
        row_number() over () rowid,
        b.boreholeid, b.boreholeno dgunr, b.elevation, b.komnavn, b.geom,
        ls.top, ls.bottom, (ls.bottom-ls.top) thickness,ls.rocksymbol, ls.litho, ls.longtext
    from (select bh.*, k.komnavn
          from jupiter.borehole bh
          inner join map.komgr k on st_within(bh.geom, k.geom)
          where komnavn in ('Faxe', 'Stevns')) as b
    inner join (select l.*, s.litho, s.longtext from jupiter.lithsamp l
                inner join mstjupiter.stratum_dgusymbol s on l.rocksymbol = s.dgu_symbol
                where s.litho in ('k')) ls using (boreholeid)
    order by b.boreholeid, ls.top;


/*
    Get the thickness of the Quarternary layers as defined in mstjupiter.stratum_dgusymbol.
    Caveat: the well might not end in a prequarternary layer. Needs another view for showing
    wells with no prequartery layers present for uncertainty.
*/
drop view if exists mstjupiter.faxestevns_quaternary_thickness;
create or replace view mstjupiter.faxestevns_quaternary_thickness as
    with all_layers as (
        select
            row_number() over () rowid,
            b.boreholeid, b.boreholeno dgunr, b.elevation, b.komnavn, b.geom,
            ls.top, ls.bottom, (ls.bottom-ls.top) thickness,ls.rocksymbol, ls.litho, ls.longtext
        from (select bh.*, k.komnavn
              from jupiter.borehole bh
              inner join map.komgr k on st_within(bh.geom, k.geom)
              where komnavn in ('Faxe', 'Stevns')) as b
        inner join (select l.*, s.litho, s.longtext from jupiter.lithsamp l
                    inner join mstjupiter.stratum_dgusymbol s on l.rocksymbol = s.dgu_symbol
                    where s.litho in ('k')) ls using (boreholeid)
        order by b.boreholeid)
    select row_number() over () rowid, dgunr, sum(thickness) quarternary_thickness, elevation, geom from all_layers
    group by dgunr, elevation, geom
    order by dgunr;

/*
    Count the distribution of quarternary, tertiary, pretertiary and unknown layers in DK wells
*/
drop view if exists  mstjupiter.stratum_distribution;
create or replace view mstjupiter.stratum_distribution as
    with temp as (
        select
               b.boreholeid, b.boreholeno, count(s.litho) count_stratum, s.litho,
               b.geom
        from jupiter.borehole b
        inner join jupiter.lithsamp l using (boreholeid)
        inner join mstjupiter.stratum_dgusymbol s on s.dgu_symbol = l.rocksymbol
        --where b.boreholeno ilike '223.%'
        --where b.boreholeno = '223.   65' or b.boreholeno = '223.   75'
        group by s.litho, b.boreholeid
        ),
        temp2 as (
            select
                boreholeno,
                case
                    when litho = 'k' then count_stratum else 0
                end count_kvartaer,
                case
                   when litho = 't' then count_stratum else 0
                   end count_tertiary,
                case
                   when litho = 'p' then count_stratum else 0
                   end count_pretertiary,
                case
                   when litho = 'u' then count_stratum else 0
                   end count_unknown,
                geom
            from temp
        )
    select boreholeno,
           sum(count_kvartaer) n_kvartaer,
           sum(count_tertiary) n_tertiaer,
           sum(count_pretertiary) n_pretertiaer,
           sum(count_unknown) n_ukendt,
           geom
    from temp2
    group by boreholeno, geom;
    --order by boreholeno;

/*
    Sum the thickness of layers in four groups:
    k = quarternary, t = tertiary, p = pretertiary, u = unknown
    based off the chronostratigraphic correlation in mstjupiter.stratum_dgusymbol
*/
drop view if exists  mstjupiter.stratum_distribution_thickness;
create or replace view mstjupiter.stratum_distribution_thickness as
    with temp1 as (
        -- sum lithologies for each lithos group for each well
        select
               b.boreholeid, b.boreholeno, sum(l.bottom-l.top) thickness, s.litho, b.geom
        from jupiter.borehole b
        inner join jupiter.lithsamp l using (boreholeid)
        inner join mstjupiter.stratum_dgusymbol s on s.dgu_symbol = l.rocksymbol
        --where b.boreholeno ilike '223.%'
        --where b.boreholeno = '223.   65' or b.boreholeno = '223.   75'
        group by s.litho, b.boreholeid
    ),
    temp2 as (
            -- Pivot the four groups: from records to fields (unique boreholeid)
            select
                boreholeid,
                boreholeno,
                case
                    when litho = 'k' then thickness else 0
                end quarternary_m,
                case
                   when litho = 't' then thickness else 0
                   end tertiary_m,
                case
                   when litho = 'p' then thickness else 0
                   end pretertiary_m,
                case
                   when litho = 'u' then thickness else 0
                   end unknown_m,
                geom
            from temp1
    ),
    temp3 as (
        -- Sum the thickness of each group layer
        select boreholeid, boreholeno,
            sum(quarternary_m) quarternary_m,
            sum(tertiary_m) tertiary_m,
            sum(pretertiary_m) pretertiary_m,
            sum(unknown_m) unknown_m,
            geom
        from temp2
        group by boreholeid, boreholeno, geom),
    temp4 as (
        -- Join drilldepth with max bottom layer depth
        select b.boreholeid, b.boreholeno, b.drilldepth, max(l.bottom) max_bottom
        from jupiter.borehole b
        inner join jupiter.lithsamp l using (boreholeid)
        group by boreholeid),
    temp5 as (
        -- Coalensce drilldepth and max_bottom
        select boreholeid, boreholeno, coalesce(drilldepth, max_bottom) drill_bottom
        from temp4
    )
-- Join layer group thickness and drill_bottom (either drilldepth or max_bottom)
select t3.*, t5.drill_bottom
from temp3 t3
left join temp5 t5 using (boreholeid);


/*
    Sum the thickness of layers in four groups:
    k = quarternary, t = tertiary, p = pretertiary, u = unknown
    based off the chronostratigraphic correlation in mstjupiter.stratum_dgusymbol

    TEMP VIEW REORDER

*/
drop materialized view if exists  mstjupiter.stratum_distribution_thickness_stevnsfaxe;
create materialized view mstjupiter.stratum_distribution_thickness_stevnsfaxe as
    with temp1 as (
        select
               b.boreholeid, b.boreholeno, sum(l.bottom-l.top) thickness, s.litho, b.geom
        from jupiter.borehole b
        inner join jupiter.lithsamp l using (boreholeid)
        inner join mstjupiter.stratum_dgusymbol s on s.dgu_symbol = l.rocksymbol
        --where b.boreholeno ilike '223.%'
        --where b.boreholeno = '223.   65' or b.boreholeno = '223.   75'
        group by s.litho, b.boreholeid),
    temp2 as (
            select
                boreholeno,
                case
                    when litho = 'k' then thickness else 0
                end quarternary_m,
                case
                   when litho = 't' then thickness else 0
                   end tertiary_m,
                case
                   when litho = 'p' then thickness else 0
                   end pretertiary_m,
                case
                   when litho = 'u' then thickness else 0
                   end unknown_m,
                geom
            from temp1
        )
select
    boreholeno,
    sum(quarternary_m) quarternary_m,
    sum(tertiary_m) tertiary_m,
    sum(pretertiary_m) pretertiary_m,
    sum(unknown_m) unknown_m,
    geom
from temp2
group by boreholeno, geom;


/*
    Find wells with no prequarternary layers present. This will clarify the uncertainity where the bottom
    of the well does not reach the prequarternary ie the thickness of quarternary is not completely correct
    since there was no drill through of the quarternary layers.
*/
drop view if exists mstjupiter.faxestevns_prequarternary_not_present;
create or replace view mstjupiter.faxestevns_prequarternary_not_present as
        select distinct on (b.boreholeid)
            b.boreholeid, b.boreholeno dgunr, b.drilldepth, b.geom, ls.longtext
        from (select bh.*, k.komnavn
              from jupiter.borehole bh
              inner join map.komgr k on st_within(bh.geom, k.geom)
              where komnavn in ('Faxe', 'Stevns')) as b
        inner join (select l.*, s.litho, s.longtext from jupiter.lithsamp l
                    inner join mstjupiter.stratum_dgusymbol s on l.rocksymbol = s.dgu_symbol
                    where s.litho not in ('t', 'p')) ls using (boreholeid)
        order by b.boreholeid;


/*
    TEMP
*/
select * from mstjupiter.stratum_dgusymbol
where dgu_symbol not in (select code from jupiter.code where code.codetype = 44);

select * from jupiter.code
where code not in (select dgu_symbol from mstjupiter.stratum_dgusymbol) and codetype = 44;