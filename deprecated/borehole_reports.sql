/* NOTE: This script is redundant as URLs are now generated on the client side
 * */
BEGIN;

ALTER TABLE jupiter.borehole DROP COLUMN IF EXISTS report_url_html;
ALTER TABLE jupiter.borehole ADD report_url_html text NULL;
UPDATE jupiter.borehole SET report_url_html = 'https://data.geus.dk/JupiterWWW/borerapport.jsp?borid=' || borid;

ALTER TABLE jupiter.borehole DROP COLUMN IF EXISTS report_url_pdf;
ALTER TABLE jupiter.borehole ADD report_url_pdf text NULL;
UPDATE jupiter.borehole SET report_url_pdf = 'https://jupiter.geus.dk/cgi-bin/svgrapport.dll?borid=' || borid || '&format=pdf';

COMMIT;
