-- -h 10.33.131.50 -p 5432 -U grukosadmin -d pcjupiterxl
-- -h C1400020 -p 5432 -U postgres -d pcjupiterxl
/*
ANDAM
2018-09-13 13:58:49 CEST (+0200 UTC)

All boreholes start at QUAL_LOC = QUAL_DAT = QUAL_SUM = 0. The QUAL_LOC and
QUAL_DAT parameters are procedurally increased if various criteria are met.

*/

BEGIN;

    -- Drop columns if they exist
    ALTER TABLE jupiter.borehole
    DROP COLUMN IF EXISTS quality_location,
    DROP COLUMN IF EXISTS quality_data,
    DROP COLUMN IF EXISTS quality_sum;

    -- Add columns, default to the value 0
    ALTER TABLE jupiter.borehole
    ADD COLUMN quality_location INTEGER DEFAULT 0,
    ADD COLUMN quality_data INTEGER DEFAULT 0,
    ADD COLUMN quality_sum INTEGER DEFAULT 0;

    -- Qry_1_Position -- If coordinates are in UTM32EUREF89, quality is 1
    UPDATE jupiter.borehole
    SET quality_location = 1, quality_data = 1
    WHERE xutm32euref89 IS NOT NULL
    AND yutm32euref89 IS NOT NULL;

    -- Qry_2_Depth
    --EXPLAIN (ANALYZE)
    UPDATE jupiter.borehole AS b
    SET quality_location = 2, quality_data = 2
    FROM jupiter.screen AS s
    WHERE b.borid = s.borid AND
    ((b.quality_location = 1 AND b.quality_data = 1
            AND s.top IS NOT NULL
            AND s.bottom IS NOT NULL
            AND b.drilldepth IS NOT NULL)
        OR
        (b.quality_location = 1 AND b.quality_data = 1
            AND drilldepth IS NOT NULL
            AND s.screenno IS NULL));

    -- Qry_3_dat_Lithology
    UPDATE jupiter.borehole AS b
    SET quality_data = 3
    FROM jupiter.lithsamp AS l
    WHERE b.borid = l.borid AND
    (b.quality_data = 2 
        AND l.rocksymbol NOT ILIKE 'x'
        AND l.rocksymbol NOT ILIKE 'b'
        AND l.rocksymbol NOT ILIKE 'u');

    -- Qry_3_lok_Lokaliseringsskema
    UPDATE jupiter.borehole AS b
    SET quality_location = 3
    FROM jupiter.boredoc AS bd, jupiter.storedoc AS sd
    WHERE b.boreholeno = bd.boreholeno AND sd.fileid = bd.fileid AND
    (b.quality_location > 1
        AND (bd.doctype ILIKE 'L')
        AND ((sd.comments NOT ILIKE '%skan%' AND
            sd.comments NOT ILIKE '%skan%')) OR
            sd.comments IS NULL);
            
    -- Qry_4_dat_Geolog
    UPDATE jupiter.borehole as b
    SET quality_data = 4
    FROM jupiter.lithsamp as l
    WHERE b.borid = l.borid AND
    (b.quality_data = 3 AND char_length(l.rocksymbol) = 2);

    -- Qry_4_lok_DGPS
    UPDATE jupiter.borehole
    SET quality_location = 4
    WHERE (quality_location > 1
        AND (elevametho ILIKE 'P'
            OR elevametho ILIKE 'N'))
    OR (quality_location > 1
        AND (locatmetho ILIKE 'DG'
            OR locatmetho ILIKE 'GR'
            OR locatmetho ILIKE 'I'));

    -- Qry_5_dat_Boremetode
    UPDATE jupiter.borehole AS b
    SET quality_data = 5
    FROM jupiter.drilmeth AS dm
    WHERE b.borid = dm.borid AND
    (b.quality_data = 4 AND
        (dm.method ILIKE 'G'
            OR dm.method ILIKE 'T'
            OR dm.method ILIKE 'I'
            OR dm.method ILIKE 'L'
            OR dm.method ILIKE 'R'));

    -- Qry_5_lok_Synkpejl
    UPDATE jupiter.borehole AS b
    SET quality_location = 5
    FROM jupiter.watlevel AS wl
    WHERE b.boreholeno = wl.boreholeno AND
    (b.quality_location > 1 AND wl.watlevelroundno IS NOT NULL);


    -- Compute quality_sum
    UPDATE jupiter.borehole
    SET quality_sum = 1
    WHERE quality_location > 0 AND quality_data > 0;

    UPDATE jupiter.borehole
    SET quality_sum = 2
    WHERE quality_location > 1 AND quality_data > 1;

    UPDATE jupiter.borehole
    SET quality_sum = 3
    WHERE quality_location > 2 AND quality_data > 2;

    UPDATE jupiter.borehole
    SET quality_sum = 4
    WHERE quality_location > 3 AND quality_data > 3;

    UPDATE jupiter.borehole
    SET quality_sum = 5
    WHERE quality_location > 4 AND quality_data > 4;

    -- Show statistics
    SELECT quality_location, COUNT(*)
    FROM jupiter.borehole
    GROUP BY quality_location;

    SELECT quality_data, COUNT(*)
    FROM jupiter.borehole
    GROUP BY quality_data;

    SELECT quality_sum, COUNT(*)
    FROM jupiter.borehole
    GROUP BY quality_sum;
    SELECT quality_location, quality_data, COUNT(*)

    FROM jupiter.borehole
    GROUP BY quality_location, quality_data;

COMMIT;
