/*
 * Owner: 	Danish Environmental Protection Agency (EPA) 
 * Descr: 	This sql script updates MST-GKOs pcjupiterxl postgresql database
 * 			via GEUS' SQL-gateway --where existing tables are updated using 
 * 			select statements (upserts) executed on a foreign data wrapper. 
 * 			The script is overall optimized toward lowering the execution time
 * 			of the queries from the geus foreign wrapper.
 * 			This means for the large tables, the uuids are translated prior to updating 
 * 			such that the fdw queries does not have to translate bytea into uuid. This is done
 * 			to lower the queries time of execution on the fdw server due to 
 * 			timeout errors.
 * ERT: 	~50 min 
 * Author: 	Simon Makwarth <simak@mst.dk>
 * Created: 2022-02-22
 * License:	GNU GPL v3
 */

BEGIN;

	-- create mstjupiter schema if it does not exists

	CREATE SCHEMA IF NOT EXISTS mstjupiter;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON SCHEMA mstjupiter IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: Data from pcjupiterxl'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- create jupiter schema if it does not exists

	CREATE SCHEMA IF NOT EXISTS jupiter;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON SCHEMA jupiter IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: MSTGKO derived data from pcjupiterxl'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	CREATE TABLE IF NOT EXISTS mstjupiter.mst_exportdate AS 
		(
			SELECT 
				'test_string'::TEXT AS tablename,
				'1900-01-01'::timestamp AS updatetime
			LIMIT 0
		)
	;

	ALTER TABLE mstjupiter.mst_exportdate
		DROP CONSTRAINT IF EXISTS mst_exportdate_pkey CASCADE
	; 

	ALTER TABLE mstjupiter.mst_exportdate
		ADD PRIMARY KEY (tablename)
	;

COMMIT;

-- pltchemanalysis

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.pltchemanalysis AS
		(
			SELECT 
				sampleid,
				compoundno,
				analysisno,
				qualitycontrol,
				amountreported,
				unitreported,
				compoundreported,
				amount,
				unit,
				"attribute",
				detectionlimit,
				filtering,
				analysissite,
				"method",
				laboratory,
				remark,
				journalno,
				preparation,
				packaging,
				preservation,
				analysisid,
				reporteddetectionlimit,
				analysisdate,
				laboratoryreceiveddate,
				absoluteexpmeasuncertainty,
				relativeexpmeasuncertainty,
				analysisresponsible,
				accreditedindicator,
				fractionationmethod,
				analysisresultidentifier,
				sortno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_pltchemsample, 'hex') AS UUID) AS guid_pltchemsample,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				compoundno_desc,
				unit_desc,
				analysissite_desc	
			FROM geus_fdw.pltchemanalysis
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.pltchemanalysis 
		DROP CONSTRAINT IF EXISTS pltchemanalysis_pkey CASCADE
	;

	ALTER TABLE jupiter.pltchemanalysis 
		DROP CONSTRAINT IF EXISTS fk_pcs_pca CASCADE
	;

	ALTER TABLE jupiter.pltchemanalysis 
		DROP CONSTRAINT IF EXISTS fk_cpl_pca CASCADE
	;

	DROP INDEX IF EXISTS pltchemanalysis_sampleid_idx;
	
	DROP INDEX IF EXISTS pltchemanalysis_amount_idx;

	DROP INDEX IF EXISTS pltchemanalysis_compoundno_idx;

	TRUNCATE jupiter.pltchemanalysis;

	INSERT INTO jupiter.pltchemanalysis AS t1
		(
			sampleid,
			compoundno,
			analysisno,
			qualitycontrol,
			amountreported,
			unitreported,
			compoundreported,
			amount,
			unit,
			"attribute",
			detectionlimit,
			filtering,
			analysissite,
			"method",
			laboratory,
			remark,
			journalno,
			preparation,
			packaging,
			preservation,
			analysisid,
			reporteddetectionlimit,
			analysisdate,
			laboratoryreceiveddate,
			absoluteexpmeasuncertainty,
			relativeexpmeasuncertainty,
			analysisresponsible,
			accreditedindicator,
			fractionationmethod,
			analysisresultidentifier,
			sortno,
			guid,
			guid_pltchemsample,
			insertdate,
			updatedate,
			insertuser,
			updateuser,
			compoundno_desc,
			unit_desc,
			analysissite_desc
		)
		(
			SELECT
				sampleid,
				compoundno,
				analysisno,
				qualitycontrol,
				amountreported,
				unitreported,
				compoundreported,
				amount,
				unit,
				"attribute",
				detectionlimit,
				filtering,
				analysissite,
				"method",
				laboratory,
				remark,
				journalno,
				preparation,
				packaging,
				preservation,
				analysisid,
				reporteddetectionlimit,
				analysisdate,
				laboratoryreceiveddate,
				absoluteexpmeasuncertainty,
				relativeexpmeasuncertainty,
				analysisresponsible,
				accreditedindicator,
				fractionationmethod,
				analysisresultidentifier,
				sortno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_pltchemsample, 'hex') AS UUID) AS guid_pltchemsample,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				compoundno_desc,
				unit_desc,
				analysissite_desc
			FROM geus_fdw.pltchemanalysis t2
		) 
		ON CONFLICT DO NOTHING
--		ON CONFLICT (guid) --ON CONSTRAINT pltchemanalysis_pkey 
--			DO UPDATE SET 
--				sampleid = excluded.sampleid,
--				compoundno = excluded.compoundno,
--				analysisno = excluded.analysisno,
--				qualitycontrol = excluded.qualitycontrol,
--				amountreported = excluded.amountreported,
--				unitreported = excluded.unitreported,
--				compoundreported = excluded.compoundreported,
--				amount = excluded.amount,
--				unit = excluded.unit,
--				"attribute" = excluded."attribute",
--				detectionlimit = excluded.detectionlimit,
--				filtering = excluded.filtering,
--				analysissite = excluded.analysissite,
--				"method" = excluded."method",
--				laboratory = excluded.laboratory,
--				remark = excluded.remark,
--				journalno = excluded.journalno,
--				preparation = excluded.preparation,
--				packaging = excluded.packaging,
--				preservation = excluded.preservation,
--				analysisid = excluded.analysisid,
--				reporteddetectionlimit = excluded.reporteddetectionlimit,
--				analysisdate = excluded.analysisdate,
--				laboratoryreceiveddate = excluded.laboratoryreceiveddate,
--				absoluteexpmeasuncertainty = excluded.absoluteexpmeasuncertainty,
--				relativeexpmeasuncertainty = excluded.relativeexpmeasuncertainty,
--				analysisresponsible = excluded.analysisresponsible,
--				accreditedindicator = excluded.accreditedindicator,
--				fractionationmethod = excluded.fractionationmethod,
--				analysisresultidentifier = excluded.analysisresultidentifier,
--				sortno = excluded.sortno,
--				guid = excluded.guid,
--				guid_pltchemsample = excluded.guid_pltchemsample,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate,
--				insertuser = excluded.insertuser,
--				updateuser = excluded.updateuser,
--				compoundno_desc = excluded.compoundno_desc,
--				unit_desc = excluded.unit_desc,
--				analysissite_desc = excluded.analysissite_desc
			--WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.pltchemanalysis 
		ADD PRIMARY KEY (guid)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.pltchemanalysis IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl pltchemanalysis'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'pltchemanalysis',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	CREATE INDEX IF NOT EXISTS pltchemanalysis_sampleid_idx
		ON jupiter.pltchemanalysis(sampleid)
	;
	
	CREATE INDEX IF NOT EXISTS pltchemanalysis_amount_idx
		ON jupiter.pltchemanalysis(amount)
	;
	
	CREATE INDEX IF NOT EXISTS pltchemanalysis_compoundno_idx
		ON jupiter.pltchemanalysis(compoundno)
	;

	-- give access to read user

	GRANT USAGE ON SCHEMA jupiter TO grukosreader;
	GRANT SELECT ON ALL TABLES IN SCHEMA jupiter TO grukosreader;

COMMIT;

