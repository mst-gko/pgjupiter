/*
 * author: Simon Makwarth <simak@mst.dk>
 * dependencies: foreign data wrapper between jupiter and geus' sql-gateway
 */

BEGIN;

	DROP TABLE IF EXISTS mstjupiter.grw_reports CASCADE;
	
	CREATE TABLE mstjupiter.grw_reports AS 
		(
			SELECT 
				gr.*,
				ST_Transform(ST_GeomFromText(gr.mbr_wkt, 4326), 25832) AS geom
			FROM geus_fdw.grw_reports gr 
			WHERE url IS NOT NULL
		)
	;

	ALTER TABLE mstjupiter.grw_reports 
		ADD COLUMN area_km2 NUMERIC DEFAULT NULL
	;

	UPDATE mstjupiter.grw_reports gr
		SET area_km2 = (st_area(gr.geom) / 1000^2)
	; 


	UPDATE mstjupiter.grw_reports gr
		SET geom = ST_Envelope(st_buffer(gr.geom, 100))
		WHERE ST_GeometryType(gr.geom) IN ('ST_Point', 'ST_LineString')
	; 
	
	UPDATE mstjupiter.grw_reports gr
		SET geom = ST_Envelope(k.geom)
		FROM "map".kommunegraense k
		WHERE gr.geom IS NULL
			AND k.komnavn = gr.kommunenavn
	;

	UPDATE mstjupiter.grw_reports gr
		SET geom = ST_Envelope(k.geom)
		FROM "map".kommunegraense k
		WHERE gr.geom IS NULL
			AND 
				(
					regexp_replace(REPLACE(lower(gr::TEXT), 'å', 'aa'), '\s+', '', 'g') 
						ILIKE lower(concat('%', k.komnavn, '%'))
					OR 
					regexp_replace(lower(gr::TEXT), '\s+', '', 'g') 
						ILIKE lower(concat('%', k.komnavn, '%'))
				)
	;

	UPDATE mstjupiter.grw_reports gr
		SET geom = ST_MakeEnvelope(336048.5, 6191554.5, 398541.4, 6230929.5, 25832)
		WHERE gr.geom IS NULL
	; 
	
	ALTER TABLE mstjupiter.grw_reports 
		ADD PRIMARY KEY (id);
	
	CREATE INDEX grw_reports_geom_idx
		ON mstjupiter.grw_reports 
		USING GIST (geom);
		
	CREATE INDEX grw_reports_rapportid_idx
		ON mstjupiter.grw_reports 
		USING BTREE (rapportid);
	
	CREATE INDEX grw_reports_bilagsnummer_idx
		ON mstjupiter.grw_reports 
		USING BTREE (bilagsnummer);
	
	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mstjupiter.grw_reports IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' simak Table metadata and bound geometry from Geus rapport database'
			     || '''';
		END
	$do$
	;

	GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO grukosreader;

COMMIT;
