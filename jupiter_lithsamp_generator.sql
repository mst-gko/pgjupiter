/*
    20220503 - JAKLA
    Query for lithsample lithology GIS geometry generator
    make_line(
	make_point(x($geometry), y($geometry) + ("top"  ) ),
	make_point(x($geometry), y($geometry) + ("Bottom"  ) ) )
*/
begin;

set search_path = jupiter, mstjupiter;

drop table if exists mstjupiter.lithsamp_generator;
create table mstjupiter.lithsamp_generator as
    select l.boreholeid, b.boreholeno, b.use, b.purpose, ct.use_text,
           l.top, l.bottom, l.rocksymbol, rs.dgu_symbol,
           CASE
                WHEN RIGHT(rocksymbol, 1) = 's' THEN '#ff5400'
                WHEN RIGHT(rocksymbol, 1) = 'g' THEN '#ff5400'
                WHEN RIGHT(rocksymbol, 1) = 'q' THEN '#ff00ff'
                WHEN RIGHT(rocksymbol, 1) = 'a' THEN '#ff8000'
                WHEN RIGHT(rocksymbol, 1) = 'r' THEN '#ccb3ff'
                WHEN rocksymbol = 'ml' THEN '#dbb800'
                WHEN rocksymbol = 'dl'THEN '#ffba33'
                WHEN rocksymbol IN ('ol', 'pl', 'll') THEN '#000cff'
                WHEN rocksymbol = 'gl' THEN '#b3ffff'
                WHEN rocksymbol NOT IN ('dl', 'ml', 'ol', 'pl', 'll') AND RIGHT(rocksymbol, 1) = 'l' THEN '#9d711f'
                WHEN RIGHT(rocksymbol, 1) = 'j' THEN '#9d711f'
                WHEN RIGHT(rocksymbol, 1) = 'i' THEN '#faf59e'
                WHEN RIGHT(rocksymbol, 1) = 'k' THEN '#80ff99'
                WHEN RIGHT(rocksymbol, 1) = 't' THEN '#deff00'
                WHEN RIGHT(rocksymbol, 1) = 'p' THEN '#c16544'
				WHEN RIGHT(rocksymbol, 2) = 'ed' THEN '#52ffff'
                ELSE '#efefef'
            END AS html_color,
           b.geom
    from jupiter.lithsamp l
    inner join jupiter.borehole b on b.boreholeid = l.boreholeid
    left join (select code, longtext use_text from jupiter.code where codetype = 855) ct on ct.code = b.use
    left join (select code, longtext dgu_symbol from jupiter.code where codetype = 44) rs on rs.code = l.rocksymbol
    order by l.boreholeid, l.top;
--[2022-05-02 09:15:35] 1,670,647 rows affected in 6 s 704 ms, codetype 855 and 44 are not consistant
--[2022-05-02 09:16:37] 2,340,615 rows affected in 8 s 565 ms
--[2022-05-02 13:35:14] 2,340,615 rows affected in 10 s 559 ms
comment on table mstjupiter.lithsamp_generator is '20220503 JAKLA - Query for borehole lithology QGIS geometry generator - Git: jupiter_lithsamp_generator.sql';

grant select on all tables in schema jupiter to grukosreader;
grant select on all tables in schema mstjupiter to grukosreader;

commit;