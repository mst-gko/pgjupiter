"""
    Scheduled task Python script checking if the geus exporttime of PCJupiterXL is less than one day.
    Result send as email.
"""
def send_email_win32com(subject, body):
    import win32com.client as win32

    MAIL_TO = 'jakla@mst.dk; simak@mst.dk; zicos@mst.dk; jsa@geus.dk'

    outlook = win32.Dispatch('outlook.application')
    mail = outlook.CreateItem(0)
    mail.To = MAIL_TO
    mail.Subject = subject
    # mail.Body = 'We use htmlbody instead'
    mail.HTMLBody = f'<h2>Message from MST PCJupiterXL database:<br><br> {body}</h2>'

    # To attach a file to the email (optional):
    # attachment = "Path to the attachment"
    # mail.Attachments.Add(attachment)

    mail.Send()


def get_exporttime():
    import psycopg2

    TABLE = 'jupiter.exporttime'
    DATABASE = 'pcjupiterxl'
    HOST = '10.33.131.50'
    USER = 'grukosreader'
    PW = 'gebr470ne'

    sql = f'select exporttime from {TABLE}'

    conn = psycopg2.connect(host=HOST, port=5432, user=USER, password=PW, database=DATABASE)
    cur = conn.cursor()
    cur.execute(sql)

    exporttime = cur.fetchone()[0]  # only one element in tuple

    conn.commit()
    cur.close()
    conn.close()

    return exporttime


if __name__ == "__main__":
    from datetime import datetime

    exporttime = get_exporttime()
    exporttime_days = (datetime.now() - exporttime).days

    if exporttime_days > 2:
        send_email_win32com('Automail: Error in daily MST PCJupiterXL restore!', f'Not good - database has NOT been restored - it is now {exporttime_days} days old (ignore message if it is Monday).\n\nexporttime_days = (datetime.now() - exporttime).days')
    #else:
        #send_email_win32com('Automail: Jupiter restore ok', f'Successive restored last PCJupiterXL of {exporttime}.')



