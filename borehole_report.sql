-- Rapportudtræk
-- 20240102
-- Jakob Lanstorp, Miljøstyrelsen
-- Server: localhost
-- DB: sandbox
-- Schema: temp_jakla
-- Gitlab: C:\gitlab\tilstandsvurdering\borehole_report.sql

/*
    What: Find boreholes with no lithology data, but with a bore report present
    Who: Jakob Lanstorp, Miljøstyrelsen
    Gitlab: ..gitlab\pgjupiter\borehole_report.sql
*/
set search_path to jupiter, public;
show search_path;

drop table if exists temp_jakla.borehole_no_lith_geom;
create table temp_jakla.borehole_no_lith_geom as
with bore_no_lith as (
        --Extract boreholes with no lithology
        select b.boreholeid, b.boreholeno, b.drilldepth, b.location, b.purpose, b.xutm, b.yutm, b.geom
        from jupiter.borehole b
        where not exists (select 1 from lithsamp s where s.boreholeid = b.boreholeid)
        and b.geom is not null
    ),
    bore_report as (
        -- Extract boreholes with bore report
        select b.boreholeid, d.fileid
        from jupiter.borehole b
        inner join (select * from jupiter.boredoc where doctype = 'B') d on b.boreholeid = d.boreholeid
    )
select b.boreholeid,
       b.boreholeno,
       b.purpose,
       b.drilldepth,
       b.location,
       b.xutm,
       b.yutm,
       b.geom,
       r.fileid,
       s.filetype,
       s.comments,
       s.url
    from bore_no_lith b
    inner join bore_report r using (boreholeid)
    inner join jupiter.storedoc s on s.fileid = r.fileid
    --where b.boreholeno = '  5.  701'
order by s.insertdate;
--[2024-01-03 11:29:22] 132,507 rows affected in 3 s 310 ms
--[2024-01-03 12:48:22] 85,358 rows affected in 3 s 239 ms -> and b.geom is not null
--[2024-01-03 14:02:57] 85,358 rows affected in 3 s 274 ms

--
select count(distinct boreholeid)
from temp_jakla.borehole_no_lith_geom;
-- Der er 61.338 borehuller (med eller uden geometri) uden lithologisk beskrivelse,
-- hvor der samtidigt er en eller flere borerapporter pr. borehul.
-- Der er 41.870 unikke borehuller med geometri

/*
    Now borehole localization, no lithology, handled above
*/
drop table if exists temp_jakla.borehole_location;
create table temp_jakla.borehole_location as
with borehole as (
    select boreholeid,
           boreholeno,
           namingsys,
           purpose,
           drilldepth,
           comments borehole_comment,
           various,
           xutm,
           yutm,
           utmzone,
           datum,
           mapsheet,
           sys34x,
           sys34y,
           sys34zone,
           locatmetho,
           locatquali,
           locatsourc,
           borhpostc,
           borhtownno,
           countyno,
           municipal,
           houseownas,
           landregno,
           drilstdate,
           drilendate,
           abandondat,
           prevborhno,
           borhtownno2007,
           locquali,
           loopareano,
           insertdate,
           updatedate,
           insertuser,
           updateuser,
           dataowner
           --geom
    from (select * from jupiter.borehole where geom is null) b
),
 bore_report as (
        -- Extract boreholes with bore report
        select b.boreholeid, d.fileid
        from jupiter.borehole b
        inner join (select * from jupiter.boredoc where doctype = 'B') d on b.boreholeid = d.boreholeid
 )
select b.*,
       concat('https://data.geus.dk/JupiterWWW/borerapport.jsp?dgunr=', replace(b.boreholeno, ' ', '')) url_boring,
       r.fileid,
       s.filetype,
       s.comments report_comment,
       s.url url_report
from borehole b
inner join bore_report r using (boreholeid)
inner join jupiter.storedoc s on s.fileid = r.fileid;
--31.178 Alle geom is null
--23.894 Sløjfet fjernet
--63.591 Når borerapporter er medtaget, dvs dgunr er ikke unik
--[2024-01-04 15:01:17] 63,591 rows affected in 2 s 129 ms

SELECT pg_read_file('pg_hba.conf');