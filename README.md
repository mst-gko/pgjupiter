# PG Jupiter

TODO
One Paragraph of project description goes here

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See maintains guide in wiki files for how to do this.

### Prerequisites

What things you need to install the software and how to install them

```
See maintains guide in wiki files.
```

### Installing

A step by step series of examples that tell you how to get a development env running

See maintains guide in wiki files.

## Script description

### Substance_All_data.sql
This script return a list chemistry analysis with no more than one value from each sample.


### Substance_All_data_filtered.sql
This script return a list chemistry analysis with no more than one value from each sample.
Boreholes without geographic position is left out, alongside samples without intakenumber.

### Substance_Arsenic_latest.sql
This script return a list of latest chemistry analysis for each intake on boreholes, where value for arsen and geographic position is present.

### Substance_Chloride_and_Sulphate_latest.sql
This script return a list of latest chemistry analysis for each intake on boreholes, where value for chloride, sulphate, and geographic position is present.
Note: if the same latest time there are two analysis, there one contain chloride and the other sulphate, but none of them contain both, then it will be left out.

### Substance_Chloride_latest.sql
This script return a list of latest chemistry analysis for each intake on boreholes, where value for chloride and geographic position is present.

### Substance_Iron_latest.sql
This script return a list of latest chemistry analysis for each intake on boreholes, where value for iron and geographic position is present.

### Substance_NVOC_latest.sql
This script return a list of latest chemistry analysis for each intake on boreholes, where value for Carbon,org,NVOC and geographic position is present.

### Substance_NitrateIronSulphate_watertype_latest.sql
This script return a list of latest chemistry analysis and water types for each intake on boreholes, where value for nitrate, Iron sulphate, and geographic position is present.
Note: if the same latest time there are two analysis, there one contain one component and the other the rest, but none of them contain all three, then it will be left out.

### Substance_Nitrate_and_Sulphate_latest.sql
This script return a list of latest chemistry analysis for each intake on boreholes, where value for nitrate, sulphate, and geographic position is present.
Note: if the same latest time there are two analysis, there one contain one component and the other the rest, but none of them contain all three, then it will be left out.

### Substance_Nitrate_latest.sql
This script return a list of latest chemistry analysis for each intake on boreholes, where value for nitrate and geographic position is present.

### Substance_Sodium_chloride_IonExchange_latest.sql
This script return a list of latest chemistry analysis and value for ionexchange for each intake on boreholes, where value for natrium, chloride, and geographic position is present.
Note: if the same latest timee there are two analysis, there one contain one component and the other the rest, but none of them contain all three, then it will be left out.

### Substance_Sulphate_latest.sql
This script return a list of latest chemistry analysis for each intake on boreholes, where value for sulphate and geographic position is present.


