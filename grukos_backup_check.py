"""
    Scheduled task Python script checking if the grukos backup is less than one day.
    Result send as email if above 0.
"""


def send_email_win32com(subject, body):
    import win32com.client as win32

    MAIL_TO = 'jakla@mst.dk; simak@mst.dk; zicos@mst.dk'

    outlook = win32.Dispatch('outlook.application')
    mail = outlook.CreateItem(0)
    mail.To = MAIL_TO
    mail.Subject = subject
    # mail.Body = 'We use htmlbody instead'
    mail.HTMLBody = f'<h2>Alert message from GRUKOS backup:<br><br> {body}</h2>'

    # To attach a file to the email (optional):
    # attachment = "Path to the attachment"
    # mail.Attachments.Add(attachment)

    mail.Send()


def get_backup_time(path='F:/GKO/Arkiv/Backup/GRUKOS'):
    import os.path
    import glob
    import datetime

    # fetch "last modification"-date of backupfile
    list_of_files = glob.glob(f'{path}/*.backup')
    # latest_file = max(list_of_files, key=os.path.getctime)
    latest_file = max(list_of_files, key=os.path.getmtime)
    timestamp_file = os.path.getmtime(latest_file)
    file_datetime = datetime.datetime.fromtimestamp(timestamp_file)

    # fetch current date
    today = datetime.datetime.now()

    # calculate days between file date and current date
    delta_datetime = today - file_datetime
    delta_days = delta_datetime.days

    return delta_days


if __name__ == "__main__":
    days_until_modification = get_backup_time()

    if days_until_modification != 0:
        print(f'Days since last backup: {days_until_modification}')
        send_email_win32com(
            'Automail: Error GRUKOS backup',
            f'Automail: Error GRUKOS backup did not execute. Days since last backup is {days_until_modification}'
        )
    else:
        print(f'Days since last backup: {days_until_modification}')
        # send_email_win32com('Automail: Jupiter restore ok',
        #                     f'Successive restored last PCJupiterXL, '
        #                     f'Days since last backup is {days_until_modification}.')





