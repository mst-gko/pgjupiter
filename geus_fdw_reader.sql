
CREATE VIEW fdw."abandgrout" AS 
    (
        SELECT *
        FROM geus_fdw."abandgrout"
    )
;
    
CREATE VIEW fdw."abandparam" AS 
    (
        SELECT *
        FROM geus_fdw."abandparam"
    )
;
    
CREATE VIEW fdw."analyses" AS 
    (
        SELECT *
        FROM geus_fdw."analyses"
    )
;
    
CREATE VIEW fdw."biostra" AS 
    (
        SELECT *
        FROM geus_fdw."biostra"
    )
;
    
CREATE VIEW fdw."borecatchcond" AS 
    (
        SELECT *
        FROM geus_fdw."borecatchcond"
    )
;
    
CREATE VIEW fdw."boredoc" AS 
    (
        SELECT *
        FROM geus_fdw."boredoc"
    )
;
    
CREATE VIEW fdw."borehole" AS 
    (
        SELECT *
        FROM geus_fdw."borehole"
    )
;
    
CREATE VIEW fdw."borehole_igis" AS 
    (
        SELECT *
        FROM geus_fdw."borehole_igis"
    )
;
    
CREATE VIEW fdw."borehole_injunction" AS 
    (
        SELECT *
        FROM geus_fdw."borehole_injunction"
    )
;
    
CREATE VIEW fdw."borhdiam" AS 
    (
        SELECT *
        FROM geus_fdw."borhdiam"
    )
;
    
CREATE VIEW fdw."borhrema" AS 
    (
        SELECT *
        FROM geus_fdw."borhrema"
    )
;
    
CREATE VIEW fdw."casing" AS 
    (
        SELECT *
        FROM geus_fdw."casing"
    )
;
    
CREATE VIEW fdw."catchperm" AS 
    (
        SELECT *
        FROM geus_fdw."catchperm"
    )
;
    
CREATE VIEW fdw."chrostra" AS 
    (
        SELECT *
        FROM geus_fdw."chrostra"
    )
;
    
CREATE VIEW fdw."climstra" AS 
    (
        SELECT *
        FROM geus_fdw."climstra"
    )
;
    
CREATE VIEW fdw."code" AS 
    (
        SELECT *
        FROM geus_fdw."code"
    )
;
    
CREATE VIEW fdw."code_103" AS 
    (
        SELECT *
        FROM geus_fdw."code_103"
    )
;
    
CREATE VIEW fdw."code_104" AS 
    (
        SELECT *
        FROM geus_fdw."code_104"
    )
;
    
CREATE VIEW fdw."code_105" AS 
    (
        SELECT *
        FROM geus_fdw."code_105"
    )
;
    
CREATE VIEW fdw."code_106" AS 
    (
        SELECT *
        FROM geus_fdw."code_106"
    )
;
    
CREATE VIEW fdw."code_107" AS 
    (
        SELECT *
        FROM geus_fdw."code_107"
    )
;
    
CREATE VIEW fdw."code_14" AS 
    (
        SELECT *
        FROM geus_fdw."code_14"
    )
;
    
CREATE VIEW fdw."code_148" AS 
    (
        SELECT *
        FROM geus_fdw."code_148"
    )
;
    
CREATE VIEW fdw."code_17" AS 
    (
        SELECT *
        FROM geus_fdw."code_17"
    )
;
    
CREATE VIEW fdw."code_2" AS 
    (
        SELECT *
        FROM geus_fdw."code_2"
    )
;
    
CREATE VIEW fdw."code_2143" AS 
    (
        SELECT *
        FROM geus_fdw."code_2143"
    )
;
    
CREATE VIEW fdw."code_2151" AS 
    (
        SELECT *
        FROM geus_fdw."code_2151"
    )
;
    
CREATE VIEW fdw."code_2153" AS 
    (
        SELECT *
        FROM geus_fdw."code_2153"
    )
;
    
CREATE VIEW fdw."code_218" AS 
    (
        SELECT *
        FROM geus_fdw."code_218"
    )
;
    
CREATE VIEW fdw."code_221" AS 
    (
        SELECT *
        FROM geus_fdw."code_221"
    )
;
    
CREATE VIEW fdw."code_222" AS 
    (
        SELECT *
        FROM geus_fdw."code_222"
    )
;
    
CREATE VIEW fdw."code_224" AS 
    (
        SELECT *
        FROM geus_fdw."code_224"
    )
;
    
CREATE VIEW fdw."code_225" AS 
    (
        SELECT *
        FROM geus_fdw."code_225"
    )
;
    
CREATE VIEW fdw."code_25" AS 
    (
        SELECT *
        FROM geus_fdw."code_25"
    )
;
    
CREATE VIEW fdw."code_26" AS 
    (
        SELECT *
        FROM geus_fdw."code_26"
    )
;
    
CREATE VIEW fdw."code_32" AS 
    (
        SELECT *
        FROM geus_fdw."code_32"
    )
;
    
CREATE VIEW fdw."code_35" AS 
    (
        SELECT *
        FROM geus_fdw."code_35"
    )
;
    
CREATE VIEW fdw."code_356" AS 
    (
        SELECT *
        FROM geus_fdw."code_356"
    )
;
    
CREATE VIEW fdw."code_357" AS 
    (
        SELECT *
        FROM geus_fdw."code_357"
    )
;
    
CREATE VIEW fdw."code_358" AS 
    (
        SELECT *
        FROM geus_fdw."code_358"
    )
;
    
CREATE VIEW fdw."code_359" AS 
    (
        SELECT *
        FROM geus_fdw."code_359"
    )
;
    
CREATE VIEW fdw."code_36" AS 
    (
        SELECT *
        FROM geus_fdw."code_36"
    )
;
    
CREATE VIEW fdw."code_366" AS 
    (
        SELECT *
        FROM geus_fdw."code_366"
    )
;
    
CREATE VIEW fdw."code_37" AS 
    (
        SELECT *
        FROM geus_fdw."code_37"
    )
;
    
CREATE VIEW fdw."code_38" AS 
    (
        SELECT *
        FROM geus_fdw."code_38"
    )
;
    
CREATE VIEW fdw."code_39" AS 
    (
        SELECT *
        FROM geus_fdw."code_39"
    )
;
    
CREATE VIEW fdw."code_40" AS 
    (
        SELECT *
        FROM geus_fdw."code_40"
    )
;
    
CREATE VIEW fdw."code_41" AS 
    (
        SELECT *
        FROM geus_fdw."code_41"
    )
;
    
CREATE VIEW fdw."code_42" AS 
    (
        SELECT *
        FROM geus_fdw."code_42"
    )
;
    
CREATE VIEW fdw."code_43" AS 
    (
        SELECT *
        FROM geus_fdw."code_43"
    )
;
    
CREATE VIEW fdw."code_44" AS 
    (
        SELECT *
        FROM geus_fdw."code_44"
    )
;
    
CREATE VIEW fdw."code_46" AS 
    (
        SELECT *
        FROM geus_fdw."code_46"
    )
;
    
CREATE VIEW fdw."code_48" AS 
    (
        SELECT *
        FROM geus_fdw."code_48"
    )
;
    
CREATE VIEW fdw."code_49" AS 
    (
        SELECT *
        FROM geus_fdw."code_49"
    )
;
    
CREATE VIEW fdw."code_5" AS 
    (
        SELECT *
        FROM geus_fdw."code_5"
    )
;
    
CREATE VIEW fdw."code_50" AS 
    (
        SELECT *
        FROM geus_fdw."code_50"
    )
;
    
CREATE VIEW fdw."code_51" AS 
    (
        SELECT *
        FROM geus_fdw."code_51"
    )
;
    
CREATE VIEW fdw."code_52" AS 
    (
        SELECT *
        FROM geus_fdw."code_52"
    )
;
    
CREATE VIEW fdw."code_53" AS 
    (
        SELECT *
        FROM geus_fdw."code_53"
    )
;
    
CREATE VIEW fdw."code_54" AS 
    (
        SELECT *
        FROM geus_fdw."code_54"
    )
;
    
CREATE VIEW fdw."code_56" AS 
    (
        SELECT *
        FROM geus_fdw."code_56"
    )
;
    
CREATE VIEW fdw."code_57" AS 
    (
        SELECT *
        FROM geus_fdw."code_57"
    )
;
    
CREATE VIEW fdw."code_58" AS 
    (
        SELECT *
        FROM geus_fdw."code_58"
    )
;
    
CREATE VIEW fdw."code_59" AS 
    (
        SELECT *
        FROM geus_fdw."code_59"
    )
;
    
CREATE VIEW fdw."code_60" AS 
    (
        SELECT *
        FROM geus_fdw."code_60"
    )
;
    
CREATE VIEW fdw."code_61" AS 
    (
        SELECT *
        FROM geus_fdw."code_61"
    )
;
    
CREATE VIEW fdw."code_62" AS 
    (
        SELECT *
        FROM geus_fdw."code_62"
    )
;
    
CREATE VIEW fdw."code_63" AS 
    (
        SELECT *
        FROM geus_fdw."code_63"
    )
;
    
CREATE VIEW fdw."code_64" AS 
    (
        SELECT *
        FROM geus_fdw."code_64"
    )
;
    
CREATE VIEW fdw."code_65" AS 
    (
        SELECT *
        FROM geus_fdw."code_65"
    )
;
    
CREATE VIEW fdw."code_66" AS 
    (
        SELECT *
        FROM geus_fdw."code_66"
    )
;
    
CREATE VIEW fdw."code_67" AS 
    (
        SELECT *
        FROM geus_fdw."code_67"
    )
;
    
CREATE VIEW fdw."code_68" AS 
    (
        SELECT *
        FROM geus_fdw."code_68"
    )
;
    
CREATE VIEW fdw."code_69" AS 
    (
        SELECT *
        FROM geus_fdw."code_69"
    )
;
    
CREATE VIEW fdw."code_70" AS 
    (
        SELECT *
        FROM geus_fdw."code_70"
    )
;
    
CREATE VIEW fdw."code_704" AS 
    (
        SELECT *
        FROM geus_fdw."code_704"
    )
;
    
CREATE VIEW fdw."code_706" AS 
    (
        SELECT *
        FROM geus_fdw."code_706"
    )
;
    
CREATE VIEW fdw."code_707" AS 
    (
        SELECT *
        FROM geus_fdw."code_707"
    )
;
    
CREATE VIEW fdw."code_708" AS 
    (
        SELECT *
        FROM geus_fdw."code_708"
    )
;
    
CREATE VIEW fdw."code_71" AS 
    (
        SELECT *
        FROM geus_fdw."code_71"
    )
;
    
CREATE VIEW fdw."code_710" AS 
    (
        SELECT *
        FROM geus_fdw."code_710"
    )
;
    
CREATE VIEW fdw."code_711" AS 
    (
        SELECT *
        FROM geus_fdw."code_711"
    )
;
    
CREATE VIEW fdw."code_712" AS 
    (
        SELECT *
        FROM geus_fdw."code_712"
    )
;
    
CREATE VIEW fdw."code_713" AS 
    (
        SELECT *
        FROM geus_fdw."code_713"
    )
;
    
CREATE VIEW fdw."code_714" AS 
    (
        SELECT *
        FROM geus_fdw."code_714"
    )
;
    
CREATE VIEW fdw."code_715" AS 
    (
        SELECT *
        FROM geus_fdw."code_715"
    )
;
    
CREATE VIEW fdw."code_717" AS 
    (
        SELECT *
        FROM geus_fdw."code_717"
    )
;
    
CREATE VIEW fdw."code_718" AS 
    (
        SELECT *
        FROM geus_fdw."code_718"
    )
;
    
CREATE VIEW fdw."code_72" AS 
    (
        SELECT *
        FROM geus_fdw."code_72"
    )
;
    
CREATE VIEW fdw."code_726" AS 
    (
        SELECT *
        FROM geus_fdw."code_726"
    )
;
    
CREATE VIEW fdw."code_727" AS 
    (
        SELECT *
        FROM geus_fdw."code_727"
    )
;
    
CREATE VIEW fdw."code_728" AS 
    (
        SELECT *
        FROM geus_fdw."code_728"
    )
;
    
CREATE VIEW fdw."code_729" AS 
    (
        SELECT *
        FROM geus_fdw."code_729"
    )
;
    
CREATE VIEW fdw."code_73" AS 
    (
        SELECT *
        FROM geus_fdw."code_73"
    )
;
    
CREATE VIEW fdw."code_730" AS 
    (
        SELECT *
        FROM geus_fdw."code_730"
    )
;
    
CREATE VIEW fdw."code_731" AS 
    (
        SELECT *
        FROM geus_fdw."code_731"
    )
;
    
CREATE VIEW fdw."code_732" AS 
    (
        SELECT *
        FROM geus_fdw."code_732"
    )
;
    
CREATE VIEW fdw."code_733" AS 
    (
        SELECT *
        FROM geus_fdw."code_733"
    )
;
    
CREATE VIEW fdw."code_734" AS 
    (
        SELECT *
        FROM geus_fdw."code_734"
    )
;
    
CREATE VIEW fdw."code_735" AS 
    (
        SELECT *
        FROM geus_fdw."code_735"
    )
;
    
CREATE VIEW fdw."code_736" AS 
    (
        SELECT *
        FROM geus_fdw."code_736"
    )
;
    
CREATE VIEW fdw."code_737" AS 
    (
        SELECT *
        FROM geus_fdw."code_737"
    )
;
    
CREATE VIEW fdw."code_738" AS 
    (
        SELECT *
        FROM geus_fdw."code_738"
    )
;
    
CREATE VIEW fdw."code_739" AS 
    (
        SELECT *
        FROM geus_fdw."code_739"
    )
;
    
CREATE VIEW fdw."code_74" AS 
    (
        SELECT *
        FROM geus_fdw."code_74"
    )
;
    
CREATE VIEW fdw."code_740" AS 
    (
        SELECT *
        FROM geus_fdw."code_740"
    )
;
    
CREATE VIEW fdw."code_741" AS 
    (
        SELECT *
        FROM geus_fdw."code_741"
    )
;
    
CREATE VIEW fdw."code_742" AS 
    (
        SELECT *
        FROM geus_fdw."code_742"
    )
;
    
CREATE VIEW fdw."code_743" AS 
    (
        SELECT *
        FROM geus_fdw."code_743"
    )
;
    
CREATE VIEW fdw."code_744" AS 
    (
        SELECT *
        FROM geus_fdw."code_744"
    )
;
    
CREATE VIEW fdw."code_745" AS 
    (
        SELECT *
        FROM geus_fdw."code_745"
    )
;
    
CREATE VIEW fdw."code_746" AS 
    (
        SELECT *
        FROM geus_fdw."code_746"
    )
;
    
CREATE VIEW fdw."code_747" AS 
    (
        SELECT *
        FROM geus_fdw."code_747"
    )
;
    
CREATE VIEW fdw."code_748" AS 
    (
        SELECT *
        FROM geus_fdw."code_748"
    )
;
    
CREATE VIEW fdw."code_749" AS 
    (
        SELECT *
        FROM geus_fdw."code_749"
    )
;
    
CREATE VIEW fdw."code_75" AS 
    (
        SELECT *
        FROM geus_fdw."code_75"
    )
;
    
CREATE VIEW fdw."code_750" AS 
    (
        SELECT *
        FROM geus_fdw."code_750"
    )
;
    
CREATE VIEW fdw."code_751" AS 
    (
        SELECT *
        FROM geus_fdw."code_751"
    )
;
    
CREATE VIEW fdw."code_752" AS 
    (
        SELECT *
        FROM geus_fdw."code_752"
    )
;
    
CREATE VIEW fdw."code_753" AS 
    (
        SELECT *
        FROM geus_fdw."code_753"
    )
;
    
CREATE VIEW fdw."code_754" AS 
    (
        SELECT *
        FROM geus_fdw."code_754"
    )
;
    
CREATE VIEW fdw."code_755" AS 
    (
        SELECT *
        FROM geus_fdw."code_755"
    )
;
    
CREATE VIEW fdw."code_756" AS 
    (
        SELECT *
        FROM geus_fdw."code_756"
    )
;
    
CREATE VIEW fdw."code_757" AS 
    (
        SELECT *
        FROM geus_fdw."code_757"
    )
;
    
CREATE VIEW fdw."code_758" AS 
    (
        SELECT *
        FROM geus_fdw."code_758"
    )
;
    
CREATE VIEW fdw."code_759" AS 
    (
        SELECT *
        FROM geus_fdw."code_759"
    )
;
    
CREATE VIEW fdw."code_760" AS 
    (
        SELECT *
        FROM geus_fdw."code_760"
    )
;
    
CREATE VIEW fdw."code_761" AS 
    (
        SELECT *
        FROM geus_fdw."code_761"
    )
;
    
CREATE VIEW fdw."code_762" AS 
    (
        SELECT *
        FROM geus_fdw."code_762"
    )
;
    
CREATE VIEW fdw."code_766" AS 
    (
        SELECT *
        FROM geus_fdw."code_766"
    )
;
    
CREATE VIEW fdw."code_767" AS 
    (
        SELECT *
        FROM geus_fdw."code_767"
    )
;
    
CREATE VIEW fdw."code_768" AS 
    (
        SELECT *
        FROM geus_fdw."code_768"
    )
;
    
CREATE VIEW fdw."code_77" AS 
    (
        SELECT *
        FROM geus_fdw."code_77"
    )
;
    
CREATE VIEW fdw."code_771" AS 
    (
        SELECT *
        FROM geus_fdw."code_771"
    )
;
    
CREATE VIEW fdw."code_774" AS 
    (
        SELECT *
        FROM geus_fdw."code_774"
    )
;
    
CREATE VIEW fdw."code_78" AS 
    (
        SELECT *
        FROM geus_fdw."code_78"
    )
;
    
CREATE VIEW fdw."code_79" AS 
    (
        SELECT *
        FROM geus_fdw."code_79"
    )
;
    
CREATE VIEW fdw."code_80" AS 
    (
        SELECT *
        FROM geus_fdw."code_80"
    )
;
    
CREATE VIEW fdw."code_807" AS 
    (
        SELECT *
        FROM geus_fdw."code_807"
    )
;
    
CREATE VIEW fdw."code_808" AS 
    (
        SELECT *
        FROM geus_fdw."code_808"
    )
;
    
CREATE VIEW fdw."code_81" AS 
    (
        SELECT *
        FROM geus_fdw."code_81"
    )
;
    
CREATE VIEW fdw."code_812" AS 
    (
        SELECT *
        FROM geus_fdw."code_812"
    )
;
    
CREATE VIEW fdw."code_813" AS 
    (
        SELECT *
        FROM geus_fdw."code_813"
    )
;
    
CREATE VIEW fdw."code_814" AS 
    (
        SELECT *
        FROM geus_fdw."code_814"
    )
;
    
CREATE VIEW fdw."code_815" AS 
    (
        SELECT *
        FROM geus_fdw."code_815"
    )
;
    
CREATE VIEW fdw."code_816" AS 
    (
        SELECT *
        FROM geus_fdw."code_816"
    )
;
    
CREATE VIEW fdw."code_822" AS 
    (
        SELECT *
        FROM geus_fdw."code_822"
    )
;
    
CREATE VIEW fdw."code_823" AS 
    (
        SELECT *
        FROM geus_fdw."code_823"
    )
;
    
CREATE VIEW fdw."code_83" AS 
    (
        SELECT *
        FROM geus_fdw."code_83"
    )
;
    
CREATE VIEW fdw."code_830" AS 
    (
        SELECT *
        FROM geus_fdw."code_830"
    )
;
    
CREATE VIEW fdw."code_831" AS 
    (
        SELECT *
        FROM geus_fdw."code_831"
    )
;
    
CREATE VIEW fdw."code_832" AS 
    (
        SELECT *
        FROM geus_fdw."code_832"
    )
;
    
CREATE VIEW fdw."code_833" AS 
    (
        SELECT *
        FROM geus_fdw."code_833"
    )
;
    
CREATE VIEW fdw."code_84" AS 
    (
        SELECT *
        FROM geus_fdw."code_84"
    )
;
    
CREATE VIEW fdw."code_841" AS 
    (
        SELECT *
        FROM geus_fdw."code_841"
    )
;
    
CREATE VIEW fdw."code_846" AS 
    (
        SELECT *
        FROM geus_fdw."code_846"
    )
;
    
CREATE VIEW fdw."code_847" AS 
    (
        SELECT *
        FROM geus_fdw."code_847"
    )
;
    
CREATE VIEW fdw."code_848" AS 
    (
        SELECT *
        FROM geus_fdw."code_848"
    )
;
    
CREATE VIEW fdw."code_851" AS 
    (
        SELECT *
        FROM geus_fdw."code_851"
    )
;
    
CREATE VIEW fdw."code_852" AS 
    (
        SELECT *
        FROM geus_fdw."code_852"
    )
;
    
CREATE VIEW fdw."code_853" AS 
    (
        SELECT *
        FROM geus_fdw."code_853"
    )
;
    
CREATE VIEW fdw."code_854" AS 
    (
        SELECT *
        FROM geus_fdw."code_854"
    )
;
    
CREATE VIEW fdw."code_855" AS 
    (
        SELECT *
        FROM geus_fdw."code_855"
    )
;
    
CREATE VIEW fdw."code_856" AS 
    (
        SELECT *
        FROM geus_fdw."code_856"
    )
;
    
CREATE VIEW fdw."code_86" AS 
    (
        SELECT *
        FROM geus_fdw."code_86"
    )
;
    
CREATE VIEW fdw."code_867" AS 
    (
        SELECT *
        FROM geus_fdw."code_867"
    )
;
    
CREATE VIEW fdw."code_868" AS 
    (
        SELECT *
        FROM geus_fdw."code_868"
    )
;
    
CREATE VIEW fdw."code_869" AS 
    (
        SELECT *
        FROM geus_fdw."code_869"
    )
;
    
CREATE VIEW fdw."code_870" AS 
    (
        SELECT *
        FROM geus_fdw."code_870"
    )
;
    
CREATE VIEW fdw."code_872" AS 
    (
        SELECT *
        FROM geus_fdw."code_872"
    )
;
    
CREATE VIEW fdw."code_91" AS 
    (
        SELECT *
        FROM geus_fdw."code_91"
    )
;
    
CREATE VIEW fdw."code_92" AS 
    (
        SELECT *
        FROM geus_fdw."code_92"
    )
;
    
CREATE VIEW fdw."code_926" AS 
    (
        SELECT *
        FROM geus_fdw."code_926"
    )
;
    
CREATE VIEW fdw."code_927" AS 
    (
        SELECT *
        FROM geus_fdw."code_927"
    )
;
    
CREATE VIEW fdw."code_928" AS 
    (
        SELECT *
        FROM geus_fdw."code_928"
    )
;
    
CREATE VIEW fdw."code_93" AS 
    (
        SELECT *
        FROM geus_fdw."code_93"
    )
;
    
CREATE VIEW fdw."code_97" AS 
    (
        SELECT *
        FROM geus_fdw."code_97"
    )
;
    
CREATE VIEW fdw."code_99" AS 
    (
        SELECT *
        FROM geus_fdw."code_99"
    )
;
    
CREATE VIEW fdw."codetype" AS 
    (
        SELECT *
        FROM geus_fdw."codetype"
    )
;
    
CREATE VIEW fdw."compoundgroup" AS 
    (
        SELECT *
        FROM geus_fdw."compoundgroup"
    )
;
    
CREATE VIEW fdw."compoundgrouplist" AS 
    (
        SELECT *
        FROM geus_fdw."compoundgrouplist"
    )
;
    
CREATE VIEW fdw."compoundlist" AS 
    (
        SELECT *
        FROM geus_fdw."compoundlist"
    )
;
    
CREATE VIEW fdw."contylst" AS 
    (
        SELECT *
        FROM geus_fdw."contylst"
    )
;
    
CREATE VIEW fdw."dataset" AS 
    (
        SELECT *
        FROM geus_fdw."dataset"
    )
;
    
CREATE VIEW fdw."datylst" AS 
    (
        SELECT *
        FROM geus_fdw."datylst"
    )
;
    
CREATE VIEW fdw."dbinfo" AS 
    (
        SELECT *
        FROM geus_fdw."dbinfo"
    )
;
    
CREATE VIEW fdw."deposenv" AS 
    (
        SELECT *
        FROM geus_fdw."deposenv"
    )
;
    
CREATE VIEW fdw."discharg" AS 
    (
        SELECT *
        FROM geus_fdw."discharg"
    )
;
    
CREATE VIEW fdw."dispensation" AS 
    (
        SELECT *
        FROM geus_fdw."dispensation"
    )
;
    
CREATE VIEW fdw."distylst" AS 
    (
        SELECT *
        FROM geus_fdw."distylst"
    )
;
    
CREATE VIEW fdw."drilmeth" AS 
    (
        SELECT *
        FROM geus_fdw."drilmeth"
    )
;
    
CREATE VIEW fdw."drwfirm" AS 
    (
        SELECT *
        FROM geus_fdw."drwfirm"
    )
;
    
CREATE VIEW fdw."drwfirmref" AS 
    (
        SELECT *
        FROM geus_fdw."drwfirmref"
    )
;
    
CREATE VIEW fdw."drwplant" AS 
    (
        SELECT *
        FROM geus_fdw."drwplant"
    )
;
    
CREATE VIEW fdw."drwplantcompanytype" AS 
    (
        SELECT *
        FROM geus_fdw."drwplantcompanytype"
    )
;
    
CREATE VIEW fdw."drwplantintake" AS 
    (
        SELECT *
        FROM geus_fdw."drwplantintake"
    )
;
    
CREATE VIEW fdw."drwplantlandreg" AS 
    (
        SELECT *
        FROM geus_fdw."drwplantlandreg"
    )
;
    
CREATE VIEW fdw."dsetgein" AS 
    (
        SELECT *
        FROM geus_fdw."dsetgein"
    )
;
    
CREATE VIEW fdw."dsetsw" AS 
    (
        SELECT *
        FROM geus_fdw."dsetsw"
    )
;
    
CREATE VIEW fdw."dsetylst" AS 
    (
        SELECT *
        FROM geus_fdw."dsetylst"
    )
;
    
CREATE VIEW fdw."exportmetadata" AS 
    (
        SELECT *
        FROM geus_fdw."exportmetadata"
    )
;
    
CREATE VIEW fdw."exporttime" AS 
    (
        SELECT *
        FROM geus_fdw."exporttime"
    )
;
    
CREATE VIEW fdw."fielddoc" AS 
    (
        SELECT *
        FROM geus_fdw."fielddoc"
    )
;
    
CREATE VIEW fdw."fieldwatcond" AS 
    (
        SELECT *
        FROM geus_fdw."fieldwatcond"
    )
;
    
CREATE VIEW fdw."fieldwatsoiltype" AS 
    (
        SELECT *
        FROM geus_fdw."fieldwatsoiltype"
    )
;
    
CREATE VIEW fdw."fossils" AS 
    (
        SELECT *
        FROM geus_fdw."fossils"
    )
;
    
CREATE VIEW fdw."frequencymodification" AS 
    (
        SELECT *
        FROM geus_fdw."frequencymodification"
    )
;
    
CREATE VIEW fdw."geinslst" AS 
    (
        SELECT *
        FROM geus_fdw."geinslst"
    )
;
    
CREATE VIEW fdw."geitylst" AS 
    (
        SELECT *
        FROM geus_fdw."geitylst"
    )
;
    
CREATE VIEW fdw."geotechnics" AS 
    (
        SELECT *
        FROM geus_fdw."geotechnics"
    )
;
    
CREATE VIEW fdw."gravpack" AS 
    (
        SELECT *
        FROM geus_fdw."gravpack"
    )
;
    
CREATE VIEW fdw."grout" AS 
    (
        SELECT *
        FROM geus_fdw."grout"
    )
;
    
CREATE VIEW fdw."grw_reports" AS 
    (
        SELECT *
        FROM geus_fdw."grw_reports"
    )
;
    
CREATE VIEW fdw."grwairanalysis" AS 
    (
        SELECT *
        FROM geus_fdw."grwairanalysis"
    )
;
    
CREATE VIEW fdw."grwairsample" AS 
    (
        SELECT *
        FROM geus_fdw."grwairsample"
    )
;
    
CREATE VIEW fdw."grwairsampledoc" AS 
    (
        SELECT *
        FROM geus_fdw."grwairsampledoc"
    )
;
    
CREATE VIEW fdw."grwairsampleremark" AS 
    (
        SELECT *
        FROM geus_fdw."grwairsampleremark"
    )
;
    
CREATE VIEW fdw."grwchemanalysis" AS 
    (
        SELECT *
        FROM geus_fdw."grwchemanalysis"
    )
;
    
CREATE VIEW fdw."grwchemsample" AS 
    (
        SELECT *
        FROM geus_fdw."grwchemsample"
    )
;
    
CREATE VIEW fdw."grwchemsampledoc" AS 
    (
        SELECT *
        FROM geus_fdw."grwchemsampledoc"
    )
;
    
CREATE VIEW fdw."grwsampleremark" AS 
    (
        SELECT *
        FROM geus_fdw."grwsampleremark"
    )
;
    
CREATE VIEW fdw."grwsoilanalysis" AS 
    (
        SELECT *
        FROM geus_fdw."grwsoilanalysis"
    )
;
    
CREATE VIEW fdw."grwsoilsample" AS 
    (
        SELECT *
        FROM geus_fdw."grwsoilsample"
    )
;
    
CREATE VIEW fdw."grwsoilsampledoc" AS 
    (
        SELECT *
        FROM geus_fdw."grwsoilsampledoc"
    )
;
    
CREATE VIEW fdw."grwsoilsampleremark" AS 
    (
        SELECT *
        FROM geus_fdw."grwsoilsampleremark"
    )
;
    
CREATE VIEW fdw."halttyls" AS 
    (
        SELECT *
        FROM geus_fdw."halttyls"
    )
;
    
CREATE VIEW fdw."heisylst" AS 
    (
        SELECT *
        FROM geus_fdw."heisylst"
    )
;
    
CREATE VIEW fdw."hemalt" AS 
    (
        SELECT *
        FROM geus_fdw."hemalt"
    )
;
    
CREATE VIEW fdw."hemcoil" AS 
    (
        SELECT *
        FROM geus_fdw."hemcoil"
    )
;
    
CREATE VIEW fdw."hemcoilgeotylst" AS 
    (
        SELECT *
        FROM geus_fdw."hemcoilgeotylst"
    )
;
    
CREATE VIEW fdw."hemcoilset" AS 
    (
        SELECT *
        FROM geus_fdw."hemcoilset"
    )
;
    
CREATE VIEW fdw."hemcoiltylst" AS 
    (
        SELECT *
        FROM geus_fdw."hemcoiltylst"
    )
;
    
CREATE VIEW fdw."hemdat" AS 
    (
        SELECT *
        FROM geus_fdw."hemdat"
    )
;
    
CREATE VIEW fdw."hemdatlist" AS 
    (
        SELECT *
        FROM geus_fdw."hemdatlist"
    )
;
    
CREATE VIEW fdw."hemdattylst" AS 
    (
        SELECT *
        FROM geus_fdw."hemdattylst"
    )
;
    
CREATE VIEW fdw."hemhea" AS 
    (
        SELECT *
        FROM geus_fdw."hemhea"
    )
;
    
CREATE VIEW fdw."hemline" AS 
    (
        SELECT *
        FROM geus_fdw."hemline"
    )
;
    
CREATE VIEW fdw."hempos" AS 
    (
        SELECT *
        FROM geus_fdw."hempos"
    )
;
    
CREATE VIEW fdw."hemsubtylst" AS 
    (
        SELECT *
        FROM geus_fdw."hemsubtylst"
    )
;
    
CREATE VIEW fdw."hist_borehole_closed_cause" AS 
    (
        SELECT *
        FROM geus_fdw."hist_borehole_closed_cause"
    )
;
    
CREATE VIEW fdw."hist_borehole_use" AS 
    (
        SELECT *
        FROM geus_fdw."hist_borehole_use"
    )
;
    
CREATE VIEW fdw."hltyplst" AS 
    (
        SELECT *
        FROM geus_fdw."hltyplst"
    )
;
    
CREATE VIEW fdw."ignordat" AS 
    (
        SELECT *
        FROM geus_fdw."ignordat"
    )
;
    
CREATE VIEW fdw."injunctionmeasure" AS 
    (
        SELECT *
        FROM geus_fdw."injunctionmeasure"
    )
;
    
CREATE VIEW fdw."intake" AS 
    (
        SELECT *
        FROM geus_fdw."intake"
    )
;
    
CREATE VIEW fdw."intakecatchment" AS 
    (
        SELECT *
        FROM geus_fdw."intakecatchment"
    )
;
    
CREATE VIEW fdw."jupfiede" AS 
    (
        SELECT *
        FROM geus_fdw."jupfiede"
    )
;
    
CREATE VIEW fdw."jupfield" AS 
    (
        SELECT *
        FROM geus_fdw."jupfield"
    )
;
    
CREATE VIEW fdw."jupheade" AS 
    (
        SELECT *
        FROM geus_fdw."jupheade"
    )
;
    
CREATE VIEW fdw."jupheadr" AS 
    (
        SELECT *
        FROM geus_fdw."jupheadr"
    )
;
    
CREATE VIEW fdw."juphiera" AS 
    (
        SELECT *
        FROM geus_fdw."juphiera"
    )
;
    
CREATE VIEW fdw."juplist" AS 
    (
        SELECT *
        FROM geus_fdw."juplist"
    )
;
    
CREATE VIEW fdw."jupmarks" AS 
    (
        SELECT *
        FROM geus_fdw."jupmarks"
    )
;
    
CREATE VIEW fdw."juptabde" AS 
    (
        SELECT *
        FROM geus_fdw."juptabde"
    )
;
    
CREATE VIEW fdw."juptable" AS 
    (
        SELECT *
        FROM geus_fdw."juptable"
    )
;
    
CREATE VIEW fdw."jupvfide" AS 
    (
        SELECT *
        FROM geus_fdw."jupvfide"
    )
;
    
CREATE VIEW fdw."jupvifie" AS 
    (
        SELECT *
        FROM geus_fdw."jupvifie"
    )
;
    
CREATE VIEW fdw."laydmlst" AS 
    (
        SELECT *
        FROM geus_fdw."laydmlst"
    )
;
    
CREATE VIEW fdw."laytylst" AS 
    (
        SELECT *
        FROM geus_fdw."laytylst"
    )
;
    
CREATE VIEW fdw."limitlist" AS 
    (
        SELECT *
        FROM geus_fdw."limitlist"
    )
;
    
CREATE VIEW fdw."lithsamp" AS 
    (
        SELECT *
        FROM geus_fdw."lithsamp"
    )
;
    
CREATE VIEW fdw."lithstra" AS 
    (
        SELECT *
        FROM geus_fdw."lithstra"
    )
;
    
CREATE VIEW fdw."locality" AS 
    (
        SELECT *
        FROM geus_fdw."locality"
    )
;
    
CREATE VIEW fdw."localityborhole" AS 
    (
        SELECT *
        FROM geus_fdw."localityborhole"
    )
;
    
CREATE VIEW fdw."localitygeotechdoc" AS 
    (
        SELECT *
        FROM geus_fdw."localitygeotechdoc"
    )
;
    
CREATE VIEW fdw."localityplant" AS 
    (
        SELECT *
        FROM geus_fdw."localityplant"
    )
;
    
CREATE VIEW fdw."localitysamplesite" AS 
    (
        SELECT *
        FROM geus_fdw."localitysamplesite"
    )
;
    
CREATE VIEW fdw."logcurve" AS 
    (
        SELECT *
        FROM geus_fdw."logcurve"
    )
;
    
CREATE VIEW fdw."logcurveinstrument" AS 
    (
        SELECT *
        FROM geus_fdw."logcurveinstrument"
    )
;
    
CREATE VIEW fdw."logdata" AS 
    (
        SELECT *
        FROM geus_fdw."logdata"
    )
;
    
CREATE VIEW fdw."loghea" AS 
    (
        SELECT *
        FROM geus_fdw."loghea"
    )
;
    
CREATE VIEW fdw."logproperty" AS 
    (
        SELECT *
        FROM geus_fdw."logproperty"
    )
;
    
CREATE VIEW fdw."lov_gen_bytetype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_gen_bytetype"
    )
;
    
CREATE VIEW fdw."lov_gen_datum" AS 
    (
        SELECT *
        FROM geus_fdw."lov_gen_datum"
    )
;
    
CREATE VIEW fdw."lov_gen_encodingtype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_gen_encodingtype"
    )
;
    
CREATE VIEW fdw."lov_gen_heightsystem" AS 
    (
        SELECT *
        FROM geus_fdw."lov_gen_heightsystem"
    )
;
    
CREATE VIEW fdw."lov_gen_utmzone" AS 
    (
        SELECT *
        FROM geus_fdw."lov_gen_utmzone"
    )
;
    
CREATE VIEW fdw."lov_ger_datasettype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_datasettype"
    )
;
    
CREATE VIEW fdw."lov_ger_datatype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_datatype"
    )
;
    
CREATE VIEW fdw."lov_ger_geoelinstrument" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_geoelinstrument"
    )
;
    
CREATE VIEW fdw."lov_ger_geoelinsttype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_geoelinsttype"
    )
;
    
CREATE VIEW fdw."lov_ger_hemaltitudetype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_hemaltitudetype"
    )
;
    
CREATE VIEW fdw."lov_ger_hemlinetype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_hemlinetype"
    )
;
    
CREATE VIEW fdw."lov_ger_logbhfluid" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_logbhfluid"
    )
;
    
CREATE VIEW fdw."lov_ger_logdirection" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_logdirection"
    )
;
    
CREATE VIEW fdw."lov_ger_logtype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_logtype"
    )
;
    
CREATE VIEW fdw."lov_ger_logwellcondition" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_logwellcondition"
    )
;
    
CREATE VIEW fdw."lov_ger_meplayoutdim" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_meplayoutdim"
    )
;
    
CREATE VIEW fdw."lov_ger_meplayouttype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_meplayouttype"
    )
;
    
CREATE VIEW fdw."lov_ger_mepsettingtype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_mepsettingtype"
    )
;
    
CREATE VIEW fdw."lov_ger_mepsubtype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_mepsubtype"
    )
;
    
CREATE VIEW fdw."lov_ger_model_settingtype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_model_settingtype"
    )
;
    
CREATE VIEW fdw."lov_ger_modelsubtype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_modelsubtype"
    )
;
    
CREATE VIEW fdw."lov_ger_modeltype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_modeltype"
    )
;
    
CREATE VIEW fdw."lov_ger_onedvlayerconstype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_onedvlayerconstype"
    )
;
    
CREATE VIEW fdw."lov_ger_onedvlayersettype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_onedvlayersettype"
    )
;
    
CREATE VIEW fdw."lov_ger_onedvmodelsubtype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_onedvmodelsubtype"
    )
;
    
CREATE VIEW fdw."lov_ger_onedvpossettype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_onedvpossettype"
    )
;
    
CREATE VIEW fdw."lov_ger_pachasettingtype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_pachasettingtype"
    )
;
    
CREATE VIEW fdw."lov_ger_pafilesettingtype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_pafilesettingtype"
    )
;
    
CREATE VIEW fdw."lov_ger_purpose" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_purpose"
    )
;
    
CREATE VIEW fdw."lov_ger_role" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_role"
    )
;
    
CREATE VIEW fdw."lov_ger_seiacquisetting" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_seiacquisetting"
    )
;
    
CREATE VIEW fdw."lov_ger_seifilecategory" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_seifilecategory"
    )
;
    
CREATE VIEW fdw."lov_ger_seifiletype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_seifiletype"
    )
;
    
CREATE VIEW fdw."lov_ger_seilowvellayer" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_seilowvellayer"
    )
;
    
CREATE VIEW fdw."lov_ger_seistacktype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_seistacktype"
    )
;
    
CREATE VIEW fdw."lov_ger_settingunit" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_settingunit"
    )
;
    
CREATE VIEW fdw."lov_ger_skytemdatalistkey" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_skytemdatalistkey"
    )
;
    
CREATE VIEW fdw."lov_ger_skytemdatcode" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_skytemdatcode"
    )
;
    
CREATE VIEW fdw."lov_ger_skytemdevicetype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_skytemdevicetype"
    )
;
    
CREATE VIEW fdw."lov_ger_skytemfloattype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_skytemfloattype"
    )
;
    
CREATE VIEW fdw."lov_ger_skytemnavcode" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_skytemnavcode"
    )
;
    
CREATE VIEW fdw."lov_ger_skytemnavlistkey" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_skytemnavlistkey"
    )
;
    
CREATE VIEW fdw."lov_ger_skytemreceiver" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_skytemreceiver"
    )
;
    
CREATE VIEW fdw."lov_ger_skytemsegfilttyp" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_skytemsegfilttyp"
    )
;
    
CREATE VIEW fdw."lov_ger_skytemsegsettype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_skytemsegsettype"
    )
;
    
CREATE VIEW fdw."lov_ger_skytemseqlistkey" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_skytemseqlistkey"
    )
;
    
CREATE VIEW fdw."lov_ger_skytemtransloop" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_skytemtransloop"
    )
;
    
CREATE VIEW fdw."lov_ger_skytemwaveform" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_skytemwaveform"
    )
;
    
CREATE VIEW fdw."lov_ger_software" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_software"
    )
;
    
CREATE VIEW fdw."lov_ger_softwaretype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_softwaretype"
    )
;
    
CREATE VIEW fdw."lov_ger_temsegcoupled" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_temsegcoupled"
    )
;
    
CREATE VIEW fdw."lov_ger_temsegsettingtype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_temsegsettingtype"
    )
;
    
CREATE VIEW fdw."lov_ger_temsubtype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_temsubtype"
    )
;
    
CREATE VIEW fdw."lov_ger_twodvmoddistanctyp" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_twodvmoddistanctyp"
    )
;
    
CREATE VIEW fdw."lov_ger_twodvmodistopomod" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_twodvmodistopomod"
    )
;
    
CREATE VIEW fdw."lov_ger_twodvsettingtype" AS 
    (
        SELECT *
        FROM geus_fdw."lov_ger_twodvsettingtype"
    )
;
    
CREATE VIEW fdw."mapsheet" AS 
    (
        SELECT *
        FROM geus_fdw."mapsheet"
    )
;
    
CREATE VIEW fdw."measuringstation" AS 
    (
        SELECT *
        FROM geus_fdw."measuringstation"
    )
;
    
CREATE VIEW fdw."mepdat" AS 
    (
        SELECT *
        FROM geus_fdw."mepdat"
    )
;
    
CREATE VIEW fdw."mepfilt" AS 
    (
        SELECT *
        FROM geus_fdw."mepfilt"
    )
;
    
CREATE VIEW fdw."mephea" AS 
    (
        SELECT *
        FROM geus_fdw."mephea"
    )
;
    
CREATE VIEW fdw."mepipdat" AS 
    (
        SELECT *
        FROM geus_fdw."mepipdat"
    )
;
    
CREATE VIEW fdw."mepipsetup" AS 
    (
        SELECT *
        FROM geus_fdw."mepipsetup"
    )
;
    
CREATE VIEW fdw."meplyout" AS 
    (
        SELECT *
        FROM geus_fdw."meplyout"
    )
;
    
CREATE VIEW fdw."meppulse" AS 
    (
        SELECT *
        FROM geus_fdw."meppulse"
    )
;
    
CREATE VIEW fdw."mepset" AS 
    (
        SELECT *
        FROM geus_fdw."mepset"
    )
;
    
CREATE VIEW fdw."mepsetda" AS 
    (
        SELECT *
        FROM geus_fdw."mepsetda"
    )
;
    
CREATE VIEW fdw."mepwave" AS 
    (
        SELECT *
        FROM geus_fdw."mepwave"
    )
;
    
CREATE VIEW fdw."minerals" AS 
    (
        SELECT *
        FROM geus_fdw."minerals"
    )
;
    
CREATE VIEW fdw."minorcomps" AS 
    (
        SELECT *
        FROM geus_fdw."minorcomps"
    )
;
    
CREATE VIEW fdw."model" AS 
    (
        SELECT *
        FROM geus_fdw."model"
    )
;
    
CREATE VIEW fdw."modsettyplst" AS 
    (
        SELECT *
        FROM geus_fdw."modsettyplst"
    )
;
    
CREATE VIEW fdw."modsubtyplst" AS 
    (
        SELECT *
        FROM geus_fdw."modsubtyplst"
    )
;
    
CREATE VIEW fdw."modsw" AS 
    (
        SELECT *
        FROM geus_fdw."modsw"
    )
;
    
CREATE VIEW fdw."moedition" AS 
    (
        SELECT *
        FROM geus_fdw."moedition"
    )
;
    
CREATE VIEW fdw."moeditionset" AS 
    (
        SELECT *
        FROM geus_fdw."moeditionset"
    )
;
    
CREATE VIEW fdw."msetylst" AS 
    (
        SELECT *
        FROM geus_fdw."msetylst"
    )
;
    
CREATE VIEW fdw."municipality2007" AS 
    (
        SELECT *
        FROM geus_fdw."municipality2007"
    )
;
    
CREATE VIEW fdw."odvdoi" AS 
    (
        SELECT *
        FROM geus_fdw."odvdoi"
    )
;
    
CREATE VIEW fdw."odvdoidepth" AS 
    (
        SELECT *
        FROM geus_fdw."odvdoidepth"
    )
;
    
CREATE VIEW fdw."odvfwres" AS 
    (
        SELECT *
        FROM geus_fdw."odvfwres"
    )
;
    
CREATE VIEW fdw."odvfwrestimeseries" AS 
    (
        SELECT *
        FROM geus_fdw."odvfwrestimeseries"
    )
;
    
CREATE VIEW fdw."odvfwrparamtyplst" AS 
    (
        SELECT *
        FROM geus_fdw."odvfwrparamtyplst"
    )
;
    
CREATE VIEW fdw."odvlacon" AS 
    (
        SELECT *
        FROM geus_fdw."odvlacon"
    )
;
    
CREATE VIEW fdw."odvlaset" AS 
    (
        SELECT *
        FROM geus_fdw."odvlaset"
    )
;
    
CREATE VIEW fdw."odvlayer" AS 
    (
        SELECT *
        FROM geus_fdw."odvlayer"
    )
;
    
CREATE VIEW fdw."odvmepda" AS 
    (
        SELECT *
        FROM geus_fdw."odvmepda"
    )
;
    
CREATE VIEW fdw."odvmeppo" AS 
    (
        SELECT *
        FROM geus_fdw."odvmeppo"
    )
;
    
CREATE VIEW fdw."odvmodse" AS 
    (
        SELECT *
        FROM geus_fdw."odvmodse"
    )
;
    
CREATE VIEW fdw."odvpdsep" AS 
    (
        SELECT *
        FROM geus_fdw."odvpdsep"
    )
;
    
CREATE VIEW fdw."odvpos" AS 
    (
        SELECT *
        FROM geus_fdw."odvpos"
    )
;
    
CREATE VIEW fdw."odvposet" AS 
    (
        SELECT *
        FROM geus_fdw."odvposet"
    )
;
    
CREATE VIEW fdw."odvstlst" AS 
    (
        SELECT *
        FROM geus_fdw."odvstlst"
    )
;
    
CREATE VIEW fdw."odvsuppllayer" AS 
    (
        SELECT *
        FROM geus_fdw."odvsuppllayer"
    )
;
    
CREATE VIEW fdw."odvsuppllayerparam" AS 
    (
        SELECT *
        FROM geus_fdw."odvsuppllayerparam"
    )
;
    
CREATE VIEW fdw."odvsuppllayerparamtylst" AS 
    (
        SELECT *
        FROM geus_fdw."odvsuppllayerparamtylst"
    )
;
    
CREATE VIEW fdw."olstylst" AS 
    (
        SELECT *
        FROM geus_fdw."olstylst"
    )
;
    
CREATE VIEW fdw."onedvmod" AS 
    (
        SELECT *
        FROM geus_fdw."onedvmod"
    )
;
    
CREATE VIEW fdw."opstylst" AS 
    (
        SELECT *
        FROM geus_fdw."opstylst"
    )
;
    
CREATE VIEW fdw."outtake" AS 
    (
        SELECT *
        FROM geus_fdw."outtake"
    )
;
    
CREATE VIEW fdw."pacescha" AS 
    (
        SELECT *
        FROM geus_fdw."pacescha"
    )
;
    
CREATE VIEW fdw."pacesdat" AS 
    (
        SELECT *
        FROM geus_fdw."pacesdat"
    )
;
    
CREATE VIEW fdw."paceshea" AS 
    (
        SELECT *
        FROM geus_fdw."paceshea"
    )
;
    
CREATE VIEW fdw."pacespos" AS 
    (
        SELECT *
        FROM geus_fdw."pacespos"
    )
;
    
CREATE VIEW fdw."pachaset" AS 
    (
        SELECT *
        FROM geus_fdw."pachaset"
    )
;
    
CREATE VIEW fdw."pafil" AS 
    (
        SELECT *
        FROM geus_fdw."pafil"
    )
;
    
CREATE VIEW fdw."pafilset" AS 
    (
        SELECT *
        FROM geus_fdw."pafilset"
    )
;
    
CREATE VIEW fdw."pama" AS 
    (
        SELECT *
        FROM geus_fdw."pama"
    )
;
    
CREATE VIEW fdw."pameas" AS 
    (
        SELECT *
        FROM geus_fdw."pameas"
    )
;
    
CREATE VIEW fdw."papro" AS 
    (
        SELECT *
        FROM geus_fdw."papro"
    )
;
    
CREATE VIEW fdw."paprotic" AS 
    (
        SELECT *
        FROM geus_fdw."paprotic"
    )
;
    
CREATE VIEW fdw."paprotime" AS 
    (
        SELECT *
        FROM geus_fdw."paprotime"
    )
;
    
CREATE VIEW fdw."party" AS 
    (
        SELECT *
        FROM geus_fdw."party"
    )
;
    
CREATE VIEW fdw."paticpos" AS 
    (
        SELECT *
        FROM geus_fdw."paticpos"
    )
;
    
CREATE VIEW fdw."pcstylst" AS 
    (
        SELECT *
        FROM geus_fdw."pcstylst"
    )
;
    
CREATE VIEW fdw."pfstylst" AS 
    (
        SELECT *
        FROM geus_fdw."pfstylst"
    )
;
    
CREATE VIEW fdw."plant_injunction" AS 
    (
        SELECT *
        FROM geus_fdw."plant_injunction"
    )
;
    
CREATE VIEW fdw."plantdoc" AS 
    (
        SELECT *
        FROM geus_fdw."plantdoc"
    )
;
    
CREATE VIEW fdw."pltairanalysis" AS 
    (
        SELECT *
        FROM geus_fdw."pltairanalysis"
    )
;
    
CREATE VIEW fdw."pltairsample" AS 
    (
        SELECT *
        FROM geus_fdw."pltairsample"
    )
;
    
CREATE VIEW fdw."pltairsampledoc" AS 
    (
        SELECT *
        FROM geus_fdw."pltairsampledoc"
    )
;
    
CREATE VIEW fdw."pltairsampleremark" AS 
    (
        SELECT *
        FROM geus_fdw."pltairsampleremark"
    )
;
    
CREATE VIEW fdw."pltchemanalysis" AS 
    (
        SELECT *
        FROM geus_fdw."pltchemanalysis"
    )
;
    
CREATE VIEW fdw."pltchemsample" AS 
    (
        SELECT *
        FROM geus_fdw."pltchemsample"
    )
;
    
CREATE VIEW fdw."pltchemsampledoc" AS 
    (
        SELECT *
        FROM geus_fdw."pltchemsampledoc"
    )
;
    
CREATE VIEW fdw."pltchemsampleremark" AS 
    (
        SELECT *
        FROM geus_fdw."pltchemsampleremark"
    )
;
    
CREATE VIEW fdw."poiairanalysis" AS 
    (
        SELECT *
        FROM geus_fdw."poiairanalysis"
    )
;
    
CREATE VIEW fdw."poiairsample" AS 
    (
        SELECT *
        FROM geus_fdw."poiairsample"
    )
;
    
CREATE VIEW fdw."poiairsampledoc" AS 
    (
        SELECT *
        FROM geus_fdw."poiairsampledoc"
    )
;
    
CREATE VIEW fdw."poiairsampleremark" AS 
    (
        SELECT *
        FROM geus_fdw."poiairsampleremark"
    )
;
    
CREATE VIEW fdw."poisoilanalysis" AS 
    (
        SELECT *
        FROM geus_fdw."poisoilanalysis"
    )
;
    
CREATE VIEW fdw."poisoilsample" AS 
    (
        SELECT *
        FROM geus_fdw."poisoilsample"
    )
;
    
CREATE VIEW fdw."poisoilsampledoc" AS 
    (
        SELECT *
        FROM geus_fdw."poisoilsampledoc"
    )
;
    
CREATE VIEW fdw."poisoilsampleremark" AS 
    (
        SELECT *
        FROM geus_fdw."poisoilsampleremark"
    )
;
    
CREATE VIEW fdw."poiwateranalysis" AS 
    (
        SELECT *
        FROM geus_fdw."poiwateranalysis"
    )
;
    
CREATE VIEW fdw."poiwatersample" AS 
    (
        SELECT *
        FROM geus_fdw."poiwatersample"
    )
;
    
CREATE VIEW fdw."poiwatersampledoc" AS 
    (
        SELECT *
        FROM geus_fdw."poiwatersampledoc"
    )
;
    
CREATE VIEW fdw."poiwatersampleremark" AS 
    (
        SELECT *
        FROM geus_fdw."poiwatersampleremark"
    )
;
    
CREATE VIEW fdw."project" AS 
    (
        SELECT *
        FROM geus_fdw."project"
    )
;
    
CREATE VIEW fdw."projectphase" AS 
    (
        SELECT *
        FROM geus_fdw."projectphase"
    )
;
    
CREATE VIEW fdw."projpurp" AS 
    (
        SELECT *
        FROM geus_fdw."projpurp"
    )
;
    
CREATE VIEW fdw."projregi" AS 
    (
        SELECT *
        FROM geus_fdw."projregi"
    )
;
    
CREATE VIEW fdw."pumping" AS 
    (
        SELECT *
        FROM geus_fdw."pumping"
    )
;
    
CREATE VIEW fdw."rcksymco" AS 
    (
        SELECT *
        FROM geus_fdw."rcksymco"
    )
;
    
CREATE VIEW fdw."recovery" AS 
    (
        SELECT *
        FROM geus_fdw."recovery"
    )
;
    
CREATE VIEW fdw."renaborh" AS 
    (
        SELECT *
        FROM geus_fdw."renaborh"
    )
;
    
CREATE VIEW fdw."samplesite" AS 
    (
        SELECT *
        FROM geus_fdw."samplesite"
    )
;
    
CREATE VIEW fdw."schludat" AS 
    (
        SELECT *
        FROM geus_fdw."schludat"
    )
;
    
CREATE VIEW fdw."schluhea" AS 
    (
        SELECT *
        FROM geus_fdw."schluhea"
    )
;
    
CREATE VIEW fdw."schluseg" AS 
    (
        SELECT *
        FROM geus_fdw."schluseg"
    )
;
    
CREATE VIEW fdw."screen" AS 
    (
        SELECT *
        FROM geus_fdw."screen"
    )
;
    
CREATE VIEW fdw."secanalysis" AS 
    (
        SELECT *
        FROM geus_fdw."secanalysis"
    )
;
    
CREATE VIEW fdw."secsample" AS 
    (
        SELECT *
        FROM geus_fdw."secsample"
    )
;
    
CREATE VIEW fdw."secsamplemix" AS 
    (
        SELECT *
        FROM geus_fdw."secsamplemix"
    )
;
    
CREATE VIEW fdw."segcolst" AS 
    (
        SELECT *
        FROM geus_fdw."segcolst"
    )
;
    
CREATE VIEW fdw."seiacquiset" AS 
    (
        SELECT *
        FROM geus_fdw."seiacquiset"
    )
;
    
CREATE VIEW fdw."seicdpposition" AS 
    (
        SELECT *
        FROM geus_fdw."seicdpposition"
    )
;
    
CREATE VIEW fdw."seihea" AS 
    (
        SELECT *
        FROM geus_fdw."seihea"
    )
;
    
CREATE VIEW fdw."seiline" AS 
    (
        SELECT *
        FROM geus_fdw."seiline"
    )
;
    
CREATE VIEW fdw."seilineinstrument" AS 
    (
        SELECT *
        FROM geus_fdw."seilineinstrument"
    )
;
    
CREATE VIEW fdw."seirawdatafile" AS 
    (
        SELECT *
        FROM geus_fdw."seirawdatafile"
    )
;
    
CREATE VIEW fdw."seistackfile" AS 
    (
        SELECT *
        FROM geus_fdw."seistackfile"
    )
;
    
CREATE VIEW fdw."seivelomodel" AS 
    (
        SELECT *
        FROM geus_fdw."seivelomodel"
    )
;
    
CREATE VIEW fdw."setunlst" AS 
    (
        SELECT *
        FROM geus_fdw."setunlst"
    )
;
    
CREATE VIEW fdw."skydat" AS 
    (
        SELECT *
        FROM geus_fdw."skydat"
    )
;
    
CREATE VIEW fdw."skydatlist" AS 
    (
        SELECT *
        FROM geus_fdw."skydatlist"
    )
;
    
CREATE VIEW fdw."skydatseg" AS 
    (
        SELECT *
        FROM geus_fdw."skydatseg"
    )
;
    
CREATE VIEW fdw."skydatval" AS 
    (
        SELECT *
        FROM geus_fdw."skydatval"
    )
;
    
CREATE VIEW fdw."skydev" AS 
    (
        SELECT *
        FROM geus_fdw."skydev"
    )
;
    
CREATE VIEW fdw."skydevtype" AS 
    (
        SELECT *
        FROM geus_fdw."skydevtype"
    )
;
    
CREATE VIEW fdw."skydevtypelst" AS 
    (
        SELECT *
        FROM geus_fdw."skydevtypelst"
    )
;
    
CREATE VIEW fdw."skyhea" AS 
    (
        SELECT *
        FROM geus_fdw."skyhea"
    )
;
    
CREATE VIEW fdw."skyline" AS 
    (
        SELECT *
        FROM geus_fdw."skyline"
    )
;
    
CREATE VIEW fdw."skylinepos" AS 
    (
        SELECT *
        FROM geus_fdw."skylinepos"
    )
;
    
CREATE VIEW fdw."skynav" AS 
    (
        SELECT *
        FROM geus_fdw."skynav"
    )
;
    
CREATE VIEW fdw."skynavdev" AS 
    (
        SELECT *
        FROM geus_fdw."skynavdev"
    )
;
    
CREATE VIEW fdw."skynavlist" AS 
    (
        SELECT *
        FROM geus_fdw."skynavlist"
    )
;
    
CREATE VIEW fdw."skynavval" AS 
    (
        SELECT *
        FROM geus_fdw."skynavval"
    )
;
    
CREATE VIEW fdw."skyprocdat" AS 
    (
        SELECT *
        FROM geus_fdw."skyprocdat"
    )
;
    
CREATE VIEW fdw."skyprocseg" AS 
    (
        SELECT *
        FROM geus_fdw."skyprocseg"
    )
;
    
CREATE VIEW fdw."skyseg" AS 
    (
        SELECT *
        FROM geus_fdw."skyseg"
    )
;
    
CREATE VIEW fdw."skysegfilt" AS 
    (
        SELECT *
        FROM geus_fdw."skysegfilt"
    )
;
    
CREATE VIEW fdw."skysegloop" AS 
    (
        SELECT *
        FROM geus_fdw."skysegloop"
    )
;
    
CREATE VIEW fdw."skysegset" AS 
    (
        SELECT *
        FROM geus_fdw."skysegset"
    )
;
    
CREATE VIEW fdw."skysegsetlst" AS 
    (
        SELECT *
        FROM geus_fdw."skysegsetlst"
    )
;
    
CREATE VIEW fdw."skysegtimes" AS 
    (
        SELECT *
        FROM geus_fdw."skysegtimes"
    )
;
    
CREATE VIEW fdw."skysegwave" AS 
    (
        SELECT *
        FROM geus_fdw."skysegwave"
    )
;
    
CREATE VIEW fdw."skyseq" AS 
    (
        SELECT *
        FROM geus_fdw."skyseq"
    )
;
    
CREATE VIEW fdw."skyseqlist" AS 
    (
        SELECT *
        FROM geus_fdw."skyseqlist"
    )
;
    
CREATE VIEW fdw."storedoc" AS 
    (
        SELECT *
        FROM geus_fdw."storedoc"
    )
;
    
CREATE VIEW fdw."substancefrequency" AS 
    (
        SELECT *
        FROM geus_fdw."substancefrequency"
    )
;
    
CREATE VIEW fdw."supplanalyseparam" AS 
    (
        SELECT *
        FROM geus_fdw."supplanalyseparam"
    )
;
    
CREATE VIEW fdw."swlst" AS 
    (
        SELECT *
        FROM geus_fdw."swlst"
    )
;
    
CREATE VIEW fdw."swtylst" AS 
    (
        SELECT *
        FROM geus_fdw."swtylst"
    )
;
    
CREATE VIEW fdw."tabledoc" AS 
    (
        SELECT *
        FROM geus_fdw."tabledoc"
    )
;
    
CREATE VIEW fdw."task" AS 
    (
        SELECT *
        FROM geus_fdw."task"
    )
;
    
CREATE VIEW fdw."tdvcell" AS 
    (
        SELECT *
        FROM geus_fdw."tdvcell"
    )
;
    
CREATE VIEW fdw."tdvfwres" AS 
    (
        SELECT *
        FROM geus_fdw."tdvfwres"
    )
;
    
CREATE VIEW fdw."tdvmodse" AS 
    (
        SELECT *
        FROM geus_fdw."tdvmodse"
    )
;
    
CREATE VIEW fdw."tdvmoset" AS 
    (
        SELECT *
        FROM geus_fdw."tdvmoset"
    )
;
    
CREATE VIEW fdw."tdvprpoi" AS 
    (
        SELECT *
        FROM geus_fdw."tdvprpoi"
    )
;
    
CREATE VIEW fdw."tdvstlst" AS 
    (
        SELECT *
        FROM geus_fdw."tdvstlst"
    )
;
    
CREATE VIEW fdw."temdat" AS 
    (
        SELECT *
        FROM geus_fdw."temdat"
    )
;
    
CREATE VIEW fdw."temfilt" AS 
    (
        SELECT *
        FROM geus_fdw."temfilt"
    )
;
    
CREATE VIEW fdw."temhea" AS 
    (
        SELECT *
        FROM geus_fdw."temhea"
    )
;
    
CREATE VIEW fdw."tempos" AS 
    (
        SELECT *
        FROM geus_fdw."tempos"
    )
;
    
CREATE VIEW fdw."temseg" AS 
    (
        SELECT *
        FROM geus_fdw."temseg"
    )
;
    
CREATE VIEW fdw."temseset" AS 
    (
        SELECT *
        FROM geus_fdw."temseset"
    )
;
    
CREATE VIEW fdw."temstlst" AS 
    (
        SELECT *
        FROM geus_fdw."temstlst"
    )
;
    
CREATE VIEW fdw."temwave" AS 
    (
        SELECT *
        FROM geus_fdw."temwave"
    )
;
    
CREATE VIEW fdw."topmolst" AS 
    (
        SELECT *
        FROM geus_fdw."topmolst"
    )
;
    
CREATE VIEW fdw."treatmentplant" AS 
    (
        SELECT *
        FROM geus_fdw."treatmentplant"
    )
;
    
CREATE VIEW fdw."tsetylst" AS 
    (
        SELECT *
        FROM geus_fdw."tsetylst"
    )
;
    
CREATE VIEW fdw."tsstylst" AS 
    (
        SELECT *
        FROM geus_fdw."tsstylst"
    )
;
    
CREATE VIEW fdw."twodvmod" AS 
    (
        SELECT *
        FROM geus_fdw."twodvmod"
    )
;
    
CREATE VIEW fdw."watlevel" AS 
    (
        SELECT *
        FROM geus_fdw."watlevel"
    )
;
    
CREATE VIEW fdw."watlevmp" AS 
    (
        SELECT *
        FROM geus_fdw."watlevmp"
    )
;
    
CREATE VIEW fdw."watlevround" AS 
    (
        SELECT *
        FROM geus_fdw."watlevround"
    )
;
    
CREATE VIEW fdw."wenndat" AS 
    (
        SELECT *
        FROM geus_fdw."wenndat"
    )
;
    
CREATE VIEW fdw."wennhea" AS 
    (
        SELECT *
        FROM geus_fdw."wennhea"
    )
;
    
CREATE VIEW fdw."wennpos" AS 
    (
        SELECT *
        FROM geus_fdw."wennpos"
    )
;
    
CREATE VIEW fdw."wrrcatchment" AS 
    (
        SELECT *
        FROM geus_fdw."wrrcatchment"
    )
;
    
CREATE VIEW fdw."wrrexport" AS 
    (
        SELECT *
        FROM geus_fdw."wrrexport"
    )
;
    
CREATE VIEW fdw."wrrfeebill" AS 
    (
        SELECT *
        FROM geus_fdw."wrrfeebill"
    )
;
    
CREATE VIEW fdw."wrrsupply" AS 
    (
        SELECT *
        FROM geus_fdw."wrrsupply"
    )
;
    
CREATE VIEW fdw."wrrwatcourdrain" AS 
    (
        SELECT *
        FROM geus_fdw."wrrwatcourdrain"
    )
;
    
CREATE VIEW fdw."wrrwatcourinflu" AS 
    (
        SELECT *
        FROM geus_fdw."wrrwatcourinflu"
    )
;
    
CREATE VIEW fdw."www_grwchemanalysiswater" AS 
    (
        SELECT *
        FROM geus_fdw."www_grwchemanalysiswater"
    )
;
    
CREATE VIEW fdw."www_grwchemsamplewater" AS 
    (
        SELECT *
        FROM geus_fdw."www_grwchemsamplewater"
    )
;
    
CREATE VIEW fdw."www_pltchemwater" AS 
    (
        SELECT *
        FROM geus_fdw."www_pltchemwater"
    )
;
    
CREATE VIEW fdw."www_search_page" AS 
    (
        SELECT *
        FROM geus_fdw."www_search_page"
    )
;
    
CREATE VIEW fdw."ztypelst" AS 
    (
        SELECT *
        FROM geus_fdw."ztypelst"
    )
;
    
GRANT USAGE ON SCHEMA jupiter TO grukosreader;
GRANT SELECT ON ALL TABLES IN SCHEMA jupiter TO grukosreader;

GRANT USAGE ON SCHEMA mstjupiter TO grukosreader;
GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO grukosreader;

GRANT USAGE ON SCHEMA fdw TO grukosreader;
GRANT SELECT ON ALL TABLES IN SCHEMA fdw TO grukosreader;
