/*
    JAKLA:  20240923 Bestilling #15022 Miljøministeriets Departementet

    Find alle pesticid overskridelser i grundvandsboringer
    Find hvor mange procent af en kommunens aktive boringer som er overskredet pesticidgrænsen

    KS: ANNAR
    Code: "..\gitlab\pgjupiter\pesticide_dept_20240923.sql"
*/

/*
    Find EXCENDANCES in active boreholes (analysis from 2023-2024) in V01, V02 plants pr. municipality
*/
drop table if exists temp_jakla.temp_kommuner_excedance;
create table temp_jakla.temp_kommuner_exedance as
with count_active_excedance_borehole as (
    select count(*) cnt, kommune
    from (
        select count(*), boreholeno, kommune
        from mstjupiter.pesticide_gvk_exceedance
        where extract(year from sampledate) in (2023, 2024) and companytype in ('V01', 'V02')
        group by boreholeno, kommune
        order by 1 desc
    ) cnt
    group by kommune
    order by 1 desc
),
-- Number of active boreholes in each municipality (if analysis, then active)
count_active_borehole as (
    select count(*) cnt, kommune
    from (
        select count(*), boreholeno, kommune
        from mstjupiter.pesticide_gvk_all
        where extract(year from sampledate) in (2023, 2024) and companytype in ('V01', 'V02')
        group by boreholeno, kommune
        order by 1 desc
    ) sub
    group by kommune
)
select coalesce(b2.cnt, 0) count_excedance, b1.cnt count_all,
       coalesce(round((b2.cnt::float / b1.cnt::float * 100)::numeric, 1),0) pct_excedance,
       b1.kommune
from count_active_borehole b1
left join count_active_excedance_borehole b2 using (kommune)
order by 3 desc;

/*
    Find PRESENCE in active boreholes (analysis from 2023-2024) in V01, V02 plants pr. municipality
 */
drop table if exists temp_jakla.temp_kommuner_presance;
create table temp_jakla.temp_kommuner_presance as
with count_active_presence_borehole as (
    select count(*) cnt, kommune
    from (
        select count(*), boreholeno, kommune
        from mstjupiter.pesticide_gvk_presence
        where extract(year from sampledate) in (2023, 2024) and companytype in ('V01', 'V02')
        group by boreholeno, kommune
        order by 1 desc
    ) cnt
    group by kommune
    order by 1 desc
),
-- Number of active boreholes in each municipality (if analysis, then active)
count_active_borehole as (
    select count(*) cnt, kommune
    from (
        select count(*), boreholeno, kommune
        from mstjupiter.pesticide_gvk_all
        where extract(year from sampledate) in (2023, 2024) and companytype in ('V01', 'V02')
        group by boreholeno, kommune
        order by 1 desc
    ) sub
    group by kommune
)
select coalesce(b2.cnt, 0) count_presance, b1.cnt count_all,
       coalesce(round((b2.cnt::float / b1.cnt::float * 100)::numeric, 1),0) pct_presance,
       b1.kommune
from count_active_borehole b1
left join count_active_presence_borehole b2 using (kommune)
order by 3 desc;
--[2024-09-26 10:14:17] 96 rows retrieved starting from 1 in 2 s 199 ms (execution: 2 s 136 ms, fetching: 63 ms)


--select longtext, code from jupiter.code
--where codetype = 808 and code::int < 900;

select count_excedance,
       e.count_all count_all_e,
       pct_excedance,
       count_presance,
       p.count_all count_all_p,
       pct_presance,
       p.kommune
from temp_jakla.temp_kommuner_exedance e
inner join temp_jakla.temp_kommuner_presance p on e.kommune = p.kommune
where e.count_all <> p.count_all;