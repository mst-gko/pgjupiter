/*
 * lith_in_borscreen
 * descr: fetch lithology description within borehole screens from jupiter db
 * author: Simon Makwarth <simak@mst.dk>
 */

DROP TABLE IF EXISTS mstjupiter.lith_in_borscreen CASCADE;

CREATE TABLE mstjupiter.lith_in_borscreen AS 
	(
		WITH
			lith AS 
				(
					SELECT 
						ls.boreholeid, 
						ls.rocksymbol,
						ls.totaldescr,
						ls.top,
						ls.bottom
					FROM jupiter.lithsamp ls
				)
		SELECT DISTINCT ON (s.guid, i.guid)
			s.guid::uuid AS guid_screen,	
			i.guid::uuid AS guid_intake,
			string_agg(DISTINCT l.rocksymbol, ', ') AS lith_screen_agg, 
			string_agg(DISTINCT l.totaldescr, ' || ') AS totaldescr_agg
		FROM jupiter.intake i 
		INNER JOIN jupiter.screen s ON i.guid = s.guid_intake 
		INNER JOIN lith l ON s.boreholeid = l.boreholeid
		WHERE l.top < s.bottom 
			AND l.bottom > s.top
		GROUP BY s.guid, i.guid
	)
;