/*
    Queries for QGIS QLR Browser (Kortskab)
    Jakob Lanstorp Janunar 2020
*/
SET search_path TO jupiter;
GRANT USAGE ON SCHEMA mstjupiter TO grukosreader;
GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO grukosreader;

--GRANT USAGE ON SCHEMA jupiter TO grukos_reader;
--GRANT SELECT ON ALL TABLES IN SCHEMA jupiter TO grukos_reader;

--SET search_path TO fohm;
--GRANT USAGE ON SCHEMA fohm TO grukosreader;
--GRANT SELECT ON ALL TABLES IN SCHEMA fohm TO grukosreader;

/*
    Tema:   Alle boringer med deres boringsanvendelse
    table:   jupiter.qlr_boring
    TOC:    Boring
    QLR:    10~Boring.qlr
*/
BEGIN;
    DROP TABLE IF EXISTS mstjupiter.qlr_boring;

    CREATE TABLE mstjupiter.qlr_boring AS (
        SELECT
               row_number() OVER (ORDER BY b.boreholeno DESC) AS id,
               c.longtext,
               b.*,
               'https://data.geus.dk/JupiterWWW/borerapport.jsp?borid=' || b.boreholeid AS url_bor
        FROM jupiter.borehole b
        LEFT JOIN jupiter.code c on c.code = b.use and c.codetype = 855
    );

    ALTER TABLE mstjupiter.qlr_boring OWNER TO grukosadmin;
    GRANT ALL ON TABLE mstjupiter.qlr_boring TO grukosadmin;

    GRANT SELECT ON TABLE jupiter.borehole TO grukosreader;
    GRANT SELECT ON TABLE mstjupiter.qlr_boring TO grukosreader;
COMMIT;


/*
    Tema:   Alle vandforsyningsboringer
    table:   jupiter.qlr_vandforsyningsboring
    TOC:    Vandforsyningsboring
    QLR:    20~Vandforsyningsboring.qlr
*/
BEGIN;
    DROP TABLE IF EXISTS mstjupiter.qlr_vandforsyningsboring;

    CREATE TABLE mstjupiter.qlr_vandforsyningsboring AS (
        SELECT DISTINCT
            row_number() OVER (ORDER BY b.boreholeno, p.plantid DESC) AS id,
            c.longtext,
            t.companytype,
            p.plantid,
            b.*,
            'https://data.geus.dk/JupiterWWW/borerapport.jsp?borid=' || b.boreholeid AS url_bor
        FROM jupiter.borehole b
            INNER JOIN jupiter.drwplantintake pi USING (boreholeid)
            INNER JOIN jupiter.drwplant p ON p.plantid = pi.plantid
            INNER JOIN jupiter.drwplantcompanytype t on t.plantid = pi.plantid
            INNER JOIN jupiter.code c on c.code = t.companytype
   );

    ALTER TABLE mstjupiter.qlr_vandforsyningsboring OWNER TO grukosadmin;
    GRANT ALL ON TABLE mstjupiter.qlr_vandforsyningsboring TO grukosadmin;

    GRANT SELECT ON TABLE jupiter.borehole TO grukosreader;
    GRANT SELECT ON TABLE jupiter.drwplantintake TO grukosreader;
    GRANT SELECT ON TABLE jupiter.drwplantcompanytype TO grukosreader;
    GRANT SELECT ON TABLE jupiter.code TO grukosreader;

    GRANT SELECT ON TABLE mstjupiter.qlr_vandforsyningsboring TO grukosreader;
COMMIT;


/*
    Tema:   Almene vandforsyningsboring
    table:   jupiter.qlr_almen_vandforsyningsboring
    TOC:    Almen_vandforsyningsboring
    QLR:    30~Almen_vandforsyningsboring.qlr
*/
BEGIN;
    DROP TABLE IF EXISTS mstjupiter.qlr_almen_vandforsyningsboring;

    CREATE TABLE mstjupiter.qlr_almen_vandforsyningsboring AS (
        SELECT
            row_number() OVER (ORDER BY b.boreholeno, p.plantid DESC) AS id,
            c1.longtext companytype_longtext,
            t.companytype,
            c2.longtext AS use_longtext,
            pi.intakeno,
            b.* FROM jupiter.borehole b
        INNER JOIN jupiter.drwplantintake pi USING (boreholeid)
        INNER JOIN jupiter.drwplant p ON p.plantid = pi.plantid
        INNER JOIN jupiter.drwplantcompanytype t on t.plantid = pi.plantid
        INNER JOIN jupiter.code c1 on c1.code = t.companytype AND c1.codetype = 852
        INNER JOIN jupiter.code c2 on c2.code = b.use AND c2.codetype = 855
        WHERE t.companytype in ('V01', 'V02')
    );

    ALTER TABLE mstjupiter.qlr_almen_vandforsyningsboring OWNER TO grukosadmin;
    GRANT ALL ON TABLE mstjupiter.qlr_almen_vandforsyningsboring TO grukosadmin;

    GRANT SELECT ON TABLE jupiter.borehole TO grukosreader;
    GRANT SELECT ON TABLE jupiter.drwplantintake TO grukosreader;
    GRANT SELECT ON TABLE jupiter.drwplantcompanytype TO grukosreader;
    GRANT SELECT ON TABLE jupiter.code TO grukosreader;

    GRANT SELECT ON TABLE mstjupiter.qlr_almen_vandforsyningsboring TO grukosreader;
COMMIT;

/*
    Udtræk til Ingeniøren 20211028
    - Hvor mange drikkevandsboringer er der i hele Danmark?
    - Hvor mange af dem er der gennem tidens løb taget PFOS-prøver fra?
*/
with water_well as (
    select distinct (boreholeid),
                    intakeno,
                    companytype_longtext
    from mstjupiter.qlr_almen_vandforsyningsboring
    where "use_longtext" <> 'Sløjfet/opgivet/opfyldt boring'
      and "use_longtext" in ('Vandforsyningsboring/nødvandsforsyningsboring/sænkning', 'Vandværksboring',
                             'Privat husholdning/drikkevand udenfor vandværk')
--not in ('V01', 'V02');
--9104 records
--2448 companytype = 'V01'
--6656 companytype = 'V02'
), pfos_wells as (
    select distinct (s.boreholeid)
    from jupiter.grwchemsample s
    inner join jupiter.grwchemanalysis a on s.sampleid = a.sampleid
    where a.compoundno = 2268
----7788
)
select distinct w.boreholeid
from water_well w
inner join pfos_wells p on w.boreholeid = p.boreholeid;
--1131 poster



/*
    Table:   jupiter.qlr_anlaeg
    Tema:   Alle anlæg
    TOC:    Anlæg
    QLR:    40~Anlæg.qlr
*/
BEGIN;
    DROP TABLE IF EXISTS mstjupiter.anlaeg;

    CREATE TABLE mstjupiter.anlaeg AS (
       SELECT
            row_number() over () AS id,
            p.plantid anlaegsid,
            p.supplant,
            p.municipalityno2007 kommunenr,
            p.plantname anlaegnavn,
            p.plantaddress anlaegadr,
            p.plantpostalcode anlaegpostnr,
            dataowner.longtext  dataejer,
            active.longtext aktiv,
            permit.longtext tilladelsesstatus,
            companytype.longtext virksomhedstype,
            planttype.longtext anlaegstype,
            --indvformaal.longtext indvindingsformaal,
            watertype.longtext vandtype,
            concat('https://data.geus.dk/JupiterWWW/anlaeg.jsp?anlaegid=', p.plantid) url_plant,
            p.locatremark kommentar,
            p.geom
        FROM jupiter.drwplant p
            LEFT JOIN jupiter.drwplantcompanytype t using (plantid)
            LEFT JOIN (select * from jupiter.code where codetype = 734) planttype on planttype.code = p.planttype::TEXT
            --LEFT JOIN (select * from jupiter.code where codetype = 741) indvformaal on indvformaal.code = p.vrrpurpose::text
            LEFT JOIN (select * from jupiter.code where codetype = 737 ) watertype on watertype.code = p.watertype::text
            LEFT JOIN (select * from jupiter.code where codetype = 733) active on active.code = p.active::text
            LEFT JOIN (select * from jupiter.code where codetype = 867 ) dataowner on dataowner.code = p.dataowner::text
            LEFT JOIN (select * from jupiter.code where codetype = 852) companytype ON companytype.code = t.companytype
            LEFT JOIN (select * from jupiter.code where codetype = 735) permit ON permit.code = p.permit::text);

    COMMENT ON TABLE mstjupiter.anlaeg is '20211004 Water plant with most codes translated.';

    CREATE INDEX anlaeg_geom_idx ON mstjupiter.anlaeg USING gist (geom);

    ALTER TABLE mstjupiter.anlaeg OWNER TO grukosadmin;
    GRANT ALL ON TABLE mstjupiter.anlaeg TO grukosadmin;

    GRANT SELECT ON TABLE jupiter.drwplant TO grukosreader;
    GRANT SELECT ON TABLE jupiter.drwplantcompanytype TO grukosreader;

    GRANT SELECT ON TABLE mstjupiter.anlaeg TO grukosreader;
COMMIT;

/*
    Table:   jupiter.qlr_anlaeg_aktive
    Tema:   Alle anlæg
    TOC:    Anlæg
    QLR:    40~Anlæg.qlr
*/
BEGIN;
    DROP TABLE IF EXISTS mstjupiter.anlaeg_aktiv;

    CREATE TABLE mstjupiter.anlaeg_aktiv AS
       SELECT
           row_number() over () AS id,
           a.anlaegsid,
           a.supplant,
           a.kommunenr,
           a.anlaegnavn,
           a.anlaegadr,
           a.anlaegpostnr,
           a.dataejer,
           a.aktiv,
           a.virksomhedstype,
           a.anlaegstype,
           --a.indvindingsformaal,
           a.vandtype,
           a.url_plant,
           a.kommentar,
           c.amount,
           c.startdate,
           c.enddate,
           a.geom
       FROM mstjupiter.anlaeg a
       INNER JOIN (
           SELECT * FROM jupiter.wrrcatchment
           WHERE amount > 0 AND EXTRACT(YEAR FROM now())-1 = EXTRACT(YEAR FROM enddate)) c on c.plantid = a.anlaegsid
       WHERE COALESCE(a.aktiv, ' ') NOT ILIKE 'Inaktiv' AND COALESCE(a.tilladelsesstatus, ' ') NOT ILIKE ('Tilladelse bortfaldet') ;

    COMMENT ON TABLE mstjupiter.anlaeg_aktiv is '20211005 Active plants.';

    CREATE INDEX anlaeg_aktiv_geom_idx ON mstjupiter.anlaeg_aktiv USING gist (geom);

    ALTER TABLE mstjupiter.anlaeg_aktiv OWNER TO grukosadmin;
    GRANT ALL ON TABLE mstjupiter.anlaeg_aktiv TO grukosadmin;

    GRANT SELECT ON TABLE jupiter.drwplant TO grukosreader;
    GRANT SELECT ON TABLE jupiter.drwplantcompanytype TO grukosreader;

    GRANT SELECT ON TABLE mstjupiter.anlaeg_aktiv TO grukosreader;
COMMIT;


/*
    table:   jupiter.qlr_almen_vandvaerk
    Tema:   Find almene vandværker (Et alment vandværker, som forsyner mere end 9 husholdninger)
    TOC:    Almen_vandværk
    QLR:    50~Almen_vandværk.qlr

    Note:
    V01=Offentlige fælles vandforsyningsanlæg
    V02=Private fælles vandforsyningsanlæg
    Some plants has a null geometry
    Add latest wrrcatchment data for indicating if plant is active
*/
BEGIN;
    DROP TABLE IF EXISTS mstjupiter.almen_vandvaerk;

    CREATE TABLE mstjupiter.almen_vandvaerk AS (
        SELECT DISTINCT ON (p.plantid)
               row_number() OVER (ORDER BY plantid) AS id,
               p.planttype anlaegsid,
               p.municipalityno2007 kommunenr,
               p.plantname anlaegsnavn,
               p.plantaddress anlaegsadr,
               p.plantpostalcode anlaegspostnr,
               pc.companytype virksomhedstype,
			   p.active,
               c.longtext virksonhedstypetext,
               'https://data.geus.dk/JupiterWWW/anlaeg.jsp?anlaegid=' ||  p.plantid::TEXT AS url_plant,
               wrrc.startdate::DATE,
               wrrc.enddate::DATE,
               wrrc.amount,
               p.geom
        FROM jupiter.drwplant p
        INNER JOIN jupiter.drwplantcompanytype pc using (plantid)
        INNER JOIN jupiter.code c on c.code = pc.companytype
        LEFT JOIN (SELECT * FROM jupiter.wrrcatchment WHERE amount > 0 ) wrrc USING (plantid)
        WHERE pc.companytype IN ('V01', 'V02') AND p.geom IS NOT NULL
        ORDER BY p.plantid, wrrc.enddate::DATE DESC
    );

--select 1 mycol union select 2 mycol union select 3 mycol  order by 1 DESC;

    COMMENT ON TABLE mstjupiter.almen_vandvaerk is '20211005 Almene vandværk inklusiv sidste indvindingsindmeldelse.';

    CREATE INDEX almem_vandvaerk_geom_idx ON mstjupiter.almen_vandvaerk USING gist (geom);

    ALTER TABLE mstjupiter.almen_vandvaerk OWNER TO grukosadmin;
    GRANT ALL ON TABLE mstjupiter.almen_vandvaerk TO grukosadmin;

    GRANT SELECT ON TABLE jupiter.drwplant TO grukosreader;
    GRANT SELECT ON TABLE mstjupiter.almen_vandvaerk TO grukosreader;
COMMIT;


/*
    table:   qlr_almen_vandvaerk_indvinding
    Tema:   Alle indvindinger igennem tiden fra almene vandværksanlæg
    TOC:    Almen_vandværkindvinding
    QLR:    60~Almen_vandværkindvinding.qlr
*/
BEGIN;
    DROP TABLE IF EXISTS mstjupiter.qlr_almen_vandvaerk_indvinding;

    CREATE TABLE mstjupiter.qlr_almen_vandvaerk_indvinding AS (
        SELECT
               row_number() OVER (ORDER BY plantid) AS id,
               dense_rank() over (order by plantid),
               p.plantid anlaegsid,
               p.municipalityno2007 kommunenr,
               p.plantname anlaegsnavn,
               p.plantaddress anlaegsadr,
               p.plantpostalcode anlaegpostnr,
               pc.companytype virksomhedstype,
               c.longtext virksomhedstypetext,
               w.startdate indvindingstart,
               w.enddate indvindingslut,
               w.amount indvindingsmaengde,
               'https://data.geus.dk/JupiterWWW/anlaeg.jsp?anlaegid=' ||  p.plantid::TEXT AS url_plant,
               p.geom
        FROM jupiter.drwplant p
        INNER JOIN jupiter.drwplantcompanytype pc using (plantid)
        INNER JOIN jupiter.code c on c.code = pc.companytype
        INNER JOIN jupiter.wrrcatchment w using (plantid)
        WHERE pc.companytype in ('V01', 'V02') AND p.geom IS NOT NULL
    );
    ALTER TABLE mstjupiter.qlr_almen_vandvaerk_indvinding OWNER TO grukosadmin;
    GRANT ALL ON TABLE mstjupiter.qlr_almen_vandvaerk_indvinding TO grukosadmin;

    GRANT SELECT ON TABLE jupiter.drwplant TO grukosreader;
    GRANT SELECT ON TABLE mstjupiter.qlr_almen_vandvaerk_indvinding TO grukosreader;
COMMIT;

/*
    table:   qlr_almen_vandvaerk_indvinding_2018
    Tema:   Seneste indvindinger fra alment vandværksanlæg med aktiv tilladelse
    TOC:    Almen_vandværkindvinding_2018
    QLR:    70~Almen_vandværkindvinding_2018.qlr
    TODO:   Der er flere tilladelser på en anlæg, nogle er udløbet andre er ikke
*/
BEGIN;
    DROP TABLE IF EXISTS mstjupiter.qlr_almen_vandvaerk_indvinding_2018;

    CREATE TABLE mstjupiter.qlr_almen_vandvaerk_indvinding_2018 AS (
        SELECT
            row_number() OVER (ORDER BY plantid) AS id,
            p.plantid anlaegsid,
            p.municipalityno2007,
            p.plantname anlaegsnavn,
            p.plantaddress anlaegsadr,
            p.plantpostalcode anlaegpostnr,
			p.active,
            pc.companytype virksomhedstype,
            c.longtext virksomhedstypetext,
            w.amount indvundetmaengde,
            date(w.startdate) indvindingsperiodetart,
            date(w.enddate) indvindingsperiodeslut,
            cp.amountperyear tilladelsepraar,
            cp.revoked,
            date(cp.startdate) tilladelsestart,
            date(cp.enddate) tilladelseslut,
            'https://data.geus.dk/JupiterWWW/anlaeg.jsp?anlaegid=' ||  p.plantid::TEXT AS url_plant,
            p.geom
        FROM jupiter.drwplant p
        INNER JOIN jupiter.drwplantcompanytype pc using (plantid)
        INNER JOIN jupiter.code c on c.code = pc.companytype
        INNER JOIN jupiter.catchperm cp using (plantid)
        INNER JOIN jupiter.wrrcatchment w using (plantid)
        WHERE
            pc.companytype in ('V01', 'V02')
            AND cp.startdate < now()
            AND cp.enddate > now()
            AND EXTRACT(year FROM w.enddate) = 2018
        ORDER BY plantid, p.municipalityno2007
    );
    ALTER TABLE mstjupiter.qlr_almen_vandvaerk_indvinding_2018 OWNER TO grukosadmin;
    GRANT ALL ON TABLE mstjupiter.qlr_almen_vandvaerk_indvinding_2018 TO grukosadmin;

    GRANT SELECT ON TABLE jupiter.drwplant TO grukosreader;
    GRANT SELECT ON TABLE mstjupiter.qlr_almen_vandvaerk_indvinding_2018 TO grukosreader;
COMMIT;

/*
    table:   qlr_almen_vandvaerk_tilladelse
    Tema:   alment vandværksanlæg med alle tilladelse
    TOC:    Indvindingtilladelser
    QLR:    80~Indvindingstilladelser.qlr
*/
BEGIN;
    DROP TABLE IF EXISTS mstjupiter.qlr_almen_vandvaerk_tilladelse;

    CREATE TABLE mstjupiter.qlr_almen_vandvaerk_tilladelse AS (
        SELECT
            row_number() OVER (ORDER BY plantid) AS id,
            p.plantid anlaegsid,
            p.municipalityno2007,
            p.plantname anlaegsnavn,
            p.plantaddress anlaegsadr,
            p.plantpostalcode anlaegpostnr,
			p.active,
            pc.companytype virksomhedstype,
            c.longtext virksomhedstypetext,
            cp.permissionid tilladelsesid,
            cp.amountperyear tilladelsepraar,
            date(cp.startdate) tilladelsestart,
            date(cp.enddate) tilladelseslut,
            case
                when cp.enddate < now() or cp.revoked = '1' then 'Nej'
                else 'Ja'
            end aktiv,
            case
                when cp.revoked = '0' then 'Nej'
                when cp.revoked = '1' then 'Ja'
            end tilbagekaldt,
            'https://data.geus.dk/JupiterWWW/anlaeg.jsp?anlaegid=' ||  p.plantid::TEXT AS url_plant,
            p.geom
        FROM jupiter.drwplant p
        INNER JOIN jupiter.drwplantcompanytype pc using (plantid)
        INNER JOIN jupiter.code c on c.code = pc.companytype
        INNER JOIN jupiter.catchperm cp using (plantid)
        WHERE
            pc.companytype in ('V01', 'V02')
        ORDER BY plantid, p.municipalityno2007
    );
    ALTER TABLE mstjupiter.qlr_almen_vandvaerk_tilladelse OWNER TO grukosadmin;
    GRANT ALL ON TABLE mstjupiter.qlr_almen_vandvaerk_tilladelse TO grukosadmin;

    GRANT SELECT ON TABLE jupiter.drwplant, jupiter.drwplantcompanytype, jupiter.code, jupiter.catchperm TO grukosreader;
    GRANT SELECT ON TABLE mstjupiter.qlr_almen_vandvaerk_tilladelse TO grukosreader;
COMMIT;

/*
    err_active_borehole_with_no_reference_to_plant
*/
DROP TABLE IF EXISTS mstjupiter.err_active_borehole_with_no_reference_to_plant;
create TABLE mstjupiter.err_active_borehole_with_no_reference_to_plant as
SELECT DISTINCT b.boreholeid,
                b.boreholeno,
                b.purpose,
                b.use,
                b.municipal AS municipalityname
FROM jupiter.borehole b
WHERE b.use = 'VV'::text
  AND b.abandondat IS NULL
  AND NOT (EXISTS(SELECT 1
                  FROM jupiter.drwplantintake
                  WHERE drwplantintake.boreholeid = b.boreholeid));

ALTER TABLE mstjupiter.err_active_borehole_with_no_reference_to_plant OWNER TO grukosadmin;
GRANT ALL ON TABLE mstjupiter.err_active_borehole_with_no_reference_to_plant TO grukosadmin;
GRANT SELECT ON TABLE jupiter.borehole, jupiter.drwplantintake TO grukosreader;
GRANT SELECT ON TABLE mstjupiter.err_active_borehole_with_no_reference_to_plant TO grukosreader;

/*
    err_mstjupiter.plant_with_more_than_one_active_permit
*/
DROP TABLE if exists mstjupiter.err_plant_with_more_than_one_active_permit;
create TABLE mstjupiter.err_plant_with_more_than_one_active_permit as
WITH plant_count AS (
    SELECT count(*) AS number_of_active_catchment_permits_pr_plant,
           pl_1.plantid
    FROM jupiter.drwplant pl_1
             LEFT JOIN jupiter.catchperm cp USING (plantid)
    WHERE cp.startdate < now()
      AND cp.enddate > now()
    GROUP BY pl_1.plantid
    ORDER BY (count(*))
)
SELECT row_number() over () row_id,
       pc.number_of_active_catchment_permits_pr_plant,
       pc.plantid,
       pl.plantname,
       pl.municipalityno2007 AS kommunenr,
       mu.name
FROM plant_count pc
         JOIN jupiter.drwplant pl USING (plantid)
         JOIN jupiter.drwplantcompanytype ct USING (plantid)
         JOIN jupiter.municipality2007 mu USING (municipalityno2007)
WHERE pc.number_of_active_catchment_permits_pr_plant > 1
  AND ((ct.companytype = ANY (ARRAY ['V01'::text, 'V02'::text])) OR ct.companytype IS NULL)
ORDER BY pc.number_of_active_catchment_permits_pr_plant;

ALTER TABLE mstjupiter.err_plant_with_more_than_one_active_permit OWNER TO grukosadmin;
GRANT ALL ON TABLE mstjupiter.err_plant_with_more_than_one_active_permit TO grukosadmin;
GRANT SELECT ON TABLE jupiter.drwplant, jupiter.drwplantcompanytype, jupiter.municipality2007 TO grukosreader;
GRANT SELECT ON TABLE mstjupiter.err_plant_with_more_than_one_active_permit TO grukosreader;

/*
    err_mstjupiter.plant_without_companytype
*/
DROP TABLE mstjupiter."err_plant_without_companytype";
CREATE TABLE mstjupiter.err_plant_without_companytype(plantid, plantname, municipalityno2007, name, companytype) as
SELECT pl.plantid,
       pl.plantname,
       pl.municipalityno2007,
       mu.name,
       'MISSING COMPANYTYPE'::text AS companytype
FROM jupiter.drwplant pl
         LEFT JOIN jupiter.municipality2007 mu ON pl.municipalityno = mu.municipalityno2007
         LEFT JOIN jupiter.drwplantcompanytype ct ON pl.plantid = ct.plantid
WHERE ct.plantid IS NULL
ORDER BY mu.name;

ALTER TABLE mstjupiter.err_plant_without_companytype OWNER TO grukosadmin;
GRANT ALL ON TABLE mstjupiter.err_plant_without_companytype TO grukosadmin;

GRANT SELECT ON TABLE jupiter.drwplant, jupiter.municipality2007, jupiter.drwplantcompanytype TO grukosreader;
GRANT SELECT ON TABLE mstjupiter.err_plant_without_companytype TO grukosreader;
