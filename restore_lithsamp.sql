/*
 * Owner: 	Danish Environmental Protection Agency (EPA) 
 * Descr: 	This sql script updates MST-GKOs pcjupiterxl postgresql database
 * 			via GEUS' SQL-gateway --where existing tables are updated using 
 * 			select statements (upserts) executed on a foreign data wrapper. 
 * 			The script is overall optimized toward lowering the execution time
 * 			of the queries from the geus foreign wrapper.
 * 			This means for the large tables, the uuids are translated prior to updating 
 * 			such that the fdw queries does not have to translate bytea into uuid. This is done
 * 			to lower the queries time of execution on the fdw server due to 
 * 			timeout errors.
 * ERT: 	~10 min 
 * Author: 	Simon Makwarth <simak@mst.dk>
 * Created: 2022-02-22
 * License:	GNU GPL v3
 */
	
BEGIN;

	-- create mstjupiter schema if it does not exists

	CREATE SCHEMA IF NOT EXISTS mstjupiter;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON SCHEMA mstjupiter IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: Data from pcjupiterxl'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- create jupiter schema if it does not exists

	CREATE SCHEMA IF NOT EXISTS jupiter;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON SCHEMA jupiter IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: MSTGKO derived data from pcjupiterxl'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	CREATE TABLE IF NOT EXISTS mstjupiter.mst_exportdate AS 
		(
			SELECT 
				'test_string'::TEXT AS tablename,
				'1900-01-01'::timestamp AS updatetime
			LIMIT 0
		)
	;

	ALTER TABLE mstjupiter.mst_exportdate
		DROP CONSTRAINT IF EXISTS mst_exportdate_pkey CASCADE
	; 

	ALTER TABLE mstjupiter.mst_exportdate
		ADD PRIMARY KEY (tablename)
	;

COMMIT;

-- lithsamp

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.lithsamp AS
		(
			SELECT 
				boreholeid,
				sampleid,
				boreholeno,
				sampleno,
				bagno,
				top,
				bottom,
				sampledep,
				sampletop,
				samplebottom,
				samplekept,
				depestim,
				drillcolor,
				drillrockt,
				drillcolsb,
				drilldescr,
				drillbagno,
				drillsampleref,
				drillremrk,
				rocktype,
				texture,
				color,
				"structure",
				hardness,
				cementatio,
				diagenesis,
				calcareous,
				classifica,
				trivialnam,
				rocksymbol,
				sorting,
				rounding,
				remarks,
				grainshape,
				minorcomps,
				analyses,
				minerals,
				fossils,
				oldcolor,
				munsellcolor,
				totaldescr,
				otherdescr,
				sampletype,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.lithsamp ls
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.lithsamp 
		DROP CONSTRAINT IF EXISTS lithsamp_pkey CASCADE
	;

	DROP INDEX IF EXISTS lithsamp_boreholeid_idx;

	DROP INDEX IF EXISTS lithsamp_rocksymbol_idx;

	DROP INDEX IF EXISTS lithsamp_top_idx;

	DROP INDEX IF EXISTS lithsamp_bottom_idx;

	ALTER TABLE jupiter.lithsamp 
		DROP CONSTRAINT IF EXISTS fk_borehole_lithsamp CASCADE
	;

	TRUNCATE jupiter.lithsamp;

	INSERT INTO jupiter.lithsamp AS t1
		(
			boreholeid,
			sampleid,
			boreholeno,
			sampleno,
			bagno,
			top,
			bottom,
			sampledep,
			sampletop,
			samplebottom,
			samplekept,
			depestim,
			drillcolor,
			drillrockt,
			drillcolsb,
			drilldescr,
			drillbagno,
			drillsampleref,
			drillremrk,
			rocktype,
			texture,
			color,
			"structure",
			hardness,
			cementatio,
			diagenesis,
			calcareous,
			classifica,
			trivialnam,
			rocksymbol,
			sorting,
			rounding,
			remarks,
			grainshape,
			minorcomps,
			analyses,
			minerals,
			fossils,
			oldcolor,
			munsellcolor,
			totaldescr,
			otherdescr,
			sampletype,
			guid,
			guid_borehole,
			insertdate,
			updatedate,
			insertuser,
			updateuser
		)
		(
			SELECT 
				boreholeid,
				sampleid,
				boreholeno,
				sampleno,
				bagno,
				top,
				bottom,
				sampledep,
				sampletop,
				samplebottom,
				samplekept,
				depestim,
				drillcolor,
				drillrockt,
				drillcolsb,
				drilldescr,
				drillbagno,
				drillsampleref,
				drillremrk,
				rocktype,
				texture,
				color,
				"structure",
				hardness,
				cementatio,
				diagenesis,
				calcareous,
				classifica,
				trivialnam,
				rocksymbol,
				sorting,
				rounding,
				remarks,
				grainshape,
				minorcomps,
				analyses,
				minerals,
				fossils,
				oldcolor,
				munsellcolor,
				totaldescr,
				otherdescr,
				sampletype,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.lithsamp t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (guid) --ON CONSTRAINT lithsamp_pkey 
--			DO UPDATE SET 
--				boreholeid = excluded.boreholeid,
--				sampleid = excluded.sampleid,
--				boreholeno = excluded.boreholeno,
--				sampleno = excluded.sampleno,
--				bagno = excluded.bagno,
--				top = excluded.top,
--				bottom = excluded.bottom,
--				sampledep = excluded.sampledep,
--				sampletop = excluded.sampletop,
--				samplebottom = excluded.samplebottom,
--				samplekept = excluded.samplekept,
--				depestim = excluded.depestim,
--				drillcolor = excluded.drillcolor,
--				drillrockt = excluded.drillrockt,
--				drillcolsb = excluded.drillcolsb,
--				drilldescr = excluded.drilldescr,
--				drillbagno = excluded.drillbagno,
--				drillsampleref = excluded.drillsampleref,
--				drillremrk = excluded.drillremrk,
--				rocktype = excluded.rocktype,
--				texture = excluded.texture,
--				color = excluded.color,
--				"structure" = excluded."structure",
--				hardness = excluded.hardness,
--				cementatio = excluded.cementatio,
--				diagenesis = excluded.diagenesis,
--				calcareous = excluded.calcareous,
--				classifica = excluded.classifica,
--				trivialnam = excluded.trivialnam,
--				rocksymbol = excluded.rocksymbol,
--				sorting = excluded.sorting,
--				rounding = excluded.rounding,
--				remarks = excluded.remarks,
--				grainshape = excluded.grainshape,
--				minorcomps = excluded.minorcomps,
--				analyses = excluded.analyses,
--				minerals = excluded.minerals,
--				fossils = excluded.fossils,
--				oldcolor = excluded.oldcolor,
--				munsellcolor = excluded.munsellcolor,
--				totaldescr = excluded.totaldescr,
--				otherdescr = excluded.otherdescr,
--				sampletype = excluded.sampletype,
--				guid = excluded.guid,
--				guid_borehole = excluded.guid_borehole,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate,
--				insertuser = excluded.insertuser,
--				updateuser = excluded.updateuser
			--WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;
	
	ALTER TABLE jupiter.lithsamp 
		ADD PRIMARY KEY (guid)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.lithsamp IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl lithsamp'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	CREATE INDEX IF NOT EXISTS lithsamp_boreholeid_idx
		ON jupiter.lithsamp(boreholeid)
	;
	
	CREATE INDEX IF NOT EXISTS lithsamp_rocksymbol_idx
		ON jupiter.lithsamp(rocksymbol)
	;
	
	CREATE INDEX IF NOT EXISTS lithsamp_top_idx
		ON jupiter.lithsamp(top)
	;
	
	CREATE INDEX IF NOT EXISTS lithsamp_bottom_idx
		ON jupiter.lithsamp(bottom)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'lithsamp',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	-- give access to read user

	GRANT USAGE ON SCHEMA jupiter TO grukosreader;
	GRANT SELECT ON ALL TABLES IN SCHEMA jupiter TO grukosreader;

COMMIT;

