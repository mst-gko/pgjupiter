/*
 ***************************************************************************
  adm_roles.sql
  Danish Environmental Protection Agency (EPA)
  Jakob Lanstorp, (c)
  -Create index
   begin                : 2018-04-06
   copyright            : (C) 2018 EPA
   email                : jalan@mst.dk
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*/

SET search_path TO jupiter;


-- change owner to grukosadmin
ALTER DATABASE pcjupiterxl OWNER TO grukosadmin;
ALTER SCHEMA jupiter OWNER TO grukosadmin;
GRANT USAGE ON SCHEMA jupiter TO grukosadmin;
GRANT ALL ON SCHEMA jupiter TO grukosadmin;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA jupiter TO grukosadmin;

-- Change ownership of tables in Jupiter schema
DO
$$
DECLARE
    row record;
BEGIN
    FOR row IN SELECT tablename FROM pg_tables WHERE schemaname = 'jupiter'
    LOOP
        IF row.tablename = ANY( ARRAY['layer_styles', 'spatial_ref_sys']) THEN
          raise notice 'Don''t change ownership of: %', row.tablename;
        ELSE
          raise notice 'Changing ownership of: %', row.tablename;

          EXECUTE 'ALTER TABLE jupiter.' || quote_ident(row.tablename) || ' OWNER TO grukosadmin;';
        END IF;
    END LOOP;
END;
$$;

/*
  Jupiter reader role used by all users but postgres
*/
DO
$do$
BEGIN
	IF NOT EXISTS 
		(
			SELECT * 
			FROM pg_catalog.pg_roles  
			WHERE  rolname = 'jupiterrole'
		) 
	THEN
      CREATE ROLE jupiterrole WITH PASSWORD 'mst' NOLOGIN;
	END IF;
END
$do$;

GRANT USAGE ON SCHEMA jupiter TO jupiterrole;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO jupiterrole;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA jupiter TO jupiterrole;

ALTER ROLE jupiterrole SET SEARCH_PATH = "$user", public, jupiter;
GRANT USAGE ON SCHEMA public TO jupiterrole;
GRANT USAGE ON SCHEMA jupiter TO jupiterrole;
