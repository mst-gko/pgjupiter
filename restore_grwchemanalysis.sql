/*
 * Owner: 	Danish Environmental Protection Agency (EPA) 
 * Descr: 	This sql script updates MST-GKOs pcjupiterxl postgresql database
 * 			via GEUS' SQL-gateway --where existing tables are updated using 
 * 			select statements (upserts) executed on a foreign data wrapper. 
 * 			The script is overall optimized toward lowering the execution time
 * 			of the queries from the geus foreign wrapper.
 * 			The scripts are divided to lower the queries time of execution on the fdw server due to 
 * 			timeout errors.
 * ERT: 	~15-30 min 
 * Author: 	Simon Makwarth <simak@mst.dk>
 * Created: 2022-02-22
 * Updated: 2022-06-22
 * License:	GNU GPL v3
 */

BEGIN; 

	-- create mstjupiter schema if it does not exists

	CREATE SCHEMA IF NOT EXISTS mstjupiter;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON SCHEMA mstjupiter IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: Data from pcjupiterxl'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- create jupiter schema if it does not exists

	CREATE SCHEMA IF NOT EXISTS jupiter;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON SCHEMA jupiter IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: MSTGKO derived data from pcjupiterxl'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	CREATE TABLE IF NOT EXISTS mstjupiter.mst_exportdate AS 
		(
			SELECT 
				'test_string'::TEXT AS tablename,
				'1900-01-01'::timestamp AS updatetime
			LIMIT 0
		)
	;

	ALTER TABLE mstjupiter.mst_exportdate
		DROP CONSTRAINT IF EXISTS mst_exportdate_pkey CASCADE
	; 

	ALTER TABLE mstjupiter.mst_exportdate
		ADD PRIMARY KEY (tablename)
	;

COMMIT; 

-- grwchemanalysis

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.grwchemanalysis AS
		(
			SELECT 
				sampleid,
				compoundno,
				analysisno,
				analysissite,
				fieldfiltration,
				"attribute",
				amount,
				unit,
				reportedcompoundno,
				reportedunit,
				reportedamount,
				laboratory,
				laboratoryrefno,
				analysismethod,
				qualitycontrol,
				remark,
				preprocessing,
				preservation,
				packing,
				detectionlimit,
				analysisid,
				reporteddetectionlimit,
				analysisdate,
				laboratoryreceiveddate,
				absoluteexpmeasuncertainty,
				relativeexpmeasuncertainty,
				analysisresponsible,
				accreditedindicator,
				fractionationmethod,
				analysisresultidentifier,
				sortno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_grwchemsample, 'hex') AS UUID) AS guid_grwchemsample,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				compoundno_desc,
				unit_desc,
				analysissite_desc
			FROM geus_fdw.grwchemanalysis gcs
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.grwchemanalysis 
		DROP CONSTRAINT IF EXISTS fk_gcs_gca CASCADE
	;
	
	ALTER TABLE jupiter.grwchemanalysis 
		DROP CONSTRAINT IF EXISTS fk_cpl_gca CASCADE
	;

	DROP INDEX IF EXISTS grwchemanalysis_sampleid_idx;

	DROP INDEX IF EXISTS grwchemanalysis_amount_idx;

	DROP INDEX IF EXISTS grwchemanalysis_compoundno_idx;

	ALTER TABLE jupiter.grwchemanalysis 
		DROP CONSTRAINT IF EXISTS grwchemanalysis_pkey CASCADE
	;

	TRUNCATE jupiter.grwchemanalysis;

	INSERT INTO jupiter.grwchemanalysis AS t1
		(
			sampleid,
			compoundno,
			analysisno,
			analysissite,
			fieldfiltration,
			"attribute",
			amount,
			unit,
			reportedcompoundno,
			reportedunit,
			reportedamount,
			laboratory,
			laboratoryrefno,
			analysismethod,
			qualitycontrol,
			remark,
			preprocessing,
			preservation,
			packing,
			detectionlimit,
			analysisid,
			reporteddetectionlimit,
			analysisdate,
			laboratoryreceiveddate,
			absoluteexpmeasuncertainty,
			relativeexpmeasuncertainty,
			analysisresponsible,
			accreditedindicator,
			fractionationmethod,
			analysisresultidentifier,
			sortno,
			guid,
			guid_grwchemsample,
			insertdate,
			updatedate,
			insertuser,
			updateuser,
			compoundno_desc,
			unit_desc,
			analysissite_desc
		)
		(
			SELECT
				sampleid,
				compoundno,
				analysisno,
				analysissite,
				fieldfiltration,
				"attribute",
				amount,
				unit,
				reportedcompoundno,
				reportedunit,
				reportedamount,
				laboratory,
				laboratoryrefno,
				analysismethod,
				qualitycontrol,
				remark,
				preprocessing,
				preservation,
				packing,
				detectionlimit,
				analysisid,
				reporteddetectionlimit,
				analysisdate,
				laboratoryreceiveddate,
				absoluteexpmeasuncertainty,
				relativeexpmeasuncertainty,
				analysisresponsible,
				accreditedindicator,
				fractionationmethod,
				analysisresultidentifier,
				sortno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_grwchemsample, 'hex') AS UUID) AS guid_grwchemsample,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				compoundno_desc,
				unit_desc,
				analysissite_desc
			FROM geus_fdw.grwchemanalysis t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (guid) --ON CONSTRAINT grwchemanalysis_pkey 
--			DO UPDATE SET 
--				sampleid = excluded.sampleid,
--				compoundno = excluded.compoundno, 
--				analysisno = excluded.analysisno,
--				analysissite = excluded.analysissite,
--				fieldfiltration = excluded.fieldfiltration,
--				"attribute" = excluded."attribute",
--				amount = excluded.amount,
--				unit = excluded.unit,
--				reportedcompoundno = excluded.reportedcompoundno,
--				reportedunit = excluded.reportedunit ,
--				reportedamount = excluded.reportedamount,
--				laboratory = excluded.laboratory,
--				laboratoryrefno = excluded.laboratoryrefno,
--				analysismethod = excluded.analysismethod,
--				qualitycontrol = excluded.qualitycontrol,
--				remark = excluded.remark,
--				preprocessing = excluded.preprocessing,
--				preservation = excluded.preservation,
--				packing = excluded.packing,
--				detectionlimit = excluded.detectionlimit,
--				analysisid = excluded.analysisid,
--				reporteddetectionlimit = excluded.reporteddetectionlimit,
--				analysisdate = excluded.analysisdate,
--				laboratoryreceiveddate = excluded.laboratoryreceiveddate,
--				absoluteexpmeasuncertainty = excluded.absoluteexpmeasuncertainty,
--				relativeexpmeasuncertainty = excluded.relativeexpmeasuncertainty,
--				analysisresponsible = excluded.analysisresponsible,
--				accreditedindicator = excluded.accreditedindicator,
--				fractionationmethod = excluded.fractionationmethod,
--				analysisresultidentifier = excluded.analysisresultidentifier,
--				sortno = excluded.sortno,
--				guid = excluded.guid, 
--				guid_grwchemsample = excluded.guid_grwchemsample,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate,
--				insertuser = excluded.insertuser,
--				updateuser = excluded.updateuser,
--				compoundno_desc = excluded.compoundno_desc,
--				unit_desc = excluded.unit_desc,
--				analysissite_desc = excluded.analysissite_desc
			--WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.grwchemanalysis 
		ADD PRIMARY KEY (guid)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.grwchemanalysis IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl grwchemanalysis'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'grwchemanalysis',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	CREATE INDEX IF NOT EXISTS grwchemanalysis_sampleid_idx
		ON jupiter.grwchemanalysis(sampleid)
	;
	
	CREATE INDEX IF NOT EXISTS grwchemanalysis_amount_idx
		ON jupiter.grwchemanalysis(amount)
	;
	
	CREATE INDEX IF NOT EXISTS grwchemanalysis_compoundno_idx
		ON jupiter.grwchemanalysis(compoundno)
	;

	-- give access to read user

	GRANT USAGE ON SCHEMA jupiter TO grukosreader;
	GRANT SELECT ON ALL TABLES IN SCHEMA jupiter TO grukosreader;

COMMIT;

