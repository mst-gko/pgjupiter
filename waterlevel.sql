/* 
 ***************************************************************************
  waterlevel.sql
  
  Danish Environmental Protection Agency (EPA)
  Jakob Lanstorp, (c) December 2017
                                   
  -Materialized view handling water levels (pejlinger)
  
   begin                : 2018-01-14
   copyright            : (C) 2018 EPA
   email                : jalan@mst.dk
   
 	Change log
  		- 	date:2021-03-24: 
	  		author: Simon Makwarth <simak@mst.dk> 
	  		1) Changed mat.view. to tables within mstjupiter,
	  		2) waterlevel view is ditinctly on watlevelid and
	  		3) latest waterlevel is now sorted by intake not borehole
        -   date: 20211001
            author: Jakob Lanstorp <jakla@mst.dk>
            1) Prefix mstvw and mstmvw removed
            2) Materialized views moved to tables
            3) QlrBrowser updated
*/

BEGIN;
    /*
      View Waterlevel (Pejlinger).
      Abstract parent view for children waterlavel tables. Not to be exposed in QlrBrowser.
    */
	DROP TABLE IF EXISTS mstjupiter.waterlevel CASCADE;
	CREATE TABLE mstjupiter.waterlevel AS
        SELECT DISTINCT ON (wl.watlevelid)
            b.boreholeno,
            b.boreholeid,
            wl.intakeno,
            wl.guid_intake,
            wl.watlevgrsu AS vandst_mut, --Det målte vandspejl beregnet som meter under terrænoverfladen
            wl.watlevmsl AS vandstkote,  --Det målte vandspejl beregnet som meter over havniveau (online DVR90 ved download det valgt kotesystem)
            wl.timeofmeas::DATE AS pejledato,
            b.geom
        FROM jupiter.borehole b
        INNER JOIN jupiter.watlevel wl USING (boreholeid)
        WHERE b.geom IS NOT NULL;

    COMMENT ON TABLE mstjupiter.waterlevel IS '20211001 - Abstract parent view for children waterlavel tables. Not to be exposed in QlrBrowser.';

    /*
        Table water levels for all dates
    */
	DROP TABLE IF EXISTS mstjupiter.waterlevel_all_dates;
	CREATE TABLE mstjupiter.waterlevel_all_dates AS (
	  SELECT
	    row_number() OVER () AS row_id,
	    *
	  FROM mstjupiter.waterlevel
	);
	--[2017-12-12 18:49:09] completed in 17s 266ms
	--[2021-10-01 11:12:55] 13,780,815 rows affected in 51 s 379 ms

    COMMENT ON TABLE mstjupiter.waterlevel_all_dates IS '20211001 - All waterlevels, exposed in QlrBrowser.';

    /*
        Tables water level for the most recent waterlevel measure in each well
    */
	DROP TABLE IF EXISTS mstjupiter.waterlevel_latest_dates;
	CREATE TABLE mstjupiter.waterlevel_latest_dates AS (
	  SELECT DISTINCT ON (guid_intake)
	  	row_number() OVER () AS row_id,
	    *
	  FROM mstjupiter.waterlevel
	  ORDER BY guid_intake, boreholeno, pejledato DESC NULLS LAST
	);
	--[2017-12-12 18:51:04] completed in 1m 19s 913ms
	--[2021-10-01 11:15:58] 205,670 rows affected in 1 m 25 s 632 ms

    COMMENT ON TABLE mstjupiter.waterlevel_latest_dates IS '20211001 - The most recent waterlevels for each well, exposed in QlrBrowser.';

	GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO jupiterrole;

    /*
        Table water levels for year 2015 to now
    */
	DROP TABLE IF EXISTS mstjupiter.waterlevel_2015_now;
	CREATE TABLE mstjupiter.waterlevel_2015_now AS (
	  SELECT
	    row_number() OVER () AS row_id,
	    *
	  FROM mstjupiter.waterlevel
	  WHERE EXTRACT(YEAR FROM pejledato) >= 2015);
    --[2021-10-01 11:34:27] 4,179,766 rows affected in 47 s 587 ms

    COMMENT ON TABLE mstjupiter.waterlevel_2015_now IS '20211001 - All waterlevels from year >= 2015 to now, exposed in QlrBrowser.';

	GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO jupiterrole;

    /*
        Count number of waterlevel measures for each borehole intake
    */
	DROP TABLE IF EXISTS mstjupiter.waterlevel_count;
	CREATE TABLE mstjupiter.waterlevel_count AS (
	    WITH antal as (
          SELECT count(*) antal,
                 boreholeno,
                 intakeno,
                 geom
          FROM mstjupiter.waterlevel
          GROUP BY boreholeno, intakeno, geom
      )
	  SELECT
	         ROW_NUMBER() OVER () row_id,
	         *
	  FROM antal
	);

    COMMENT ON TABLE mstjupiter.waterlevel_count IS '20211001 - Count number of waterlevel measures for each borehole intake, exposed in QlrBrowser.';

	GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO jupiterrole;

    /*
        Count number of waterlevel measures for each borehole intake after 2015 - inclusive
    */
	DROP TABLE IF EXISTS mstjupiter.waterlevel_count_2015;
	CREATE TABLE mstjupiter.waterlevel_count_2015 AS (
	    WITH year_2015 as (
	        SELECT *
	        FROM mstjupiter.waterlevel
	        WHERE EXTRACT(YEAR FROM pejledato) >= 2015
        ),
	    antal as (
          SELECT count(*) antal,
                 boreholeno,
                 intakeno,
                 geom
          FROM year_2015
          GROUP BY boreholeno, intakeno, geom
      )
	  SELECT
	         ROW_NUMBER() OVER () row_id,
	         *
	  FROM antal
	);
    --[2021-10-01 13:37:02] 205,670 rows affected in 1 m 13 s 256 ms

    COMMENT ON TABLE mstjupiter.waterlevel_count_2015 IS '20211001 - Count number of waterlevel measures for each borehole intake since 2015, exposed in QlrBrowser.';

	GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO jupiterrole;
COMMIT;


/*
    TEST
    --http://data.geus.dk/JupiterWWW/borerapport.jsp?atlasblad=0&loebenr=0&bogstav=&dgunr=38.770&submit=Vis+boringsdata
    SELECT * FROM jupiter.watlevel
      WHERE boreholeno =' 38.  770';

    --http://data.geus.dk/JupiterWWW/borerapport.jsp?atlasblad=0&loebenr=0&bogstav=&dgunr=58.553&submit=Vis+boringsdata
    SELECT * FROM jupiter.watlevel
      WHERE boreholeno =' 58.  553';
*/
