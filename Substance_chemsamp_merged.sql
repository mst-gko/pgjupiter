/*
 * screens per intake and combine screen if they are continuous 
 */

BEGIN;

/*
    20241127 Jakob Lanstorp
    VFS bestilling til GKO. Udtræk af TFA i drikkevand
*/
drop view if exists mstjupiter.mstvw_substance_tfa_drinkingwater;
create or replace view mstjupiter.mstvw_substance_tfa_drinkingwater as
    with companytype_latest as (
        select distinct on (plantid) plantid, companytype
        from jupiter.drwplantcompanytype
        order by plantid, insertdate desc nulls last
    )
    select row_number() OVER () id,
           p.plantid,
           p.plantname,
           p.plantaddress,
           p.municipalityno2007 komnr,
           m.name komnavn,
           ct.longtext companytype_text,
           t.companytype,
           s.sampleid,
           s.sampledate::date,
           a.compoundno,
           c.longtext compound,
           a.detectionlimit,
           a.attribute,
           a.amount,
           un.longtext unit,
           case
               when s.samplestatus = 1 then concat('Afventer godkendelse (', ss.longtext, ')')
           else
               ss.longtext
           end samplestatus,
           a.analysisid,
           p.geom
    from jupiter.drwplant p
    inner join jupiter.pltchemsample s using (plantid)
    inner join jupiter.pltchemanalysis a on a.sampleid = s.sampleid
    inner join jupiter.code c on a.compoundno::text = c.code
    inner join companytype_latest t using (plantid)
    inner join jupiter.municipality2007 m using (municipalityno2007)
    left join (select * from jupiter.code where codetype = 870) ss on ss.code = s.samplestatus::text
    left join (select * from jupiter.code where codetype = 852) ct ON ct.code = t.companytype
    left join (select * from jupiter.code where codetype = 752) un ON un.code = a.unit::text
    where a.compoundno = 2251   --Trifluoreddikesyre
    and c.codetype = 221;       --Stoffer;

comment on view mstjupiter.mstvw_substance_tfa_drinkingwater is '20241127 Trifluoreddikesyre analysis in drinkingwater from PCjupiterXL in Substance_chemsamp_merged.sql';

alter table mstjupiter.mstvw_substance_tfa_drinkingwater
    owner to grukosadmin;

grant select on mstjupiter.mstvw_substance_tfa_drinkingwater to grukosreader;
grant select on mstjupiter.mstvw_substance_tfa_drinkingwater to jupiterrole;


/*
    20240829 Jakob Lanstorp, Uran analysis in drinkingwater.
    Published in map library: Jupiter -> Kemi -> Enkelte stoffer
*/
drop view if exists mstjupiter.mstvw_substance_uran_drinkingwater;
create or replace view mstjupiter.mstvw_substance_uran_drinkingwater as
    with companytype_latest as (
        select distinct on (plantid) plantid, companytype
        from jupiter.drwplantcompanytype
        order by plantid, insertdate desc nulls last
    )
    select row_number() OVER () id,
           p.plantid,
           p.plantname,
           p.plantaddress,
           p.municipalityno2007 komnr,
           m.name komnavn,
           ct.longtext companytype_text,
           t.companytype,
           s.sampleid,
           s.sampledate::date,
           a.compoundno,
           c.longtext compound,
           a.detectionlimit,
           a.attribute,
           a.amount,
           un.longtext unit,
           case
               when s.samplestatus = 1 then concat('Afventer godkendelse (', ss.longtext, ')')
           else
               ss.longtext
           end samplestatus,
           a.analysisid,
           p.geom
    from jupiter.drwplant p
    inner join jupiter.pltchemsample s using (plantid)
    inner join jupiter.pltchemanalysis a on a.sampleid = s.sampleid
    inner join jupiter.code c on a.compoundno::text = c.code
    inner join companytype_latest t using (plantid)
    inner join jupiter.municipality2007 m using (municipalityno2007)
    left join (select * from jupiter.code where codetype = 870) ss on ss.code = s.samplestatus::text
    left join (select * from jupiter.code where codetype = 852) ct ON ct.code = t.companytype
    left join (select * from jupiter.code where codetype = 752) un ON un.code = a.unit::text
    where a.compoundno = 351    --Uran
    and c.codetype = 221;       --Stoffer
--20241011 returns 633 records

comment on view mstjupiter.mstvw_substance_uran_drinkingwater is '20240902 Uran analysis in drinkingwater from PCjupiterXL in Substance_chemsamp_merged.sql';

alter table mstjupiter.mstvw_substance_uran_drinkingwater
    owner to grukosadmin;

grant select on mstjupiter.mstvw_substance_uran_drinkingwater to grukosreader;
grant select on mstjupiter.mstvw_substance_uran_drinkingwater to jupiterrole;

/*
    20240829 Jakob Lanstorp, Arsene analysis in drinkingwater.
    Published in map library: Jupiter -> Kemi -> Enkelte stoffer
*/
drop view if exists mstjupiter.mstvw_substance_arsen_drinkingwater;
create or replace view mstjupiter.mstvw_substance_arsen_drinkingwater as
    with companytype_latest as (
        select distinct on (plantid) plantid, companytype
        from jupiter.drwplantcompanytype
        order by plantid, insertdate desc nulls last
    )
    select row_number() OVER () id,
           p.plantid,
           p.plantname,
           p.plantaddress,
           p.municipalityno2007 komnr,
           m.name komnavn,
           ct.longtext companytype_text,
           t.companytype,
           s.sampleid,
           s.sampledate::date,
           a.compoundno,
           c.longtext compound,
           a.detectionlimit,
           a.attribute,
           a.amount,
           un.longtext unit,
           case
               when s.samplestatus = 1 then concat('Afventer godkendelse (', ss.longtext, ')')
           else
               ss.longtext
           end samplestatus,
           a.analysisid,
           p.geom
    from jupiter.drwplant p
    inner join jupiter.pltchemsample s using (plantid)
    inner join jupiter.pltchemanalysis a on a.sampleid = s.sampleid
    inner join jupiter.code c on a.compoundno::text = c.code
    inner join companytype_latest t using (plantid)
    inner join jupiter.municipality2007 m using (municipalityno2007)
    left join (select * from jupiter.code where codetype = 870) ss on ss.code = s.samplestatus::text
    left join (select * from jupiter.code where codetype = 852) ct ON ct.code = t.companytype
    left join (select * from jupiter.code where codetype = 752) un ON un.code = a.unit::text
    where a.compoundno = 270    --Arsen
    and c.codetype = 221;       --Stoffer
--20241011 returns 633 records

comment on view mstjupiter.mstvw_substance_arsen_drinkingwater is '20241018 Arsene analysis in drinkingwater from PCjupiterXL in Substance_chemsamp_merged.sql';

alter table mstjupiter.mstvw_substance_arsen_drinkingwater
    owner to grukosadmin;

grant select on mstjupiter.mstvw_substance_arsen_drinkingwater to grukosreader;
grant select on mstjupiter.mstvw_substance_arsen_drinkingwater to jupiterrole;

/*
    20240829 Jakob Lanstorp, Uran analysis in groundwater
    Exposed in map library: Jupiter -> Kemi -> Enkelte stoffer
*/
drop view if exists mstjupiter.mstvw_substance_uran_groundwater;
create or replace view mstjupiter.mstvw_substance_uran_groundwater as
    select row_number() over (order by pl.plantname, gcs.boreholeno, gcs.sampledate) as id,
           gca.analysisno,
           gcs.boreholeno,
           gcs.intakeno,
           gca.sampleid,
           gcs.sampledate::date,
           gca.compoundno,
           gca.detectionlimit,
           cpl.long_text as compoundname,
           gca."attribute",
           gca.amount,
           un.longtext unit,
           case
               when gcs.samplestatus = 1 then concat('Afventer godkendelse (', ss.longtext, ')')
           else
               ss.longtext
           end samplestatus,
           pl.plantid,
           pl.plantname,
           pl.companytype,
           pl.companytypename,
           pl.municipalityno2007 komnr,
           pl.komnavn,
           gcs.project,
           b.geom
    from jupiter.grwchemanalysis gca
    inner join jupiter.grwchemsample gcs using (sampleid)
    left join (select * from jupiter.code where codetype = 752) un ON un.code = gca.unit::text
    left join (select code, longtext from jupiter.code where codetype = 870) ss on ss.code = gcs.samplestatus::text
    inner join jupiter.compoundlist cpl using (compoundno)
    inner join jupiter.borehole b using (boreholeid)
    inner join (
        select distinct dpi.boreholeid,
                        dp.plantid,
                        dp.plantname,
                        dp.planttype,
                        dpct.companytype,
                        c.longtext companytypename,
                        dp.municipalityno2007,
                        m.name komnavn
        from jupiter.drwplant dp
        inner join jupiter.drwplantcompanytype dpct using (plantid)
        inner join jupiter.code c on c.code = dpct.companytype
        inner join jupiter.drwplantintake dpi using (plantid)
        inner join jupiter.municipality2007 m on m.municipalityno2007 = dp.municipalityno2007
        order by dp.plantid
    ) pl on gcs.boreholeid = pl.boreholeid
    where gca.compoundno = 351 --Uran
    order by pl.plantname, gcs.boreholeno, gcs.sampledate desc;

comment on view mstjupiter.mstvw_substance_uran_groundwater is '20240902 Uran analysis in groundwater from PCjupiterXL';

alter table mstjupiter.mstvw_substance_uran_groundwater
    owner to grukosadmin;

grant select on mstjupiter.mstvw_substance_uran_groundwater to grukosreader;
grant select on mstjupiter.mstvw_substance_uran_groundwater to jupiterrole;

----------------------------------------------------------------------------------------

	DROP VIEW IF EXISTS mstjupiter.combined_screens_per_intake CASCADE;

	CREATE OR REPLACE VIEW mstjupiter.combined_screens_per_intake AS 
		(
			WITH 
				intake_with_more_filter AS 
				-- find alle intag med flere filtre tilknyttet (safety - burde være redundant)
					(
						SELECT s.guid_intake
						FROM jupiter.screen s 
						GROUP BY s.guid_intake 
						HAVING count(DISTINCT guid) > 1
					),
				fetch_lag_intake AS 
				-- find næste top, bund og guid_screen for samme indtag, sorteret efter top
					(
						SELECT 
							guid_intake, 
							guid,
							top,
							bottom,
							lag(guid, 1) OVER (PARTITION BY guid_intake ORDER BY top DESC) AS guid_lag,
							lag(top, 1) OVER (PARTITION BY guid_intake ORDER BY top DESC) AS top_lag,
							lag(bottom, 1) OVER (PARTITION BY guid_intake ORDER BY top DESC) AS bottom_lag
						FROM jupiter.screen s 
						INNER JOIN intake_with_more_filter USING (guid_intake)
						ORDER BY top 
					),
				grp_intake_continuous_filter AS 
				-- find højeste op og laveste bund for indtag for screens kommer lige efter hinanden
					(
						SELECT 
							guid_intake,
							min(top) AS top,
							max(bottom_lag) AS bottom,
							string_agg(guid::TEXT || ' | ' || guid_lag::TEXT, ' | ') AS guid_screen_agg 
						FROM fetch_lag_intake
						WHERE bottom = top_lag
						GROUP BY guid_intake
					)
			-- find alle screens for alle indtag og tag udelukkende den sammenkoblede screen hvis den findes
			SELECT DISTINCT 
				s.guid_intake, 
				COALESCE(cs.top, s.top) AS top,
				COALESCE(cs.bottom, s.bottom) AS bottom
			FROM jupiter.screen s
			LEFT JOIN grp_intake_continuous_filter cs ON cs.guid_intake = s.guid_intake
				AND cs.guid_screen_agg ILIKE '%' || s.guid || '%'
			WHERE COALESCE(cs.top, s.top, cs.bottom, s.bottom) IS NOT NULL
		)
	;

COMMIT;

/* 
	VIEW inorganic_crosstab
*/

BEGIN;

	DROP VIEW IF EXISTS mstjupiter.chemsamp_crosstab_json CASCADE;

	CREATE OR REPLACE VIEW mstjupiter.chemsamp_crosstab_json AS (
		-- lav json objekt til at anvende til pivot tabel
		-- https://postgresql.verite.pro/blog/2018/06/19/crosstab-pivot.html
		SELECT 	
			sampleid,
		   	jsonb_object_agg(compoundno, amount ORDER BY compoundno) AS samples_json
		FROM (
		     	SELECT 
		     		gca.sampleid, 
		     		gca.compoundno, 
		     		CASE
						WHEN gca.attribute LIKE '<%' 
							OR gca.attribute LIKE 'o%' 
							OR gca.attribute LIKE '0%' 
							THEN 0::NUMERIC
						WHEN gca.attribute LIKE 'B%'
							OR gca.attribute LIKE 'A%' 
							OR gca.attribute LIKE 'C%'
							OR gca.attribute LIKE 'D%' 
							OR gca.attribute LIKE 'S%' 
							OR gca.attribute LIKE '!%' 
							OR gca.attribute LIKE '/%' 
							OR gca.attribute LIKE '*%' 
							OR gca.attribute LIKE '>%' 
							THEN -99::NUMERIC
						ELSE gca.amount
						END AS amount
		        FROM jupiter.grwchemanalysis gca
		   ) as s 
		GROUP BY sampleid
	);

	DROP VIEW IF EXISTS mstjupiter.inorganic_crosstab CASCADE;

	CREATE OR REPLACE VIEW mstjupiter.inorganic_crosstab AS 
		(
			WITH json_to_crosstab AS 
			-- lav pivot tabel ud fra json objektet
				(
					SELECT 
						sampleid,
						(samples_json ->> '53')::NUMERIC AS Alkalinitet_total_TA,
						(samples_json ->> '267')::NUMERIC AS Aluminium,
						(samples_json ->> '240')::NUMERIC AS Ammoniak_ammonium,
						(samples_json ->> '242')::NUMERIC AS Ammonium_N ,
						(samples_json ->> '4')::NUMERIC AS Anioner_total,
						(samples_json ->> '270')::NUMERIC AS Arsen,
						(samples_json ->> '271')::NUMERIC AS Barium,
						(samples_json ->> '274')::NUMERIC AS Bly,
						(samples_json ->> '275')::NUMERIC AS Bor,
						(samples_json ->> '277')::NUMERIC AS Bromid,
						(samples_json ->> '280')::NUMERIC AS Calcium,
						(samples_json ->> '75')::NUMERIC AS Carbon_organisk_NVOC,
						(samples_json ->> '56')::NUMERIC AS Carbonat,
						(samples_json ->> '58')::NUMERIC AS Carbondioxid_aggr,
						(samples_json ->> '297')::NUMERIC AS Chlorid,
						(samples_json ->> '1167')::NUMERIC AS Dihydrogensulfid,
						(samples_json ->> '308')::NUMERIC AS Fluorid,
						(samples_json ->> '59')::NUMERIC AS Hydrogencarbonat,
						(samples_json ->> '35')::NUMERIC AS Inddampningsrest,
						(samples_json ->> '312')::NUMERIC AS Jern,
						(samples_json ->> '317')::NUMERIC AS Kalium,
						(samples_json ->> '3')::NUMERIC AS Kationer_total,
						(samples_json ->> '318')::NUMERIC AS Kobber,
						(samples_json ->> '304')::NUMERIC AS Kobolt_Co,
						(samples_json ->> '5')::NUMERIC AS Konduktivitet,
						(samples_json ->> '321')::NUMERIC AS Magnesium,
						(samples_json ->> '322')::NUMERIC AS Mangan,
						(samples_json ->> '356')::NUMERIC AS Methan,
						(samples_json ->> '324')::NUMERIC AS Natrium,
						(samples_json ->> '60')::NUMERIC AS Natriumhydrogencarbonat,
						(samples_json ->> '326')::NUMERIC AS Nikkel,
						(samples_json ->> '246')::NUMERIC AS Nitrat,
						(samples_json ->> '243')::NUMERIC AS Nitrit,
						(samples_json ->> '255')::NUMERIC AS Orthophosphat,
						(samples_json ->> '256')::NUMERIC AS Ortho_phosphat_P,
						(samples_json ->> '50')::NUMERIC AS Oxygen_indhold,
						(samples_json ->> '198')::NUMERIC AS Permanganattal_KMnO4,
						(samples_json ->> '13')::NUMERIC AS pH,
						(samples_json ->> '261')::NUMERIC AS Phosphor_total_P,
						(samples_json ->> '1620')::NUMERIC AS Redoxpotentiale,
						(samples_json ->> '330')::NUMERIC AS Siliciumdioxid,
						(samples_json ->> '331')::NUMERIC AS Strontium,
						(samples_json ->> '335')::NUMERIC AS Sulfat,
						(samples_json ->> '338')::NUMERIC AS Sulfit_S,
						(samples_json ->> '1154')::NUMERIC AS Temperatur,
						(samples_json ->> '36')::NUMERIC AS Tørstof_total,
						(samples_json ->> '353')::NUMERIC AS Zink
					FROM mstjupiter.chemsamp_crosstab_json ic
				)
			SELECT DISTINCT
			-- frasorter prøver hvis de udelukkende indenholder sampleid, temperatur, tørstof_total, ph og/eller konduktivitet
				t.*
			FROM json_to_crosstab AS t
			where jsonb_strip_nulls(to_jsonb(t) - 'sampleid' - 'temperatur' - 'tørstof_total' - 'ph' - 'konduktivitet') <> '{}'
		)
	;

COMMIT;


/*
 * View ionbalance og forvitringsgrad
 */

BEGIN;

	DROP VIEW IF EXISTS mstjupiter.forvitgrad_ionbalance_ionbytning;
	
	CREATE VIEW mstjupiter.forvitgrad_ionbalance_ionbytning AS 
		(
			WITH calc_ion_bal AS
			-- beregn forvitringsgrad, sum kation og -anion og ionbytning
				(
					SELECT 
						sampleid,
						CASE 
							WHEN hydrogencarbonat = 0
								THEN -99
							ELSE round(2*((calcium/40.1)+(magnesium/24.3))/(hydrogencarbonat/61.0), 2) 
							END AS forvitgrad,
						round((chlorid/35.5)+(hydrogencarbonat/61.0)+(2*sulfat/96.1)+(nitrat/62.0), 2) AS sum_anion,
						round((2*calcium/40.1)+(2*magnesium/24.3)+(natrium/23.0)+(kalium/39.1), 2) AS sum_kation, 
						CASE 
							WHEN chlorid = 0 
								THEN -99 
							ELSE round((natrium/22.99)/(chlorid/35.45), 2)
							END AS ionexchange
					FROM mstjupiter.inorganic_crosstab ic 
				)
			-- beregn ionbalancen ud fra sum kation og -anion
			SELECT DISTINCT  
				sampleid,
				forvitgrad,
				sum_anion,
				sum_kation,
				CASE 
					WHEN sum_kation+sum_anion = 0
						THEN -99
					ELSE round(100*(sum_kation-sum_anion)/(sum_kation+sum_anion), 2)
					END AS ionbalance,
				ionexchange
			FROM calc_ion_bal
			WHERE COALESCE(forvitgrad, sum_anion, sum_kation, ionexchange) IS NOT NULL 
		)
	;

COMMIT;

/*
 * View use/purpose integer
 * DEPRECATED - NOT USED ANYMORE
 */

BEGIN;

	DROP VIEW IF EXISTS mstjupiter.use_purpose_int;
	
	CREATE VIEW mstjupiter.use_purpose_int AS 
		(
			WITH 
				tmp1 AS 
					(
						SELECT b.use
						FROM jupiter.borehole b
						UNION ALL 
						SELECT b.purpose
						FROM jupiter.borehole b
					),
				tmp2 AS 
					(
						SELECT DISTINCT 
							use
						FROM tmp1
						WHERE use IS NOT NULL
					)
			SELECT 
				row_number() OVER (ORDER BY use) AS use_int,
				use
			FROM tmp2
		)
	;
	
COMMIT;

/*
 * VIEW chem_samp used for generating all the following tables
 */

BEGIN;

	DROP VIEW IF EXISTS mstjupiter.chem_samp;
	
	CREATE VIEW mstjupiter.chem_samp AS 
		(
			SELECT DISTINCT
				cs.sampleid,
				cs.guid_intake,
			 	concat(bh.boreholeno, '_', take.intakeno) AS DGU_Indtag,
				bh.boreholeno,
				take.stringno,
		        take.intakeno,
		        bh.drilldepth,
				cs.sampledate,
				cs.sampledate::DATE AS Dato,
				date_trunc('day', cs.sampledate)::date - date '1900-01-01' + 2 AS Dato_vaerdi,
				EXTRACT(YEAR FROM cs.sampledate)::NUMERIC AS Aarstal,
				concat(bh.boreholeno, '_', take.intakeno, '_', date_trunc('day', cs.sampledate)::date - date '1900-01-01' + 2) AS UNIK,
				cs.project,
				bh.use,
				bh.purpose,
				--upi.use_int,
				--upi2.use_int AS purpose_int,
				fa.layer AS fohm_layer, 
				NULL AS magasin,
				s.top AS dybde_filtertop,
				s.bottom AS dybde_filterbund,
				bh.elevation - s.top AS kote_filtertop,
				bh.elevation - s.bottom AS kote_filterbund,
				bh.xutm32euref89,
				bh.yutm32euref89,  
				bh.geom,		
				fi.sum_anion AS SUMANOINSCALCULATED,
				fi.sum_kation AS SUMCATIONSCALCULATED,
				fi.ionbalance AS Ionbalance,
				fi.forvitgrad,
				fi.ionexchange,
				lib.lith_screen_agg AS lith_in_screen
			FROM jupiter.grwchemsample cs
			LEFT JOIN jupiter.intake take ON cs.guid_intake = take.guid 
			--LEFT JOIN jupiter.screen s ON cs.guid_intake = s.guid_intake 
			LEFT JOIN mstjupiter.combined_screens_per_intake s ON cs.guid_intake = s.guid_intake 
			LEFT JOIN mstjupiter.lith_in_borscreen lib ON cs.guid_intake = lib.guid_intake 
			INNER JOIN jupiter.borehole bh ON cs.guid_borehole = bh.guid  
			LEFT JOIN mstjupiter.forvitgrad_ionbalance_ionbytning fi ON cs.sampleid = fi.sampleid
			--LEFT JOIN mstjupiter.use_purpose_int upi ON bh.use = upi.use
			--LEFT JOIN mstjupiter.use_purpose_int upi2 ON bh.purpose = upi2.use
			LEFT JOIN fohm_fdw.fohm_filter_lith_aquifer fa ON cs.guid_intake = fa.guid_intake::uuid
		)
	;

COMMIT;

/*
 * fetch approved and discarted chem samples from MST-GKO datakvalitet tabel
 */

DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_grwchemanalysis_approved_discarted;
	
CREATE TABLE mstjupiter.mstmvw_substance_grwchemanalysis_approved_discarted AS 
	(
		WITH 
			approved_discarted AS 
			-- fetch latest quality control per analysis
				(
					SELECT DISTINCT ON (guid_grwchemanalysis)
						*
					FROM dataquality.qa_grwchemanalysis qa
					ORDER BY guid_grwchemanalysis, opretdato DESC NULLS LAST 
				),
			compound_agg AS 
			-- aggregate strings of compoundnames per sample
				(
					SELECT 
						gca.sampleid,
						qa.is_approved::TEXT AS is_approved,
						string_agg(DISTINCT qa.person || ' - ' || date(qa.opretdato), ' | ') AS previous_user,
						string_agg(cp.compoundno_descr, ',' ORDER BY cp.compoundno_descr) AS compound_agg
					FROM approved_discarted qa
					LEFT JOIN jupiter.grwchemanalysis gca ON gca.guid = uuid(qa.guid_grwchemanalysis)
					LEFT JOIN 
						(
							SELECT 	
								code::NUMERIC AS compoundno,
								longtext AS compoundno_descr
							FROM jupiter.code
							WHERE codeType = 221
						) AS cp USING (compoundno)
					GROUP BY sampleid, qa.is_approved::TEXT 
				),
			-- aggregate as json object per sample and wheter analysis is approved or discarted
			compound_json AS 
				(
					SELECT 
						sampleid, 
						json_object_agg(is_approved, compound_agg ORDER BY is_approved) AS json_compound
					FROM compound_agg 
					GROUP BY sampleid
				)
		-- unpack json object as a pivot table
		SELECT DISTINCT 
			b.boreholeid,
			b.boreholeno, 
			sampleid,
			json_compound ->> 'false' AS frasorterede_stoffer,
			previous_user,
			--json_compound ->> 'true' AS tidligere_godkendte_stoffer,
			b.geom
		FROM compound_json
		LEFT JOIN compound_agg USING (sampleid)
		LEFT JOIN jupiter.grwchemsample gcs USING (sampleid)
		LEFT JOIN jupiter.borehole b USING (boreholeid)
	)
;

/* 
 * mstvw_substance_All_data
 */

BEGIN;

	DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_All_data CASCADE;
	
	CREATE TABLE mstjupiter.mstmvw_substance_All_data AS 
		(
			SELECT DISTINCT 
				row_number() OVER () AS row_id,
				cs.*,
				ic.Alkalinitet_total_TA,
				ic.Aluminium,
				ic.Ammoniak_ammonium,
				ic.Ammonium_N ,
				ic.Anioner_total,
				ic.Arsen,
				ic.Barium,
				ic.Bly,
				ic.Bor,
				ic.Bromid,
				ic.Calcium,
				ic.Carbon_organisk_NVOC,
				ic.Carbonat,
				ic.Carbondioxid_aggr,
				ic.Chlorid,
				ic.Dihydrogensulfid,
				ic.Fluorid,
				ic.Hydrogencarbonat,
				ic.Inddampningsrest,
				ic.Jern,
				ic.Kalium,
				ic.Kationer_total,
				ic.Kobber,
				ic.Kobolt_Co,
				ic.Konduktivitet,
				ic.Magnesium,
				ic.Mangan,
				ic.Methan,
				ic.Natrium,
				ic.Natriumhydrogencarbonat,
				ic.Nikkel,
				ic.Nitrat,
				ic.Nitrit,
				ic.Orthophosphat,
				ic.Ortho_phosphat_P,
				ic.Oxygen_indhold,
				ic.Permanganattal_KMnO4,
				ic.pH,
				ic.Phosphor_total_P,
				ic.Redoxpotentiale,
				ic.Siliciumdioxid,
				ic.Strontium,
				ic.Sulfat,
				ic.Sulfit_S,
				ic.Temperatur,
				ic.Tørstof_total,
				ic.Zink
			FROM mstjupiter.inorganic_crosstab ic
			INNER JOIN mstjupiter.chem_samp cs USING (sampleid)
			WHERE COALESCE(
				ic.Alkalinitet_total_TA,
				ic.Aluminium,
				ic.Ammoniak_ammonium,
				ic.Ammonium_N ,
				ic.Anioner_total,
				ic.Arsen,
				ic.Barium,
				ic.Bly,
				ic.Bor,
				ic.Bromid,
				ic.Calcium,
				ic.Carbon_organisk_NVOC,
				ic.Carbonat,
				ic.Carbondioxid_aggr,
				ic.Chlorid,
				ic.Dihydrogensulfid,
				ic.Fluorid,
				ic.Hydrogencarbonat,
				ic.Inddampningsrest,
				ic.Jern,
				ic.Kalium,
				ic.Kationer_total,
				ic.Kobber,
				ic.Kobolt_Co,
				ic.Konduktivitet,
				ic.Magnesium,
				ic.Mangan,
				ic.Methan,
				ic.Natrium,
				ic.Natriumhydrogencarbonat,
				ic.Nikkel,
				ic.Nitrat,
				ic.Nitrit,
				ic.Orthophosphat,
				ic.Ortho_phosphat_P,
				ic.Oxygen_indhold,
				ic.Permanganattal_KMnO4,
				ic.pH,
				ic.Phosphor_total_P,
				ic.Redoxpotentiale,
				ic.Siliciumdioxid,
				ic.Strontium,
				ic.Sulfat,
				ic.Sulfit_S,
				ic.Temperatur,
				ic.Tørstof_total,
				ic.Zink
				) IS NOT NULL 
		  	ORDER BY cs.boreholeno, cs.intakeno, cs.sampledate 
		)
	;
	
	CREATE INDEX mstmvw_substance_All_data_geom_idx
  		ON mstjupiter.mstmvw_substance_All_data
  		USING GIST (geom)
  	;
  	
  	CREATE INDEX mstmvw_substance_All_data_sampleid_idx
  		ON mstjupiter.mstmvw_substance_All_data
  		USING BTREE (sampleid)
  	;
  
	DO
		$do$
			BEGIN
				EXECUTE 'COMMENT ON TABLE mstjupiter.mstmvw_substance_All_data IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' simak'
		     || ' Table containing all inorganic compounds from chem samples within pcjupiterxl database'
		     || '''';
			END
		$do$
	;

COMMIT;

/* 
 * mstvw_substance_All_data_filtered
 */
BEGIN;

	DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_All_data_filtered CASCADE;
	
	CREATE TABLE mstjupiter.mstmvw_substance_All_data_filtered AS 
		(
	 		SELECT DISTINCT 
	 			row_number() OVER () AS row_id,
	 			frasorterede_stoffer,
	 			previous_user,	 			
	 			cs.*,
				ic.Alkalinitet_total_TA,
				ic.Aluminium,
				ic.Ammoniak_ammonium,
				ic.Ammonium_N ,
				ic.Anioner_total,
				ic.Arsen,
				ic.Barium,
				ic.Bly,
				ic.Bor,
				ic.Bromid,
				ic.Calcium,
				ic.Carbon_organisk_NVOC,
				ic.Carbonat,
				ic.Carbondioxid_aggr,
				ic.Chlorid,
				ic.Dihydrogensulfid,
				ic.Fluorid,
				ic.Hydrogencarbonat,
				ic.Inddampningsrest,
				ic.Jern,
				ic.Kalium,
				ic.Kationer_total,
				ic.Kobber,
				ic.Kobolt_Co,
				ic.Konduktivitet,
				ic.Magnesium,
				ic.Mangan,
				ic.Methan,
				ic.Natrium,
				ic.Natriumhydrogencarbonat,
				ic.Nikkel,
				ic.Nitrat,
				ic.Nitrit,
				ic.Orthophosphat,
				ic.Ortho_phosphat_P,
				ic.Oxygen_indhold,
				ic.Permanganattal_KMnO4,
				ic.pH,
				ic.Phosphor_total_P,
				ic.Redoxpotentiale,
				ic.Siliciumdioxid,
				ic.Strontium,
				ic.Sulfat,
				ic.Sulfit_S,
				ic.Temperatur,
				ic.Tørstof_total,
				ic.Zink
			FROM mstjupiter.inorganic_crosstab ic
			INNER JOIN mstjupiter.chem_samp cs USING (sampleid)
			LEFT JOIN mstjupiter.mstmvw_substance_grwchemanalysis_approved_discarted USING (sampleid)
			WHERE (cs.geom, cs.intakeno) IS NOT NULL
			ORDER BY cs.boreholeno, cs.intakeno, cs.sampledate
		)
	;

	CREATE INDEX mstmvw_substance_All_data_filtered_geom_idx
  		ON mstjupiter.mstmvw_substance_All_data_filtered
  		USING GIST (geom)
  	;
  	
  	CREATE INDEX mstmvw_substance_All_data_filtered_sampleid_idx
  		ON mstjupiter.mstmvw_substance_All_data_filtered
  		USING BTREE (sampleid)
  	;
  
	DO
		$do$
			BEGIN
				EXECUTE 'COMMENT ON TABLE mstjupiter.mstmvw_substance_All_data_filtered IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' simak'
		     || ' Table containing all filtered inorganic compounds from chem samples within pcjupiterxl database'
		     || '''';
			END
		$do$
	;

COMMIT;

/*
 * mstvw_substance_arsenic_latest
 */
BEGIN;

	DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_arsenic_latest CASCADE;
	
	CREATE TABLE mstjupiter.mstmvw_substance_arsenic_latest AS 
		(
			WITH
				samples AS 
					(
						SELECT
							cs.*,
							ic.arsen as arsen_mugprl,
							CASE
								WHEN ic.arsen < 0 
									THEN NULL
								WHEN ic.arsen <= 0.03 
									THEN 1
								WHEN ic.arsen > 0.03 AND ic.arsen <= 1 
									THEN 2
								WHEN ic.arsen > 1 AND ic.arsen <= 2.5 
									THEN 3
								WHEN ic.arsen > 2.5 AND ic.arsen <= 5 
									THEN 4
								WHEN ic.arsen > 5 
									THEN 5
								ELSE NULL
								END AS arsen_int,
							CASE 
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC < 1990 
									THEN 1
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC between 1990 AND 2009 
									THEN 2
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC >= 2010 
									THEN 3
								ELSE NULL
								END AS Aarstal_int
						FROM mstjupiter.inorganic_crosstab ic
						INNER JOIN mstjupiter.chem_samp cs USING (sampleid)
						WHERE ic.arsen IS NOT NULL
							AND cs.geom IS NOT NULL
					)
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				ROW_NUMBER() over (ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate) AS row_id,
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
		)
	;

	CREATE INDEX mstmvw_substance_arsenic_latest_geom_idx
  		ON mstjupiter.mstmvw_substance_arsenic_latest
  		USING GIST (geom)
  	;
  	
  	CREATE INDEX mstmvw_substance_arsenic_latest_sampleid_idx
  		ON mstjupiter.mstmvw_substance_arsenic_latest
  		USING BTREE (sampleid)
  	;
  
	DO
		$do$
			BEGIN
				EXECUTE 'COMMENT ON TABLE mstjupiter.mstmvw_substance_arsenic_latest IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' simak'
		     || ' Table containing the most recent arsenic chem samples from pcjupiterxl database'
		     || '''';
			END
		$do$
	;

COMMIT;

/*
 * mstvw_substance_chloride_and_sulphate_latest
 */
BEGIN;

	DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_chloride_and_sulphate_latest CASCADE;
	
	CREATE TABLE mstjupiter.mstmvw_substance_chloride_and_sulphate_latest AS 
		(
			WITH
				samples AS 
					(
						SELECT
							cs.*,
							ic.chlorid AS klorid_mgprl,	
							ic.sulfat AS sulfat_mgprl,
							CASE 
								WHEN ic.chlorid < 0 THEN NULL
								WHEN ic.chlorid <= 30 THEN 1
								WHEN ic.chlorid > 30 AND ic.chlorid <= 75 THEN 2
								WHEN ic.chlorid > 75 AND ic.chlorid <= 125 THEN 3
								WHEN ic.chlorid > 125 AND ic.chlorid <= 250 THEN 4
								WHEN ic.chlorid > 250 THEN 5
								ELSE NULL
								END AS klorid_int,
							CASE 
								WHEN ic.sulfat < 0 THEN NULL
								WHEN ic.sulfat <= 5 THEN 1
								WHEN ic.sulfat > 5 AND ic.sulfat <= 20 THEN 2
								WHEN ic.sulfat > 20 AND ic.sulfat <= 70 THEN 3
								WHEN ic.sulfat > 70 AND ic.sulfat <= 250 THEN 4
								WHEN ic.sulfat > 250 THEN 5
								ELSE NULL
								END AS sulfat_int,
							case 
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC < 1990 THEN 1
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC between 1990 AND 2009 THEN 2
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC >= 2010 THEN 3
								ELSE NULL
								END AS Aarstal_int
						FROM mstjupiter.inorganic_crosstab ic
						INNER JOIN mstjupiter.chem_samp cs USING (sampleid)
						WHERE ic.chlorid IS NOT NULL
							AND ic.sulfat IS NOT NULL
							AND cs.geom IS NOT NULL
					)
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				ROW_NUMBER() over (ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate) AS row_id,
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
		)
	;

	CREATE INDEX mstmvw_substance_chloride_and_sulphate_latest_geom_idx
  		ON mstjupiter.mstmvw_substance_chloride_and_sulphate_latest
  		USING GIST (geom)
  	;
  	
  	CREATE INDEX mstmvw_substance_chloride_and_sulphate_latest_sampleid_idx
  		ON mstjupiter.mstmvw_substance_chloride_and_sulphate_latest
  		USING BTREE (sampleid)
  	;
  
	DO
		$do$
			BEGIN
				EXECUTE 'COMMENT ON TABLE mstjupiter.mstmvw_substance_chloride_and_sulphate_latest IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' simak'
		     || ' Table containing the most recent chloride and sulphate chem samples from pcjupiterxl database'
		     || '''';
			END
		$do$
	;

COMMIT;

/*
 * mstvw_substance_chloride_latest
 */
BEGIN;

	DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_chloride_latest CASCADE;
	
	CREATE TABLE mstjupiter.mstmvw_substance_chloride_latest AS 
		(
			WITH
				samples AS 
					(
						SELECT
							cs.*,
							ic.chlorid AS klorid_mgprl,
							ic.natrium AS natrium_mgprl,
							case
								WHEN ic.chlorid < 0 THEN NULL
								WHEN ic.chlorid <= 30 THEN 1
								WHEN ic.chlorid > 30 AND ic.chlorid <= 75 THEN 2
								WHEN ic.chlorid > 75 AND ic.chlorid <= 125 THEN 3
								WHEN ic.chlorid > 125 AND ic.chlorid <= 250 THEN 4
								WHEN ic.chlorid > 250 THEN 5
								ELSE NULL
								END AS klorid_int,
							case 
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC < 1990 THEN 1
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC between 1990 AND 2009 THEN 2
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC >= 2010 THEN 3
								ELSE NULL
								END AS Aarstal_int
						FROM mstjupiter.inorganic_crosstab ic
						INNER JOIN mstjupiter.chem_samp cs USING (sampleid)
						WHERE ic.chlorid IS NOT NULL
							AND cs.geom IS NOT NULL
					)
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				ROW_NUMBER() over (ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate) AS row_id,
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
		)
	;

	CREATE INDEX mstmvw_substance_chloride_latest_geom_idx
  		ON mstjupiter.mstmvw_substance_chloride_latest
  		USING GIST (geom)
  	;
  	
  	CREATE INDEX mstmvw_substance_chloride_latest_sampleid_idx
  		ON mstjupiter.mstmvw_substance_chloride_latest
  		USING BTREE (sampleid)
  	;
  
	DO
		$do$
			BEGIN
				EXECUTE 'COMMENT ON TABLE mstjupiter.mstmvw_substance_chloride_latest IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' simak'
		     || ' Table containing the most recent chloride samples from pcjupiterxl database'
		     || '''';
			END
		$do$
	;

COMMIT;

/*
 * mstvw_substance_iron_latest
 */
BEGIN;

	DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_iron_latest CASCADE;
	
	CREATE TABLE mstjupiter.mstmvw_substance_iron_latest AS 
		(
			WITH
				samples AS
					(
						SELECT
							cs.*,
							ic.jern AS jern_mgprl,
							case 
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC < 1990 THEN 1
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC BETWEEN 1990 AND 2009 THEN 2
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC >= 2010 THEN 3
								ELSE NULL
								END AS Aarstal_int
						FROM mstjupiter.inorganic_crosstab ic
						INNER JOIN mstjupiter.chem_samp cs USING (sampleid)
						WHERE ic.jern IS NOT NULL
							AND cs.geom IS NOT NULL
					)
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				ROW_NUMBER() over (ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate) AS row_id,
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
		)
	;

	CREATE INDEX mstmvw_substance_iron_latest_geom_idx
  		ON mstjupiter.mstmvw_substance_iron_latest
  		USING GIST (geom)
  	;
  	
  	CREATE INDEX mstmvw_substance_iron_latest_sampleid_idx
  		ON mstjupiter.mstmvw_substance_iron_latest
  		USING BTREE (sampleid)
  	;
  
	DO
		$do$
			BEGIN
				EXECUTE 'COMMENT ON TABLE mstjupiter.mstmvw_substance_iron_latest IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' simak'
		     || ' Table containing the most recent iron chem samples from pcjupiterxl database'
		     || '''';
			END
		$do$
	;

COMMIT;

/*
 * mstvw_substance_watertype_latest
 */
BEGIN;

	DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_watertype_latest CASCADE;
	
	CREATE TABLE mstjupiter.mstmvw_substance_watertype_latest AS 
		(
			WITH
				samples AS 
					(
						SELECT
							cs.*,
							ic.nitrat AS Nitrat_mgprl,		
							ic.jern AS Jern_mgprl,	
							ic.sulfat AS sulfat_mgprl,
							ic.oxygen_indhold AS Ilt_mgprl,
							ic.mangan AS mangan_mgprl,
							ic.ammonium_n AS ammonium_n_mgprl,
							CASE
					            WHEN 
						            ic.nitrat > 1
									AND ic.jern >= 0.2 
						            THEN 'X'::text
								WHEN 
						            ic.nitrat > 1 
						            AND ic.jern < 0.2
									AND ic.jern >=0
									AND (ic.oxygen_indhold IS NULL)-- OR ic.oxygen_indhold <= 0)
						            THEN 'AB'::text
								WHEN 
						            ic.nitrat > 1 
						            AND ic.jern < 0.2
									AND ic.jern >= 0
						            AND ic.oxygen_indhold < 1 
						            THEN 'B'::text
								WHEN 
						            ic.nitrat > 1 
						            AND ic.jern < 0.2
									AND ic.jern >= 0
						            AND ic.oxygen_indhold >= 1 
						            THEN 'A'::text
								WHEN
									ic.nitrat <= 1
									AND ic.nitrat >= 0
						            AND ic.jern < 0.2
									AND ic.jern >= 0
									AND ic.oxygen_indhold >= 1
									AND ic.mangan < 0.01
									AND ic.mangan >= 0
									AND ic.ammonium_n < 0.01
									AND ic.ammonium_n >= 0
						            THEN 'AY'::text
					            WHEN 
						            ic.nitrat <= 1
									AND ic.nitrat >= 0
						            AND ic.jern < 0.2
									AND ic.jern >= 0
						            THEN 'Y'::text
					            WHEN 
						            ic.nitrat <= 1
									AND ic.nitrat >= 0
						            AND ic.jern >= 0.2
						            AND ic.sulfat < 20
									AND ic.sulfat >= 0
						            THEN 'D'::text
					            WHEN 
						            ic.nitrat <= 1
									AND ic.nitrat >= 0
						            AND ic.jern >= 0.2
						            AND ic.sulfat >= 20 AND ic.sulfat < 70 
									THEN 'C1'::text
								WHEN
									ic.nitrat <= 1
									AND ic.nitrat >= 0
						            AND ic.jern >= 0.2
						            AND ic.sulfat >= 70  
						            THEN 'C2'::text
					            ELSE NULL
								END AS vandtype,
							CASE  
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC < 1990 THEN 1
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC between 1990 AND 2009 THEN 2
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC >= 2010 THEN 3
								ELSE NULL
								END AS Aarstal_int
						FROM mstjupiter.inorganic_crosstab ic
						INNER JOIN mstjupiter.chem_samp cs USING (sampleid)
						WHERE ic.jern IS NOT NULL
							AND ic.nitrat IS NOT NULL
							AND ic.sulfat IS NOT NULL
							AND cs.geom IS NOT NULL
					)
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				ROW_NUMBER() over (ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate) AS row_id,
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
		)
	;

	CREATE INDEX mstmvw_substance_watertype_latest_geom_idx
  		ON mstjupiter.mstmvw_substance_watertype_latest
  		USING GIST (geom)
  	;
  	
  	CREATE INDEX mstmvw_substance_watertype_latest_sampleid_idx
  		ON mstjupiter.mstmvw_substance_watertype_latest
  		USING BTREE (sampleid)
  	;
  
	DO
		$do$
			BEGIN
				EXECUTE 'COMMENT ON TABLE mstjupiter.mstmvw_substance_watertype_latest IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' simak'
		     || ' Table containing the most recent watertype chem samples from pcjupiterxl database'
		     || '''';
			END
		$do$
	;

COMMIT;

/*
 * mstvw_substance_nitrate_and_sulphate_latest
 */
BEGIN;

	DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_nitrate_and_sulphate_latest CASCADE;
	
	CREATE TABLE mstjupiter.mstmvw_substance_nitrate_and_sulphate_latest AS 
		(
			WITH
				samples AS 
					(
						SELECT
							cs.*,
							ic.nitrat AS Nitrat_mgprl,		
							ic.sulfat AS sulfat_mgprl,
							CASE
								WHEN ic.nitrat = -99 THEN null
								WHEN ic.nitrat <= 1 THEN 1
								WHEN ic.nitrat > 1 AND ic.nitrat <= 10 THEN 2
								WHEN ic.nitrat > 10 AND ic.nitrat <= 25 THEN 3
								WHEN ic.nitrat > 25 AND ic.nitrat <= 50 THEN 4
								WHEN ic.nitrat > 50 THEN 5
								ELSE NULL
								END AS Nitrat_int,
							CASE
								WHEN ic.sulfat = -99 THEN null
								WHEN ic.sulfat <= 5 THEN 1
								WHEN ic.sulfat > 5 AND ic.sulfat <= 20 THEN 2
								WHEN ic.sulfat > 20 AND ic.sulfat <= 70 THEN 3
								WHEN ic.sulfat > 70 AND ic.sulfat <= 250 THEN 4
								WHEN ic.sulfat > 250 THEN 5
								ELSE NULL
								END AS sulfat_int,
							CASE 
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC < 1990 THEN 1
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC BETWEEN 1990 AND 2009 THEN 2
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC >= 2010 THEN 3
								ELSE NULL
								END AS Aarstal_int
						FROM mstjupiter.inorganic_crosstab ic
						INNER JOIN mstjupiter.chem_samp cs USING (sampleid)
						WHERE ic.nitrat IS NOT NULL
							AND ic.sulfat IS NOT NULL
							AND	cs.geom IS NOT NULL
					)
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				ROW_NUMBER() over (ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate) AS row_id,
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
		)
	;

	CREATE INDEX mstmvw_substance_nitrate_and_sulphate_latest_geom_idx
  		ON mstjupiter.mstmvw_substance_nitrate_and_sulphate_latest
  		USING GIST (geom)
  	;
  	
  	CREATE INDEX mstmvw_substance_nitrate_and_sulphate_latest_sampleid_idx
  		ON mstjupiter.mstmvw_substance_nitrate_and_sulphate_latest
  		USING BTREE (sampleid)
  	;
  
	DO
		$do$
			BEGIN
				EXECUTE 'COMMENT ON TABLE mstjupiter.mstmvw_substance_nitrate_and_sulphate_latest IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' simak'
		     || ' Table containing the most recent nitrate and sulphate chem samples from pcjupiterxl database'
		     || '''';
			END
		$do$
	;

COMMIT;

/*
 * mstvw_substance_Nitrate_latest
 */
BEGIN;

	DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_Nitrate_latest CASCADE;
	
	CREATE TABLE mstjupiter.mstmvw_substance_Nitrate_latest AS 
		(
			WITH 
				samples AS 
					(
						SELECT 
							cs.*,
							ic.nitrat AS Nitrat_mgprl,
							CASE
								WHEN ic.nitrat < 0 THEN NULL
								WHEN ic.nitrat <= 1 THEN 1
								WHEN ic.nitrat > 1 AND ic.nitrat <= 10 THEN 2
								WHEN ic.nitrat > 10 AND ic.nitrat <= 25 THEN 3
								WHEN ic.nitrat > 25 AND ic.nitrat <= 50 THEN 4
								WHEN ic.nitrat > 50 THEN 5
								ELSE NULL
								END AS Nitrat_int,
							CASE 
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC < 1990 THEN 1
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC BETWEEN 1990 AND 2009 THEN 2
								WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC >= 2010 THEN 3
								ELSE NULL
								END AS Aarstal_int,
							ic.ammoniak_ammonium AS ammoniak_ammonium,
							ic.methan AS metan,
							ic.jern AS jern,
							ic.nitrit AS nitrit,
							(ic.nitrat / 50) + (nitrit / 3) AS nitrat_nitrit_eq
						FROM mstjupiter.inorganic_crosstab ic
						INNER JOIN mstjupiter.chem_samp cs USING (sampleid)
						WHERE ic.nitrat IS NOT NULL
							AND cs.geom IS NOT NULL
					)
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				ROW_NUMBER() over (ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate) AS row_id,
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
		)
	;

	CREATE INDEX mstmvw_substance_Nitrate_latest_geom_idx
  		ON mstjupiter.mstmvw_substance_Nitrate_latest
  		USING GIST (geom)
  	;
  	
  	CREATE INDEX mstmvw_substance_Nitrate_latest_sampleid_idx
  		ON mstjupiter.mstmvw_substance_Nitrate_latest
  		USING BTREE (sampleid)
  	;
  
	DO
		$do$
			BEGIN
				EXECUTE 'COMMENT ON TABLE mstjupiter.mstmvw_substance_Nitrate_latest IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' simak'
		     || ' Table containing the most recent nitrate chem samples from pcjupiterxl database'
		     || '''';
			END
		$do$
	;

COMMIT;

/*
 * mstvw_substance_nvoc_latest
 */
BEGIN;

	DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_nvoc_latest CASCADE;
	
	CREATE TABLE mstjupiter.mstmvw_substance_nvoc_latest AS 
		(
			WITH 
				samples as(
					SELECT 
						cs.*,
						ic.Carbon_organisk_NVOC AS nvoc_mgprl,
						CASE 
							WHEN ic.Carbon_organisk_NVOC < 0 THEN NULL
							WHEN ic.Carbon_organisk_NVOC <= 0.1 THEN 1
							WHEN ic.Carbon_organisk_NVOC <= 0.5 AND ic.Carbon_organisk_NVOC > 0.1 THEN 2
							WHEN ic.Carbon_organisk_NVOC <= 1 AND ic.Carbon_organisk_NVOC > 0.5 THEN 3
							WHEN ic.Carbon_organisk_NVOC <= 4 AND ic.Carbon_organisk_NVOC > 1 THEN 4
							WHEN ic.Carbon_organisk_NVOC > 4 THEN 5
							ELSE NULL
							END AS nvoc_int,
						CASE  
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC < 1990 THEN 1
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC between 1990 AND 2009 THEN 2
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC >= 2010 THEN 3
							ELSE NULL
							END AS Aarstal_int
					FROM mstjupiter.inorganic_crosstab ic
					INNER JOIN mstjupiter.chem_samp cs USING (sampleid)
					WHERE ic.Carbon_organisk_NVOC IS NOT NULL 
						AND cs.geom IS NOT NULL
				)
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				ROW_NUMBER() over (ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate) AS row_id,
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
		)
	;

	CREATE INDEX mstmvw_substance_nvoc_latest_geom_idx
  		ON mstjupiter.mstmvw_substance_nvoc_latest
  		USING GIST (geom)
  	;
  	
  	CREATE INDEX mstmvw_substance_nvoc_latest_sampleid_idx
  		ON mstjupiter.mstmvw_substance_nvoc_latest
  		USING BTREE (sampleid)
  	;
  
	DO
		$do$
			BEGIN
				EXECUTE 'COMMENT ON TABLE mstjupiter.mstmvw_substance_nvoc_latest IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' simak'
		     || ' Table containing the most recent nvoc chem samples from pcjupiterxl database'
		     || '''';
			END
		$do$
	;

COMMIT;

/*
 * mstvw_substance_sodium_chloride_IonExchange_latest
 */

BEGIN;
	
	DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_sodium_chloride_IonExchange_latest CASCADE;
	
	CREATE TABLE mstjupiter.mstmvw_substance_sodium_chloride_IonExchange_latest AS 
		(
			WITH	
				samples AS (
					SELECT
						cs.*,
						ic.natrium AS natrium_mgprl,
						ic.chlorid AS klorid_mgprl,
						CASE 
							WHEN cs.ionexchange > 2
								THEN 1
							WHEN cs.ionexchange <= 2
								AND cs.ionexchange > 1.15
								THEN 2
							WHEN cs.ionexchange <= 1.15
								AND cs.ionexchange > 0.65
								THEN 3
							WHEN cs.ionexchange <= 0.65
								AND cs.ionexchange > 0.5
								THEN 4
							WHEN cs.ionexchange != 0 
								AND cs.ionexchange <= 0.5
								THEN 5
							ELSE NULL 
							END AS Ionbytningsgrad_int, 
						CASE 
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC < 1990 
								THEN 1
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC BETWEEN 1990 AND 2009 
								THEN 2
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC >= 2010 
								THEN 3
							ELSE NULL
							END AS Aarstal_int
					FROM mstjupiter.inorganic_crosstab ic
					INNER JOIN mstjupiter.chem_samp cs USING (sampleid)
					WHERE ic.chlorid IS NOT NULL
						AND ic.natrium IS NOT NULL
						AND cs.geom IS NOT NULL
				)	
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				ROW_NUMBER() over (ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate) AS row_id,
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
		)
	;

	CREATE INDEX mstmvw_substance_sodium_chloride_IonExchange_latest_geom_idx
  		ON mstjupiter.mstmvw_substance_sodium_chloride_IonExchange_latest
  		USING GIST (geom)
  	;
  	
  	CREATE INDEX mstmvw_substance_sodium_chloride_IonExchange_latest_sampleid_idx
  		ON mstjupiter.mstmvw_substance_sodium_chloride_IonExchange_latest
  		USING BTREE (sampleid)
  	;
  
	DO
		$do$
			BEGIN
				EXECUTE 'COMMENT ON TABLE mstjupiter.mstmvw_substance_sodium_chloride_IonExchange_latest IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' simak'
		     || ' Table containing the most recent ionexchange chem samples from pcjupiterxl database'
		     || '''';
			END
		$do$
	;

COMMIT;

/*
 * mstvw_substance_sulphate_latest
 */
BEGIN;

	DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_sulphate_latest CASCADE;
	
	CREATE TABLE mstjupiter.mstmvw_substance_sulphate_latest AS 
		(
			WITH 
				samples AS (
					SELECT	
						cs.*,
						ic.sulfat  AS sulfat_mgprl,
						CASE 
							WHEN ic.sulfat < 0 THEN NULL
							WHEN ic.sulfat <= 5 THEN 1
							WHEN ic.sulfat > 5 AND ic.sulfat <= 20 THEN 2
							WHEN ic.sulfat > 20 AND ic.sulfat <= 70 THEN 3
							WHEN ic.sulfat > 70 AND ic.sulfat <= 250 THEN 4
							WHEN ic.sulfat > 250 THEN 5
							ELSE NULL
							END AS sulfat_int,
						CASE  
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC < 1990 THEN 1
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC BETWEEN 1990 AND 2009 THEN 2
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC >= 2010 THEN 3
							ELSE NULL
							END AS Aarstal_int
					FROM mstjupiter.inorganic_crosstab ic
					INNER JOIN mstjupiter.chem_samp cs USING (sampleid)
					WHERE ic.sulfat IS NOT NULL
						AND cs.geom IS NOT NULL
				)
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				ROW_NUMBER() over (ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate) AS row_id,
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
		)
	;

	CREATE INDEX mstmvw_substance_sulphate_latest_latest_geom_idx
  		ON mstjupiter.mstmvw_substance_sulphate_latest
  		USING GIST (geom)
  	;
  	
  	CREATE INDEX mstmvw_substance_sulphate_latest_sampleid_idx
  		ON mstjupiter.mstmvw_substance_sulphate_latest
  		USING BTREE (sampleid)
  	;
  
	DO
		$do$
			BEGIN
				EXECUTE 'COMMENT ON TABLE mstjupiter.mstmvw_substance_sulphate_latest IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' simak'
		     || ' Table containing the most recent sulphate chem samples from pcjupiterxl database'
		     || '''';
			END
		$do$
	;
	
COMMIT;

/*
 * mstmvw_substance_strontium_latest
 */
BEGIN;

	DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_strontium_latest CASCADE;
	
	CREATE TABLE mstjupiter.mstmvw_substance_strontium_latest AS 
		(
			WITH
				samples AS (
					SELECT
						cs.*,
						ic.strontium as strontium_mugprl,
						CASE 
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC < 1990 
								THEN 1
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC BETWEEN 1990 AND 2009 
								THEN 2
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC >= 2010 
								THEN 3
							ELSE NULL
							END AS Aarstal_int
					FROM mstjupiter.inorganic_crosstab ic
					INNER JOIN mstjupiter.chem_samp cs USING (sampleid)
					WHERE ic.strontium IS NOT NULL
						AND cs.geom IS NOT NULL
				)
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				ROW_NUMBER() over (ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate) AS row_id,
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
		)
	;

	CREATE INDEX mstmvw_substance_strontium_latest_geom_idx
  		ON mstjupiter.mstmvw_substance_strontium_latest
  		USING GIST (geom)
  	;
  	
  	CREATE INDEX mstmvw_substance_strontium_latest_sampleid_idx
  		ON mstjupiter.mstmvw_substance_strontium_latest
  		USING BTREE (sampleid)
  	;
  
	DO
		$do$
			BEGIN
				EXECUTE 'COMMENT ON TABLE mstjupiter.mstmvw_substance_strontium_latest IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' simak'
		     || ' Table containing the most recent strontium chem samples from pcjupiterxl database'
		     || '''';
			END
		$do$
	;

COMMIT;

/*
 * mstmvw_substance_fluoride_latest
 */
BEGIN;

	DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_fluoride_latest CASCADE;
	
	CREATE TABLE mstjupiter.mstmvw_substance_fluoride_latest AS 
		(
			WITH
				samples AS (
					SELECT
						cs.*,
						ic.fluorid as fluorid_mgprl,
						CASE 
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC < 1990 
								THEN 1
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC BETWEEN 1990 AND 2009 
								THEN 2
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC >= 2010 
								THEN 3
							ELSE NULL
							END AS Aarstal_int
					FROM mstjupiter.inorganic_crosstab ic
					INNER JOIN mstjupiter.chem_samp cs USING (sampleid)
					WHERE cs.geom IS NOT NULL
						AND ic.fluorid IS NOT NULL
				)
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				ROW_NUMBER() over (ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate) AS row_id,
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
		)
	;

	CREATE INDEX mstmvw_substance_fluoride_latest_geom_idx
  		ON mstjupiter.mstmvw_substance_fluoride_latest
  		USING GIST (geom)
  	;
  	
  	CREATE INDEX mstmvw_substance_fluoride_latest_sampleid_idx
  		ON mstjupiter.mstmvw_substance_fluoride_latest
  		USING BTREE (sampleid)
  	;
  
	DO
		$do$
			BEGIN
				EXECUTE 'COMMENT ON TABLE mstjupiter.mstmvw_substance_fluoride_latest IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' simak'
		     || ' Table containing the most recent fluoride chem samples from pcjupiterxl database'
		     || '''';
			END
		$do$
	;

COMMIT;

/*
 * mstmvw_substance_ammoniak_latest
 */
BEGIN;

	DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_ammoniak_latest CASCADE;
	
	CREATE TABLE mstjupiter.mstmvw_substance_ammoniak_latest AS 
		(
			WITH
				samples AS (
					SELECT
						cs.*,
						ic.Ammonium_N as Ammonium_N_mgprl,
						ic.Ammoniak_ammonium as Ammoniak_ammonium_mgprl,
						CASE 
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC < 1990 
								THEN 1
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC BETWEEN 1990 AND 2009 
								THEN 2
							WHEN EXTRACT(YEAR FROM cs.sampledate)::NUMERIC >= 2010 
								THEN 3
							ELSE NULL
							END AS Aarstal_int
					FROM mstjupiter.inorganic_crosstab ic
					INNER JOIN mstjupiter.chem_samp cs USING (sampleid)
					WHERE cs.geom IS NOT NULL
						AND ic.Ammonium_N IS NOT NULL 
				)
			SELECT DISTINCT ON (s.boreholeno, s.intakeno, s.guid_intake)
				ROW_NUMBER() over (ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate) AS row_id,
				s.*
			FROM samples s
			ORDER BY s.boreholeno, s.intakeno, s.guid_intake, s.sampledate DESC NULLS LAST 
		)
	;

	CREATE INDEX mstmvw_substance_ammoniak_latest_geom_idx
  		ON mstjupiter.mstmvw_substance_ammoniak_latest
  		USING GIST (geom)
  	;
  	
  	CREATE INDEX mstmvw_substance_ammoniak_latest_sampleid_idx
  		ON mstjupiter.mstmvw_substance_ammoniak_latest
  		USING BTREE (sampleid)
  	;
  
	DO
		$do$
			BEGIN
				EXECUTE 'COMMENT ON TABLE mstjupiter.mstmvw_substance_ammoniak_latest IS '''
		     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
		     || ' simak'
		     || ' Table containing the most recent ammoniak chem samples from pcjupiterxl database'
		     || '''';
			END
		$do$
	;

COMMIT;

/*
 * mstmvw_substance_aquifer
 */
BEGIN;

	
	DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_aquifer;
	
	CREATE TABLE mstjupiter.mstmvw_substance_aquifer AS 
		(
			SELECT DISTINCT  
	            boreholeno, 
	            intakeno,	
	            xutm32euref89, 
	            yutm32euref89, 
	            dybde_filtertop,
				dybde_filterbund, 
	            NULL AS magasin,
	            geom
	        FROM mstjupiter.chem_samp cs
	        ORDER BY boreholeno
		)
	;

COMMIT;

/*
 * mstmvw_substance_database
 */
BEGIN;

	DROP TABLE IF EXISTS mstjupiter.mstmvw_substance_database;
	
	CREATE TABLE mstjupiter.mstmvw_substance_database AS 
		(
			WITH 
				calcium AS 
					(
						SELECT sampleid 
						FROM jupiter.grwchemanalysis gca 
						WHERE compoundno = 280
					),
				chlorid AS 
					(
						SELECT sampleid 
						FROM jupiter.grwchemanalysis gca 
						WHERE compoundno = 297
					),
				nitrat AS 
					(
						SELECT sampleid 
						FROM jupiter.grwchemanalysis gca 
						WHERE compoundno = 246
					),
				sulfat AS 
					(
						SELECT sampleid 
						FROM jupiter.grwchemanalysis gca 
						WHERE compoundno = 335
					),
				hydrogencarbonat AS 
					(
						SELECT sampleid 
						FROM jupiter.grwchemanalysis gca 
						WHERE compoundno = 59
					),
				kalium AS 
					(
						SELECT sampleid 
						FROM jupiter.grwchemanalysis gca 
						WHERE compoundno = 317
					),
				magnesium AS 
					(
						SELECT sampleid 
						FROM jupiter.grwchemanalysis gca 
						WHERE compoundno = 321
					),
				natrium AS 
					(
						SELECT sampleid 
						FROM jupiter.grwchemanalysis gca 
						WHERE compoundno = 324
					),
				compare AS 
					(
						SELECT DISTINCT calcium.sampleid 
						FROM calcium
						INNER JOIN chlorid USING (sampleid)
						INNER JOIN nitrat USING (sampleid)
						INNER JOIN sulfat USING (sampleid)
						INNER JOIN hydrogencarbonat USING (sampleid)
						INNER JOIN kalium USING (sampleid)
						INNER JOIN magnesium USING (sampleid)
						INNER JOIN natrium USING (sampleid)
					)
			SELECT DISTINCT 
				gca.sampleid, 
				stof.stofnavn, 
				gca.guid AS guid_grwchemanalysis, 
				CASE 
					WHEN c.sampleid IS NULL 
						THEN FALSE 
					ELSE TRUE 
					END AS calc_ionbal,
				b.geom
			FROM jupiter.grwchemanalysis gca 
			INNER JOIN 
				(
					SELECT DISTINCT 
						sampleid
					FROM mstjupiter.chem_samp
				) AS cs USING (sampleid)
			INNER JOIN jupiter.grwchemsample gcs USING (sampleid)
			INNER JOIN 
				(
					SELECT 
						code::NUMERIC AS compoundno,
						longtext AS stofnavn
					FROM jupiter.code c
					WHERE c.codetype = 221
				) AS stof USING (compoundno)
			INNER JOIN jupiter.borehole b USING (boreholeid)
			LEFT JOIN compare c USING (sampleid)
			WHERE compoundno IN (324, 321, 317, 59, 335, 246, 297, 280)
		)
	;

COMMIT; 


GRANT USAGE ON SCHEMA mstjupiter TO jupiterrole;
GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO jupiterrole;

