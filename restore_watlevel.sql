/*
 * Owner: 	Danish Environmental Protection Agency (EPA) 
 * Descr: 	This sql script updates MST-GKOs pcjupiterxl postgresql database
 * 			via GEUS' SQL-gateway --where existing tables are updated using 
 * 			select statements (upserts) executed on a foreign data wrapper. 
 * 			The script is overall optimized toward lowering the execution time
 * 			of the queries from the geus foreign wrapper.
 * 			This means for the large tables, the uuids are translated prior to updating 
 * 			such that the fdw queries does not have to translate bytea into uuid. This is done
 * 			to lower the queries time of execution on the fdw server due to 
 * 			timeout errors.
 * ERT: 	~20 min 
 * Author: 	Simon Makwarth <simak@mst.dk>
 * Created: 2022-02-22
 * License:	GNU GPL v3
 */

BEGIN; 	
	
	-- create mstjupiter schema if it does not exists

	CREATE SCHEMA IF NOT EXISTS mstjupiter;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON SCHEMA mstjupiter IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: Data from pcjupiterxl'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- create jupiter schema if it does not exists

	CREATE SCHEMA IF NOT EXISTS jupiter;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON SCHEMA jupiter IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: MSTGKO derived data from pcjupiterxl'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	CREATE TABLE IF NOT EXISTS mstjupiter.mst_exportdate AS 
		(
			SELECT 
				'test_string'::TEXT AS tablename,
				'1900-01-01'::timestamp AS updatetime
			LIMIT 0
		)
	;

	ALTER TABLE mstjupiter.mst_exportdate
		DROP CONSTRAINT IF EXISTS mst_exportdate_pkey CASCADE
	; 

	ALTER TABLE mstjupiter.mst_exportdate
		ADD PRIMARY KEY (tablename)
	;

COMMIT;

-- watlevel

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.watlevel AS
		(
			SELECT 
				t2.watlevelid,
				t2.boreholeid,
				t2.intakeid,
				t2.boreholeno,
				t2.watlevelno,
				t2.intakeno,
				t2.timeofmeas,
				t2.project,
				t2.waterlevel,
				t2.watlevgrsu,
				t2.watlevmsl,
				t2.watlevmp,
				t2.hoursnopum,
				t2.category,
				t2."method",
				t2.quality,
				t2.refpoint,
				t2.remark,
				t2.verticaref,
				t2.atmospresshpa,
				t2.extremes,
				t2.situation,
				t2.watlevelroundno,
				t2.qualitycontrol,
				CAST(ENCODE(t2.guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(t2.guid_intake, 'hex') AS UUID) AS guid_intake,
				CAST(ENCODE(t2.guid_watlevround, 'hex') AS UUID) AS guid_watlevround,
				t2.insertdate,
				t2.updatedate,
				t2.insertuser,
				t2.updateuser,
				t2.dataowner
			FROM geus_fdw.watlevel t2
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.watlevel 
		DROP CONSTRAINT IF EXISTS watlevel_pkey CASCADE
	;

	ALTER TABLE jupiter.watlevel 
		DROP CONSTRAINT IF EXISTS fk_borehole_watlevel CASCADE
	;
	
	ALTER TABLE jupiter.watlevel 
		DROP CONSTRAINT IF EXISTS fk_intake_watlevel CASCADE
	;

	DROP INDEX IF EXISTS watlevel_boreholeid_idx CASCADE;

	DROP INDEX IF EXISTS watlevel_intakeno_idx;
	
	DROP INDEX IF EXISTS watlevel_waterlevel_idx;
	
	DROP INDEX IF EXISTS watlevel_watlevgrsu_idx;
	
	DROP INDEX IF EXISTS watlevel_watlevmsl_idx;
	
	DROP INDEX IF EXISTS watlevel_watlevmp_idx; 	

	TRUNCATE jupiter.watlevel;

	INSERT INTO jupiter.watlevel AS t1
		(
			watlevelid,
			boreholeid,
			intakeid,
			boreholeno,
			watlevelno,
			intakeno,
			timeofmeas,
			project,
			waterlevel,
			watlevgrsu,
			watlevmsl,
			watlevmp,
			hoursnopum,
			category,
			"method",
			quality,
			refpoint,
			remark,
			verticaref,
			atmospresshpa,
			extremes,
			situation,
			watlevelroundno,
			qualitycontrol,
			guid,
			guid_intake,
			guid_watlevround,
			insertdate,
			updatedate,
			insertuser,
			updateuser,
			dataowner
		)
		(
			SELECT 
				watlevelid,
				boreholeid,
				intakeid,
				boreholeno,
				watlevelno,
				intakeno,
				timeofmeas,
				project,
				waterlevel,
				watlevgrsu,
				watlevmsl,
				watlevmp,
				hoursnopum,
				category,
				"method",
				quality,
				refpoint,
				remark,
				verticaref,
				atmospresshpa,
				extremes,
				situation,
				watlevelroundno,
				qualitycontrol,
				CAST(ENCODE(t2.guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(t2.guid_intake, 'hex') AS UUID) AS guid_intake,
				CAST(ENCODE(t2.guid_watlevround, 'hex') AS UUID) AS guid_watlevround,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				dataowner
			FROM geus_fdw.watlevel t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (guid) --ON CONSTRAINT watlevel_pkey 
--			DO UPDATE SET 
--				watlevelid = excluded.watlevelid,
--				boreholeid = excluded.boreholeid,
--				intakeid = excluded.intakeid,
--				boreholeno = excluded.boreholeno,
--				watlevelno = excluded.watlevelno,
--				intakeno = excluded.intakeno,
--				timeofmeas = excluded.timeofmeas,
--				project = excluded.project,
--				waterlevel = excluded.waterlevel,
--				watlevgrsu = excluded.watlevgrsu,
--				watlevmsl = excluded.watlevmsl,
--				watlevmp = excluded.watlevmp,
--				hoursnopum = excluded.hoursnopum,
--				category = excluded.category,
--				"method" = excluded."method",
--				quality = excluded.quality,
--				refpoint = excluded.refpoint,
--				remark = excluded.remark,
--				verticaref = excluded.verticaref,
--				atmospresshpa = excluded.atmospresshpa,
--				extremes = excluded.extremes,
--				situation = excluded.situation,
--				watlevelroundno = excluded.watlevelroundno,
--				qualitycontrol = excluded.qualitycontrol,
--				guid = excluded.guid,
--				guid_intake = excluded.guid_intake,
--				guid_watlevround = excluded.guid_watlevround,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate,
--				insertuser = excluded.insertuser,
--				updateuser = excluded.updateuser,
--				dataowner = excluded.dataowner
			--WHERE COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.watlevel 
		ADD PRIMARY KEY (guid)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.watlevel IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl watlevel'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	CREATE INDEX IF NOT EXISTS watlevel_boreholeid_idx
		ON jupiter.watlevel (boreholeid)
	;
	
	CREATE INDEX IF NOT EXISTS watlevel_intakeno_idx
		ON jupiter.watlevel (intakeno)
	;
	
	CREATE INDEX IF NOT EXISTS watlevel_waterlevel_idx
		ON jupiter.watlevel (waterlevel)
	;
	
	CREATE INDEX IF NOT EXISTS watlevel_watlevgrsu_idx
		ON jupiter.watlevel (watlevgrsu)
	;
	
	CREATE INDEX IF NOT EXISTS watlevel_watlevmsl_idx
		ON jupiter.watlevel (watlevmsl)
	;
	
	CREATE INDEX IF NOT EXISTS watlevel_watlevmp_idx
		ON jupiter.watlevel (watlevmp)
	;
	
	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'watlevel',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	-- give access to read user

	GRANT USAGE ON SCHEMA jupiter TO grukosreader;
	GRANT SELECT ON ALL TABLES IN SCHEMA jupiter TO grukosreader;

COMMIT;
