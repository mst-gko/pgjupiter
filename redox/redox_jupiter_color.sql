BEGIN;

DROP TABLE IF EXISTS mstjupiter.redox_jupiter_color;

CREATE TABLE mstjupiter.redox_jupiter_color AS 
	(
		WITH 
		colors AS 
        (
            SELECT 'rød' AS col
            UNION ALL
            SELECT 'blå'
            UNION ALL
            SELECT 'gul'
            UNION ALL
            SELECT 'grøn'
            UNION ALL
            SELECT 'oliven'
            UNION ALL
            SELECT 'brun'
            UNION ALL
            SELECT 'sort'
            UNION ALL
            SELECT 'okker'
            UNION ALL
            SELECT 'hvid'
            UNION ALL
            SELECT 'grå'
            UNION ALL
            SELECT 'rust'
            UNION ALL
            SELECT ' mørk'
            UNION ALL
            SELECT ' lys'
        ),
        compare_ls_col AS 
        (
            SELECT 
                ls.guid, 
                col
            FROM jupiter.lithsamp ls, colors
            WHERE ls.totaldescr ILIKE '%' || col || '%'
        ),
        ls_col_grp AS 
        (
            SELECT 
                guid, 
                string_agg(DISTINCT col, ', ') AS totaldescr_color
            FROM compare_ls_col
            GROUP BY guid
        )
        SELECT DISTINCT
            '<a href="' || 'https://data.geus.dk/JupiterWWW/borerapport.jsp?borid=' || b.boreholeid || '">' || replace(b.boreholeno, ' ', '') || '</a>' as url, 
            b.boreholeid, b.boreholeno,
            ls.top, ls.bottom, ls.rocksymbol, 
            cd.color_descr,	cd1.drilcol_descr,
            cd2.oldcolor_descr,	cd3.muncolor_descr,
            CASE
                WHEN cd.color_descr IS NULL 
                    AND cd2.oldcolor_descr IS NULL 
                    THEN lcg.totaldescr_color
                ELSE NULL
                END AS color_from_totaldescr,
        	b.geom
        FROM jupiter.lithsamp ls
        INNER JOIN jupiter.borehole b ON b.boreholeid = ls.boreholeid
        LEFT JOIN jupiter.boredoc bd ON bd.boreholeid = ls.boreholeid 
        LEFT JOIN 
            (
                SELECT c.code, c.longtext AS color_descr
                FROM jupiter.code c 
                WHERE c.codetype = 48
            ) AS cd ON cd.code = ls.color
        LEFT JOIN 
            (
                SELECT c.code, c.longtext AS drilcol_descr
                FROM jupiter.code c 
                WHERE c.codetype = 48
            ) AS cd1 ON cd1.code = ls.drillcolor
        LEFT JOIN 
            (
                SELECT c.code, c.longtext AS oldcolor_descr
                FROM jupiter.code c 
                WHERE c.codetype = 51
            ) AS cd2 ON cd2.code = ls.oldcolor
        LEFT JOIN 
            (
                SELECT c.code, c.longtext AS muncolor_descr
                FROM jupiter.code c 
                WHERE c.codetype = 79
            ) AS cd3 ON cd3.code = ls.munsellcolor
        LEFT JOIN ls_col_grp lcg ON ls.guid = lcg.guid
        WHERE b.geom IS NOT NULL
            AND COALESCE(
                ls.color, ls.munsellcolor, 
                ls.oldcolor, ls.drillcolor,
                totaldescr_color
                ) IS NOT NULL 
        ORDER BY b.boreholeno, ls.top
	)
;

CREATE INDEX redox_jupiter_color_geom_idx
	ON mstjupiter.redox_jupiter_color
	USING GIST (geom)
;

DO
$do$
    BEGIN
        EXECUTE 'COMMENT ON TABLE mstjupiter.redox_jupiter_color IS '''
     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
     || ' | user: simak'
     || ' | descr: farvebeskrivelser i jupiter til redox udtraek'
     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
     || '''';
    END
$do$
;

GRANT USAGE ON SCHEMA mstjupiter TO grukosreader;
GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO grukosreader;

COMMIT; 