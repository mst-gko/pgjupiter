BEGIN;

DROP TABLE IF EXISTS mstjupiter.redox_jupiter_drillreport;

CREATE TABLE mstjupiter.redox_jupiter_drillreport AS 
	(
		SELECT DISTINCT    
		    sd.url, b.boreholeno, bd.versionno,  
		    concat(b.boreholeno, '_', bd.doctype, '_', bd.versionno) as bor_ver,
		    b.geom
		FROM jupiter.storedoc sd
		LEFT JOIN jupiter.boredoc bd USING (fileid)
		LEFT JOIN jupiter.borehole b USING (boreholeid) 
		WHERE b.geom IS NOT NULL
		    AND bd.doctype IN ('B', 'BG', 'J')
		ORDER BY b.boreholeno, bd.versionno
	)
;

CREATE INDEX redox_jupiter_drillreport_geom_idx
	ON mstjupiter.redox_jupiter_drillreport
	USING GIST (geom)
;

DO
$do$
    BEGIN
        EXECUTE 'COMMENT ON TABLE mstjupiter.redox_jupiter_drillreport IS '''
     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
     || ' | user: simak'
     || ' | descr: borerapporter i jupiter til redox udtraek'
     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
     || '''';
    END
$do$
;

GRANT USAGE ON SCHEMA mstjupiter TO grukosreader;
GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO grukosreader;

COMMIT; 