BEGIN; 

	DROP TABLE IF EXISTS mstjupiter.borehole_most_recent_visit;

	CREATE TABLE mstjupiter.borehole_most_recent_visit AS 
		(
			WITH 
				bh AS 
					(
						SELECT 
							boreholeid, boreholeno, 
							b.drilendate AS drilendate_max, 
							b.use, b.geom
						FROM jupiter.borehole b
						WHERE drilendate <= current_date
					),
				gcs AS 
					(
						SELECT 
							boreholeid, 
							max(sampledate) AS sampledate_max
						FROM jupiter.grwchemsample g 
						WHERE sampledate <= current_date
						GROUP BY boreholeid
					),
				wl AS 
					(
						SELECT 
							wl.boreholeid, 
							max(timeofmeas) AS timeofmeas_max
						FROM jupiter.watlevel wl
						WHERE timeofmeas <= current_date
						GROUP BY boreholeid	
					)
			SELECT 
				boreholeid, boreholeno,
				EXTRACT ( YEAR FROM current_date )
					- EXTRACT ( YEAR FROM ( GREATEST (drilendate_max, sampledate_max, timeofmeas_max) ) ) 
					AS yrs_since_visit,
				use, geom,
				drilendate_max, sampledate_max, timeofmeas_max
			FROM bh
			LEFT JOIN gcs USING (boreholeid)
			LEFT JOIN wl USING (boreholeid)
		)
	;

	CREATE INDEX borehole_most_recent_visit_geom_idx
		ON mstjupiter.borehole_most_recent_visit
		USING GIST (geom)
	;

	CREATE INDEX borehole_most_recent_visit_idx
		ON mstjupiter.borehole_most_recent_visit
		USING BTREE (yrs_since_visit)
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE mstjupiter.borehole_most_recent_visit IS '''
			     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
			     || ' | user: simak'
			     || ' | descr: Table containing years since the borehole was last visited'
			     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
			     || '''';
		END
	$do$
	;

	GRANT USAGE ON SCHEMA mstjupiter TO jupiterrole;
	GRANT SELECT ON ALL TABLES IN SCHEMA mstjupiter TO jupiterrole;

COMMIT;
