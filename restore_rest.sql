/*
 * Owner: 	Danish Environmental Protection Agency (EPA) 
 * Descr: 	This sql script updates MST-GKOs pcjupiterxl postgresql database
 * 			via GEUS' SQL-gateway --where existing tables are updated using 
 * 			select statements executed on a foreign data wrapper. 
 * 			The script is overall optimized toward lowering the execution time
 * 			of the queries from the geus foreign wrapper.
 * 			This means for the large tables, the uuids are translated prior to updating 
 * 			such that the fdw queries does not have to translate bytea into uuid. This is done
 * 			to lower the queries time of execution on the fdw server due to 
 * 			timeout errors.
 * ERT: 	~10-15 min 
 * License:	GNU GPL v3
 * Author: 	Simon Makwarth <simak@mst.dk>
 * Created: 2022-02-22
 * Updated:	2023-01-10 jakla@mst.dk added intake_groundwaterbody
 * Updated:	2023-07-11 simak@mst.dk removed columns catchpurpose and vrrpurpose from table drwplant
 * Updated:	2023-xx-xx xxx@mst.dk added xxx
 */

/*
 * update from geus fdw (foreign data wrapper)
 */

BEGIN;

	-- create mstjupiter schema if it does not exists

	CREATE SCHEMA IF NOT EXISTS mstjupiter;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON SCHEMA mstjupiter IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: Data from pcjupiterxl'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	-- create jupiter schema if it does not exists

	CREATE SCHEMA IF NOT EXISTS jupiter;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON SCHEMA jupiter IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: MSTGKO derived data from pcjupiterxl'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

	CREATE TABLE IF NOT EXISTS mstjupiter.mst_exportdate AS 
		(
			SELECT 
				'test_string'::TEXT AS tablename,
				'1900-01-01'::timestamp AS updatetime
			LIMIT 0
		)
	;

	ALTER TABLE mstjupiter.mst_exportdate
		DROP CONSTRAINT IF EXISTS mst_exportdate_pkey CASCADE
	; 

	ALTER TABLE mstjupiter.mst_exportdate
		ADD PRIMARY KEY (tablename)
	;

COMMIT;

-- codetype 

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.codetype AS
		(
			SELECT 
				codetype,
				shorttext,
				longtext,
				lookuptype,
				lookuptabl,
				insertdate,
				updatedate
			FROM geus_fdw.codetype
			LIMIT 0
		)
	;

	TRUNCATE jupiter.codetype CASCADE;

	ALTER TABLE jupiter.codetype 
		DROP CONSTRAINT IF EXISTS codetype_pkey CASCADE
	;

	INSERT INTO jupiter.codetype AS t1
		(
			codetype,
			shorttext,
			longtext,
			lookuptype,
			lookuptabl,
			insertdate,
			updatedate
		)
		(
			SELECT 
				codetype,
				shorttext,
				longtext,
				lookuptype,
				lookuptabl,
				insertdate,
				updatedate
			FROM geus_fdw.codetype t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (codetype) --ON CONSTRAINT codetype_pkey 
--			DO UPDATE SET 
--				codetype = excluded.codetype,
--				shorttext = excluded.shorttext,
--				longtext = excluded.longtext,
--				lookuptype = excluded.lookuptype,
--				lookuptabl = excluded.lookuptabl,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.codetype 
		ADD PRIMARY KEY (codetype)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'codetype',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.codetype IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl codetype'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- code 

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.code AS
		(
			SELECT 
				code,
				codetype,
				shorttext,
				longtext,
				sortno,
				insertdate,
				updatedate
			FROM geus_fdw.code
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.code 
		DROP CONSTRAINT IF EXISTS code_pkey CASCADE
	;

	ALTER TABLE jupiter.code 
		DROP CONSTRAINT IF EXISTS fk_codetype_code CASCADE
	;

	DROP INDEX IF EXISTS code_code_idx;

	DROP INDEX IF EXISTS code_codetype_idx;

	TRUNCATE jupiter.code CASCADE;

	INSERT INTO jupiter.code AS t1
		(
			code,
			codetype,
			shorttext,
			longtext,
			sortno,
			insertdate,
			updatedate
		)
		(
			SELECT 
				code,
				codetype,
				shorttext,
				longtext,
				sortno,
				insertdate,
				updatedate
			FROM geus_fdw.code t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (code, codetype) --ON CONSTRAINT code_pkey 
--			DO UPDATE SET 
--				code = excluded.code,
--				codetype = excluded.codetype,
--				shorttext = excluded.shorttext,
--				longtext = excluded.longtext,
--				sortno = excluded.sortno,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.code 
		ADD PRIMARY KEY (code, codetype)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'code',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.code IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl code'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- municipality2007

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.municipality2007 AS
		(
			SELECT 
				municipalityno2007,
				envcen,
				region,
				"name",
				xutmmin,
				yutmmin,
				xutmmax,
				yutmmax,
				www,
				insertdate,
				updatedate
			FROM geus_fdw.municipality2007
			LIMIT 0
		)
	;
	
	ALTER TABLE jupiter.municipality2007 
		DROP CONSTRAINT IF EXISTS municipality2007_pkey CASCADE
	;

	TRUNCATE jupiter.municipality2007;

	INSERT INTO jupiter.municipality2007 AS t1
		(
			municipalityno2007,
			envcen,
			region,
			"name",
			xutmmin,
			yutmmin,
			xutmmax,
			yutmmax,
			www,
			insertdate,
			updatedate
		)
		(
			SELECT 
				municipalityno2007,
				envcen,
				region,
				"name",
				xutmmin,
				yutmmin,
				xutmmax,
				yutmmax,
				www,
				insertdate,
				updatedate
			FROM geus_fdw.municipality2007 t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (municipalityno2007) --ON CONSTRAINT municipality2007_pkey 
--			DO UPDATE SET 
--				municipalityno2007 = excluded.municipalityno2007,
--				envcen = excluded.envcen,  
--				region = excluded.region,
--				"name" = excluded."name",
--				xutmmin = excluded.xutmmin,
--				yutmmin = excluded.yutmmin,
--				xutmmax = excluded.xutmmax,
--				yutmmax = excluded.yutmmax,
--				www = excluded.www,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.municipality2007 
		ADD PRIMARY KEY (municipalityno2007)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'municipality2007',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.municipality2007 IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl municipality2007'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- borehole

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.borehole AS 
		(
			SELECT 
				boreholeid,
				boreholeno,
				namingsys,
				purpose,
				use,
				status,
				drilldepth,
				elevation,
				ctrpeleva,
				verticaref,
				ctrpdescr,
				ctrpprecis,
				ctrpzprecis,
				ctrpheight,
				elevametho,
				elevaquali,
				elevasourc,
				"location",
				"comments",
				various,
				xutm,
				yutm,
				utmzone,
				datum,
				mapsheet,
				mapdistx,
				mapdisty,
				sys34x,
				sys34y,
				sys34zone,
				latitude,
				longitude,
				locatmetho,
				locatquali,
				locatsourc,
				borhpostc,
				borhtownno,
				countyno,
				municipal,
				houseownas,
				landregno,
				driller,
				drilllogno,
				drillborno,
				reportedby,
				consultant,
				consulogno,
				consuborno,
				drilledfor,
				drforadres,
				drforpostc,
				drilstdate,
				drilendate,
				abandondat,
				prevborhno,
				numsuplbor,
				samrecedat,
				samdescdat,
				numofsampl,
				numsamsto,
				litholnote,
				togeusdate,
				grumocountyno,
				grumoborno,
				grumobortype,
				grumoareano,
				borhtownno2007,
				locquali,
				loopareano,
				loopstation,
				looptype,
				usechangedate,
				envcen,
				abandcause,
				abandcontr,
				startdayunknown,
				startmnthunknwn,
				wwboreholeno,
				xutm32euref89,
				yutm32euref89,
				zdvr90,
				installation,
				workingconditions,
				approach,
				accessremark,
				locatepersonemail,
				preservationzone,
				protectionzone,
				region,
				usechangecause,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				dataowner,
				ST_SetSRID(st_makepoint(xutm32euref89, yutm32euref89), 25832) AS geom
			FROM geus_fdw.borehole b
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.borehole 
		DROP CONSTRAINT IF EXISTS borehole_pkey CASCADE
	;

	ALTER TABLE jupiter.borehole 
		DROP CONSTRAINT IF EXISTS boreholeid_unique CASCADE
	;

	ALTER TABLE jupiter.borehole 
		DROP CONSTRAINT IF EXISTS boreholeno_unique CASCADE
	;

	DROP INDEX IF EXISTS borehole_geom_idx;

	DROP INDEX IF EXISTS borehole_boreholeid_idx;
	
	DROP INDEX IF EXISTS borehole_boreholeno_idx;

	TRUNCATE jupiter.borehole;

	INSERT INTO jupiter.borehole AS t1
		(
			boreholeid,
			boreholeno,
			namingsys,
			purpose,
			use,
			status,
			drilldepth,
			elevation,
			ctrpeleva,
			verticaref,
			ctrpdescr,
			ctrpprecis,
			ctrpzprecis,
			ctrpheight,
			elevametho,
			elevaquali,
			elevasourc,
			"location",
			"comments",
			various,
			xutm,
			yutm,
			utmzone,
			datum,
			mapsheet,
			mapdistx,
			mapdisty,
			sys34x,
			sys34y,
			sys34zone,
			latitude,
			longitude,
			locatmetho,
			locatquali,
			locatsourc,
			borhpostc,
			borhtownno,
			countyno,
			municipal,
			houseownas,
			landregno,
			driller,
			drilllogno,
			drillborno,
			reportedby,
			consultant,
			consulogno,
			consuborno,
			drilledfor,
			drforadres,
			drforpostc,
			drilstdate,
			drilendate,
			abandondat,
			prevborhno,
			numsuplbor,
			samrecedat,
			samdescdat,
			numofsampl,
			numsamsto,
			litholnote,
			togeusdate,
			grumocountyno,
			grumoborno,
			grumobortype,
			grumoareano,
			borhtownno2007,
			locquali,
			loopareano,
			loopstation,
			looptype,
			usechangedate,
			envcen,
			abandcause,
			abandcontr,
			startdayunknown,
			startmnthunknwn,
			wwboreholeno,
			xutm32euref89,
			yutm32euref89,
			zdvr90,
			installation,
			workingconditions,
			approach,
			accessremark,
			locatepersonemail,
			preservationzone,
			protectionzone,
			region,
			usechangecause,
			guid,
			insertdate,
			updatedate,
			insertuser,
			updateuser,
			dataowner,
			geom
		)
		(
			SELECT 
				boreholeid,
				boreholeno,
				namingsys,
				purpose,
				use,
				status,
				drilldepth,
				elevation,
				ctrpeleva,
				verticaref,
				ctrpdescr,
				ctrpprecis,
				ctrpzprecis,
				ctrpheight,
				elevametho,
				elevaquali,
				elevasourc,
				"location",
				"comments",
				various,
				xutm,
				yutm,
				utmzone,
				datum,
				mapsheet,
				mapdistx,
				mapdisty,
				sys34x,
				sys34y,
				sys34zone,
				latitude,
				longitude,
				locatmetho,
				locatquali,
				locatsourc,
				borhpostc,
				borhtownno,
				countyno,
				municipal,
				houseownas,
				landregno,
				driller,
				drilllogno,
				drillborno,
				reportedby,
				consultant,
				consulogno,
				consuborno,
				drilledfor,
				drforadres,
				drforpostc,
				drilstdate,
				drilendate,
				abandondat,
				prevborhno,
				numsuplbor,
				samrecedat,
				samdescdat,
				numofsampl,
				numsamsto,
				litholnote,
				togeusdate,
				grumocountyno,
				grumoborno,
				grumobortype,
				grumoareano,
				borhtownno2007,
				locquali,
				loopareano,
				loopstation,
				looptype,
				usechangedate,
				envcen,
				abandcause,
				abandcontr,
				startdayunknown,
				startmnthunknwn,
				wwboreholeno,
				xutm32euref89,
				yutm32euref89,
				zdvr90,
				installation,
				workingconditions,
				approach,
				accessremark,
				locatepersonemail,
				preservationzone,
				protectionzone,
				region,
				usechangecause,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				dataowner,
				ST_SetSRID(st_makepoint(xutm32euref89, yutm32euref89), 25832) AS geom
			FROM geus_fdw.borehole t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (guid) --ON CONSTRAINT borehole_pkey 
--			DO UPDATE SET 
--				status = excluded.status,
--				drilldepth = excluded.drilldepth,
--				elevation = excluded.elevation,
--				ctrpeleva = excluded.ctrpeleva,
--				verticaref = excluded.verticaref,
--				ctrpdescr = excluded.ctrpdescr,
--				ctrpprecis = excluded.ctrpprecis,
--				ctrpzprecis = excluded.ctrpzprecis,
--				ctrpheight = excluded.ctrpheight,
--				elevametho = excluded.elevametho,
--				elevaquali = excluded.elevaquali,
--				elevasourc = excluded.elevasourc,
--				"location" = excluded."location",
--				"comments" = excluded."comments",
--				various = excluded.various,
--				xutm = excluded.xutm,
--				yutm = excluded.yutm,
--				utmzone = excluded.utmzone,
--				datum = excluded.datum,
--				mapsheet = excluded.mapsheet,
--				mapdistx = excluded.mapdistx,
--				mapdisty = excluded.mapdisty,
--				sys34x = excluded.sys34x,
--				sys34y = excluded.sys34y,
--				sys34zone = excluded.sys34zone,
--				latitude = excluded.latitude,
--				longitude = excluded.longitude,
--				locatmetho = excluded.locatmetho,
--				locatquali = excluded.locatquali,
--				locatsourc = excluded.locatsourc,
--				borhpostc = excluded.borhpostc,
--				borhtownno = excluded.borhtownno,
--				countyno = excluded.countyno,
--				municipal = excluded.municipal,
--				houseownas = excluded.houseownas,
--				landregno = excluded.landregno,
--				driller = excluded.driller,
--				drilllogno = excluded.drilllogno,
--				drillborno = excluded.drillborno,
--				reportedby = excluded.reportedby,
--				consultant = excluded.consultant,
--				consulogno = excluded.consulogno,
--				consuborno = excluded.consuborno,
--				drilledfor = excluded.drilledfor,
--				drforadres = excluded.drforadres,
--				drforpostc = excluded.drforpostc,
--				drilstdate = excluded.drilstdate,
--				drilendate = excluded.drilendate,
--				abandondat = excluded.abandondat,
--				prevborhno = excluded.prevborhno,
--				numsuplbor = excluded.numsuplbor,
--				samrecedat = excluded.samrecedat,
--				samdescdat = excluded.samdescdat,
--				numofsampl = excluded.numofsampl,
--				numsamsto = excluded.numsamsto,
--				litholnote = excluded.litholnote,
--				togeusdate = excluded.togeusdate,
--				grumocountyno = excluded.grumocountyno,
--				grumoborno = excluded.grumoborno,
--				grumobortype = excluded.grumobortype,
--				grumoareano = excluded.grumoareano,
--				borhtownno2007 = excluded.borhtownno2007,
--				locquali = excluded.locquali,
--				loopareano = excluded.loopareano,
--				loopstation = excluded.loopstation,
--				looptype = excluded.looptype,
--				usechangedate = excluded.usechangedate,
--				envcen = excluded.envcen,
--				abandcause = excluded.abandcause,
--				abandcontr = excluded.abandcontr,
--				startdayunknown = excluded.startdayunknown,
--				startmnthunknwn = excluded.startmnthunknwn,
--				wwboreholeno = excluded.wwboreholeno,
--				xutm32euref89 = excluded.xutm32euref89,
--				yutm32euref89 = excluded.yutm32euref89,
--				zdvr90 = excluded.zdvr90,
--				installation = excluded.installation,
--				workingconditions = excluded.workingconditions,
--				approach = excluded.approach,
--				accessremark = excluded.accessremark,
--				locatepersonemail = excluded.locatepersonemail,
--				preservationzone = excluded.preservationzone,
--				protectionzone = excluded.protectionzone,
--				region = excluded.region,
--				usechangecause = excluded.usechangecause,	
--				guid = excluded.guid,			
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate,
--				insertuser = excluded.insertuser,
--				updateuser = excluded.updateuser,
--				dataowner  = excluded.dataowner,
--				geom = excluded.geom 
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;	

	ALTER TABLE jupiter.borehole 
		ADD PRIMARY KEY (guid)
	;

	ALTER TABLE jupiter.borehole
		ADD CONSTRAINT boreholeid_unique UNIQUE (boreholeid)
	;

	ALTER TABLE jupiter.borehole
		ADD CONSTRAINT boreholeno_unique UNIQUE (boreholeno)
	;

	CREATE INDEX IF NOT EXISTS borehole_geom_idx
		ON jupiter.borehole
		USING GIST (geom)
	;
	
	CREATE INDEX IF NOT EXISTS borehole_boreholeid_idx
		ON jupiter.borehole (boreholeid)
	;
	
	CREATE INDEX IF NOT EXISTS borehole_boreholeno_idx
		ON jupiter.borehole (boreholeno)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'borehole',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.borehole IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl borehole'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- geus_fdw.hist_borehole_coord

BEGIN;

	create table if not exists jupiter.hist_borehole_coord as
		(
			SELECT
				ROW_NUMBER() OVER () AS mst_id,
				boreholeid,
				boreholeno,
				datum,
				ctrpdescr,
				ctrpeleva,
				ctrpheight,
				ctrpprecis,
				locatsourc,
				locatquali,
				locatmetho,
				elevation,
				elevasourc,
				elevaquali,
				elevametho,
				verticaref,
				utmzone,
				xutm,
				yutm,
				username,
				--"options",
				cast(encode(guid_borehole, 'hex') as uuid) as guid 
			from geus_fdw.hist_borehole_coord
			limit 0
		)
	;

	TRUNCATE jupiter.hist_borehole_coord CASCADE;	

	ALTER TABLE jupiter.hist_borehole_coord 
		DROP CONSTRAINT IF EXISTS hist_borehole_coord_pkey CASCADE
	;

	ALTER TABLE jupiter.hist_borehole_coord 
		DROP CONSTRAINT IF EXISTS fk_hist_borehole_coord_borehole CASCADE
	;

	INSERT INTO jupiter.hist_borehole_coord
		(
			mst_id,
			boreholeid,
			boreholeno,
			datum,
			ctrpdescr,
			ctrpeleva,
			ctrpheight,
			ctrpprecis,
			locatsourc,
			locatquali,
			locatmetho,
			elevation,
			elevasourc,
			elevaquali,
			elevametho,
			verticaref,
			utmzone,
			xutm,
			yutm,
			username,
			--options,
			guid
		)
		(
			SELECT 
				ROW_NUMBER() OVER () AS mst_id,
				boreholeid,
				boreholeno,
				datum,
				ctrpdescr,
				ctrpeleva,
				ctrpheight,
				ctrpprecis,
				locatsourc,
				locatquali,
				locatmetho,
				elevation,
				elevasourc,
				elevaquali,
				elevametho,
				verticaref,
				utmzone,
				xutm,
				yutm,
				username,
				--options,
				cast(encode(guid_borehole, 'hex') as uuid) as guid
			from geus_fdw.hist_borehole_coord
		) ON CONFLICT DO NOTHING 
	;

	ALTER TABLE jupiter.hist_borehole_coord 
		ADD PRIMARY KEY (mst_id)
	;

	CREATE INDEX IF NOT EXISTS hist_borehole_coord_boreholeid_idx
		ON jupiter.hist_borehole_coord (boreholeid)
	;
	
	CREATE INDEX IF NOT EXISTS hist_borehole_coord_boreholeno_idx
		ON jupiter.hist_borehole_coord (boreholeno)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'hist_borehole_coord',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.hist_borehole_coord IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: jakla'
	     || ' | descr: pcjupiterxl hist_borehole_coord'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- drilmeth

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.drilmeth AS
		(
			SELECT 
				boreholeid,
				methodid,
				boreholeno,
				methodno,
				top,
				bottom,
				"method",
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.drilmeth dm
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.drilmeth 
		DROP CONSTRAINT IF EXISTS drilmeth_pkey CASCADE
	;

	ALTER TABLE jupiter.drilmeth 
		DROP CONSTRAINT IF EXISTS fk_borehole_drilmeth CASCADE
	;

	TRUNCATE jupiter.drilmeth;
	
	INSERT INTO jupiter.drilmeth AS t1
		(
			boreholeid,
			methodid,
			boreholeno,
			methodno,
			top,
			bottom,
			"method",
			guid,
			guid_borehole,
			insertdate,
			updatedate,
			insertuser,
			updateuser
		)
		(
			SELECT 
				boreholeid,
				methodid,
				boreholeno,
				methodno,
				top,
				bottom,
				"method",
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.drilmeth t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (guid) --ON CONSTRAINT drilmeth_pkey 
--			DO UPDATE SET  
--				boreholeid = excluded.boreholeid,
--				methodid = excluded.methodid,
--				boreholeno = excluded.boreholeno,
--				methodno = excluded.methodno,
--				top = excluded.top,
--				bottom = excluded.bottom,
--				"method" = excluded."method",
--				guid = excluded.guid,
--				guid_borehole = excluded.guid_borehole,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate,
--				insertuser = excluded.insertuser,
--				updateuser = excluded.updateuser
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.drilmeth 
		ADD PRIMARY KEY (guid)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'drilmeth',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.drilmeth IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl drilmeth'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- intake

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.intake AS
		(
			SELECT 
				boreholeid,
				intakeid,
				boreholeno,
				intakeno,
				stringno,
				waterage,
				depositno,
				deposittype,
				mainclass,
				monitoringtype,
				reservoirrock,
				reservoirtype,
				specialusable,
				watertabletype,
				soundability,
				soundabilityremark,
				soundtubeinsidediam,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.intake i
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.intake 
		DROP CONSTRAINT IF EXISTS intake_pkey CASCADE
	;

	ALTER TABLE jupiter.intake 
		DROP CONSTRAINT IF EXISTS fk_borehole_intake CASCADE
	;

	DROP INDEX IF EXISTS intake_intakeno_idx;
	
	DROP INDEX IF EXISTS intake_guid_borehole_idx;

	DROP INDEX IF EXISTS intake_intakeid_idx;

	TRUNCATE jupiter.intake;
	
	INSERT INTO jupiter.intake AS t1
		(
			boreholeid,
			intakeid,
			boreholeno,
			intakeno,
			stringno,
			waterage,
			depositno,
			deposittype,
			mainclass,
			monitoringtype,
			reservoirrock,
			reservoirtype,
			specialusable,
			watertabletype,
			soundability,
			soundabilityremark,
			soundtubeinsidediam,
			guid,
			guid_borehole,
			insertdate,
			updatedate,
			insertuser,
			updateuser
		)
		(
			SELECT 
				boreholeid,
				intakeid,
				boreholeno,
				intakeno,
				stringno,
				waterage,
				depositno,
				deposittype,
				mainclass,
				monitoringtype,
				reservoirrock,
				reservoirtype,
				specialusable,
				watertabletype,
				soundability,
				soundabilityremark,
				soundtubeinsidediam,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.intake t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (guid) --ON CONSTRAINT intake_pkey 
--			DO UPDATE SET  
--				boreholeid = excluded.boreholeid,
--				intakeid = excluded.intakeid,
--				boreholeno = excluded.boreholeno,
--				intakeno = excluded.intakeno,
--				stringno = excluded.stringno,
--				waterage = excluded.waterage,
--				depositno = excluded.depositno,
--				deposittype = excluded.deposittype,
--				mainclass = excluded.mainclass,
--				monitoringtype = excluded.monitoringtype,
--				reservoirrock = excluded.reservoirrock,
--				reservoirtype = excluded.reservoirtype,
--				specialusable = excluded.specialusable,
--				watertabletype = excluded.watertabletype,
--				soundability = excluded.soundability,
--				soundabilityremark = excluded.soundabilityremark,
--				soundtubeinsidediam = excluded.soundtubeinsidediam,
--				guid = excluded.guid,
--				guid_borehole = excluded.guid_borehole,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate,
--				insertuser = excluded.insertuser,
--				updateuser = excluded.updateuser
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.intake 
		ADD PRIMARY KEY (guid)
	;

	CREATE INDEX IF NOT EXISTS intake_intakeno_idx
		ON jupiter.intake(intakeno)
	;
	
	CREATE INDEX IF NOT EXISTS intake_guid_borehole_idx
		ON jupiter.intake(guid_borehole)
	;
	
	CREATE INDEX IF NOT EXISTS intake_intakeid_idx
		ON jupiter.intake(intakeid)
	;
	
	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'intake',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.intake IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl intake'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT; 

-- screen

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.screen AS
		(
			SELECT 
				boreholeid,
				intakeid,
				screenid,
				boreholeno,
				screenno,
				intakeno,
				top,
				bottom,
				"diameter",
				unit,
				diametermm,
				material,
				strength,
				slotopenin,
				startdate,
				enddate,
				wallthickn,
				fitting,
				topbotquali,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_intake, 'hex') AS UUID) AS guid_intake,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.screen s 
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.screen 
		DROP CONSTRAINT IF EXISTS screen_pkey CASCADE
	;

	ALTER TABLE jupiter.screen 
		DROP CONSTRAINT IF EXISTS fk_intake_screen CASCADE
	;

	ALTER TABLE jupiter.screen 
		DROP CONSTRAINT IF EXISTS fk_borehole_screen CASCADE
	;

	DROP INDEX IF EXISTS screen_screenno_idx;
	
	DROP INDEX IF EXISTS screen_screenid_idx;

	TRUNCATE jupiter.screen;

	INSERT INTO jupiter.screen AS t1
		(
			boreholeid,
			intakeid,
			screenid,
			boreholeno,
			screenno,
			intakeno,
			top,
			bottom,
			"diameter",
			unit,
			diametermm,
			material,
			strength,
			slotopenin,
			startdate,
			enddate,
			wallthickn,
			fitting,
			topbotquali,
			guid,
			guid_intake,
			insertdate,
			updatedate,
			insertuser,
			updateuser
		)
		(
			SELECT 
				boreholeid,
				intakeid,
				screenid,
				boreholeno,
				screenno,
				intakeno,
				top,
				bottom,
				"diameter",
				unit,
				diametermm,
				material,
				strength,
				slotopenin,
				startdate,
				enddate,
				wallthickn,
				fitting,
				topbotquali,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_intake, 'hex') AS UUID) AS guid_intake,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.screen t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (guid) --ON CONSTRAINT screen_pkey 
--			DO UPDATE SET 
--				boreholeid = excluded.boreholeid,
--				intakeid = excluded.intakeid,
--				screenid = excluded.screenid,
--				boreholeno = excluded.boreholeno,
--				screenno = excluded.screenno,
--				intakeno = excluded.intakeno,
--				top = excluded.top,
--				bottom = excluded.bottom,
--				"diameter" = excluded."diameter",
--				unit = excluded.unit,
--				diametermm = excluded.diametermm,
--				material = excluded.material,
--				strength = excluded.strength,
--				slotopenin = excluded.slotopenin,
--				startdate = excluded.startdate,
--				enddate = excluded.enddate,
--				wallthickn = excluded.wallthickn,
--				fitting = excluded.fitting,
--				topbotquali = excluded.topbotquali,
--				guid = excluded.guid,
--				guid_intake = excluded.guid_intake,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate,
--				insertuser = excluded.insertuser,
--				updateuser = excluded.updateuser
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.screen 
		ADD PRIMARY KEY (guid)
	;

	CREATE INDEX IF NOT EXISTS screen_screenno_idx
		ON jupiter.screen(screenno)
	;
	
	CREATE INDEX IF NOT EXISTS screen_screenid_idx
		ON jupiter.screen(screenid)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'screen',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.screen IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl screen'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- drwplant

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.drwplant AS
		(
			SELECT 
				plantid,
				municipalityno2007,
				envcen,
				region,
				administratorid,
				active,
				planttype,
				permit,
				NULL AS municipalityno,
				NULL AS planttypeno,
				NULL AS serialno,
				NULL AS subno,
				NULL AS countyjournalno,
				NULL AS municipalityno2,
				utmzone,
				datum,
				xutm,
				yutm,
				plantname,
				plantaddress,
				plantpostalcode,
				NULL AS permitdate,
				NULL AS permitamount,
				NULL AS permitexpiredate,
				watertype,
				"owner",
				NULL AS reportingcounty,
				NULL AS companyserialno,
				dischargeto,
				supplant,
				locatremark,
				startdate,
				enddate,
				verticaref,
				gridtype,
				locatmetho,
				elevametho,
				areaha,
				xutm32euref89,
				yutm32euref89,
				propertyno,
				NULL AS feeduty,
				NULL AS feedutyamount,
				NULL AS feeaddressid,
				dataowner,
				ctrlmunicipalno,
				"method",
				conversionfactor,
				www,
				participantvatno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				--CAST(ENCODE(guid_drwfirm, 'hex') AS UUID) AS guid_drwfirm,
				NULL AS guid_drwfirm,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				ST_SetSRID(st_makepoint(xutm32euref89, yutm32euref89),25832) AS geom
			FROM geus_fdw.drwplant dp
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.drwplant 
		DROP CONSTRAINT IF EXISTS drwplant_pkey CASCADE
	;

	ALTER TABLE jupiter.drwplant 
		DROP CONSTRAINT IF EXISTS fk_municipality CASCADE
	;

	DROP INDEX IF EXISTS drwplant_geom_idx;

	DROP INDEX IF EXISTS drwplant_plantid_idx;

	DROP INDEX IF EXISTS drwplant_plantname_idx;

	TRUNCATE jupiter.drwplant;

	INSERT INTO jupiter.drwplant AS t1
		(
			plantid,
			municipalityno2007,
			envcen,
			region,
			administratorid,
			active,
			planttype,
			permit,
			municipalityno,
			planttypeno,
			serialno,
			subno,
			countyjournalno,
			municipalityno2,
			utmzone,
			datum,
			xutm,
			yutm,
			plantname,
			plantaddress,
			plantpostalcode,
			permitdate,
			permitamount,
			permitexpiredate,
			watertype,
			"owner",
			reportingcounty,
			companyserialno,
			dischargeto,
			supplant,
			locatremark,
			startdate,
			enddate,
			verticaref,
			gridtype,
			locatmetho,
			elevametho,
			areaha,
			xutm32euref89,
			yutm32euref89,
			propertyno,
			feeduty,
			feedutyamount,
			feeaddressid,
			dataowner,
			ctrlmunicipalno,
			"method",
			conversionfactor,
			www,
			participantvatno,
			guid,
			guid_drwfirm,
			insertdate,
			updatedate,
			insertuser,
			updateuser,
			geom
		)
		(
			SELECT 
				plantid,
				municipalityno2007,
				envcen,
				region,
				administratorid,
				active,
				planttype,
				permit,
				NULL AS municipalityno,
				NULL AS planttypeno,
				NULL AS serialno,
				NULL AS subno,
				NULL AS countyjournalno,
				NULL AS municipalityno2,
				utmzone,
				datum,
				xutm,
				yutm,
				plantname,
				plantaddress,
				plantpostalcode,
				NULL AS permitdate,
				NULL AS permitamount,
				NULL AS permitexpiredate,
				watertype,
				"owner",
				NULL AS reportingcounty,
				NULL AS companyserialno,
				dischargeto,
				supplant,
				locatremark,
				startdate,
				enddate,
				verticaref,
				gridtype,
				locatmetho,
				elevametho,
				areaha,
				xutm32euref89,
				yutm32euref89,
				propertyno,
				NULL AS feeduty,
				NULL AS feedutyamount,
				NULL AS feeaddressid,
				dataowner,
				ctrlmunicipalno,
				"method",
				conversionfactor,
				www,
				participantvatno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				--CAST(ENCODE(guid_drwfirm, 'hex') AS UUID) AS guid_drwfirm,
				NULL AS guid_drwfirm,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				ST_SetSRID(st_makepoint(xutm32euref89, yutm32euref89),25832) AS geom
			FROM geus_fdw.drwplant t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (guid) --ON CONSTRAINT drwplant_pkey 
--			DO UPDATE SET 
--				plantid = excluded.plantid,
--				municipalityno2007 = excluded.municipalityno2007,
--				envcen = excluded.envcen,
--				region = excluded.region,
--				administratorid = excluded.administratorid,
--				active = excluded.active,					
--				planttype = excluded.planttype,
--				permit = excluded.permit,
--				municipalityno = excluded.municipalityno,
--				planttypeno = excluded.planttypeno,
--				serialno = excluded.serialno,
--				subno = excluded.subno,
--				countyjournalno = excluded.countyjournalno,
--				municipalityno2 = excluded.municipalityno2,
--				utmzone = excluded.utmzone,
--				datum = excluded.datum,
--				xutm = excluded.xutm,
--				yutm = excluded.yutm,
--				plantname = excluded.plantname,
--				plantaddress = excluded.plantaddress,
--				plantpostalcode = excluded.plantpostalcode,
--				permitdate = excluded.permitdate,
--				permitamount = excluded.permitamount,
--				permitexpiredate = excluded.permitexpiredate,
--				watertype = excluded.watertype,
--				"owner" = excluded."owner",
--				vrrpurpose = excluded.vrrpurpose,
--				reportingcounty = excluded.reportingcounty,
--				companyserialno = excluded.companyserialno,
--				dischargeto = excluded.dischargeto,
--				supplant = excluded.supplant,
--				locatremark = excluded.locatremark,
--				startdate = excluded.startdate,
--				enddate = excluded.enddate,
--				verticaref = excluded.verticaref,
--				gridtype = excluded.gridtype,
--				locatmetho = excluded.locatmetho,
--				elevametho = excluded.elevametho,
--				areaha = excluded.areaha,
--				xutm32euref89 = excluded.xutm32euref89,
--				yutm32euref89 = excluded.yutm32euref89,
--				propertyno = excluded.propertyno,
--				feeduty = excluded.feeduty,
--				feedutyamount = excluded.feedutyamount,
--				feeaddressid = excluded.feeaddressid,
--				dataowner = excluded.dataowner,
--				ctrlmunicipalno = excluded.ctrlmunicipalno,
--				"method" = excluded."method",
--				conversionfactor = excluded.conversionfactor,
--				www = excluded.www,
--				participantvatno = excluded.participantvatno,
--				guid = excluded.guid,
--				guid_drwfirm = excluded.guid_drwfirm,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate,
--				insertuser = excluded.insertuser,
--				updateuser = excluded.updateuser,
--				geom = excluded.geom
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.drwplant 
		ADD PRIMARY KEY (guid)
	;

	CREATE INDEX IF NOT EXISTS drwplant_geom_idx
		ON jupiter.drwplant
		USING GIST (geom)
	;
	
	CREATE INDEX IF NOT EXISTS drwplant_plantid_idx
		ON jupiter.drwplant(plantid)
	;
	
	CREATE INDEX IF NOT EXISTS drwplant_plantname_idx
		ON jupiter.drwplant(plantname)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'drwplant',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.drwplant IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl drwplant'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- drwplantintake

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.drwplantintake AS
		(
			SELECT 
				intakeplantid,
				boreholeid,
				intakeid,
				plantid,
				boreholeno,
				intakeno,
				startdate,
				enddate,
				intakeusage,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				CAST(ENCODE(guid_intake, 'hex') AS UUID) AS guid_intake,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.drwplantintake dpi
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.drwplantintake 
		DROP CONSTRAINT IF EXISTS drwplantintake_pkey CASCADE
	;

	ALTER TABLE jupiter.drwplantintake 
		DROP CONSTRAINT IF EXISTS fk_intake_dpi CASCADE
	;

	ALTER TABLE jupiter.drwplantintake 
		DROP CONSTRAINT IF EXISTS fk_plant_dpi CASCADE
	;

	ALTER TABLE jupiter.drwplantintake 
		DROP CONSTRAINT IF EXISTS fk_borehole_dpi CASCADE
	;

	DROP INDEX IF EXISTS drwplantintake_boreholeid_idx;
	
	DROP INDEX IF EXISTS drwplantintake_boreholeno_idx;

	TRUNCATE jupiter.drwplantintake;

	INSERT INTO jupiter.drwplantintake AS t1
		(
			intakeplantid,
			boreholeid,
			intakeid,
			plantid,
			boreholeno,
			intakeno,
			startdate,
			enddate,
			intakeusage,
			guid,
			guid_drwplant,
			guid_intake,
			insertdate,
			updatedate,
			insertuser,
			updateuser
		)
		(
			SELECT 
				intakeplantid,
				boreholeid,
				intakeid,
				plantid,
				boreholeno,
				intakeno,
				startdate,
				enddate,
				intakeusage,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				CAST(ENCODE(guid_intake, 'hex') AS UUID) AS guid_intake,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.drwplantintake t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (guid) --ON CONSTRAINT drwplantintake_pkey 
--			DO UPDATE SET 
--				intakeplantid = excluded.intakeplantid,
--				boreholeid = excluded.boreholeid,
--				intakeid = excluded.intakeid,
--				plantid = excluded.plantid,
--				boreholeno = excluded.boreholeno,
--				intakeno = excluded.intakeno,
--				startdate = excluded.startdate,
--				enddate = excluded.enddate,
--				intakeusage = excluded.intakeusage,
--				guid = excluded.guid,
--				guid_drwplant = excluded.guid_drwplant,
--				guid_intake = excluded.guid_intake,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate,
--				insertuser = excluded.insertuser,
--				updateuser = excluded.updateuser
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.drwplantintake 
		ADD PRIMARY KEY (guid)
	;

	CREATE INDEX IF NOT EXISTS drwplantintake_boreholeid_idx
		ON jupiter.drwplantintake(boreholeid)
	;
		
	CREATE INDEX IF NOT EXISTS drwplantintake_boreholeno_idx
		ON jupiter.drwplantintake(boreholeno)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'drwplantintake',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.drwplantintake IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl drwplantintake'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- compoundgrouplist

BEGIN; 

	CREATE TABLE IF NOT EXISTS jupiter.compoundgrouplist AS
		(
			SELECT 
				compoundgroupno,
				longtext,
				shorttext,
				insertdate,
				updatedate
			FROM geus_fdw.compoundgrouplist
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.compoundgrouplist 
		DROP CONSTRAINT IF EXISTS compoundgrouplist_pkey CASCADE
	;

	TRUNCATE jupiter.compoundgrouplist;

	INSERT INTO jupiter.compoundgrouplist AS t1
		(
			compoundgroupno,
			longtext,
			shorttext,
			insertdate,
			updatedate
		)
		(
			SELECT 
				compoundgroupno,
				longtext,
				shorttext,
				insertdate,
				updatedate
			FROM geus_fdw.compoundgrouplist t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (compoundgroupno) --ON CONSTRAINT compoundgrouplist_pkey 
--			DO UPDATE SET
--				compoundgroupno = excluded.compoundgroupno,
--				longtext = excluded.compoundgroupno,
--				shorttext = excluded.shorttext,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.compoundgrouplist 
		ADD PRIMARY KEY (compoundgroupno)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'compoundgrouplist',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.compoundgrouplist IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl compoundgrouplist'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- compoundlist

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.compoundlist AS
		(
			SELECT 
				compoundno,
				long_text,
				short_text,
				sortno,
				casno,
				euno,
				limitationdate,
				molarweight,
				charge,
				remark,
				drwunit,
				grwunit,
				insertdate,
				updatedate
			FROM geus_fdw.compoundlist
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.compoundlist 
		DROP CONSTRAINT IF EXISTS compoundlist_pkey CASCADE
	;

	TRUNCATE jupiter.compoundlist;

	INSERT INTO jupiter.compoundlist AS t1
		(
			compoundno,
			long_text,
			short_text,
			sortno,
			casno,
			euno,
			limitationdate,
			molarweight,
			charge,
			remark,
			drwunit,
			grwunit,
			insertdate,
			updatedate
		)
		(
			SELECT 
				compoundno,
				long_text,
				short_text,
				sortno,
				casno,
				euno,
				limitationdate,
				molarweight,
				charge,
				remark,
				drwunit,
				grwunit,
				insertdate,
				updatedate
			FROM geus_fdw.compoundlist t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (compoundno) --ON CONSTRAINT compoundlist_pkey 
--			DO UPDATE SET
--				compoundno = excluded.compoundno,
--				long_text = excluded.long_text,
--				short_text = excluded.short_text,
--				sortno = excluded.sortno,
--				casno = excluded.casno,
--				euno = excluded.euno,
--				limitationdate = excluded.limitationdate,
--				molarweight = excluded.molarweight,
--				charge = excluded.charge,
--				remark = excluded.remark,
--				drwunit = excluded.drwunit,
--				grwunit = excluded.grwunit,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.compoundlist 
		ADD PRIMARY KEY (compoundno)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'compoundlist',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.compoundlist IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl compoundlist'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- compoundgroup

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.compoundgroup AS
		(
			SELECT 
				compoundno,
				compoundgroupno,
				subcompoundgroupno,
				insertdate,
				updatedate,
				convtocompoundno,
				convfactor,
				stancode,
				stancodetype
			FROM geus_fdw.compoundgroup
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.compoundgroup 
		DROP CONSTRAINT IF EXISTS compoundgroup_pkey CASCADE
	;

	ALTER TABLE jupiter.compoundgroup 
		DROP CONSTRAINT IF EXISTS fk_cpl_cpg CASCADE
	;

	ALTER TABLE jupiter.compoundgroup
		DROP CONSTRAINT IF EXISTS fk_cpgl_cpg CASCADE
	;

	TRUNCATE jupiter.compoundgroup;

	INSERT INTO jupiter.compoundgroup AS t1
		(
			compoundno,
			compoundgroupno,
			subcompoundgroupno,
			insertdate,
			updatedate,
			convtocompoundno,
			convfactor,
			stancode,
			stancodetype
		)
		(
			SELECT 
				compoundno,
				compoundgroupno,
				subcompoundgroupno,
				insertdate,
				updatedate,
				convtocompoundno,
				convfactor,
				stancode,
				stancodetype
			FROM geus_fdw.compoundgroup t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (compoundno) --ON CONSTRAINT compoundgroup_pkey 
--			DO UPDATE SET
--				compoundno = excluded.compoundno,
--				compoundgroupno = excluded.compoundgroupno,
--				subcompoundgroupno = excluded.subcompoundgroupno,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate,
--				convtocompoundno = excluded.convtocompoundno,
--				convfactor = excluded.convfactor,
--				stancode = excluded.stancode,
--				stancodetype = excluded.stancodetype
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.compoundgroup 
		ADD PRIMARY KEY (compoundno)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'compoundgroup',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.compoundgroup IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl compoundgroup'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- grwchemsample

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.grwchemsample AS 
		(
			SELECT 
				sampleid,									
				boreholeid,
				intakeid,
				outtakeid,
				boreholeno,
				intakeno,
				outtakeno,
				top,
				bottom,
				sampledate,
				project,
				laboratory,
				laboratoriyrefno,
				purpose,
				extent,
				cause,
				sourcetype,
				watertype,
				remark,
				samplelocality,
				sumcationscalculated,
				sumanoinscalculated,
				preprocessing,
				sampledby,
				client,
				clientname,
				samplereportdate,
				validatedby,
				cleaningstart,
				cleaningend,
				cleaningyield,
				partofsampleid,
				amount,
				resamplestatus,
				unit,
				previoussampleid,
				qualitycontrol,
				labreferencename,
				projectphase,
				laboratoryreceiveddate,
				samplingfirm,
				samplingequipment,
				samplestatus,
				samplequalitymark,
				samplestatususer,
				samplestatusdate,
				dataowner,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_intake, 'hex') AS UUID) AS guid_intake,
				CAST(ENCODE(guid_outtake, 'hex') AS UUID) AS guid_outtake,
				CAST(ENCODE(guid_outtake, 'hex') AS UUID) AS guid_outtake,
				CAST(ENCODE(guid_projectphase, 'hex') AS UUID) AS guid_projectphase,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				samplenewdate,
				laboratory_desc
			FROM geus_fdw.grwchemsample t2
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.grwchemsample 
		DROP CONSTRAINT IF EXISTS grwchemsample_pkey CASCADE
	;

	ALTER TABLE jupiter.grwchemsample 
		DROP CONSTRAINT IF EXISTS fk_intake_gcs CASCADE
	;

	ALTER TABLE jupiter.grwchemsample 
		DROP CONSTRAINT IF EXISTS fk_borehole_gcs CASCADE
	;

	DROP INDEX IF EXISTS grwchemsample_sampleid_idx;

	DROP INDEX IF EXISTS grwchemsample_boreholeid_idx;

	TRUNCATE jupiter.grwchemsample;

	INSERT INTO jupiter.grwchemsample AS t1
		(
			sampleid,									
			boreholeid,
			intakeid,
			outtakeid,
			boreholeno,
			intakeno,
			outtakeno,
			top,
			bottom,
			sampledate,
			project,
			laboratory,
			laboratoriyrefno,
			purpose,
			extent,
			cause,
			sourcetype,
			watertype,
			remark,
			samplelocality,
			sumcationscalculated,
			sumanoinscalculated,
			preprocessing,
			sampledby,
			client,
			clientname,
			samplereportdate,
			validatedby,
			cleaningstart,
			cleaningend,
			cleaningyield,
			partofsampleid,
			amount,
			resamplestatus,
			unit,
			previoussampleid,
			qualitycontrol,
			labreferencename,
			projectphase,
			laboratoryreceiveddate,
			samplingfirm,
			samplingequipment,
			samplestatus,
			samplequalitymark,
			samplestatususer,
			samplestatusdate,
			dataowner,
			guid,
			guid_borehole,
			guid_intake,
			guid_outtake,
			guid_projectphase,
			insertdate,
			updatedate,
			insertuser,
			updateuser,
			samplenewdate,
			laboratory_desc
		)
		(
			SELECT 
				sampleid,									
				boreholeid,
				intakeid,
				outtakeid,
				boreholeno,
				intakeno,
				outtakeno,
				top,
				bottom,
				sampledate,
				project,
				laboratory,
				laboratoriyrefno,
				purpose,
				extent,
				cause,
				sourcetype,
				watertype,
				remark,
				samplelocality,
				sumcationscalculated,
				sumanoinscalculated,
				preprocessing,
				sampledby,
				client,
				clientname,
				samplereportdate,
				validatedby,
				cleaningstart,
				cleaningend,
				cleaningyield,
				partofsampleid,
				amount,
				resamplestatus,
				unit,
				previoussampleid,
				qualitycontrol,
				labreferencename,
				projectphase,
				laboratoryreceiveddate,
				samplingfirm,
				samplingequipment,
				samplestatus,
				samplequalitymark,
				samplestatususer,
				samplestatusdate,
				dataowner,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				CAST(ENCODE(guid_intake, 'hex') AS UUID) AS guid_intake,
				CAST(ENCODE(guid_outtake, 'hex') AS UUID) AS guid_outtake,
				CAST(ENCODE(guid_projectphase, 'hex') AS UUID) AS guid_projectphase,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				samplenewdate,
				laboratory_desc
			FROM geus_fdw.grwchemsample t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (guid) --ON CONSTRAINT grwchemsample_pkey 
--			DO UPDATE SET
--				sampleid = excluded.sampleid,
--				boreholeid = excluded.boreholeid,
--				intakeid = excluded.intakeid,
--				outtakeid = excluded.outtakeid,
--				boreholeno = excluded.boreholeno,
--				intakeno = excluded.intakeno,
--				outtakeno = excluded.outtakeno,
--				top = excluded.top,
--				bottom = excluded.bottom,
--				sampledate = excluded.sampledate,
--				project = excluded.project,
--				laboratory = excluded.laboratory,
--				laboratoriyrefno = excluded.laboratoriyrefno,
--				purpose = excluded.purpose,
--				extent = excluded.extent,
--				cause = excluded.cause,
--				sourcetype = excluded.sourcetype,
--				watertype = excluded.watertype,
--				remark = excluded.remark,
--				samplelocality = excluded.samplelocality,
--				sumcationscalculated = excluded.sumcationscalculated,
--				sumanoinscalculated = excluded.sumanoinscalculated,
--				preprocessing = excluded.preprocessing,
--				sampledby = excluded.sampledby,
--				client = excluded.client,
--				clientname = excluded.clientname,
--				samplereportdate = excluded.samplereportdate,
--				validatedby = excluded.validatedby,
--				cleaningstart = excluded.cleaningstart,
--				cleaningend = excluded.cleaningend,
--				cleaningyield = excluded.cleaningyield,
--				partofsampleid = excluded.partofsampleid,
--				amount = excluded.amount,
--				resamplestatus = excluded.resamplestatus,
--				unit = excluded.unit,
--				previoussampleid = excluded.previoussampleid,
--				qualitycontrol = excluded.qualitycontrol,
--				labreferencename = excluded.labreferencename,
--				projectphase = excluded.projectphase,
--				laboratoryreceiveddate = excluded.laboratoryreceiveddate,
--				samplingfirm = excluded.samplingfirm,
--				samplingequipment = excluded.samplingequipment,
--				samplestatus = excluded.samplestatus,
--				samplequalitymark = excluded.samplequalitymark,
--				samplestatususer = excluded.samplestatususer,
--				samplestatusdate = excluded.samplestatusdate,
--				dataowner = excluded.dataowner,
--				guid = excluded.guid,
--				guid_borehole = excluded.guid_borehole,
--				guid_intake = excluded.guid_intake,
--				guid_outtake = excluded.guid_outtake,
--				guid_projectphase = excluded.guid_projectphase,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate,
--				insertuser = excluded.insertuser,
--				updateuser = excluded.updateuser,
--				samplenewdate = excluded.samplenewdate,
--				laboratory_desc = excluded.laboratory_desc
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.grwchemsample 
		ADD PRIMARY KEY (guid)
	;

	CREATE INDEX IF NOT EXISTS grwchemsample_sampleid_idx
		ON jupiter.grwchemsample(sampleid)
	;
	
	CREATE INDEX IF NOT EXISTS grwchemsample_boreholeid_idx
		ON jupiter.grwchemsample(boreholeid)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'grwchemsample',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.grwchemsample IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl grwchemsample'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- pltchemsample

BEGIN; 

	CREATE TABLE IF NOT EXISTS jupiter.pltchemsample AS
		(
			SELECT 
				sampleid,					
				plantid,
				sampledate,
				samplesite,
				reportdate,
				journalno,
				laboratory,
				datasource,
				remark,
				extent,
				extentold,
				evaluationlaboratory,
				evaluationsite,
				evaluationjournalno,
				purpose,
				odeur,
				taste,
				look,
				colour,
				sampledby,
				measuringsiteno,
				client,
				clientname,
				pipesite,
				pipeaddress,
				pipepostalcode,
				qualitycontrol,
				estimatetemp,
				labreferencename,
				project,
				volume,
				volumeunit,
				measuringstationid,
				resamplestatus,
				previoussample,
				projectphase,
				laboratoryreceiveddate,
				samplingfirm,
				samplestatus,
				samplequalitymark,
				samplestatususer,
				samplestatusdate,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				CAST(ENCODE(guid_measuringstation, 'hex') AS UUID) AS guid_measuringstation,
				CAST(ENCODE(guid_projectphase, 'hex') AS UUID) AS guid_projectphase,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				samplenewdate,
				laboratory_desc
			FROM geus_fdw.pltchemsample pcs
			LIMIT 0
		)
	;
	
	ALTER TABLE jupiter.pltchemsample 
		DROP CONSTRAINT IF EXISTS pltchemsample_pkey CASCADE
	;

	ALTER TABLE jupiter.pltchemsample 
		DROP CONSTRAINT IF EXISTS fk_drwplant_pcs CASCADE
	;

	DROP INDEX IF EXISTS pltchemsample_sampleid_idx;

	DROP INDEX IF EXISTS pltchemsample_boreholeid_idx;

	TRUNCATE jupiter.pltchemsample;

	INSERT INTO jupiter.pltchemsample AS t1
		(
			sampleid,					
			plantid,
			sampledate,
			samplesite,
			reportdate,
			journalno,
			laboratory,
			datasource,
			remark,
			extent,
			extentold,
			evaluationlaboratory,
			evaluationsite,
			evaluationjournalno,
			purpose,
			odeur,
			taste,
			look,
			colour,
			sampledby,
			measuringsiteno,
			client,
			clientname,
			pipesite,
			pipeaddress,
			pipepostalcode,
			qualitycontrol,
			estimatetemp,
			labreferencename,
			project,
			volume,
			volumeunit,
			measuringstationid,
			resamplestatus,
			previoussample,
			projectphase,
			laboratoryreceiveddate,
			samplingfirm,
			samplestatus,
			samplequalitymark,
			samplestatususer,
			samplestatusdate,
			guid,
			guid_drwplant,
			guid_measuringstation,
			guid_projectphase,
			insertdate,
			updatedate,
			insertuser,
			updateuser,
			samplenewdate,
			laboratory_desc
		)
		(
			SELECT 
				sampleid,					
				plantid,
				sampledate,
				samplesite,
				reportdate,
				journalno,
				laboratory,
				datasource,
				remark,
				extent,
				extentold,
				evaluationlaboratory,
				evaluationsite,
				evaluationjournalno,
				purpose,
				odeur,
				taste,
				look,
				colour,
				sampledby,
				measuringsiteno,
				client,
				clientname,
				pipesite,
				pipeaddress,
				pipepostalcode,
				qualitycontrol,
				estimatetemp,
				labreferencename,
				project,
				volume,
				volumeunit,
				measuringstationid,
				resamplestatus,
				previoussample,
				projectphase,
				laboratoryreceiveddate,
				samplingfirm,
				samplestatus,
				samplequalitymark,
				samplestatususer,
				samplestatusdate,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				CAST(ENCODE(guid_measuringstation, 'hex') AS UUID) AS guid_measuringstation,
				CAST(ENCODE(guid_projectphase, 'hex') AS UUID) AS guid_projectphase,
				insertdate,
				updatedate,
				insertuser,
				updateuser,
				samplenewdate,
				laboratory_desc
			FROM geus_fdw.pltchemsample t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (guid) --ON CONSTRAINT pltchemsample_pkey 
--			DO UPDATE SET
--				sampleid = excluded.sampleid,
--				plantid = excluded.plantid,
--				sampledate = excluded.sampledate,
--				samplesite = excluded.samplesite,
--				reportdate = excluded.reportdate,
--				journalno = excluded.journalno,
--				laboratory = excluded.laboratory,
--				datasource = excluded.datasource,
--				remark = excluded.remark,
--				extent = excluded.extent,
--				extentold = excluded.extentold,
--				evaluationlaboratory = excluded.evaluationlaboratory,
--				evaluationsite = excluded.evaluationsite,
--				evaluationjournalno = excluded.evaluationjournalno,
--				purpose = excluded.purpose,
--				odeur = excluded.odeur,
--				taste = excluded.taste,
--				look = excluded.look,
--				colour = excluded.colour,
--				sampledby = excluded.sampledby,
--				measuringsiteno = excluded.measuringsiteno,
--				client = excluded.client,
--				clientname = excluded.clientname,
--				pipesite = excluded.pipesite,
--				pipeaddress = excluded.pipeaddress,
--				pipepostalcode = excluded.pipepostalcode,
--				qualitycontrol = excluded.qualitycontrol,
--				estimatetemp = excluded.estimatetemp,
--				labreferencename = excluded.labreferencename,
--				project = excluded.project,
--				volume = excluded.volume,
--				volumeunit = excluded.volumeunit,
--				measuringstationid = excluded.measuringstationid,
--				resamplestatus = excluded.resamplestatus,
--				previoussample = excluded.previoussample,
--				projectphase = excluded.projectphase,
--				laboratoryreceiveddate = excluded.laboratoryreceiveddate,
--				samplingfirm = excluded.samplingfirm,
--				samplestatus = excluded.samplestatus,
--				samplequalitymark = excluded.samplequalitymark,
--				samplestatususer = excluded.samplestatususer,
--				samplestatusdate = excluded.samplestatusdate,
--				guid = excluded.guid,
--				guid_drwplant = excluded.guid_drwplant,
--				guid_measuringstation = excluded.guid_measuringstation,
--				guid_projectphase = excluded.guid_projectphase,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate,
--				insertuser = excluded.insertuser,
--				updateuser = excluded.updateuser,
--				samplenewdate = excluded.samplenewdate,
--				laboratory_desc = excluded.laboratory_desc
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.pltchemsample 
			ADD PRIMARY KEY (guid)
		;

	CREATE INDEX IF NOT EXISTS pltchemsample_sampleid_idx
		ON jupiter.pltchemsample (sampleid)
	;

	CREATE INDEX IF NOT EXISTS pltchemsample_boreholeid_idx
		ON jupiter.pltchemsample (plantid)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'pltchemsample',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.pltchemsample IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl pltchemsample'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;	

COMMIT; 

-- catchperm

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.catchperm AS
		(
			SELECT 
				permissionid,
				permissiontype,
				startdate,
				enddate,
				journalno,
				specialterms,
				remark,
				boreholemaxdepth,
				medianminwatflow,
				"source",
				companytype,
				watertype,
				municipalityno2007,
				amountperhour,
				amountperyear,
				plantid,
				revoked,
				revokeddate,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.catchperm cp
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.catchperm 
		DROP CONSTRAINT IF EXISTS catchperm_pkey CASCADE
	;

	ALTER TABLE jupiter.catchperm 
		DROP CONSTRAINT IF EXISTS fk_drwplant_catchperm
	;

	DROP INDEX IF EXISTS catchperm_plantid_idx;

	TRUNCATE jupiter.catchperm;

	INSERT INTO jupiter.catchperm AS t1
		(
			permissionid,
			permissiontype,
			startdate,
			enddate,
			journalno,
			specialterms,
			remark,
			boreholemaxdepth,
			medianminwatflow,
			"source",
			companytype,
			watertype,
			municipalityno2007,
			amountperhour,
			amountperyear,
			plantid,
			revoked,
			revokeddate,
			guid,
			guid_drwplant,
			insertdate,
			updatedate,
			insertuser,
			updateuser
		)
		(
			SELECT 
				permissionid,
				permissiontype,
				startdate,
				enddate,
				journalno,
				specialterms,
				remark,
				boreholemaxdepth,
				medianminwatflow,
				"source",
				companytype,
				watertype,
				municipalityno2007,
				amountperhour,
				amountperyear,
				plantid,
				revoked,
				revokeddate,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.catchperm t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (guid) --ON CONSTRAINT catchperm_pkey 
--			DO UPDATE SET 
--				permissionid = excluded.permissionid,
--				permissiontype = excluded.permissiontype,
--				catchpurpose = excluded.catchpurpose,
--				startdate = excluded.startdate,
--				enddate = excluded.enddate,
--				journalno = excluded.journalno,
--				specialterms = excluded.specialterms,
--				remark = excluded.remark,
--				boreholemaxdepth = excluded.boreholemaxdepth,
--				medianminwatflow = excluded.medianminwatflow,
--				"source" = excluded."source",
--				companytype = excluded.companytype,
--				watertype = excluded.watertype,
--				municipalityno2007 = excluded.municipalityno2007,
--				amountperhour = excluded.amountperhour,
--				amountperyear = excluded.amountperyear,
--				plantid = excluded.plantid,
--				revoked = excluded.revoked,
--				revokeddate = excluded.revokeddate,
--				guid = excluded.guid,
--				guid_drwplant = excluded.guid_drwplant,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate,
--				insertuser = excluded.insertuser,
--				updateuser = excluded.updateuser
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.catchperm 
			ADD PRIMARY KEY (guid)
		;

	CREATE INDEX IF NOT EXISTS catchperm_plantid_idx
		ON jupiter.catchperm (plantid)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'catchperm',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.catchperm IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl catchperm'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- wrrcatchment

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.wrrcatchment AS
		(
			SELECT 
				plantcatchmentid,
				plantid,
				catchmentno,
				startdate,
				enddate,
				"attribute",
				amount,
				"method",
				flowmeterstart,
				flowmeterend,
				remark,
				conversionfactor,
				surfacewatervolume,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.wrrcatchment wc
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.wrrcatchment 
		DROP CONSTRAINT IF EXISTS wrrcatchment_pkey CASCADE
	;

	ALTER TABLE jupiter.wrrcatchment 
		DROP CONSTRAINT IF EXISTS fk_drwplant_wrrcatchment
	;

	DROP INDEX IF EXISTS wrrcatchment_plantid_idx;

	TRUNCATE jupiter.wrrcatchment;

	INSERT INTO jupiter.wrrcatchment AS t1
		(
			plantcatchmentid,
			plantid,
			catchmentno,
			startdate,
			enddate,
			"attribute",
			amount,
			"method",
			flowmeterstart,
			flowmeterend,
			remark,
			conversionfactor,
			surfacewatervolume,
			guid,
			guid_drwplant,
			insertdate,
			updatedate,
			insertuser,
			updateuser
		)
		(
			SELECT 
				plantcatchmentid,
				plantid,
				catchmentno,
				startdate,
				enddate,
				"attribute",
				amount,
				"method",
				flowmeterstart,
				flowmeterend,
				remark,
				conversionfactor,
				surfacewatervolume,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.wrrcatchment t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (guid) --ON CONSTRAINT wrrcatchment_pkey 
--			DO UPDATE SET 
--				plantcatchmentid = excluded.plantcatchmentid,
--				plantid = excluded.plantid,
--				catchmentno = excluded.catchmentno,
--				startdate = excluded.startdate,
--				enddate = excluded.enddate,
--				"attribute" = excluded."attribute",
--				amount = excluded.amount,
--				"method" = excluded."method",
--				flowmeterstart = excluded.flowmeterstart,
--				flowmeterend = excluded.flowmeterend,
--				remark = excluded.remark,
--				conversionfactor = excluded.conversionfactor,
--				surfacewatervolume = excluded.surfacewatervolume,
--				guid = excluded.guid,
--				guid_drwplant = excluded.guid_drwplant,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate,
--				insertuser = excluded.insertuser,
--				updateuser = excluded.updateuser
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.wrrcatchment 
		ADD PRIMARY KEY (guid)
	;

	CREATE INDEX IF NOT EXISTS wrrcatchment_plantid_idx
		ON jupiter.wrrcatchment (plantid)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'wrrcatchment',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.wrrcatchment IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl wrrcatchment'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- drwplantcompanytype

BEGIN; 

	CREATE TABLE IF NOT EXISTS jupiter.drwplantcompanytype AS
		(
			SELECT 
				plantid,
				companytype,
				companytypeno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				insertdate,
				insertuser,
				updatedate,
				updateuser
			FROM geus_fdw.drwplantcompanytype dpct
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.drwplantcompanytype 
		DROP CONSTRAINT IF EXISTS drwplantcompanytype_pkey CASCADE
	;

	ALTER TABLE jupiter.drwplantcompanytype 
		DROP CONSTRAINT IF EXISTS fk_drwplant_drwplantcompanytype
	;
	
	DROP INDEX IF EXISTS drwplantcompanytype_plantid_idx;

	TRUNCATE jupiter.drwplantcompanytype;

	INSERT INTO jupiter.drwplantcompanytype AS t1
		(
			plantid,
			companytype,
			companytypeno,
			guid,
			guid_drwplant,
			insertdate,
			insertuser,
			updatedate,
			updateuser
		)
		(
			SELECT 
				plantid,
				companytype,
				companytypeno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplant, 'hex') AS UUID) AS guid_drwplant,
				insertdate,
				insertuser,
				updatedate,
				updateuser
			FROM geus_fdw.drwplantcompanytype t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (guid) --ON CONSTRAINT drwplantcompanytype_pkey 
--			DO UPDATE SET 
--				plantid = excluded.plantid,
--				companytype = excluded.companytype,
--				companytypeno = excluded.companytypeno,
--				guid = excluded.guid,
--				guid_drwplant = excluded.guid_drwplant,
--				insertdate = excluded.insertdate,
--				insertuser = excluded.insertuser,
--				updatedate = excluded.updatedate,
--				updateuser = excluded.updateuser
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.drwplantcompanytype 
		ADD PRIMARY KEY (guid)
	;

	CREATE INDEX IF NOT EXISTS drwplantcompanytype_plantid_idx
		ON jupiter.drwplantcompanytype (plantid)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'drwplantcompanytype',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.drwplantcompanytype IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl drwplantcompanytype'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- storedoc

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.storedoc AS 
		(
			SELECT 
				fileid,
				filetype,
				url,
				"comments",
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.storedoc sd
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.storedoc 
		DROP CONSTRAINT IF EXISTS storedoc_pkey CASCADE
	;

	TRUNCATE jupiter.storedoc;

	INSERT INTO jupiter.storedoc AS t1
		(
			fileid,
			filetype,
			url,
			"comments",
			insertdate,
			updatedate,
			insertuser,
			updateuser
		)
		(
			SELECT 
				fileid,
				filetype,
				url,
				"comments",
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.storedoc t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (fileid) --ON CONSTRAINT storedoc_pkey 
--			DO UPDATE SET 
--				fileid = excluded.fileid,
--				filetype = excluded.filetype,
--				url = excluded.url,
--				"comments" = excluded."comments",
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate,
--				insertuser = excluded.insertuser,
--				updateuser = excluded.updateuser
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.storedoc 
		ADD PRIMARY KEY (fileid)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'storedoc',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.storedoc IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl storedoc'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- boredoc

BEGIN; 

	CREATE TABLE IF NOT EXISTS jupiter.boredoc AS 
		(
			SELECT 
				fileid,
				boreholeid,
				boreholeno,
				doctype,
				versionno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.boredoc bd
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.boredoc 
		DROP CONSTRAINT IF EXISTS boredoc_pkey CASCADE
	;

	ALTER TABLE jupiter.boredoc 
		DROP CONSTRAINT IF EXISTS fk_borehole_boredoc
	;

	ALTER TABLE jupiter.boredoc 
		DROP CONSTRAINT IF EXISTS fk_storedoc_boredoc
	;

	DROP INDEX IF EXISTS boredoc_fileid_idx;

	DROP INDEX IF EXISTS boredoc_boreholeid_idx;

	TRUNCATE jupiter.boredoc CASCADE;

	INSERT INTO jupiter.boredoc AS t1
		(
			fileid,
			boreholeid,
			boreholeno,
			doctype,
			versionno,
			guid,
			guid_borehole,
			insertdate,
			updatedate,
			insertuser,
			updateuser
		)
		(
			SELECT 
				fileid,
				boreholeid,
				boreholeno,
				doctype,
				versionno,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_borehole, 'hex') AS UUID) AS guid_borehole,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.boredoc t2
		) ON CONFLICT DO NOTHING 
--		ON CONFLICT (guid) --ON CONSTRAINT boredoc_pkey 
--			DO UPDATE SET 
--				fileid = excluded.fileid,
--				boreholeid = excluded.boreholeid,
--				boreholeno = excluded.boreholeno,
--				doctype = excluded.doctype,
--				versionno = excluded.versionno,
--				guid = excluded.guid,
--				guid_borehole = excluded.guid_borehole,
--				insertdate = excluded.insertdate,
--				updatedate = excluded.updatedate,
--				insertuser = excluded.insertuser,
--				updateuser = excluded.updateuser
			--where COALESCE(t1.updatedate, t1.insertdate) != COALESCE(excluded.updatedate, excluded.insertdate)
	;

	ALTER TABLE jupiter.boredoc 
		ADD PRIMARY KEY (guid)
	;

	CREATE INDEX IF NOT EXISTS boredoc_fileid_idx
		ON jupiter.boredoc (fileid)
	;

	CREATE INDEX IF NOT EXISTS boredoc_boreholeid_idx
		ON jupiter.boredoc (boreholeid)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'boredoc',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.boredoc IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl boredoc'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT; 

	-- rocksymco

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.rcksymco AS 
		(
			SELECT 
				code,
				codetype,
				shorttext,
				longtext,
				sortno,
				red,
				green,
				blue
			FROM geus_fdw.rcksymco
			LIMIT 0
		)
	;

	ALTER TABLE jupiter.rcksymco 
		DROP CONSTRAINT IF EXISTS rcksymco_pkey CASCADE
	;

	TRUNCATE jupiter.rcksymco CASCADE;	

	INSERT INTO jupiter.rcksymco
		(
			code,
			codetype,
			shorttext,
			longtext,
			sortno,
			red,
			green,
			blue
		)
		(
			SELECT 
				code,
				codetype,
				shorttext,
				longtext,
				sortno,
				red,
				green,
				blue
			FROM geus_fdw.rcksymco
		)
	;

	ALTER TABLE jupiter.rcksymco 
		ADD PRIMARY KEY (code)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'rcksymco',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.rcksymco IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl rocksymco'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- intakecatchment

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.intakecatchment AS 
		(
			SELECT
				startdate,
				intakeplantid,
				"attribute",
				enddate,
				volume,
				"method",
				flowmeter,
				flowmeterstart,
				conversionfactor,
				remark,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplantintake, 'hex') AS UUID) AS guid_drwplantintake,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.intakecatchment
			LIMIT 0
		)
	;

	TRUNCATE jupiter.intakecatchment CASCADE;	

	ALTER TABLE jupiter.intakecatchment 
		DROP CONSTRAINT IF EXISTS intakecatchment_pkey CASCADE
	;

	ALTER TABLE jupiter.intakecatchment 
		DROP CONSTRAINT IF EXISTS fk_intakecatchment_drwplantintake CASCADE
	;

	INSERT INTO jupiter.intakecatchment
		(
			startdate,
			intakeplantid,
			"attribute",
			enddate,
			volume,
			"method",
			flowmeter,
			flowmeterstart,
			conversionfactor,
			remark,
			guid,
			guid_drwplantintake,
			insertdate,
			updatedate,
			insertuser,
			updateuser
		)
		(
			SELECT 
				startdate,
				intakeplantid,
				"attribute",
				enddate,
				volume,
				"method",
				flowmeter,
				flowmeterstart,
				conversionfactor,
				remark,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_drwplantintake, 'hex') AS UUID) AS guid_drwplantintake,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.intakecatchment
		) ON CONFLICT DO NOTHING 
	;

	ALTER TABLE jupiter.intakecatchment 
		ADD PRIMARY KEY (guid)
	;

	CREATE INDEX IF NOT EXISTS intakecatchment_volume_idx
		ON jupiter.intakecatchment (volume)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'intakecatchment',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.intakecatchment IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl intakecatchment'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- discharg

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.discharg AS 
		(
			SELECT
				pumpingid,
				dischargid,
				boreholeno,
				pumpingno,
				dischargno,
				discharge,
				drawdown,
				duration,
				conductivi,
				flow,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_pumping, 'hex') AS UUID) AS guid_pumping,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.discharg
			LIMIT 0
		)
	;

	TRUNCATE jupiter.discharg CASCADE;	

	ALTER TABLE jupiter.discharg 
		DROP CONSTRAINT IF EXISTS discharg_pkey CASCADE
	;

	ALTER TABLE jupiter.discharg 
		DROP CONSTRAINT IF EXISTS fk_discharg_pumping CASCADE
	;

	ALTER TABLE jupiter.discharg 
		DROP CONSTRAINT IF EXISTS fk_discharg_borehole CASCADE
	;

	INSERT INTO jupiter.discharg
		(
			pumpingid,
			dischargid,
			boreholeno,
			pumpingno,
			dischargno,
			discharge,
			drawdown,
			duration,
			conductivi,
			flow,
			guid,
			guid_pumping,
			insertdate,
			updatedate,
			insertuser,
			updateuser
		)
		(
			SELECT 
				pumpingid,
				dischargid,
				boreholeno,
				pumpingno,
				dischargno,
				discharge,
				drawdown,
				duration,
				conductivi,
				flow,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_pumping, 'hex') AS UUID) AS guid_pumping,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.discharg
		) ON CONFLICT DO NOTHING 
	;

	ALTER TABLE jupiter.discharg 
		ADD PRIMARY KEY (guid)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'discharg',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.discharg IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl discharg'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- exporttime

BEGIN;

	CREATE TABLE IF NOT EXISTS mstjupiter.mst_exportdate AS 
		(
			SELECT 
				'test_string'::TEXT AS tablename,
				'1900-01-01'::timestamp AS updatetime
			LIMIT 0
		)
	;

	TRUNCATE jupiter.exporttime CASCADE;	

	INSERT INTO jupiter.exporttime
		(
			exporttime,
			description
		)
		(
			SELECT 
				exporttime,
				description
			FROM geus_fdw.exporttime
		)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'exporttime',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.exporttime IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl exporttime'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;



-- intake_groundwaterbody

begin;

    --drop table if exists jupiter.intake_groundwaterbody;
	create table if not exists jupiter.intake_groundwaterbody as
        select
            aquiferid,
            boreholeid,
            boreholeno,
            groundwaterbody,
            horiz_dist_aquifer,
            intake_lithology,
            intakeid,
            note,
            vert_dist_aquifer
        from geus_fdw.intake_groundwaterbody limit 0;

	alter table jupiter.intake_groundwaterbody
		drop constraint if exists grwbody_pkey cascade;

	truncate jupiter.intake_groundwaterbody;

	insert into jupiter.intake_groundwaterbody (
		aquiferid,
		boreholeid,
		boreholeno,
		groundwaterbody,
		horiz_dist_aquifer,
		intake_lithology,
		intakeid,
		note,
		vert_dist_aquifer
	)
    select
        aquiferid,
        boreholeid,
        boreholeno,
        groundwaterbody,
        horiz_dist_aquifer,
        intake_lithology,
        intakeid,
        note,
        vert_dist_aquifer
	from geus_fdw.intake_groundwaterbody
	on conflict do nothing;

	alter table jupiter.intake_groundwaterbody
	    add constraint grwbody_pkey primary key(boreholeid, intakeid);

	--Detail: Key (boreholeid)=(96294) is not present in table "borehole".
    --alter table jupiter.intake_groundwaterbody
	--	add constraint fk_borehole_groundwaterbody foreign key (boreholeid)
    --    references jupiter.borehole (boreholeid);

	insert into mstjupiter.mst_exportdate as t1 (
		tablename,
		updatetime)
	values (
		'intake_groundwaterbody',
		now())
	on conflict (tablename)
		do update set
			updatetime = excluded.updatetime
		where t1.tablename = excluded.tablename
	;

	do
	$do$
		begin
			execute 'Comment on table jupiter.intake_groundwaterbody is '''
	     || to_char(localtimestamp, 'yyyymmdd')
	     || ' | user: jakla'
	     || ' | descr: pcjupiterxl intake_groundwaterbody'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		end
	$do$;

commit;

-- limitlist

begin;

	--drop table if exists jupiter.limitlist;
	create table if not exists jupiter.limitlist as
		select
			compoundno,
			NULL AS compoundgroup,
			to_commission,
			waterworks_min,
			waterworks_max,
			pipelinenetwork_min,
			pipelinenetwork_max,
			consumer_min,
			consumer_max,
			remark,
			NULL AS compoundundergroup,
		  	recommended_min,
		  	recommended_max,
		  	unit
		from geus_fdw.limitlist limit 0
	;

	alter table jupiter.limitlist
		drop CONSTRAINT IF EXISTS limitlist_pkey cascade;

	truncate jupiter.limitlist;

	insert into jupiter.limitlist 
		(
			compoundno,
			compoundgroup,
			to_commission,
			waterworks_min,
			waterworks_max,
			pipelinenetwork_min,
			pipelinenetwork_max,
			consumer_min,
			consumer_max,
			remark,
			compoundundergroup,
			recommended_min,
			recommended_max,
			unit
     	)
		select
			compoundno,
			NULL AS compoundgroup,
			to_commission,
			waterworks_min,
			waterworks_max,
			pipelinenetwork_min,
			pipelinenetwork_max,
			consumer_min,
			consumer_max,
			remark,
			NULL AS compoundundergroup,
			recommended_min,
			recommended_max,
			unit
         from geus_fdw.limitlist
         on conflict do nothing
	;

         alter table jupiter.limitlist
             add constraint limitlist_pkey primary key(compoundno);

         insert into mstjupiter.mst_exportdate as t1 
         	(
	         	tablename,
	         	updatetime
         	)
         values 
         	(
	         	'limitlist',
	        	now()
        	)
		on conflict (tablename)
		do update SET updatetime = excluded.updatetime
	where t1.tablename = excluded.tablename
	;

	do
	$do$
		begin
			execute 'Comment on table jupiter.limitlist is '''
		|| to_char(localtimestamp, 'yyyymmdd')
		|| ' | user: jakla'
		|| ' | descr: pcjupiterxl limitlist'
		|| ' | repo: https://gitlab.com/mst-gko/pgjupiter'
		|| '''';
	end
	$do$;

commit;

-- dispensation

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.dispensation AS 
		(
			SELECT
				dispensationid,
				injunctionid,
				issuedate,
				expiration_date,
				"comments",
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_plant_injunction, 'hex') AS UUID) AS guid_plant_injunction,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.dispensation
			LIMIT 0
		)
	;

	TRUNCATE jupiter.dispensation CASCADE;	

	ALTER TABLE jupiter.dispensation 
		DROP CONSTRAINT IF EXISTS dispensation_pkey CASCADE
	;

	INSERT INTO jupiter.dispensation
		(
			dispensationid,
			injunctionid,
			issuedate,
			expiration_date,
			"comments",
			guid,
			guid_plant_injunction,
			insertdate,
			updatedate,
			insertuser,
			updateuser
		)
		(
			SELECT 
				dispensationid,
				injunctionid,
				issuedate,
				expiration_date,
				"comments",
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_plant_injunction, 'hex') AS UUID) AS guid_plant_injunction,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.dispensation
		) ON CONFLICT DO NOTHING 
	;

	ALTER TABLE jupiter.dispensation 
		ADD PRIMARY KEY (guid)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'dispensation',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.dispensation IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl discharg'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- watlevround

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.watlevround AS 
		(
			SELECT
				watlevelroundno,
				"name",
				description,
				measuredfor,
				measuredby,
				startdate,
				enddate,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.watlevround
			LIMIT 0
		)
	;

	TRUNCATE jupiter.watlevround CASCADE;	

	ALTER TABLE jupiter.watlevround 
		DROP CONSTRAINT IF EXISTS watlevround_pkey CASCADE
	;

	INSERT INTO jupiter.watlevround
		(
			watlevelroundno,
				"name",
				description,
				measuredfor,
				measuredby,
				startdate,
				enddate,
				guid,
				insertdate,
				updatedate,
				insertuser,
				updateuser
		)
		(
			SELECT 
				watlevelroundno,
				"name",
				description,
				measuredfor,
				measuredby,
				startdate,
				enddate,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.watlevround
		) ON CONFLICT DO NOTHING 
	;

	ALTER TABLE jupiter.watlevround 
		ADD PRIMARY KEY (guid)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'watlevround',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.watlevround IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl watlevround'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- pumping

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.pumping AS 
		(
			SELECT
				boreholeid,
				intakeid,
				pumpingid,
				boreholeno,
				pumpingno,
				intakeno,
				pumpstart,
				totduratio,
				transmissi,
				storacoeff,
				efficiency,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_intake, 'hex') AS UUID) AS guid_intake,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.pumping
			LIMIT 0
		)
	;

	TRUNCATE jupiter.pumping CASCADE;	

	ALTER TABLE jupiter.pumping 
		DROP CONSTRAINT IF EXISTS pumping_pkey CASCADE
	;

	ALTER TABLE jupiter.pumping 
		DROP CONSTRAINT IF EXISTS pumpingid_unique CASCADE
	;

	ALTER TABLE jupiter.pumping 
		DROP CONSTRAINT IF EXISTS fk_pumping_intake CASCADE
	;

	ALTER TABLE jupiter.pumping 
		DROP CONSTRAINT IF EXISTS fk_pumping_borehole CASCADE
	;
	
	INSERT INTO jupiter.pumping
		(
			boreholeid,
			intakeid,
			pumpingid,
			boreholeno,
			pumpingno,
			intakeno,
			pumpstart,
			totduratio,
			transmissi,
			storacoeff,
			efficiency,
			guid,
			guid_intake,
			insertdate,
			updatedate,
			insertuser,
			updateuser
		)
		(
			SELECT 
				boreholeid,
				intakeid,
				pumpingid,
				boreholeno,
				pumpingno,
				intakeno,
				pumpstart,
				totduratio,
				transmissi,
				storacoeff,
				efficiency,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_intake, 'hex') AS UUID) AS guid_intake,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.pumping
		) ON CONFLICT DO NOTHING 
	;

	ALTER TABLE jupiter.pumping 
		ADD PRIMARY KEY (guid)
	;

	ALTER TABLE jupiter.pumping 
		ADD CONSTRAINT pumpingid_unique UNIQUE (pumpingid)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'pumping',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.pumping IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl watlevround'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

-- watlevmp

BEGIN;

	CREATE TABLE IF NOT EXISTS jupiter.watlevmp AS 
		(
			SELECT
				boreholeid,
				watlevmpid,
				intakeid,
				boreholeno,
				watlevmpno,
				intakeno,
				startdate,
				enddate,
				descriptio,
				elevation,
				levprecis,
				verticaref,
				"height",
				munderctrp,
				elevametho,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_intake, 'hex') AS UUID) AS guid_intake,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.watlevmp
			LIMIT 0
		)
	;

	TRUNCATE jupiter.watlevmp CASCADE;	

	ALTER TABLE jupiter.watlevmp 
		DROP CONSTRAINT IF EXISTS watlevmp_pkey CASCADE
	;
	
	INSERT INTO jupiter.watlevmp
		(
			boreholeid,
			watlevmpid,
			intakeid,
			boreholeno,
			watlevmpno,
			intakeno,
			startdate,
			enddate,
			descriptio,
			elevation,
			levprecis,
			verticaref,
			"height",
			munderctrp,
			elevametho,
			guid,
			guid_intake,
			insertdate,
			updatedate,
			insertuser,
			updateuser
		)
		(
			SELECT 
				boreholeid,
				watlevmpid,
				intakeid,
				boreholeno,
				watlevmpno,
				intakeno,
				startdate,
				enddate,
				descriptio,
				elevation,
				levprecis,
				verticaref,
				"height",
				munderctrp,
				elevametho,
				CAST(ENCODE(guid, 'hex') AS UUID) AS guid,
				CAST(ENCODE(guid_intake, 'hex') AS UUID) AS guid_intake,
				insertdate,
				updatedate,
				insertuser,
				updateuser
			FROM geus_fdw.watlevmp
		) ON CONFLICT DO NOTHING 
	;

	ALTER TABLE jupiter.watlevmp 
		ADD PRIMARY KEY (guid)
	;

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'watlevmp',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

	DO
	$do$
		BEGIN
			EXECUTE 'COMMENT ON TABLE jupiter.watlevmp IS '''
	     || to_char(LOCALTIMESTAMP, 'YYYYMMDD')
	     || ' | user: simak'
	     || ' | descr: pcjupiterxl watlevmp'
	     || ' | repo: https://gitlab.com/mst-gko/pgjupiter'
	     || '''';
		END
	$do$
	;

COMMIT;

/*
 * 	create Foreign keys 
 */

BEGIN; 

	-- code

	ALTER TABLE jupiter.code 
		ADD CONSTRAINT fk_codetype_code FOREIGN KEY (codetype) 
		REFERENCES jupiter.codetype (codetype)
	;
	
	-- drilmeth

	ALTER TABLE jupiter.drilmeth 
		ADD CONSTRAINT fk_borehole_drilmeth FOREIGN KEY (guid_borehole) 
		REFERENCES jupiter.borehole (guid)
	;
	
	 --intake

	ALTER TABLE jupiter.intake 
		ADD CONSTRAINT fk_borehole_intake FOREIGN KEY (guid_borehole) 
		REFERENCES jupiter.borehole (guid)
	;
	
	-- screen

	ALTER TABLE jupiter.screen 
		ADD CONSTRAINT fk_intake_screen FOREIGN KEY (guid_intake) 
		REFERENCES jupiter.intake (guid)
	;
	
	ALTER TABLE jupiter.screen 
		ADD CONSTRAINT fk_borehole_screen FOREIGN KEY (boreholeid) 
		REFERENCES jupiter.borehole (boreholeid)
	;
	
	-- drwplant

	ALTER TABLE jupiter.drwplant 
		ADD CONSTRAINT fk_municipality FOREIGN KEY (municipalityno2007) 
		REFERENCES jupiter.municipality2007 (municipalityno2007)
	;
	
	-- drwplantintake

	ALTER TABLE jupiter.drwplantintake 
		ADD CONSTRAINT fk_intake_dpi FOREIGN KEY (guid_intake) 
		REFERENCES jupiter.intake (guid)
	;
	
	ALTER TABLE jupiter.drwplantintake 
		ADD CONSTRAINT fk_plant_dpi FOREIGN KEY (guid_drwplant) 
		REFERENCES jupiter.drwplant (guid)
	;
	
	ALTER TABLE jupiter.drwplantintake 
		ADD CONSTRAINT fk_borehole_dpi FOREIGN KEY (boreholeid) 
		REFERENCES jupiter.borehole (boreholeid)
	;
	
	-- compoundgroup

	ALTER TABLE jupiter.compoundgroup 
		ADD CONSTRAINT fk_cpl_cpg FOREIGN KEY (compoundno) 
		REFERENCES jupiter.compoundlist (compoundno)
	;
	
	ALTER TABLE jupiter.compoundgroup 
		ADD CONSTRAINT fk_cpgl_cpg FOREIGN KEY (compoundgroupno) 
		REFERENCES jupiter.compoundgrouplist (compoundgroupno)
	;
	
	-- grwchemsample

	ALTER TABLE jupiter.grwchemsample 
		ADD CONSTRAINT fk_intake_gcs FOREIGN KEY (guid_intake) 
		REFERENCES jupiter.intake (guid)
	;
	
	ALTER TABLE jupiter.grwchemsample 
		ADD CONSTRAINT fk_borehole_gcs FOREIGN KEY (guid_borehole) 
		REFERENCES jupiter.borehole (guid)
	;
	
	-- grwchemanalysis

	ALTER TABLE jupiter.grwchemanalysis 
		ADD CONSTRAINT fk_cpl_gca FOREIGN KEY (compoundno) 
		REFERENCES jupiter.compoundlist (compoundno)
	;
	
	ALTER TABLE jupiter.grwchemanalysis 
		ADD CONSTRAINT fk_gcs_gca FOREIGN KEY (guid_grwchemsample) 
		REFERENCES jupiter.grwchemsample (guid)
	;	
	
	-- pltchemsample

	ALTER TABLE jupiter.pltchemsample 
		ADD CONSTRAINT fk_drwplant_pcs FOREIGN KEY (guid_drwplant) 
		REFERENCES jupiter.drwplant (guid)
	;
	
	-- pltchemanalysis

	ALTER TABLE jupiter.pltchemanalysis 
		ADD CONSTRAINT fk_pcs_pca FOREIGN KEY (guid_pltchemsample) 
		REFERENCES jupiter.pltchemsample (guid)
	;
	
	ALTER TABLE jupiter.pltchemanalysis 
		ADD CONSTRAINT fk_cpl_pca FOREIGN KEY (compoundno) 
		REFERENCES jupiter.compoundlist (compoundno)
	;
	
	-- lithsamp

	ALTER TABLE jupiter.lithsamp 
		ADD CONSTRAINT fk_borehole_lithsamp FOREIGN KEY (guid_borehole) 
		REFERENCES jupiter.borehole (guid)
	;
	
	-- watlevel

	ALTER TABLE jupiter.watlevel 
		ADD CONSTRAINT fk_intake_watlevel FOREIGN KEY (guid_intake) 
		REFERENCES jupiter.intake (guid)
	;
	
	ALTER TABLE jupiter.watlevel 
		ADD CONSTRAINT fk_borehole_watlevel FOREIGN KEY (boreholeid) 
		REFERENCES jupiter.borehole (boreholeid)
	;
	
	-- catchperm

	ALTER TABLE jupiter.catchperm 
		ADD CONSTRAINT fk_drwplant_catchperm FOREIGN KEY (guid_drwplant) 
		REFERENCES jupiter.drwplant (guid)
	;
	
	-- wrrcatchment

	ALTER TABLE jupiter.wrrcatchment 
		ADD CONSTRAINT fk_drwplant_wrrcatchment FOREIGN KEY (guid_drwplant) 
		REFERENCES jupiter.drwplant (guid)
	;
	
	-- drwplantcompanytype

	ALTER TABLE jupiter.drwplantcompanytype 
		ADD CONSTRAINT fk_drwplant_drwplantcompanytype FOREIGN KEY (guid_drwplant) 
		REFERENCES jupiter.drwplant (guid)
	;
	
	-- boredoc

	ALTER TABLE jupiter.boredoc 
		ADD CONSTRAINT fk_borehole_boredoc FOREIGN KEY (guid_borehole) 
		REFERENCES jupiter.borehole (guid)
	;
	
	ALTER TABLE jupiter.boredoc 
		ADD CONSTRAINT fk_storedoc_boredoc FOREIGN KEY (fileid) 
		REFERENCES jupiter.storedoc (fileid)
	;

	-- intakecatchment

--	ALTER TABLE jupiter.intakecatchment 
--		ADD CONSTRAINT fk_intakecatchment_drwplantintake FOREIGN KEY (guid_drwplantintake) 
--		REFERENCES jupiter.drwplantintake (guid)
--	;

	-- discharg

	ALTER TABLE jupiter.discharg 
		ADD CONSTRAINT fk_discharg_borehole FOREIGN KEY (boreholeno) 
		REFERENCES jupiter.borehole (boreholeno)
	;

	-- pumping
	
	ALTER TABLE jupiter.pumping 
		ADD CONSTRAINT fk_pumping_intake FOREIGN KEY (guid_intake) 
		REFERENCES jupiter.intake (guid)
	;

	ALTER TABLE jupiter.pumping 
		ADD CONSTRAINT fk_pumping_borehole FOREIGN KEY (boreholeid) 
		REFERENCES jupiter.borehole (boreholeid)
	;

	-- discharg
	ALTER TABLE jupiter.discharg 
		ADD CONSTRAINT fk_discharg_pumping FOREIGN KEY (pumpingid) 
		REFERENCES jupiter.pumping (pumpingid)
	;

	-- update table exportdate

	INSERT INTO mstjupiter.mst_exportdate AS t1
		(
			tablename,
			updatetime
		)
		VALUES 
			(
				'foreign keys',
				now()
			)
		ON CONFLICT (tablename)
			DO UPDATE SET
				updatetime = excluded.updatetime
			WHERE t1.tablename = excluded.tablename
	;

COMMIT; 


-- give access to read user

GRANT USAGE ON SCHEMA jupiter TO grukosreader;
GRANT SELECT ON ALL TABLES IN SCHEMA jupiter TO grukosreader;
